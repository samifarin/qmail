/*
vim:tw=76:ts=4:sw=4:cindent:expandtab
*/

/* SHA1 code from libtomcrypt */

#include "sha1.h"
#include "byte.h"
#include "base32.h"

#define F0(x,y,z)  (z ^ (x & (y ^ z)))
#define F1(x,y,z)  (x ^ y ^ z)
#define F2(x,y,z)  ((x & y) | (z & (x | y)))
#define F3(x,y,z)  (x ^ y ^ z)

#define MIN(x,y) ({       \
    typeof(x) _x = (x);   \
    typeof(y) _y = (y);   \
    _x < _y ? _x : _y; })

#define MAX(x,y) ({       \
    typeof(x) _x = (x);   \
    typeof(y) _y = (y);   \
    _x > _y ? _x : _y; })

#define STORE32H(x, y)                                                      \
  { (y)[0] = (uint8)(((x)>>24)&255); (y)[1] = (uint8)(((x)>>16)&255);   \
    (y)[2] = (uint8)(((x)>> 8)&255); (y)[3] = (uint8)((x)&255); }

#define LOAD32H(x, y)                      \
  { x = ((uint32)((y)[0] & 255)<<24) | \
        ((uint32)((y)[1] & 255)<<16) | \
        ((uint32)((y)[2] & 255)<<8)  | \
        ((uint32)((y)[3] & 255)); }

#define STORE64H(x, y)                                                      \
  { (y)[0] = (uint8)(((x)>>56)&255); (y)[1] = (uint8)(((x)>>48)&255);   \
    (y)[2] = (uint8)(((x)>>40)&255); (y)[3] = (uint8)(((x)>>32)&255);   \
    (y)[4] = (uint8)(((x)>>24)&255); (y)[5] = (uint8)(((x)>>16)&255);   \
    (y)[6] = (uint8)(((x)>> 8)&255); (y)[7] = (uint8)((x)&255); }

#define LOAD64H(x, y)                                                       \
  { x = (((uint64)((y)[0] & 255))<<56)|(((uint64)((y)[1] & 255))<<48) | \
        (((uint64)((y)[2] & 255))<<40)|(((uint64)((y)[3] & 255))<<32) | \
        (((uint64)((y)[4] & 255))<<24)|(((uint64)((y)[5] & 255))<<16) | \
        (((uint64)((y)[6] & 255))<<8)|(((uint64)((y)[7] & 255))); }

#define ROR(x, y) ( ((((uint32)(x)>>(uint32)((y))) | ((uint32)(x)<<(uint32)(32-y))))
#define ROL(x, y) ( (((uint32)(x)<<(uint32)((y))) | (((uint32)(x))>>(uint32)(32-y))))

void sha1_hmac_init(sha1_hmac_state *hmac, const void *__key, uint64 keylen)
{
    uint8 buf[SHA1BLOCKSIZE];
    size_t i;
    const uint8 *key = __key;

    /* prepare K0 */
    if(keylen > SHA1BLOCKSIZE) {
        sha1_hash_memory(key, keylen, hmac->key, sizeof(hmac->key), SHA1DIGESTRAW);
        byte_zero(hmac->key + SHA1DIGESTSIZE, SHA1BLOCKSIZE-SHA1DIGESTSIZE);
    } else {
        byte_copy(hmac->key, keylen, (char*)key);
        byte_zero(hmac->key + keylen, SHA1BLOCKSIZE-keylen);
    }

    for (i = 0; i < SHA1BLOCKSIZE; i++) {
        buf[i] = hmac->key[i] ^ 0x36;
    }

    sha1_init(&hmac->md);
    sha1_process(&hmac->md, buf, SHA1BLOCKSIZE);
}

void sha1_hmac_process(sha1_hmac_state *hmac, const void *indata, uint64 inlen)
{
    sha1_process(&hmac->md, indata, inlen);
}

void sha1_hmac_done(sha1_hmac_state *hmac, void *outdata, size_t outlen, int outtype)
{
    uint8 buf1[SHA1DIGESTSIZE];
    uint8 buf2[SHA1BLOCKSIZE];
    int i;

    sha1_done(&hmac->md, buf1, SHA1DIGESTSIZE, SHA1DIGESTRAW);
    for (i = 0; i < SHA1BLOCKSIZE; i++) {
        buf2[i] = hmac->key[i] ^ 0x5C;
    }

    sha1_init(&hmac->md);
    sha1_process(&hmac->md, buf2, SHA1BLOCKSIZE);
    sha1_process(&hmac->md, buf1, SHA1DIGESTSIZE);
    sha1_done(&hmac->md, outdata, outlen, outtype);
}

void sha1_hmac_memory(const void *key, uint64 keylen,
                      const void *indata, uint64 inlen,
                      void *outdata, size_t outlen, int outtype)
{
    sha1_hmac_state hmac;

    sha1_hmac_init(&hmac, key, keylen);
    sha1_hmac_process(&hmac, indata, inlen);
    sha1_hmac_done(&hmac, outdata, outlen, outtype);
}

static void sha1_compress(sha1_hash_state*);
void sha1_compress(sha1_hash_state *md)
{
    unsigned long a, b, c, d, e, W[80], i;

    /* copy the state into 512-bits into W[0..15] */
    for (i = 0; i < 16; i++) {
        LOAD32H(W[i], md->buf + (4*i));
    }

    /* copy state */
    a = md->state[0];
    b = md->state[1];
    c = md->state[2];
    d = md->state[3];
    e = md->state[4];

    /* expand it */
    for (i = 16; i < 80; i++) {
        W[i] = ROL(W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16], 1);
    }

    /* compress */
    /* round one */
    #define FF0(a,b,c,d,e,i) e = (ROL(a, 5) + F0(b,c,d) + e + W[i] + 0x5a827999UL); b = ROL(b, 30);
    #define FF1(a,b,c,d,e,i) e = (ROL(a, 5) + F1(b,c,d) + e + W[i] + 0x6ed9eba1UL); b = ROL(b, 30);
    #define FF2(a,b,c,d,e,i) e = (ROL(a, 5) + F2(b,c,d) + e + W[i] + 0x8f1bbcdcUL); b = ROL(b, 30);
    #define FF3(a,b,c,d,e,i) e = (ROL(a, 5) + F3(b,c,d) + e + W[i] + 0xca62c1d6UL); b = ROL(b, 30);

    for (i = 0; i < 20; ) {
       FF0(a,b,c,d,e,i++);
       FF0(e,a,b,c,d,i++);
       FF0(d,e,a,b,c,i++);
       FF0(c,d,e,a,b,i++);
       FF0(b,c,d,e,a,i++);
    }

    /* round two */
    for (i = 20; i < 40; )  {
       FF1(a,b,c,d,e,i++);
       FF1(e,a,b,c,d,i++);
       FF1(d,e,a,b,c,i++);
       FF1(c,d,e,a,b,i++);
       FF1(b,c,d,e,a,i++);
    }

    /* round three */
    for (i = 40; i < 60; )  {
       FF2(a,b,c,d,e,i++);
       FF2(e,a,b,c,d,i++);
       FF2(d,e,a,b,c,i++);
       FF2(c,d,e,a,b,i++);
       FF2(b,c,d,e,a,i++);
    }

    /* round four */
    for (i = 60; i < 80; )  {
       FF3(a,b,c,d,e,i++);
       FF3(e,a,b,c,d,i++);
       FF3(d,e,a,b,c,i++);
       FF3(c,d,e,a,b,i++);
       FF3(b,c,d,e,a,i++);
    }

    #undef FF0
    #undef FF1
    #undef FF2
    #undef FF3

    /* store */
    md->state[0] = md->state[0] + a;
    md->state[1] = md->state[1] + b;
    md->state[2] = md->state[2] + c;
    md->state[3] = md->state[3] + d;
    md->state[4] = md->state[4] + e;
}

void sha1_init(sha1_hash_state * md)
{
   md->state[0] = 0x67452301UL;
   md->state[1] = 0xefcdab89UL;
   md->state[2] = 0x98badcfeUL;
   md->state[3] = 0x10325476UL;
   md->state[4] = 0xc3d2e1f0UL;
   md->curlen = 0;
   md->length = 0;
}

void sha1_process(sha1_hash_state *md, const void *__buf, uint64 len)
{
    unsigned int n;
    const char *buf = __buf;

    while (len > 0) {
        n = MIN(len, (64 - md->curlen));
        byte_copy(md->buf + md->curlen, n, (void*)buf);
        md->curlen += n;
        buf        += n;
        len        -= n;

        /* is 64 bytes full? */
        if (md->curlen == 64) {
            sha1_compress(md);
            md->length += 512;
            md->curlen = 0;
        }
    }
}

void sha1_done(sha1_hash_state *md, void *outdata, size_t outlen, int outtype)
{
    int i;
    uint8 tmp[SHA1DIGESTSIZERAW];
    uint8 tmp2[SHA1DIGESTSIZEMAX];
    static const uint8 hextbl[] = "0123456789abcdef";

    /* increase the length of the message */
    md->length += md->curlen * 8;

    /* append the '1' bit */
    md->buf[md->curlen++] = (uint8)0x80;

    /* if the length is currently above 56 bytes we append zeros
     * then compress.  Then we can fall back to padding zeros and length
     * encoding like normal.
     */
    if (md->curlen > 56) {
        while (md->curlen < 64) {
            md->buf[md->curlen++] = (uint8)0;
        }
        sha1_compress(md);
        md->curlen = 0;
    }

    /* pad upto 56 bytes of zeroes */
    while (md->curlen < 56) {
        md->buf[md->curlen++] = (uint8)0;
    }

    /* store length */
    STORE64H(md->length, md->buf+56);
    sha1_compress(md);

    /* copy output */
    for (i = 0; i < 5; i++) {
        STORE32H(md->state[i], tmp+(4*i));
    }
    switch (outtype) {
        case SHA1DIGESTRAW:
            byte_copy(outdata, (outlen > SHA1DIGESTSIZERAW) ?
                      SHA1DIGESTSIZERAW : outlen, tmp);
            break;
        case SHA1DIGESTHEX:
            for(i = 0; i < SHA1DIGESTSIZERAW; i++) {
                tmp2[i*2+0] = hextbl[(tmp[i] >> 4)];
                tmp2[i*2+1] = hextbl[(tmp[i] & 15)];
            }
            byte_copy(outdata, (outlen > SHA1DIGESTSIZEHEX) ?
                      SHA1DIGESTSIZEHEX : outlen, tmp2);
            break;
        case SHA1DIGESTBASE32:
            base32encode(tmp, tmp2, sizeof(tmp));
            byte_copy(outdata, (outlen > SHA1DIGESTSIZEBASE32) ?
                      SHA1DIGESTSIZEBASE32 : outlen, tmp2);
            break;
        case SHA1DIGESTNONE:
            break;
    }
}

void sha1_hash_memory(const void* indata, uint64 inlen,
                      void *outdata, size_t outlen, int outtype)
{
    sha1_hash_state md;

    sha1_init(&md);
    sha1_process(&md, indata, inlen);
    sha1_done(&md, outdata, outlen, outtype);
}


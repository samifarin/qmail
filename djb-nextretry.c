/*
vim:tw=76:ts=4:sts=4:sw=4:cindent:expandtab
*/

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>

#include "dns.h"
#include "datetime.h"

/* Valid values for skip (inclusive) */
#define SKIPMIN (10l)
#define SKIPMAX (100l)

static datetime_sec djbsqrt(datetime_sec x)
{
    datetime_sec y;
    datetime_sec yy;
    datetime_sec y21;
    int j;

    y = 0; yy = 0;
    for (j = 15;j >= 0;--j)
    {
        y21 = (y << (j + 1)) + (1 << (j + j));
        if (y21 <= x - yy) { y += (1 << j); yy += y21; }
    }
    return y;
}

int main(int argc, char *argv[])
{
    datetime_sec now = time(NULL);
    datetime_sec before;
    datetime_sec birth;
    datetime_sec nextretry;
    datetime_sec rndsecs;
    datetime_sec till = now + (7 * 86400); /* Show retries to one week in future */
    unsigned long skip;
    char *endptr;

    if (argc != 3) {
        fprintf(stderr, "%s: usage: [birth] [skip]\n",
                argv[0]);
        return 1;
    }

    errno = 0;
    before = strtoul(argv[1], &endptr, 10);
    if ((errno != 0) || (argv[1] == endptr)) {
        fprintf(stderr, "%s: invalid birth time\n",
                argv[0]);
        return 1;
    }
    skip = strtoul(argv[2], &endptr, 10);
    if ((errno != 0) || (argv[2] == endptr) || (skip < SKIPMIN) || (skip > SKIPMAX)) {
        fprintf(stderr, "%s: valid values for skip are %ld..%ld\n",
                argv[0], SKIPMIN, SKIPMAX);
        return 1;
    }

    birth = now - before;
    dns_random_init(); 

    while (now < till) {
        if (birth > now) nextretry = skip;
        else {
            nextretry = djbsqrt(now - birth) + skip;
            if (nextretry < 20) nextretry = 20;
        }
        if (nextretry > ((LONG_MAX - birth) / nextretry)) {
            fflush(stdout);
            fprintf(stderr, "%s: integer overflow\n",
                    argv[0]);
            return 1;
        }
        rndsecs = (charcrandom_u32() % (1 + ((nextretry * nextretry) / 90))) % (nextretry*3);
        rndsecs += nextretry * nextretry;
        nextretry = birth + rndsecs;
        fprintf(stdout, "%ld nextretry=%ld (after %lds or %ldm %02lds)\n",
               now, nextretry, nextretry - now,
               (nextretry - now)/60, (nextretry - now) % 60);
        now += nextretry - now;
    }
    fflush(stdout);

    return 0;
}


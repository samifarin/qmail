/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

/*

~control/rbl/list

list contains a number of RBL's to check for the given senders IP address.
The file consists of four TAB or SPACE separated fields.

  basedomain:
    base domain address to lookup (e.g. relays.ordb.org)

  action:
    ``addheader'' will just create a X-RBL: header
    ``reject'' will reject the RCPT TO target with 553 error unless
      (s)he's in control/unrblto
    ``whitelist'' stops processing remaining DNSbls and accepts the
      connection if correct A or TXT record is found
    ``whitelisth'' -- likewise, except that it additionally adds header like
      with ``addheader''

    --- whitelisted basedomain could be rbldns running with a good list of IPs
    --- (friends, partners) maintained by your company or something.

  matchon:
    if ``IP address'' is specified the action is only
      taken if the returned address from basedomain is equal to ``IP-address'' (v4 only)
    with ``any'' every returned IP-address will match -- in this case also every
      found IP will be listed in X-RBL field and 553 errors.
    with ``txt'' contents of the TXT record found in DNS are appended to message
      (at max 250 bytes by default, see TXTLEN in rbl.h)

  message: message to be included in X-RBL: header fields and 553 errors.
    be descriptive and don't mislead the users.

  finally, RBLINFO environment variable is appended after the message.
  you can put there for example
  "contact postmaster@example.com if you feel you are listed erraneously"

Example:
basedomain             action     matchon    message
=========================================================================
relays.ordb.org        reject     txt        listed on relays.ordb.org
sbl.spamhaus.org       reject     txt        listed on sbl.spamhaus.org
list.dsbl.org          reject     txt        listed on list.dsbl.org
internap.blackholes.us reject     127.0.0.2  listed on internap.blackholes.us
zz.countries.nerd.dk   addheader  127.0.2.54 zz.countries.nerd.dk Nigeria
bl.spamcop.net         addheader  txt        listed on bl.spamcop.net
list.example.com       whitelisth 127.0.0.5  whitelisted by A record 127.0.0.5 on list.example.com
aspath.routeviews.org  addheader  txt        AS Path

sample X-RBL field added to message:
X-RBL: 127.0.0.2 found on bl.spamcop.net (TXT record): listed on bl.spamcop.net [ Blocked - see http://spamcop.net/bl.shtml?127.0.0.2 ]

sample with DNS_TXT_DELIM1=" - " DNS_TXT_DELIM2="/"
X-RBL: 69.19.194.182 found on aspath.routeviews.org (TXT record):
  listed on aspath.routeviews.org [ 16150 8434 2119 8210 6461 19864/69.19.128.0/17 - 16150 8434 3257 209 14288 14288 14288 14288 14288 14288 14288/69.19.192.0/20 ]

http://www.declude.com/JunkMail/Support/ip4r.htm

*/

#include "dns.h"
#include "env.h"
#include "ipalloc.h"
#include "qmail.h"
#include "rbl.h"
#include "stralloc.h"
#include "str.h"
#include "strerr.h"
#include "alloc.h"
#include "control.h"
#include "error.h"
#include "open.h"
#include "readwrite.h"
#include "substdio.h"
#include "getln.h"
#include "byte.h"

#define MAX(a,b) ({typeof (a) _a = (a); typeof (b) _b = (b); _a > _b ? _a : _b; })
#define MIN(a,b) ({typeof (a) _a = (a); typeof (b) _b = (b); _a < _b ? _a : _b; })

static stralloc rblmessage = {0};
static stralloc rblmatches = {0};
static int rblprintheader;

extern void die_nomem();
static int rblinit(char*, char*);

void rblheader(struct qmail *qqt)
{
  if (!rblprintheader) return;
  if (rblmessage.s) qmail_put(qqt,rblmessage.s,rblmessage.len);
}

void rblflush(void)
{
  rblprintheader = 0;
  if (!stralloc_copys(&rblmessage, "")) die_nomem();
  if (!stralloc_copys(&rblmatches, "")) die_nomem();
}

static rbl_t rbl[MAXRBL];
static int numrbl;
static stralloc ip_reverse = {0};
static stralloc rbl_tmp = {0};

static int rbl_start(char *);
static int rbl_start(char *remoteip)
{
  unsigned int i;
  unsigned int j;
  char *ip_env;

  ip_env = remoteip;
  if (!ip_env) ip_env = "";

  if (!stralloc_copys(&ip_reverse, "")) die_nomem();

  i = str_len(ip_env);
  while (i) {
    for (j = i; j > 0; --j) {
      if (ip_env[j - 1] == '.') break;
      if (ip_env[j - 1] == ':') return 0; /* no IPv6 */
    }
    if (!stralloc_catb(&ip_reverse, ip_env + j, i - j)) die_nomem();
    if (!stralloc_cats(&ip_reverse, ".")) die_nomem();
    if (!j) break;
    i = j - 1;
  }
  return 1;
}

static char ipstr[IPFMT];
static stralloc messagetxt = {0};
static ipalloc rblsa = {0};

static int rbl_lookup(rbl_t*);
int rbl_lookup(rbl_t* curr)
{
  int i;
  int flagtrunc = 0;

  if (curr->baseaddr.s[0] == '\0') return 2;

  if (!stralloc_copy(&rbl_tmp, &ip_reverse)) die_nomem();
  if (!stralloc_cats(&rbl_tmp, curr->baseaddr.s)) die_nomem();
  if (!stralloc_copys(&messagetxt, "")) die_nomem();

  if (!str_diff("txt", curr->matchon.s)) {
    switch (dns_txt(&messagetxt, &rbl_tmp)) {
      case DNS_MEM:
      case DNS_SOFT:
        return 2; /* soft error */
      case DNS_HARD:
        return 0; /* found no match */
      default: /* found match */
        if (messagetxt.len > TXTLEN) { messagetxt.len = TXTLEN; flagtrunc = 1; }
        /* overwrite previous txt data with fresh data just fetched */
        curr->message.len = curr->origlen - 1;
        if (!stralloc_cats(&curr->message, " [ ")) die_nomem();
        if (!stralloc_cat(&curr->message, &messagetxt)) die_nomem();
        if (flagtrunc) if (!stralloc_cats(&curr->message, " ...")) die_nomem();
        if (!stralloc_cats(&curr->message, " ]")) die_nomem();
        if (!stralloc_0(&curr->message)) die_nomem();
        return 1;
    }
  }

  switch (dns_ip(&rblsa, &rbl_tmp)) {
    case DNS_MEM:
    case DNS_SOFT:
      return 2;    /* temporary dns or memory error */
    case DNS_HARD:
      return 0;    /* found no A records in DNS */
    case 0:        /* found match */
      if (!str_diff("any", curr->matchon.s)) {
        /* store found A records into the message */
        curr->message.len = curr->origlen - 1;
        if (!stralloc_cats(&curr->message, " (")) die_nomem();
        for (i = 0; i < rblsa.len; i++) {
          ipstr[ip_fmt(ipstr, &rblsa.ix[i].ip)] = 0;
          if (!stralloc_cats(&curr->message, ipstr)) die_nomem();
          if (i < (rblsa.len - 1)) if (!stralloc_cats(&curr->message, " ")) die_nomem();
        }
        if (!stralloc_cats(&curr->message, ")")) die_nomem();
        if (!stralloc_0(&curr->message)) die_nomem();
        return 1;
      }
      for (i = 0; i < rblsa.len; i++) {
        ipstr[ip_fmt(ipstr,&rblsa.ix[i].ip)] = 0;
        if (!str_diff(ipstr, curr->matchon.s)) {
          return 1;
        }
      }
      return 0;    /* no wanted A records were found */
    default:
      return 0;
  }
}

static void rbladdheader(rbl_t*, char*, int);
void rbladdheader(rbl_t* curr, char *remoteip, int flagwl)
{
  char *rbldomain;
  char *rbldomainend;
  static stralloc rbllookfor = {0};

  /* all of base, matchon and message can be trusted because these
     are under our control */
  rblprintheader = 1;
  rbldomain = rblmatches.s;
  rbldomainend = rblmatches.s + rblmatches.len;
  if (!stralloc_copys(&rbllookfor, curr->baseaddr.s)) die_nomem();
  if (!stralloc_cats(&rbllookfor, ":")) die_nomem();
  if (!stralloc_cats(&rbllookfor, curr->matchon.s)) die_nomem();
  if (!stralloc_0(&rbllookfor)) die_nomem();
  while(rbldomain < rbldomainend) {
    /* don't add duplicate matches */
    if (str_diff(rbllookfor.s, rbldomain) == 0) return;
    while (*rbldomain) rbldomain++;
    rbldomain++;
  }
  if(!stralloc_cats(&rblmatches, rbllookfor.s)) die_nomem();
  if(!stralloc_0(&rblmatches)) die_nomem();
  if(!stralloc_cats(&rblmessage, "X-RBL: ")) die_nomem();
  if(!stralloc_cats(&rblmessage, remoteip)) die_nomem();
  if(!stralloc_cats(&rblmessage, flagwl ? " (whitelisted) found on "
                    : " found on ")) die_nomem();
  if(!stralloc_cats(&rblmessage, curr->baseaddr.s)) die_nomem();
  if(!str_diff("txt", curr->matchon.s)) {
    if(!stralloc_cats(&rblmessage, " (TXT record): ")) die_nomem();
  } else if(!str_diff("any", curr->matchon.s)) {
    if(!stralloc_cats(&rblmessage, " (any A record): ")) die_nomem();
  } else {
    if(!stralloc_cats(&rblmessage, " (A record ")) die_nomem();
    if(!stralloc_cats(&rblmessage, curr->matchon.s)) die_nomem();
    if(!stralloc_cats(&rblmessage, "): ")) die_nomem();
  }
  if(!stralloc_cats(&rblmessage, curr->message.s)) die_nomem();
  if(!stralloc_cats(&rblmessage, "\n")) die_nomem();
}

int rblcheck(char *rblprefix, char *rbllist, char *remoteip, char** rblname)
{
  int i;
  char *rblinfo;

  if (!rbl_start(remoteip)) return RBL_OK; /* no IPv4 address found */
  switch (rblinit(rblprefix, rbllist)) {
    case -1:
      return RBL_ERR;
    case 0:
      return RBL_OK;
  }

  rblinfo = env_get("RBLINFO");
  for (i = 0; i < numrbl; i++) {
    switch (rbl_lookup(&rbl[i])) {
      case 1:
        *rblname = rbl[i].message.s;

        if (!str_diff("whitelist", rbl[i].action.s) ||
            !str_diff("whitelistp", rbl[i].action.s) ||
            !str_diff("whitelistf", rbl[i].action.s)) {
          return RBL_WHITELIST;
        }
        if (!str_diff("addheader", rbl[i].action.s)) {
          rbladdheader(&rbl[i], remoteip, 0);
          continue;
        }
        if (!str_diff("whitelisth", rbl[i].action.s)) {
          rbladdheader(&rbl[i], remoteip, 1);
          return RBL_WHITELIST;
        }
        if (!str_diff("rejecttemp", rbl[i].action.s)) {
          if (rblinfo) {
            rbl[i].message.len--;
            if (!stralloc_cats(&rbl[i].message, rblinfo) ||
                !stralloc_0(&rbl[i].message))
              die_nomem();
            *rblname = rbl[i].message.s;
          }
          /* temporary reject */
          return RBL_REJECTTEMP;
        }
        if (!str_diff("reject", rbl[i].action.s)) {
          if (rblinfo) {
            rbl[i].message.len--;
            if (!stralloc_cats(&rbl[i].message, rblinfo) ||
                !stralloc_0(&rbl[i].message))
              die_nomem();
            *rblname = rbl[i].message.s;
          }
        /* default reject */
        return RBL_REJECT;
        }
        break;

      case 2:
        if (!str_diff("whitelistp", rbl[i].action.s)) return RBL_WHITELISTP;
        if (!str_diff("whitelistf", rbl[i].action.s)) return RBL_WHITELISTF;
        break;
    }
  }
  return RBL_OK; /* either tagged, soft error or allowed */
}

static char rblinbuf[SUBSTDIO_INSIZE];
static substdio rblssin;
static stralloc ln = {0};

static int rblinit(rblprefix, rbllist)
char *rblprefix;
char *rbllist;
{
  int fd;
  long int tab;
  char* cp;
  long int len;
  int match;
  static stralloc rblfn = {0};

  if (!stralloc_copys(&rblfn, "control/")) return -1;
  if (!stralloc_cats(&rblfn, rblprefix)) return -1;
  if (!stralloc_cats(&rblfn, "/")) return -1;
  if (!stralloc_cats(&rblfn, rbllist)) return -1;
  if (!stralloc_0(&rblfn)) return -1;

  fd = open_read(rblfn.s);
  if (fd == -1) return ((errno == error_noent) ? 0 : -1);
  substdio_fdbuf(&rblssin, read, fd, rblinbuf, sizeof(rblinbuf));
  numrbl = 0;
  for (;;) {
    if (getln(&rblssin, &ln, &match, '\n') != 0) { (void)close(fd); return -1; }
    if (!match && !ln.len) break;
    if (ln.len && (ln.s[0] == '.')) break;
    if (ln.len && (ln.s[0] == '#')) continue;
    if (byte_chr(ln.s, ln.len, '\0') < ln.len) break;

    cp = ln.s; len = ln.len;

    tab = MIN(byte_chr(cp, len, '\t'), byte_chr(cp, len, ' '));
    if (tab == len) continue;
    if (!stralloc_copyb(&rbl[numrbl].baseaddr, cp, tab)) die_nomem();
    if (!stralloc_0(&rbl[numrbl].baseaddr)) die_nomem();
    ++tab; cp += tab; len -= tab;

    while (len && (*cp == '\t' || *cp == ' ')) { len--; cp++; }
    if (len == 0) continue;
    tab = MIN(byte_chr(cp, len, '\t'), byte_chr(cp, len, ' '));
    if (tab == len) continue;
    if (!stralloc_copyb(&rbl[numrbl].action, cp, tab)) die_nomem();
    if (!stralloc_0(&rbl[numrbl].action)) die_nomem();
    ++tab; cp += tab; len -= tab;

    while (len && (*cp == '\t' || *cp == ' ')) { len--; cp++; }
    if (len == 0) continue;
    tab = MIN(byte_chr(cp, len, '\t'), byte_chr(cp, len, ' '));
    if (tab == len) continue;
    if (!stralloc_copyb(&rbl[numrbl].matchon, cp, tab)) die_nomem();
    if (!stralloc_0(&rbl[numrbl].matchon)) die_nomem();
    ++tab; cp += tab; len -= tab;

    while (len && (*cp == '\t' || *cp == ' ')) { len--; cp++; }
    tab = byte_chr(cp, len, '\n');
    if (tab > TXTLEN) tab = TXTLEN;
    /* remove trailing whitespace */
    while (tab && ((cp[tab-1] == ' ') || (cp[tab-1] == '\t'))) tab--;
    if (tab == 0) {
      if (!stralloc_copys(&rbl[numrbl].message, "[UNKNOWN]")) die_nomem();
    } else {
      if (!stralloc_copyb(&rbl[numrbl].message, cp, tab)) die_nomem();
    }
    if (!stralloc_0(&rbl[numrbl].message)) die_nomem();
    rbl[numrbl].origlen = rbl[numrbl].message.len;

    if (++numrbl == MAXRBL) break;
  }
  (void)close(fd);
  if (!numrbl) {
    /* no valid config lines found */
    return 0;
  }

  return 1;
}


#include "charcrandom.h"
#include "helper.h"

uint16 dns_random(void)
{
  return charcrandom_u16();
}

void dns_random_init(void)
{
  (void)dns_random();
}


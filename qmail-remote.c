/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>

#include "sig.h"
#include "stralloc.h"
#include "substdio.h"
#include "subfd.h"
#include "scan.h"
#include "case.h"
#include "error.h"
#include "auto_qmail.h"
#include "control.h"
#include "dns.h"
#include "alloc.h"
#include "quote.h"
#include "fmt.h"
#include "ip.h"
#include "ipalloc.h"
#include "ipme.h"
#include "gen_alloc.h"
#include "gen_allocdefs.h"
#include "str.h"
#include "now.h"
#include "exit.h"
#include "constmap.h"
#include "tcpto.h"
#include "readwrite.h"
#include "timeoutconn.h"
#include "timeoutread.h"
#include "timeoutwrite.h"
#include "helper.h"
#include "env.h"
#include "slurpclose.h"
#include "open.h"

#ifdef TLS_REMOTE
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/x509.h>

static SSL *ssl = 0;
#endif

#define HUGESMTPTEXT 5000

static uint32 smtp_port = 25;
static uint32 qmtp_port = 209;

GEN_ALLOC_typedef(saa,stralloc,sa,len,a)
GEN_ALLOC_readyplus(saa,stralloc,sa,len,a,i,n,x,10,saa_readyplus)
static stralloc sauninit = {0};

static stralloc helohost = {0};
static stralloc outgoingip = {0};
static stralloc routes = {0};
static struct constmap maproutes;
static stralloc host = {0};
static stralloc sender = {0};
static int reqcert = 0;

static struct stat st;

static stralloc hmackey = {0};

static saa reciplist = {0};

static struct ip_address partner;
static struct ip_address outip;

static void out(s) char *s; { if (substdio_puts(subfdoutsmall,s) == -1) _exit(0); }
static void zero() { if (substdio_put(subfdoutsmall,"\0",1) == -1) _exit(0); }
static void zerodie() { zero(); substdio_flush(subfdoutsmall); _exit(0); }
static void outsafe(sa) stralloc *sa; { int i; char ch;
  for (i = 0;i < sa->len;++i) {
  ch = sa->s[i]; if (ch < 33) ch = '?'; if (ch > 126) ch = '?';
  if (substdio_put(subfdoutsmall,&ch,1) == -1) _exit(0); }
}

static void temp_noconn(int serr, int tried, struct ip_address *ip) {
  char iptemp[IPFMT];

  out("ZSorry, I wasn't able to establish an SMTP connection");
  if (tried) {
    iptemp[ip_fmt(iptemp, ip)] = 0;
    out(" to ");
    out(iptemp);
    out(": ");
    out(error_str(serr));
  } else {
    out(": TCP timeout");
  }
  out(" (#4.4.1)\n");
  zerodie();
}

static void temp_noip() { out("Zinvalid IP in control/outgoingip (#4.3.0)\n"); zerodie(); }
static void temp_nomem() { out("ZOut of memory. (#4.3.0)\n"); zerodie(); }
static void temp_oserr() { out("Z\
System resources temporarily unavailable. (#4.3.0)\n"); zerodie(); }
static void temp_fstat() { out("Z\
Unable to fstat stdin (#4.3.0)\n"); zerodie(); }
static void temp_read() { out("ZUnable to read message. (#4.3.0)\n"); zerodie(); }
static void temp_dnscanon() { out("Z\
CNAME lookup failed temporarily. (#4.4.3)\n"); zerodie(); }
static void temp_dns() { out("Z\
Sorry, I couldn't find any host by that name. (#4.1.2)\n"); zerodie(); }
static void temp_chdir() { out("Z\
Unable to switch to home directory. (#4.3.0)\n"); zerodie(); }
static void temp_control() { out("Z\
Unable to read control files. (#4.3.0)\n"); zerodie(); }
static void temp_proto() { out("Z\
recipient did not talk proper QMTP (#4.3.0)\n"); zerodie(); }
static void perm_partialline() { out("D\
SMTP cannot transfer messages with partial final lines. (#5.6.2)\n"); zerodie(); }
static void perm_usage() { out("D\
I (qmail-remote) was invoked improperly. (#5.3.5)\n"); zerodie(); }
static void perm_dns() { out("D"
  "Sorry, I couldn't find any host named ");
  outsafe(&host);
  out(". (#5.1.2)\n"); zerodie(); }
static void perm_nomx() { out("D"
  "Sorry, I couldn't find a mail exchanger or IP address. (#5.4.4)\n");
  zerodie(); }
static void perm_ambigmx() { out("D"
  "Sorry. Although I'm listed as a best-preference MX or A for that host,\n"
  "it isn't in my control/locals file, so I don't treat it as local. (#5.4.6)\n");
  zerodie(); }
static void outhost()
{
  char x[IPFMT];
  if (substdio_put(subfdoutsmall,x,ip_fmt(x,&partner)) == -1) _exit(0);
}

static int flagcritical = 0;

static void dropped() {
  out("ZConnected to ");
  outhost();
  out(" but connection died. ");
  if (flagcritical) out("Possible duplicate! ");
  out("(#4.4.2)\n");
  zerodie();
}

static uint32 timeoutconnect = 60;
static uint32 timeout = 1200;
static int smtpfd;

#ifdef TLS_REMOTE
static int flagtimedout = 0;
static void sigalrm()
{
 flagtimedout = 1;
}

int ssl_timeoutread(int tout, int fd, char *buf, int n)
{
  int r;
  int saveerrno;

  if (flagtimedout) { errno = error_timeout; return -1; }
  alarm(tout);
  r = SSL_read(ssl,buf,n);
  saveerrno = errno;
  alarm(0);
  if (flagtimedout) { errno = error_timeout; return -1; }
  errno = saveerrno;
  return r;
}

int ssl_timeoutwrite(int tout, int fd, char *buf, int n)
{
  int r;
  int saveerrno;

  if (flagtimedout) { errno = error_timeout; return -1; }
  alarm(tout);
  r = SSL_write(ssl,buf,n);
  saveerrno = errno;
  alarm(0);
  if (flagtimedout) { errno = error_timeout; return -1; }
  errno = saveerrno;
  return r;
}
#endif

int saferead(fd,buf,len) int fd; char *buf; int len;
{
  int r;
#ifdef TLS_REMOTE
  if (ssl)
    r = ssl_timeoutread(timeout,smtpfd,buf,len);
  else
#endif
  r = timeoutread(timeout,smtpfd,buf,len);
  if (r <= 0) dropped();
  return r;
}
int safewrite(fd,buf,len) int fd; char *buf; int len;
{
  int r;
#ifdef TLS_REMOTE
  if (ssl)
    r = ssl_timeoutwrite(timeout,smtpfd,buf,len);
  else
#endif
  r = timeoutwrite(timeout,smtpfd,buf,len);
  if (r <= 0) dropped();
  return r;
}

static char inbuf[1500];
static substdio ssin = SUBSTDIO_FDBUF(read,0,inbuf,sizeof inbuf);
static char smtptobuf[1500];
static substdio smtpto = SUBSTDIO_FDBUF(safewrite,-1,smtptobuf,sizeof smtptobuf);
static char smtpfrombuf[128];
static substdio smtpfrom = SUBSTDIO_FDBUF(saferead,-1,smtpfrombuf,sizeof smtpfrombuf);

static stralloc smtptext = {0};

static void get(ch)
char *ch;
{
  substdio_get(&smtpfrom,ch,1);
  if (*ch != '\r')
    if (smtptext.len < HUGESMTPTEXT)
     if (!stralloc_append(&smtptext,ch)) temp_nomem();
}

static unsigned long smtpcode()
{
  unsigned char ch;
  unsigned long code;

  if (!stralloc_copys(&smtptext,"")) temp_nomem();

  get(&ch); code = ch - '0';
  get(&ch); code = code * 10 + (ch - '0');
  get(&ch); code = code * 10 + (ch - '0');
  for (;;) {
    get(&ch);
    if (ch != '-') break;
    while (ch != '\n') get(&ch);
    get(&ch);
    get(&ch);
    get(&ch);
  }
  while (ch != '\n') get(&ch);

  return code;
}

static void outsmtptext()
{
  int i;
  if (smtptext.s) if (smtptext.len) {
    out("Remote host said: ");
    for (i = 0;i < smtptext.len;++i)
      if (!smtptext.s[i]) smtptext.s[i] = '?';
    if (substdio_put(subfdoutsmall,smtptext.s,smtptext.len) == -1) _exit(0);
    smtptext.len = 0;
  }
}

static void quit(prepend,append)
char *prepend;
char *append;
{
  substdio_putsflush(&smtpto, "QUIT\r\n");
  /* waiting for remote side is just too ridiculous */
  out(prepend);
  outhost();
  out(append);
  out(".\n");
  outsmtptext();

#if defined(TLS_REMOTE) && defined(TLSDEBUG)
#define ONELINE_NAME(X) X509_NAME_oneline(X,NULL,0)
  if(ssl) {
    X509 *peer;

    out("STARTTLS proto="); out(SSL_get_version(ssl));
    out("; cipher="); out(SSL_CIPHER_get_name(SSL_get_current_cipher(ssl)));

    /* we want certificate details */
    peer = SSL_get_peer_certificate(ssl);
    if (peer != NULL && (SSL_get_verify_result(ssl) == X509_V_OK)) {
      char *str;
      unsigned char md[EVP_MAX_MD_SIZE];
      unsigned int mdlen, i;
      uint8 nibbles[3] = {0, 0, 0};
      const uint8 hextbl[] = "0123456789abcdef";

      str = X509_NAME_oneline(X509_get_subject_name(peer), 0, 0);
      out("; subject="); out(str);
      OPENSSL_free(str);
      str = X509_NAME_oneline(X509_get_issuer_name(peer), 0, 0);
      out("; issuer="); out(str);
      OPENSSL_free(str);
      if (!X509_digest(peer, EVP_sha256(), md, &mdlen)) temp_nomem();
      out("; digest=");
      for (i = 0; i < mdlen; i++) {
        nibbles[0] = hextbl[(md[i] >> 4)];
        nibbles[1] = hextbl[(md[i] & 15)];
        out(nibbles);
      }
    }
    out(";\n");
	}
#endif

  zerodie();
}

static void blast()
{
  long int r;
  long int i;
  long int o;
  char in[4096];
  char out[4096*2+1];
  int sol;

  for (sol = 1;;) {
    r = substdio_get(&ssin, in, sizeof(in));
    if (r == 0) break;
    if (r == -1) temp_read();
    for (i = o = 0; i < r; ) {
      if (sol && in[i] == '.') {
        out[o++] = '.';
        out[o++] = in[i++];
      }
      sol = 0;
      while (i < r) {
        if (in[i] == '\n') {
          sol = 1;
          ++i;
          out[o++] = '\r';
          out[o++] = '\n';
          break;
        }

        out[o++] = in[i++];
      }
    }
    substdio_put(&smtpto,out,o);
  }

  if (!sol) perm_partialline();
  flagcritical = 1;
  substdio_put(&smtpto,".\r\n",3);
  substdio_flush(&smtpto);
}

static stralloc recip = {0};

static int ssl_set_verify_partial (SSL_CTX *ctx)
{
  int rv = 0;
#ifdef X509_V_FLAG_PARTIAL_CHAIN
  X509_VERIFY_PARAM *param;

  if (1)
  {
    param = X509_VERIFY_PARAM_new();
    if (param)
    {
      X509_VERIFY_PARAM_set_flags(param, X509_V_FLAG_PARTIAL_CHAIN);
      if (0 == SSL_CTX_set1_param(ctx, param))
      {
        rv = -1;
      }
      X509_VERIFY_PARAM_free(param);
    }
    else
    {
      rv = -2;
    }
  }
#endif
  return rv;
}

static void smtp()
{
  uint64 len;
  unsigned long code;
  int flagbother;
  int flagsize;
  int i;
  char num[FMT_ULONG];
#ifdef TLS_REMOTE
  int flagtls = 0;
  SSL_CTX *ctx;
  int saveerrno, r;
#ifdef TLSDEBUG
  char buf[1024];
#endif
#endif

  code = smtpcode();
  if ((code >= 500) && (code < 600)) quit("DConnected to "," but greeting failed");
  if ((code >= 400) && (code < 500)) return;
  if (code != 220) quit("ZConnected to "," but greeting failed");

  flagsize = 0;
  substdio_puts(&smtpto,"EHLO ");
  substdio_put(&smtpto,helohost.s,helohost.len);
  substdio_puts(&smtpto,"\r\n");
  substdio_flush(&smtpto);

  code = smtpcode();
  if (code != 250) {
   substdio_puts(&smtpto,"HELO ");
   substdio_put(&smtpto,helohost.s,helohost.len);
   substdio_puts(&smtpto,"\r\n");
   substdio_flush(&smtpto);
   code = smtpcode();
   if ((code >= 500) && (code < 600)) quit("DConnected to "," but my name was rejected");
   if ((code >= 400) && (code < 500)) return;
   if (code != 250) quit("ZConnected to "," but my name was rejected");
  }

  /* extension handling */
  for (i = 0; i < smtptext.len; i += str_chr(smtptext.s+i,'\n') + 1) {
    if (i+8 < smtptext.len && !case_diffb("SIZE", 4, smtptext.s+i+4))
      flagsize = 1;
#ifdef TLS_REMOTE
    else if (i+12 < smtptext.len && !case_diffb("STARTTLS", 8, smtptext.s+i+4))
      flagtls = 1;
#endif
  }

#ifdef TLS_REMOTE
  if (flagtls) {
    long sopt = 0;

    substdio_puts(&smtpto,"STARTTLS\r\n");
    substdio_flush(&smtpto);
    if (smtpcode() == 220) {
      SSL_library_init();
      SSL_load_error_strings();

      if(!(ctx=SSL_CTX_new(SSLv23_client_method()))) {
        smtptext.len = 0;
        out("ZTLS not available: SSL_CTX_new failed");
#ifdef TLSDEBUG
        out(": ");
        out(ERR_error_string(ERR_get_error(), buf));
#endif
        out("\n");
        zerodie();
      }

      if (!SSL_CTX_use_certificate_chain_file(ctx, "control/servercert-ec.pem")) {
        smtptext.len = 0;
        out("ZTLS not available: SSL_CTX_use_certificate_chain_file failed");
#ifdef TLSDEBUG
        out(": ");
        out(ERR_error_string(ERR_get_error(), buf));
#endif
        out("\n");
        zerodie();
      }

      if (!SSL_CTX_use_PrivateKey_file(ctx, "control/serverkey-ec.pem", SSL_FILETYPE_PEM)) {
        smtptext.len = 0;
        out("ZTLS not available: SSL_CTX_use_RSAPrivateKey_file failed");
#ifdef TLSDEBUG
        out(": ");
        out(ERR_error_string(ERR_get_error(), buf));
#endif
        out("\n");
        zerodie();
      }

      if (!SSL_CTX_check_private_key(ctx)) {
        smtptext.len = 0;
        out("ZTLS not available: SSL_CTX_check_private_key failed");
#ifdef TLSDEBUG
        out(": ");
        out(ERR_error_string(ERR_get_error(), buf));
#endif
        out("\n");
        zerodie();
      }

      if (!SSL_CTX_load_verify_locations(ctx, "control/ca.crt", NULL) ||
          !SSL_CTX_set_default_verify_paths(ctx)) {
        /* If using your own self-created CA, set SSLREQUIREPEERCERT="1" in
         * qmail-send invocation and add your CA's cert into ca.crt */
#if 0
        smtptext.len = 0;
        out("ZTLS no available: error while reading CAs");
        out(": ");
        out(ERR_error_string(ERR_get_error(), buf));
        out("\n");
        zerodie();
#endif
      }

      sopt = SSL_OP_NO_TLSv1_1 | SSL_OP_NO_TLSv1;
#ifdef SSL_OP_NO_SSLv3
      sopt |= SSL_OP_NO_SSLv3;
#endif
#ifdef SSL_OP_NO_SSLv2
      sopt |= SSL_OP_NO_SSLv2;
#endif
#ifdef SSL_OP_NO_COMPRESSION
      sopt |= SSL_OP_NO_COMPRESSION;
#endif
      SSL_CTX_set_options(ctx, sopt);

      ssl_set_verify_partial(ctx);

      if(!(ssl=SSL_new(ctx))) {
        smtptext.len = 0;
        out("ZTLS not available: error initializing SSL");
#ifdef TLSDEBUG
        out(": ");
        out(ERR_error_string(ERR_get_error(), buf));
#endif
        out("\n");
        zerodie();
      }
      if (SSL_set_fd(ssl,smtpfd) != 1) {
        smtptext.len = 0;
        out("ZTLS not available: SSL_set_fd failed\n");
        zerodie();
      }

      if (reqcert) SSL_set_verify(ssl, SSL_VERIFY_PEER, NULL);
      SSL_set_mode (ssl, SSL_MODE_AUTO_RETRY);
      //SSL_set_tlsext_host_name (ssl, hostname);

      alarm(timeout);
      r = SSL_connect(ssl); saveerrno = errno;
      alarm(0);
      if (flagtimedout) {
        smtptext.len = 0;
        out("ZTLS not available: SSL_connect timed out\n");
        zerodie();
      }
      errno = saveerrno;
      if (r <= 0) {
        smtptext.len = 0;
        out("ZTLS not available: SSL_connect failed");
#ifdef TLSDEBUG
        out(": ");
        out(ERR_error_string(ERR_get_error(), buf));
#endif
        out("\n");
        zerodie();
      } else {
        if (reqcert) {
          X509 *peercert;
          int vrfy;

          peercert = SSL_get_peer_certificate(ssl);
          if (peercert) {
            vrfy = SSL_get_verify_result(ssl);
            if (vrfy != X509_V_OK) {
              smtptext.len = 0;
              out("ZTLS not available: SSL_get_verify_result failed");
#ifdef TLSDEBUG
              out(": ");
              out(ERR_error_string(ERR_get_error(), buf));
#endif
              out("\n");
              zerodie();
            }
          }
        }
      }

      substdio_puts(&smtpto,"EHLO ");
      substdio_put(&smtpto,helohost.s, helohost.len);
      substdio_puts(&smtpto,"\r\n");
      substdio_flush(&smtpto);

      if (smtpcode() != 250) {
        quit("ZTLS connected to "," but my name was rejected");
      }
    }
  }
#endif

  substdio_puts(&smtpto,"MAIL FROM:<");
  substdio_put(&smtpto,sender.s,sender.len);
  substdio_puts(&smtpto,">");
  if (flagsize) {
    substdio_puts(&smtpto," SIZE=");
    len = st.st_size;
    len += len>>5; /* add some size for the \r chars see rcf 1870 */
    substdio_put(&smtpto,num,fmt_ullong(num,len+1));
  }
  substdio_puts(&smtpto,"\r\n");
  substdio_flush(&smtpto);
  code = smtpcode();
  if (code >= 500) quit("DConnected to "," but sender was rejected");
  if (code >= 400) quit("ZConnected to "," but sender was rejected");

  flagbother = 0;
  for (i = 0;i < reciplist.len;++i) {
    substdio_puts(&smtpto,"RCPT TO:<");
    substdio_put(&smtpto,reciplist.sa[i].s,reciplist.sa[i].len);
    substdio_puts(&smtpto,">\r\n");
    substdio_flush(&smtpto);
    code = smtpcode();
    if (code >= 500) {
      out("h"); outhost(); out(" does not like recipient.\n");
      outsmtptext(); zero();
    }
    else if (code >= 400) {
      out("s"); outhost(); out(" does not like recipient.\n");
      outsmtptext(); zero();
    }
    else {
      out("r"); zero();
      flagbother = 1;
    }
  }
  if (!flagbother) quit("DGiving up on ","");

  substdio_putsflush(&smtpto,"DATA\r\n");
  code = smtpcode();
  if (code >= 500) quit("D"," failed on DATA command");
  if (code >= 400) quit("Z"," failed on DATA command");

  blast();
  code = smtpcode();
  flagcritical = 0;
  if (code >= 500) quit("D"," failed after I sent the message");
  if (code >= 400) quit("Z"," failed after I sent the message");
  quit("K"," accepted message");
}

static int qmtp_priority(int pref)
{
  if (pref < 12800) return 0;
  if (pref > 13055) return 0;
  if (pref % 16 == 1) return 1;
  return 0;
}

static void qmtp()
{
  uint64 len;
  char *x;
  int i;
  int n;
  unsigned char ch;
  unsigned char rv;
  char num[FMT_ULONG];
  int flagbother;

  len = st.st_size;

  /* the following code was substantially taken from serialmail's serialqmtp.c */
  substdio_put(&smtpto,num,fmt_ulong(num,len+1));
  substdio_put(&smtpto,":\n",2);
  while (len > 0) {
    n = substdio_feed(&ssin);
    if (n <= 0) temp_read(); /* wise guy again */
    x = substdio_PEEK(&ssin);
    substdio_put(&smtpto,x,n);
    substdio_SEEK(&ssin,n);
    len -= n;
  }
  substdio_put(&smtpto,",",1);

  len = sender.len;
  substdio_put(&smtpto,num,fmt_ulong(num,len));
  substdio_put(&smtpto,":",1);
  substdio_put(&smtpto,sender.s,sender.len);
  substdio_put(&smtpto,",",1);

  len = 0;
  for (i = 0;i < reciplist.len;++i)
    len += fmt_ulong(num,reciplist.sa[i].len) + 1 + reciplist.sa[i].len + 1;
  substdio_put(&smtpto,num,fmt_ulong(num,len));
  substdio_put(&smtpto,":",1);
  for (i = 0;i < reciplist.len;++i) {
    substdio_put(&smtpto,num,fmt_ulong(num,reciplist.sa[i].len));
    substdio_put(&smtpto,":",1);
    substdio_put(&smtpto,reciplist.sa[i].s,reciplist.sa[i].len);
    substdio_put(&smtpto,",",1);
  }
  substdio_put(&smtpto,",",1);
  substdio_flush(&smtpto);

  flagbother = 0;

  for (i = 0;i < reciplist.len;++i) {
    len = 0;
    for (;;) {
      get(&ch);
      if (ch == ':') break;
      if (len > 200000000) temp_proto();
      if (ch - '0' > 9) temp_proto();
      len = 10 * len + (ch - '0');
    }
    if (!len) temp_proto();
    get(&ch); --len;
    if ((ch != 'Z') && (ch != 'D') && (ch != 'K')) temp_proto();

    rv = ch;
    if (!stralloc_copys(&smtptext,"qmtp: ")) temp_nomem();

    /* read message */
    while (len > 0) {
      get(&ch);
      --len;
    }
    get(&ch);
    if (ch != ',') temp_proto();
    smtptext.s[smtptext.len-1] = '\n';

    switch (rv) {
      case 'K':
        out("r"); zero();
        flagbother = 1;
        break;
      case 'D':
        out("h"); outhost(); out("  does not like recipient.\n");
        outsmtptext(); zero();
        break;
      case 'Z':
        out("h"); outhost(); out("  does not like recipient.\n");
        outsmtptext(); zero();
        break;
    }
  }
  if (!flagbother) {
    out("DGiving up on "); outhost(); out(".\n"); outsmtptext();
  } else {
    out("K");outhost();out(" accepted message.\n"); outsmtptext();
  }
  zerodie();
}

static stralloc canonhost = {0};
static stralloc canonbox = {0};

static void addrmangle(saout, s, flagalias, flagcname)
stralloc *saout; /* host has to be canonical, box has to be quoted */
char *s;
int *flagalias;
int flagcname;
{
  int j;

  *flagalias = flagcname;

  j = str_rchr(s,'@');
  if (!s[j]) {
    if (!stralloc_copys(saout,s)) temp_nomem();
    return;
  }
  if (!stralloc_copys(&canonbox,s)) temp_nomem();
  canonbox.len = j;
  if (!quote(saout,&canonbox)) temp_nomem();
  if (!stralloc_cats(saout,"@")) temp_nomem();

  if (!stralloc_copys(&canonhost,s + j + 1)) temp_nomem();
  if (flagcname)
    switch(dns_cname(&canonhost)) {
      case 0: *flagalias = 0; break;
      case DNS_MEM: temp_nomem();
      case DNS_SOFT: temp_dnscanon();
      case DNS_HARD: ; /* alias loop, not our problem */
    }

  if (!stralloc_cat(saout,&canonhost)) temp_nomem();
}

static void getcontrols()
{
  if (control_init() == -1) temp_control();
  if (control_readint(&timeout,"control/timeoutremote") == -1) temp_control();
  if (control_readint(&timeoutconnect,"control/timeoutconnect") == -1)
    temp_control();
  if (control_rldef(&helohost,"control/helohost",1,(char *) 0) != 1)
    temp_control();
  switch(control_readfile(&routes,"control/smtproutes",0)) {
    case -1:
      temp_control();
    case 0:
      if (!constmap_init(&maproutes,"",0,1)) temp_nomem(); break;
    case 1:
      if (!constmap_init(&maproutes,routes.s,routes.len,1)) temp_nomem(); break;
  }
  if (control_rldef(&outgoingip, "control/outgoingip", 0, "0.0.0.0") == -1)
    temp_control();
  if (!stralloc_0(&outgoingip)) temp_nomem();
  if (!ip_scan(outgoingip.s, &outip)) temp_noip();
}

static inline int isconnproblem(int testerrno)
{
  switch(testerrno) {
    case ETIMEDOUT: return 1;
    case ENETUNREACH: return 1;
    case EHOSTUNREACH: return 1;
    /* connection reset usually means concurrency limit reached */
    default: return 0;
  }
}

int main(argc,argv)
int argc;
char **argv;
{
  static ipalloc ip = {0};
  int i;
  char **recips;
  int prefme;
  int flagallaliases;
  int flagalias;
  char *relayhost;
  int saved_errno;
  int triedconnect;

#ifdef TLS_REMOTE
  sig_alarmcatch(sigalrm);
#endif
  sig_pipeignore();
  if (argc < 4) perm_usage();
  if (chdir(auto_qmail) == -1) temp_chdir();
  getcontrols();
  if (!stralloc_copys(&host,argv[1])) temp_nomem();

  relayhost = 0;
  reqcert = (env_get("SSLREQUIREPEERCERT") != 0);
  for (i = 0;i <= host.len;++i)
    if ((i == 0) || (i == host.len) || (host.s[i] == '.'))
      if ((relayhost = constmap(&maproutes,host.s + i,host.len - i)))
        break;
  if (relayhost && !*relayhost) relayhost = 0;

  if (relayhost) {
    i = str_chr(relayhost,':');
    if (relayhost[i]) {
      scan_ulong(relayhost + i + 1,&smtp_port);
      relayhost[i] = 0;
    }
    if (!stralloc_copys(&host,relayhost)) temp_nomem();
  }

  addrmangle(&sender,argv[2],&flagalias,0);

  if (!saa_readyplus(&reciplist,0)) temp_nomem();
  if (ipme_init() != 1) temp_oserr();
  if (fstat(0,&st) == -1) temp_fstat();

  flagallaliases = 1;
  recips = argv + 3;
  while (*recips) {
    if (!saa_readyplus(&reciplist,1)) temp_nomem();
    reciplist.sa[reciplist.len] = sauninit;
    addrmangle(reciplist.sa + reciplist.len,*recips,&flagalias,!relayhost);
    if (!flagalias) flagallaliases = 0;
    ++reciplist.len;
    ++recips;
  }

  /* if you have remotekey, select the same remote IP for each
     outgoingip/sender/recipient pair to play more nicely with
     greylisting-feature on that remote IP. */
  switch (relayhost ? dns_ip(&ip,&host) : dns_mxip(&ip, &host, 0)) {
    case DNS_MEM: temp_nomem();
    case DNS_SOFT: temp_dns();
    case DNS_HARD: perm_dns();
    case 1:
      if (ip.len <= 0) temp_dns();
  }

  if (ip.len <= 0) perm_nomx();

  dns_shuffle(&ip);

  prefme = 100000;
  for (i = 0;i < ip.len;++i)
    if (ipme_is(&ip.ix[i].ip))
      if (ip.ix[i].pref < prefme)
        prefme = ip.ix[i].pref;

  if (relayhost) prefme = 300000;
  if (flagallaliases) prefme = 500000;

  for (i = 0; i < ip.len; ++i)
    if (ip.ix[i].pref < prefme)
      break;

  if (i >= ip.len)
    perm_ambigmx();

  saved_errno = 0;
  triedconnect = 0;

  for (i = 0;i < ip.len;++i) if (ip.ix[i].pref < prefme) {
    if (tcpto(&ip.ix[i].ip)) continue;

    triedconnect = 1;

    smtpfd = socket(AF_INET,SOCK_STREAM,0);
    if (smtpfd == -1) temp_oserr();

    if (qmtp_priority(ip.ix[i].pref)) {
      if (timeoutconn(smtpfd,&ip.ix[i].ip,&outip,(unsigned int) qmtp_port,timeoutconnect) == 0) {
        tcpto_err(&ip.ix[i].ip,0);
        partner = ip.ix[i].ip;
        qmtp(); /* does not return */
      }
      close(smtpfd);
      smtpfd = socket(AF_INET, SOCK_STREAM, 0);
      if (smtpfd == -1) temp_oserr();
    }
    if (timeoutconn(smtpfd,&ip.ix[i].ip,&outip,(unsigned int) smtp_port,timeoutconnect) == 0) {
      tcpto_err(&ip.ix[i].ip,0);
      partner = ip.ix[i].ip;
      smtp(); /* should not return unless the start code or the HELO code
                 returns a temporary failure (see RFC2821) */
      errno = error_proto; /* Better than random errno */
    }
    saved_errno = errno;
    tcpto_err(&ip.ix[i].ip, isconnproblem(saved_errno));
    close(smtpfd);
  }

  temp_noconn(saved_errno, triedconnect, &ip.ix[i-1].ip);

  return 0;
}


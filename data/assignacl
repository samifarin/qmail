# NOTE: lower case keys
# M1: found match in badmailfrom
# M2: temporary error while resolving domain (DNS, memory)
# M3: found no IPs for domain
# M4: like M3, except give 451 instead of 553 error
# M6: null (<>) envelope sender and NOBOUNCE env var was set
# M7: envelope sender maybe faked (e.g. x@hotmail.com but not connecting from hotmail.com)
#     setting FAKEDTEMP causes temporary rejection
# M8: broken DNS for connecting IP (no PTR record and matching A record for it)
#     (relies on BROKENDNS env var set by tcpserver)
# M9: like M8, except temporary error
# MA: Anti-Bogus-Bounce-Scheme, ABBS check is done for this recipient
#     MA does not imply null envelope sender, use also MD if needed
# MB: null (bounce, <>) envelope sender and recipient does not want them
# MC: STARTTLS not issued or X509 cert verify failed
# MD: envelope sender was not <> and recipient accepts only
#     Delivery Status Notifications
# ME  MEP: Pass - valid X509 cert with PKSC9 email acts like L3
#     MEF: Fail - invalid X509 cert without PKCS9 email causes 553 error
#
# MIFlistbadfrom: use DNS blacklist from file ``control/rbl/listbadfrom''
#                  for MAIL FROM's domain IPs
# MITlistbadto:    use DNS blacklist from file ``control/rbl/listbadto''
#                  for RCPT TO's domain IPs
#     gives 450 / 553 error if IPs was listed on queried DNSbl
#     no whitelisting
#
# MNFlistbadfrom: same as MIFlistbadfrom except domain's nameservers'
#                 IPs are checked
# MNTlistbadto:   likewise
#
# MP    remotehost PCRE check
# MPT?? give 450 error (temporary)
# MPP?? give 550 error (permanent)
#       ACL "MPTbadhosts" reads PCRE pattern from control/pcre/badhosts
#       and gives 450 error if pattern matches remotehost.
#       If remotehost is unknown, if matching is done.
#
# MR    RHS - Right Hand Side
#       needs three characters as parameters and then the RHSBL.
# MRT?? give temporary 450 error if match found
# MRt?? likewise, except give temporary 450 error also
#       if there was temporary DNS/memory problem
# MRP?? give permanent 553 error if match found
# MRp?? likewise, except give temporary 450 error
#       if there was temporary DNS/memory problem
# MR?M? use domain in MAIL FROM
# MR?R? use domain in RCPT TO
# MR?H? use domain in $TCPREMOTEHOST.  This does nothing if
#       that environment variable is not set.
# MR??A A search for A records
# MR??T T search for TXT records
#
# MS: recipient is not single recipient (multiple RCPT TOs)
# MT: recipient mailbox has AESTRAP enabled
#     MTfoo to read config from control/aestrap/foo instead of default
#
# H2: temporary error while resolving helohost (DNS, memory)
# H3: found no IPs for helohost
# H4: helohost was IP but pointed to the wrong IP
# H5: helohost did not have '.'
# H6: helohost did not point to the connecting IP (strict check)
# H7: helohost used was our greeting host
# H8: helohost was the same as recipient's mailbox
# H9: invalid use of SMTP PIPELINING with HELO/EHLO
# HO: helohost was $LOCALIP, our Own IP
#
# CD0 xyz: Check DNS status: matches if DNS broken (see M8 and M9);
#          additionally, execute ACL xyz.
# CD1 xyz: Check DNS status: matches if DNS ok;
#          additionally, execute ACL xyz.
# Example: "CD0 MG:" means apply ACL MG (greylisting) if DNS is broken
# If DNS is ok with ACL "CD0 xyz", xyz is skipped.
# If DNS is broken with ACL "CD1 xyz", xyz is skipped.
#
# L1: RBLSMTPD temporary
# L2: RBLSMTPD permanent
# L3: RBLSMTPD whitelist - if matched, stop checking remaining rules 
#
# return codes for RCPT TO checks
# 2xx: give 2xx code 
# 4xx: give 4xx code
# 5xx: give 5xx code
#
# return codes for DATA checks
# !2xx: give 2xx code (in DATA checks)
# !4xx: give 4xx code (in DATA checks)
# !5xx: give 5xx code (in DATA checks)
#
# S[uint64]: maximum mail size in bytes (e.g. ``S1048576'')
#
# Dlistdef: use DNS blacklist / whitelist from file ``control/rbl/listdef''
#
# Pfoo: initialize PCRE patterns from file control/pcre/foo
#       EXCEPTION: P0 means to skip reading any more PCRE pattern files.
#                  this also means you can't read patterns from
#                  file control/pcre/0 .
#
# X1: MS executable check
# X2: MS Word check
# X3: bad charset check
# X4: RFC2047 check (non-us-ascii-in-headers)
# X5: some attachment matched hash in badsha1 
# X6: bad MIME headers (invalid/no boundaries and/or endings or reached
#     max nesting level or boundary len was not 1-70 chars or boundary
#     contained invalid chars (``bchars'' in RFC2046))
# X7: no Message-ID in message
# XZ: ZIP attachment check
# XP: check PCRE status 
#     without this all of the 'P' ACLs are useless.
#
# Q1: QMAILQUEUE="bin/qmail-queue"
# Q2: QMAILQUEUE="bin/bogorun" , give 554 error if bogofilter detects spam
#                                and save the spam into qfilter's temp dir 
# Q3: QMAILQUEUE="bin/bogorun" , don't reject possible spam, add X-Bogosity header
# Q4: QMAILQUEUE="bin/bogorun" , give 554 error if bogofilter detects spam
#                                but do not keep a copy of the spam in qfilter's temp dir
# Q5: QMAILQUEUE="bin/bogorun" , give 250 status if bogofilter detects spam
#                                and discard the spam
#
# TG[string]: content-[T]ype [G]ood (e.g. ``TGtext/plain'')
# TB[string]: content-[T]ype [B]ad (e.g. ``TBtext/html'')
#
# special first chars in the keys:
# a: a as in alias
# f: reserved for MAIL FROMs
# t: reserverd for RCPT TOs
# rt: reserved for RCPT TOs (domain in rcpthosts)
# df: default ACL to consult when MAIL FROM key not found
# dt: default ACL to consult when RCPT TO key not found
# wt: reserved for wildcard RCPT TOs
# wf: reserved for wildcard MAIL FROMs (e.g. ``+wf.fi@friend:250 Finnish friends OK:'')
# o:  'override' for domain checks: checked as the very first rule
#     =ort@aestest.example.com:MT:M3:S10000:250 mmkay:
#     =ortpostmaster@aestest.example.com:=aset1:
#       aestest.example.com accepts only aestrap-recipients,
#       except postmaster@aestest.example.com is also accepted,
#       but not other local mailboxes
# u: reserved for user accounts (either from SMTP AUTH or RELAYCLIENT)
# h: HELO/EHLO hostname
#
=asettelintar:Ptelintar:XP:
=asetdsn:Q3:MA:MD:H8:X5:X1:Dlistlocal:L3:Dlistsafe:250 ABBS verified OK:
=asetaes:MS:Q3:MT:Dlistlocal:Dlistaspath:250 AESTRAP check passed:
=aset1:Q1:M3:M5:S131072:X5:X1:Dlistlocal:
=aset2:Q3:M3:X5:X1:X2:Dlistlocal:
=aset3:Q2:M1:M3:M5:M6:Dlistinfo:L3:M2:M7:M8:M9:Dlistdeflocal:L1:L2:H1:H2:H3:H4:H5:H7:H8:H9:Dlistdef:X5:X2:X3:X4:X1:X6:X7:
=aset3wl:M1:M2:M3:X5:X1:Dlistinfo:
=aset4:S131072:Q2:M1:M3:M5:M6:Dlistinfo:L3:M2:M7:M8:M9:Dlistdeflocal:L1:L2:H1:H2:H3:H4:H5:H7:H8:H9:Dlistdef:X5:X2:X3:X4:X1:X6:X7:
=asetdeny:553: 
=asetdenytemp:450:
=asetvirus:553 virus spreader:
=abad:553 bad recipient (#5.1.1):
=aok:250 ok:
#
=tpostmaster:=aset1:
=tpostmaster@:=aset1:
=tabuse:=aset1:
=tabuse@:=aset1:
#
=hhelo:450 are you really HELO?:
=fcheck@user.com:450 Mail\:\:CheckUser FAILED, see http\://www.amishrakefight.org/gfy/:
=fverify@testmail.com:450 Internal error 0xdeadbeef, please report this bug to gorlach@searchutilities.com:
=f:S131072:Q1:TGtext/plain:TGmultipart/report:TGmultipart/mixed:M2:M3:M6:X7:
=t:553 empty recipient not allowed (#5.1.3):
=tootbusyspamtrap@example.com:=bad:
+rtslrn:452 this message-id did not opt in:
=tusenetaddy_128kb_maxsize@example.com:=aset4:
=t@b.example.com:=asetdsn:
=t@u.example.com:=asetaes:
# allow only text/plain messages shorter than 16384 bytes from *.biz
+wf.biz@:TGtext/plain:S16384:
#
=df::
=dt:=aset3:
=dfwl::
=dtwl:=aset3wl:
=dfbogo::
=dtbogo:Q2:M1:M3:X5:X1:
=dfbogo2::
=dtbogo2:Q3:M1:M3:X5:X1:
=u:X1:Q4:M2:M3:M5:MITlistbadmxip:
.


#ifndef CDBMAKE_H
#define CDBMAKE_H

#include "helper.h"

#define CDBMAKE_HPLIST 1000

struct cdbmake_hp { uint32 h; uint32 p; } ;

struct cdbmake_hplist {
  struct cdbmake_hp hp[CDBMAKE_HPLIST];
  struct cdbmake_hplist *next;
  int num;
} ;

struct cdbmake {
  char final[2048];
  uint32 count[256];
  uint32 start[256];
  struct cdbmake_hplist *head;
  struct cdbmake_hp *split; /* includes space for hash */
  struct cdbmake_hp *hash;
  uint32 numentries;
} ;

extern void cdbmake_init(struct cdbmake*);
extern void cdbmake_pack(unsigned char*, uint32);
extern int cdbmake_add(struct cdbmake*, uint32, uint32, char *(*alloc)());
extern int cdbmake_split(struct cdbmake*, char *(*alloc)());
extern uint32 cdbmake_throw(struct cdbmake*, uint32, int);

#endif

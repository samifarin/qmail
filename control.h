#ifndef CONTROL_H
#define CONTROL_H

#include "stralloc.h"
#include "helper.h"

extern int control_init(void);
extern int control_readline(stralloc*, char*);
extern int control_rldef(stralloc*, char*, int, char*);
extern int control_readint(uint32*, char*);
extern int control_readint64(uint64*, char*);
extern int control_readfile(stralloc*, char*, int);

#endif

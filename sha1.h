#ifndef _SHA1_H_
#define _SHA1_H_

#include <stddef.h>

#include "helper.h"

#define SHA1BLOCKSIZE 64
#define SHA1DIGESTSIZE 20
#define SHA1DIGESTSIZERAW SHA1DIGESTSIZE
#define SHA1DIGESTSIZEHEX 40
#define SHA1DIGESTSIZEBASE32 32
#define SHA1DIGESTSIZEMAX SHA1DIGESTSIZEHEX

#define SHA1DIGESTRAW 0
#define SHA1DIGESTHEX 1
#define SHA1DIGESTBASE32 2
#define SHA1DIGESTNONE 3

typedef struct {
    uint64 length;
    uint32 state[5];
    unsigned long curlen;
    uint8 buf[64];
} sha1_hash_state;

typedef struct {
    sha1_hash_state md;
    uint8 key[SHA1BLOCKSIZE];
} sha1_hmac_state;

void sha1_hmac_init(sha1_hmac_state /*@out@*/*, const void*, uint64);
void sha1_hmac_process(sha1_hmac_state*, const void*, uint64);
void sha1_hmac_done(sha1_hmac_state*, void /*@out@*/*, size_t, int);
void sha1_hmac_memory(const void*, uint64, const void*, uint64, void*, size_t, int);

void sha1_init(sha1_hash_state /*@out@*/*);
void sha1_process(sha1_hash_state*, const void*, uint64);
void sha1_done(sha1_hash_state*, void /*@out@*/*, size_t, int);
void sha1_hash_memory(const void*, uint64, void*, size_t, int);

#endif


#include <unistd.h>
#include "scan.h"
#include "env.h"
#include "ucspitls.h"

int ucspitls(uint32 *fdptr)
{
  uint32 fd;
  char *fdstr;

  fdstr = env_get("SSLCTLFD");
  if (!fdstr) return 0;
  if (!scan_ulong(fdstr, &fd)) return 0;
  if (write(fd, "Y", 1) != 1) return 0;
  *fdptr = fd;

  fdstr = env_get("SSLREADFD");
  if (!fdstr) return 0;
  if (!scan_ulong(fdstr, &fd)) return 0;
  if (dup2(fd, 0) == -1) return 0;

  fdstr = env_get("SSLWRITEFD");
  if (!fdstr) return 0;
  if (!scan_ulong(fdstr, &fd)) return 0;
  if (dup2((int)fd,1) == -1) return 0;

  return 1;
}

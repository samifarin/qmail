#include <time.h>

int main(void) {
  struct timespec req;

  req.tv_sec = 0;
  req.tv_nsec = 0;
  nanosleep(&req, &req);
  return 0;
}


#ifndef _QMAIL_SQLITE3_H_
#define _QMAIL_SQLITE3_H_

#include <sqlite3.h>

#include "stralloc.h"
#include "fmt.h"
#include "helper.h"

#define ISO8601FMT (17)
/* number of milliseconds to retry on busy database */
#define SQLITE_BUSYWAIT (10000)

#define TYPE_UINT64 (0)
#define TYPE_TEXT   (1)

typedef struct {
  char greyid[ACL_GREY_IDLEN + 1];
  uint64 created;
  char s_created[ISO8601FMT];
  uint64 expires;
  char s_expires[ISO8601FMT];
  uint64 windowstart;
  char s_windowstart[ISO8601FMT];
  uint64 windowend;
  char s_windowend[ISO8601FMT];
  uint64 nrblocked;
  char s_nrblocked[FMT_ULONG];
  uint64 nrpassed;
  char s_nrpassed[FMT_ULONG];
  uint64 lastblocked;
  char s_lastblocked[ISO8601FMT];
  uint64 lastpassed;
  char s_lastpassed[ISO8601FMT];
  uint64 nridqueries;
  char s_nridqueries[FMT_ULONG];
  uint64 flags;
  uint64 to_initial;
  uint64 to_window;
  uint64 to_expire;
  uint64 perminitial;
  uint64 maxrecip;
  uint64 recipleft;
} greyinfo_t;

typedef struct {
  uint64 flags;
  uint64 initial;
  uint64 window;
  uint64 expire;
  uint64 perminitial;
  uint64 maxrecip;
} greyopts_t;

extern int __must_check greylist(char*, greyopts_t*, uint64*, const char**);
extern int __must_check greylist_getinfo(char*, greyinfo_t*, const char**);
extern int random_busy_handler(void*, int);

#ifndef SQLITE_VERSION_NUMBER
#  error SQLITE_VERSION_NUMBER not defined
#endif
#if (SQLITE_VERSION_NUMBER<3002006)
#  error only sqlite-3.2.6 and newer are supported
#endif

#endif


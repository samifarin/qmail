/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

#include <stdlib.h>
#include <stdint.h>

#include "dns.h"
#include "error.h"
#include "byte.h"
#include "alloc.h"
#include "env.h"
#include "helper.h"
#include "str.h"

static stralloc out = {0};
static struct ip_mx ix;

static int flagtxt1;
/* delimeter char between each RR - RFC1035 sections 3.3 and 3.3.14 */
static char *dns_txt_delim1;
/* delimeter char between labels in each RR */
static int flagtxt2;
static char *dns_txt_delim2;

int dns_cname(stralloc *sa) { 
  int loop;
  const char *x = env_get("DNSNOCNAME");

  for (loop = 0;loop < 10;++loop) {
    if (!sa->len) return loop;
    if (sa->s[sa->len - 1] == ']') return loop;
    if (sa->s[sa->len - 1] == '.') { --sa->len; continue; }
    
    if (x) return loop;
    if (dns_qmail_resolve(&out, sa, DNS_T_CNAME) == -1) {
      if (errno == error_nomem) return DNS_MEM;
      if (errno == error_proto) return loop;
      return DNS_SOFT;
    }
    
    if (!out.len) return loop;
    if (!stralloc_copy(sa,&out)) return DNS_MEM;
  }
  return DNS_HARD; /* alias loop */
}

static int dns_doe() {
  if (errno == error_proto) return DNS_HARD;
  if (errno == error_nomem) return DNS_MEM;
  return DNS_SOFT;
}

static int test_brackets(ipalloc *ia, stralloc *sa) {
  if (!stralloc_copy(&out, sa)) return DNS_MEM;
  if (!stralloc_0(&out)) return DNS_MEM;
  if (out.s[0])
    if (!out.s[ip_scan(out.s,&ix.ip)] || 
        !out.s[ip_scanbracket(out.s,&ix.ip)]) {
      ix.pref = 0;    
      if (!ipalloc_append(ia,&ix)) return DNS_MEM;
      return 0;
    }
  return 1;
}

static int compar_ix(const void* comp1, const void* comp2)
{
  struct ip_mx *ix1;
  struct ip_mx *ix2;

  ix1 = (struct ip_mx*)comp1;
  ix2 = (struct ip_mx*)comp2;
  if (ix1->pref == ix2->pref)
    return byte_diff(ix1->ip.d, 4, ix2->ip.d);
  else
    return (ix1->pref - ix2->pref);
}

static void permutation(struct ip_mx *ix, int nrips)
{
  uint32 rnd;
  int nrints = 0;
  struct ip_mx tmp;

  if (nrips <= 1) return;

  /* do random permutation */
  while (nrips > 1) {
    rnd = charcrandom_uniform32(nrips);
    nrips--;
    byte_copy(&tmp, sizeof(tmp), &ix[rnd]);
    byte_copy(&ix[rnd], sizeof(tmp), &ix[nrips]);
    byte_copy(&ix[nrips], sizeof(tmp), &tmp);
  }
}

void dns_shuffle(ipalloc *ia)
{
  int pref;
  int start;
  int curr = 0;

  if (ia->len <= 1) return;

  /* sort IPs first by preference, then by IP */
  qsort(&ia->ix[0], ia->len, sizeof(struct ip_mx), compar_ix);

  do {
    start = curr;
    pref = ia->ix[start].pref;
    while ((pref == ia->ix[curr].pref) && curr < ia->len)
      curr++;
    /* found more than one IP address with same pref */
    permutation(&ia->ix[start], curr - start);
  } while (curr < ia->len);
}

static int dns_ipplus(ipalloc *ia, stralloc *sa, int pref) {
  int j;

  switch (test_brackets(ia, sa)) {
    case DNS_MEM: return DNS_MEM;
    case 0: return 0;
  }

  if (dns_qmail_resolve(&out, sa, DNS_T_A) == -1) return dns_doe();
  if (out.len < 4) return DNS_HARD;

  for (ix.pref=pref, j=0; j+4 <= out.len; j += 4) {
    byte_copy(&ix.ip, 4, out.s + j);
    if (!ipalloc_append(ia,&ix)) return DNS_MEM;
  }
  return (0);
}

int dns_ip(ipalloc *ia, stralloc *sa) {
  if (!ipalloc_readyplus(ia,0)) return DNS_MEM;
  ia->len = 0;
  return (dns_ipplus(ia,sa,0));
}

int dns_mxip(ipalloc *ia, stralloc *sa, unsigned int __unused_dnsrnd) {
  size_t i, j, nummx;
  int flagsoft = 0;
  struct mx { stralloc sa; unsigned short p; } *mx;

  if (!ipalloc_readyplus(ia,0)) return DNS_MEM;
  switch (test_brackets(ia,sa)) {
    case DNS_MEM: return DNS_MEM;
    case 0: return 0;
  }

  if (dns_qmail_resolve(&out,sa,DNS_T_MX) == -1) {
    return dns_doe();
  } else if (!out.len) {
    return (dns_ip(ia,sa));
  }

  i=2;
  nummx=0;
  while (i < out.len) 
    if (out.s[i] == '\0') {
      nummx++;
      i += 3;
    } else {
      i++;
    }

  if (nummx > (SIZE_MAX / (sizeof(struct mx)))) return DNS_MEM;
  mx = (struct mx *) alloc(nummx * sizeof(struct mx));
  if (!mx) return DNS_MEM;

  i=0;
  j=0;
  while (j < nummx) {
    mx[j].p = 256*(unsigned char)out.s[i] + (unsigned char)out.s[i+1];
    mx[j].sa.s = 0;
    if (!stralloc_copys(&mx[j].sa, out.s+i+2)) {
      while (j > 0) alloc_free(mx[--j].sa.s);
      alloc_free(mx);
      return DNS_MEM;
    }
    i += 3 + mx[j++].sa.len;
  }

  while (nummx > 0)  {
    switch (dns_ipplus(ia, &mx[nummx-1].sa, mx[nummx-1].p)) {
      case DNS_MEM:
      case DNS_SOFT: 
        flagsoft = 1;
        break;
    }

    alloc_free(mx[--nummx].sa.s);
  }

  alloc_free(mx);

  return flagsoft;
}

int dns_nsip(ipalloc *ia, stralloc *sa) {
  int i;
  int flagsoft = 0;
  static stralloc nsdomain = {0};
  static stralloc nsdomain_tmp = {0};

  if (!ipalloc_readyplus(ia,0)) return DNS_MEM;
  if (dns_qmail_resolve(&out,sa,DNS_T_NS) == -1) {
    return dns_doe();
  } else if (!out.len) {
    return 0; 
  }

  i=0;
  if (!stralloc_copy(&nsdomain, &out)) return DNS_MEM;
  while (i < nsdomain.len) {
    if (!stralloc_copys(&nsdomain_tmp, nsdomain.s+i)) return DNS_MEM;
    switch (dns_ipplus(ia, &nsdomain_tmp, 0)) {
      case DNS_MEM:
      case DNS_SOFT:
        flagsoft = 1;
        break;
    }
    while (nsdomain.s[i] != '\0') i++;
    i++;
  }

  return flagsoft;
}

int dns_ns(stralloc *saout, stralloc *sa) {
  if (dns_qmail_resolve(&out,sa,DNS_T_NS) == -1) {
    return dns_doe();
  } else if (!out.len) {
    saout->len = 0;
    return 0; 
  }

  if (!stralloc_copy(saout, &out)) return DNS_MEM;
  return 0;
}

int dns_ptr(stralloc *sa, struct ip_address *ip) {
  if (dns_name4(sa, (char *)ip->d) == -1) {
    if (errno == error_nomem) return DNS_MEM;
    return DNS_SOFT;
  }
  if (!sa->len) return DNS_HARD;
  return 0;
}

int dns_txt_packet(stralloc *out,const char *buf,unsigned int len)
{
  unsigned int pos;
  char header[12];
  uint16 numanswers;
  uint16 datalen;
  unsigned char ch;
  unsigned int txtlen;
  int i;
  int flagfound = 0;

  if (!stralloc_copys(out,"")) return -1;

  pos = dns_packet_copy(buf,len,0,header,12); if (!pos) return -1;
  uint16_unpack_big(header + 6,&numanswers);
  pos = dns_packet_skipname(buf,len,pos); if (!pos) return -1;
  pos += 4;

  while (numanswers--) {
    pos = dns_packet_skipname(buf,len,pos); if (!pos) return -1;
    pos = dns_packet_copy(buf,len,pos,header,10); if (!pos) return -1;
    uint16_unpack_big(header + 8,&datalen);
    if (byte_equal(header,2,DNS_T_TXT))
      if (byte_equal(header + 2,2,DNS_C_IN)) {
        if (pos + datalen > len) return -1;
        txtlen = 0;
        for (i = 0;i < datalen;++i) {
          ch = buf[pos + i];
          if (!txtlen) {
            txtlen = ch;
            if (i && (flagtxt2 == 2)) {
              if (!stralloc_cats(out, dns_txt_delim2)) return -1;
            }
          } else {
            --txtlen;
            if (ch < 32) ch = '?';
            if (ch > 126) ch = '?';
            if (!stralloc_append(out,&ch)) return -1;
          }
        }
        if (flagtxt1 == 2)
          if (!stralloc_cats(out, dns_txt_delim1)) return -1;
        flagfound = 1;
      }
    pos += datalen;
  }
  if (flagfound && (flagtxt1 == 2)) out->len -= str_len(dns_txt_delim1);
  return flagfound ? 0 : DNS_HARD;
}

static char *q = 0;

int dns_txt(stralloc *out,const stralloc *fqdn)
{
  int ret;

  if (!dns_domain_fromdot(&q,fqdn->s,fqdn->len)) return -1;
  if (!flagtxt1) {
    flagtxt1 = 1;
    dns_txt_delim1 = env_get("DNS_TXT_DELIM1");
    if (dns_txt_delim1 && *dns_txt_delim1) flagtxt1 = 2;
  }    
  if (dns_resolve(q,DNS_T_TXT) == -1) return -1;
  if (!flagtxt2) {
    flagtxt2 = 1;
    dns_txt_delim2 = env_get("DNS_TXT_DELIM2");
    if (dns_txt_delim2 && *dns_txt_delim2) flagtxt2 = 2;
  }   
  ret = dns_txt_packet(out,dns_resolve_tx.packet,dns_resolve_tx.packetlen);
  if (ret == -1) return -1;
  dns_transmit_free(&dns_resolve_tx);
  dns_domain_free(&q);
  return (ret == DNS_HARD) ? DNS_HARD : 0;
}


#ifndef FMTQFN_H
#define FMTQFN_H

#include "helper.h"

extern unsigned int fmtqfn(char*, char*, uint64, int);

#define FMTQFN 60 /* maximum space needed, if len(dirslash) <= 10 */

#endif

#ifndef STRALLOC_H
#define STRALLOC_H

#include "helper.h"
#include "gen_alloc.h"
#include "alloc.h"

GEN_ALLOC_typedef(stralloc,char,s,len,a)

extern int __must_check stralloc_ready(stralloc*, unsigned long int);
extern int __must_check stralloc_readyplus(stralloc*, unsigned long int);
extern int __must_check stralloc_copy(stralloc*, stralloc*);
extern int __must_check stralloc_cat(stralloc*, stralloc*);
extern int __must_check stralloc_copys(stralloc*, char*);
extern int __must_check stralloc_cats(stralloc*, char*);
extern int __must_check stralloc_copyb(stralloc*, char*, unsigned long int);
extern int __must_check stralloc_catb(stralloc*, char*, unsigned long int);
extern int __must_check stralloc_append(stralloc*, char*); /* beware: this takes a pointer to 1 char */
extern int __must_check stralloc_starts(stralloc*, char*);

#define stralloc_0(sa) stralloc_append(sa,"")

#endif

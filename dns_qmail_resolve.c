/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

#include "stralloc.h"
#include "helper.h"
#include "byte.h"
#include "dns.h"

static char *q = 0;

static int dns_qmail_packet(stralloc *out,const char *buf,unsigned int len,char type[2])
{
  unsigned int pos;
  char header[12];
  char pref[2];
  uint16 numanswers;
  uint16 datalen;

  if (!stralloc_copys(out,"")) return -1;

  pos = dns_packet_copy(buf,len,0,header,12); if (!pos) return -1;
  uint16_unpack_big(header + 6,&numanswers);
  pos = dns_packet_skipname(buf,len,pos); if (!pos) return -1;
  pos += 4;

  while (numanswers--) {
    pos = dns_packet_skipname(buf,len,pos); if (!pos) return -1;
    pos = dns_packet_copy(buf,len,pos,header,10); if (!pos) return -1;
    uint16_unpack_big(header + 8,&datalen);
    if (byte_equal(header,2,type))
      if (byte_equal(header + 2,2,DNS_C_IN))
        switch (type[1]) {
        case '\1':  /* A */
          if (datalen == 4) {
            if (!dns_packet_copy(buf,len,pos,header,4)) return -1;
            if (!stralloc_catb(out,header,4)) return -1;
          }
          break;
        case '\5':  /* CNAME */
          if (!dns_packet_getname(buf,len,pos,&q)) return -1;
          if (!dns_domain_todot_cat(out,q)) return -1;
          return 0;
        case '\17': /* MX */
          if (!dns_packet_copy(buf,len,pos,pref,2)) return -1;
          if (!dns_packet_getname(buf,len,pos + 2,&q)) return -1;
          if (!stralloc_catb(out,pref,2)) return -1;
          if (!dns_domain_todot_cat(out,q)) return -1;
          if (!stralloc_0(out)) return -1;
          break;
        case '\2':  /* NS */
          if (!dns_packet_getname(buf,len,pos,&q)) return -1;
          if (!dns_domain_todot_cat(out,q)) return -1;
          if (!stralloc_0(out)) return -1;
          break;
        }
    pos += datalen;
  }
  return 0;
}

int dns_qmail_resolve(stralloc *out, const stralloc *fqdn, const char type[2])
{
  if (!dns_domain_fromdot(&q,fqdn->s,fqdn->len)) return -1;
  if (dns_resolve(q,type) == -1) return -1;
  if (dns_qmail_packet(out,dns_resolve_tx.packet,dns_resolve_tx.packetlen,type) == -1) return -1;
  dns_transmit_free(&dns_resolve_tx);
  dns_domain_free(&q);
  return 0;
}

#ifndef CONSTMAP_H
#define CONSTMAP_H

#include <stddef.h>
#include <unistd.h>
#include <stdint.h>
#include <limits.h>

#include "helper.h"

typedef size_t constmap_hash;

struct constmap {
  size_t num;
  constmap_hash mask;
  constmap_hash *hash;
  ssize_t *first;
  ssize_t *next;
  char **input;
  size_t *inputlen;
} ;

extern int __must_check constmap_init(struct constmap*, char*, size_t, int);
extern void constmap_free(struct constmap*);
extern char* __must_check constmap(struct constmap*, char*, size_t);

#endif

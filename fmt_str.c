#include "fmt.h"

unsigned long int fmt_str(s,t)
char *s; char *t;
{
  unsigned long int len;
  char ch;
  len = 0;
  if (s) { while ((ch = t[len])) s[len++] = ch; }
  else while (t[len]) len++;
  return len;
}

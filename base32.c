/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

/* RFC3548 */

#include "base32.h"

const unsigned char base32[] = "abcdefghijklmnopqrstuvwxyz234567";

const unsigned char base32tbl[] = {
    ['a'] = 0,  ['b'] = 1,  ['c'] = 2,  ['d'] =  3, ['e'] =  4, ['f'] = 5,
    ['g'] = 6,  ['h'] = 7,  ['i'] = 8,  ['j'] =  9, ['k'] = 10, ['l'] = 11,
    ['m'] = 12, ['n'] = 13, ['o'] = 14, ['p'] = 15, ['q'] = 16, ['r'] = 17,
    ['s'] = 18, ['t'] = 19, ['u'] = 20, ['v'] = 21, ['w'] = 22, ['x'] = 23,
    ['y'] = 24, ['z'] = 25, ['2'] = 26, ['3'] = 27, ['4'] = 28, ['5'] = 29,
    ['6'] = 30, ['7'] = 31
};

const unsigned char validbase32[] = {
    ['a'] = 1, ['b'] = 1, ['c'] = 1, ['d'] = 1, ['e'] = 1, ['f'] = 1,
    ['g'] = 1, ['h'] = 1, ['i'] = 1, ['j'] = 1, ['k'] = 1, ['l'] = 1,
    ['m'] = 1, ['n'] = 1, ['o'] = 1, ['p'] = 1, ['q'] = 1, ['r'] = 1,
    ['s'] = 1, ['t'] = 1, ['u'] = 1, ['v'] = 1, ['w'] = 1, ['x'] = 1,
    ['y'] = 1, ['z'] = 1, ['2'] = 1, ['3'] = 1, ['4'] = 1, ['5'] = 1,
    ['6'] = 1, ['7'] = 1
};

void base32encode(void* __in, void* __out, unsigned long int len)
{
  unsigned char ch1, ch2;
  int bits;
  unsigned char *in = __in;
  unsigned char *out = __out;

  for (ch2 = 0, bits = 5; len; len--) {
    ch1 = *in++;
    if (bits > 3) {
      ch2 |= ch1 >> (8 - bits);
      *out++ = base32[ch2];
      bits -= 3;
      ch2 = (ch1 << bits) & 31;
    } else {
      ch2 |= ch1 >> (8 - bits);
      *out++ = base32[ch2];
      ch2 = (ch1 >> (3 - bits)) & 31;
      *out++ = base32[ch2];
      bits += 2;
      ch2 = (ch1 << bits) & 31;
    }
  }
}

void base32decode(void* __in, void* __out, unsigned long int len)
{
  unsigned char ch1, ch2, chtmp;
  int bits;
  unsigned char *in = __in;
  unsigned char *out = __out;

  for (ch2 = 0, bits = 8; len; len--) {
    chtmp = *in++;
    ch1 = base32tbl[chtmp];
    /* invalid base32 char results into ch1 == 0 */
    if (bits > 5) {
      bits -= 5;
      ch2 |= ch1 << bits;
    } else {
      ch2 |= ch1 >> (5 - bits);
      *out++ = ch2;
      bits += 3;
      ch2 = (ch1 << bits);
    }
  }
}


#include "scan.h"

unsigned int scan_ulong(s,u) const char *s; uint32 *u;
{
  unsigned int pos;
  uint32 result;
  uint32 c;

  pos = 0; result = 0;
  while ((c = (uint32) (unsigned char) (s[pos] - '0')) < 10)
    { result = result * 10 + c; ++pos; }
  *u = result; return pos;
}

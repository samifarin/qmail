/*
vim:tw=76:ts=2:sts=2:sw=2:cindent:expandtab
*/

/* show TCP info after socket shutdown(), works with Linux 2.4 and Linux 2.6 */
/* consumes extra time while waiting for shutdown() to complete */
/* also needed in BANNERDELAY feature */
#define USE_TCP_INFO 1

/* this is needed to detect SMTP clients violating PIPELINING
   at HELO or DATA phase.  Change 1 to 0 if code does not compile
   and submit me a fix for your OS.  With this feature disabled
   and USE_TCP_INFO enabled qmail-smtpd is still able to exit inside
   one second after SMTP client has closed connection, but it doesn't
   detect malware sending data before permitted.
   With USE_TCP_INFO and USE_LINUX_TCP_IOCTL disabled you have to wait
   till the BANNERDELAY elapses.
 */
#define USE_LINUX_TCP_IOCTL 1

#include <sys/types.h>
#include <locale.h>
#include <netinet/in.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/wait.h>

#if USE_LINUX_TCP_IOCTL
#include <linux/sockios.h>
#endif

#include "crypto_verify.h"
#include "sig.h"
#include "readwrite.h"
#include "stralloc.h"
#include "substdio.h"
#include "alloc.h"
#include "auto_qmail.h"
#include "control.h"
#include "received.h"
#include "constmap.h"
#include "error.h"
#include "ipme.h"
#include "ip.h"
#include "qmail.h"
#include "str.h"
#include "fmt.h"
#include "scan.h"
#include "byte.h"
#include "case.h"
#include "env.h"
#include "now.h"
#include "exit.h"
#include "rcpthosts.h"
#include "rbl.h"
#include "timeoutread.h"
#include "timeoutwrite.h"
#include "commands.h"
#include "strerr.h"
#include "wait.h"
#include "fd.h"
#include "dns.h"
#include "base64.h"
#include "sha1.h"
#include "rdtscl.h"
#include "open.h"
#include "slurpclose.h"
#include "helper.h"
#include "qmail-acl.h"
#include "aestrap.h"
#include "aes.h"
#include "base32.h"
#include "getln.h"
#include "subfd.h"
#include "qmail-sqlite3.h"
#include "list.h"
#include "datetime.h"
#include "cdb.h"

#define PCRE_PUT_DEBUG 0
#ifdef PCRE_SMTPD
#include <pcre.h>
#define OVECSZ (3*33)
static int ovec[OVECSZ];
#endif

#if USE_TCP_INFO
#include <netinet/tcp.h>
#endif
static char* gen_tcpinfo(void);
static int tcpinfook = 0;

#ifdef TLS_SMTPD

#include "ucspitls.h"

static int tlsok = 0;
static stralloc cert_status_str = {0};
static int cert_status = ACL_CERT_NOSTARTTLS;
static int flagtlsinit;
static stralloc cert_email = {0};
#endif

#define SHOWSHA1
#define VERBOSEQMAILID

#define MAXMIMENESTING 50
#define MAXHOPS 100
#define MAXLOGLEN 900

static uint64 databytes = 0;
static char databytes_s[FMT_ULONG];
static uint64 acl_maxsz = 0;
static char acl_maxsz_s[FMT_ULONG];
static uint64 bytesread = 0;
static char bytesread_s[FMT_ULONG];
static uint64 smtpsize; /* >0 if connecting client did ``MAIL FROM:<box@host> SIZE=123''
                           and parameter was valid */
static char smtpsize_s[FMT_ULONG];
static uint32 mfchk = 0;
static uint32 bingocount = 3;
static uint32 maxrcpt = 100;
static unsigned char bogowanted = '?';
static char *origqq = 0;               /* original QMAILQUEUE env var, which                                                             is reset to original at each smtp_mail() */
static uint32 timeout = 1200;
static uint32 abbslifetime = 1296000; /* 15 days */
struct timespec tspec;
struct timespec tspec_sid;
static int flagstray;
static int foundstray;
static int flagnullconn = 3; /* 0: sent 4xx/5xx error to RCPT TO
                                   or done DATA after a successful RCPT TO */
static const uint8 hextbl[] = "0123456789abcdef";

static stralloc qmailid = {0};
static stralloc smtpauthid = {0};
static stralloc aestrap_info = {0};
static stralloc aestrap_info_curr = {0};

static int safewrite(fd,buf,len) int fd; char *buf; int len;
{
  int r;

  r = timeoutwrite(timeout,fd,buf,len);
  if (r <= 0) {
    strerr_warn4("qmail-smtpd: id ", qmailid.s,
                 flagnullconn ? " EXIT_SAFEWRITE_NULLCONN " : " EXIT_SAFEWRITE ",
                 gen_tcpinfo(), 0);
    _exit(1);
  }
  return r;
}

static stralloc str_charset = {0};
static stralloc str_sha1 = {0};
static stralloc str_ct = {0};
static stralloc str_tcpi = {0};
static stralloc lineout = {0};
static stralloc helohost = {0};
static stralloc helohost_rcv = {0};
static char *fakehelo; /* pointer into helohost, or 0 */
static int flagbadhelo = ACL_HELO_NOHELO;
static stralloc addr = {0}; /* will be 0-terminated, if addrparse returns 1 */
static int decision = ACL_RBLSMTPD_OK; /* 0=undecided, 1=whitelisted, 2=temporary reject, 3=permanent reject */
static char *rbltext;
static stralloc envacl = {0};
/* append this env var to cdb lookups in acl_get for default mailboxes
   (df, dt keys) */
char *acldfl;
static char *remoteip;
static char *remoteport;

static int seenmail = 0;
static int flagbarf = ACL_MF_OK; /* defined if seenmail */
static int flagcontent = ACL_CONTENT_NOMSGID;

#ifdef PCRE_SMTPD
struct acl_pcre_state_t {
  pcre *pcre_pattern;
  pcre_extra *pcre_hints;
  stralloc matchtext;
  unsigned long matchon;
  struct list_head list;
  struct {
    unsigned int dfl_match:1;
  } flags;
};

struct list_head pcre_active;
static stralloc pcre_active_h;
static stralloc pcre_active_m;
static stralloc pcre_active_r;
static stralloc pcre_active_H;
static stralloc pcre_active_M;
static stralloc pcre_active_R;

struct list_head pcre_inactive;

static uint32 flagpcre = 0; /* 1=enable PCRE matching
                              control/pcre/enable or $PCREENABLE */
static uint32 pcredebug = 0;
static int acl_pcre_status = 0; /* 1=we are going to do some PCRE checks */
static stralloc pcre_line_match = {0};
static stralloc pcre_reject_dfl = {0};
static const unsigned char *pcre_tables = NULL;

static stralloc str_badsigs = {0};

static stralloc badhosts = {0};
static int badhostsok = 0;
static stralloc badhosts_last = {0};
static pcre *badhosts_pcre_pattern;
static pcre_extra *badhosts_pcre_hints;
#endif

static stralloc pcre_line = {0};
static stralloc mailfrom = {0};
static stralloc rcptto = {0};
static stralloc safemailfrom = {0};
static stralloc safercptto = {0};
static int recipcount;
static stralloc acl_helo = {0};
static stralloc acl_from = {0};
static stralloc acl_to = {0};
static stralloc acl_checks = {0};
static stralloc acl_user = {0}; /* SMTP AUTH username or RELAYCLIENT contents */
static int flagaestrap;

static stralloc sa_mf = {0};
static ipalloc ia_mf = {0};

static uint32 sd_debug = 0;
static uint32 sd_www = 0;
static int acl_sd_status = 0;
static uint32 sd_giveuptime = 120;
static uint32 sd_maxdomains = 50;
static stralloc spamdomain = {0};
static stralloc spamdomain_comment;
static stralloc spamdomain_ns = {0};
#define DOMCACHE_HSIZE (256)
#define DOMCACHE_HMASK (DOMCACHE_HSIZE-1)
static stralloc domcache_htable[DOMCACHE_HSIZE];
static int domcache_count = 0;
static char *spamdomainip;
static int sd_tld_ok = 0;
static stralloc sd_tld = {0};
static struct constmap map_sd_tld;
static int sd_2ld_ok = 0;
static stralloc sd_2ld = {0};
static struct constmap map_sd_2ld;
struct acl_sd_state_t {
  stralloc nameserver;
  stralloc comment;
  unsigned long flags;
  struct list_head list;
};
static struct list_head sd_active;
static struct list_head sd_inactive;

static int acl_hdrip_status = 0;
static int acl_hdrip_ok = 0;
static stralloc hdrip = {0};
static struct constmap map_hdrip;
static stralloc hdrip_servers = {0};
static stralloc hdrip_err = {0};

static greyopts_t greyopts = {
  .initial = 1805,      /* 30 minutes + 5s */
  .window = 14400,      /* 4 hours */
  .expire = 259200,     /* 3 days */
  .perminitial = 0,
  .maxrecip = 100,
  .flags = (ACL_GREY_FL_KEY_IP | ACL_GREY_FL_KEY_SIZE | ACL_GREY_FL_KEY_HELO |
            ACL_GREY_FL_KEY_FROM | ACL_GREY_FL_KEY_TO),
};
static stralloc greylistmsg = {0};
static greyinfo_t greyinfo;
static char greyid[ACL_GREY_IDLEN + 1];
static int flaggreyid;
static int flaggreyid_ok;
static uint64 greydelayed_total;
static uint64 greydelayed_min;
static uint64 greydelayed_max;
static stralloc str_grey = {0};
static stralloc str_greydelays = {0};

static struct qmail qqt;

static char ssoutbuf[512];
static substdio ssout = SUBSTDIO_FDBUF(safewrite, 1, ssoutbuf, sizeof(ssoutbuf));

static void flush() { substdio_flush(&ssout); }
static void out(s) char *s; { substdio_puts(&ssout,s); }

/* RFC3463 */
static void die_read() {
  strerr_warn4("qmail-smtpd: id ", qmailid.s,
               flagnullconn ? " EXIT_READ_ERROR_NULLCONN " : " EXIT_READ_ERROR ",
               gen_tcpinfo(), 0);
  _exit(1);
}

static void die_alarm() {
  out("451 timeout (#4.4.2)\r\n"); flush();
  strerr_warn4("qmail-smtpd: id ", qmailid.s,
               flagnullconn ? " EXIT_TIMEOUT_NULLCONN " : " EXIT_TIMEOUT ",
               gen_tcpinfo(), 0);
  _exit(1);
}

static int saferead(fd,buf,len) int fd; char *buf; int len;
{
  int r;

  flush();
  r = timeoutread(timeout,fd,buf,len);
  if (r == -1) if (errno == error_timeout) die_alarm();
  if (r <= 0) die_read();
  return r;
}
static char ssinbuf[1024];
static substdio ssin = SUBSTDIO_FDBUF(saferead, 0, ssinbuf, sizeof(ssinbuf));
static uint32 ssl_rfd = 0;
static uint32 ssl_wfd = 1;

void die_nomem() {
  out("421 out of memory (#4.3.0)\r\n"); flush();
  strerr_warn4("qmail-smtpd: id ", qmailid.s,
               flagnullconn ? " EXIT_OOM_NULLCONN " : " EXIT_OOM ",
               gen_tcpinfo(), 0);
  _exit(1); }

static void my_nanosleep(time_t secs, long nsecs)
{
  struct timespec req;
  int ret;

  req.tv_sec = secs;
  req.tv_nsec = nsecs;
  do {
    ret = nanosleep(&req, &req);
  } while ((ret == -1) && (errno == error_intr));
}

static int is_socket(int s)
{
  int val;
  socklen_t vallen;

  vallen = sizeof(val);
  return (getsockopt(s, SOL_SOCKET, SO_TYPE, &val, &vallen) == 0);
}

static int is_connected(int s)
{
#if USE_TCP_INFO
  struct tcp_info info;
  socklen_t optlen;
  int ret;

  if (!tcpinfook) return 1;
  optlen = sizeof(info);
  ret = getsockopt(s, SOL_TCP, TCP_INFO, (void*)&info, &optlen);
  if (ret == -1) return 1; /* XXX */
  return (info.tcpi_state == TCP_ESTABLISHED);
#else
  /* getpeername does not work */
  return 1;
#endif
}

static int socket_inqueue(int s)
{
  int val;

  if (ioctl(s, FIONREAD, &val) == 0) return val;
  return 0;
}

#if USE_LINUX_TCP_IOCTL
static int socket_outqueue(int s)
{
  int val;

  if (ioctl(s, SIOCOUTQ, &val) == 0) return val;
  return 0;
}
#else
static int socket_outqueue(int s)
{
    return 0;
}
#endif

static void append_cputime(stralloc *strtmp)
{
  struct timespec tsp;
  char slong1[FMT_ULONG];
  char slong2[FMT_ULONG];

  if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tsp) == 0) {
    slong1[fmt_ulong(slong1, tsp.tv_sec)] = 0;
    slong2[fmt_ulong0(slong2, tsp.tv_nsec, 9)] = 0;
    if (!stralloc_cats(strtmp, "CPUTIME=")) die_nomem();
    if (!stralloc_cats(strtmp, slong1)) die_nomem();
    if (!stralloc_cats(strtmp, ".")) die_nomem();
    if (!stralloc_cats(strtmp, slong2)) die_nomem();
    if (!stralloc_cats(strtmp, "s ")) die_nomem();
  }
}

#if USE_TCP_INFO

/*
  it will look like this in the logs:
     2004-03-16 00:19:05.017349500 qmail-smtpd: id 22774/1079389143.819553/82.141.70.99/131.193.178.160:4911 EXIT_SHUTDOWN
     2004-03-16 00:19:05.173694500 qmail-smtpd: id 22774/1079389143.819553/82.141.70.99/131.193.178.160:4911 EXIT_QUIT TCP_INFO STATE=TCP_CLOSE CA_STATE=TCP_CA_Open OPT=(TIMESTAMPS WSCALE(snd=1,rcv=0)) RTO=385000 ATO=40000 RTT=157125 RTTVAR=16250 RCV_SSTHRESH=9702 SND_SSTHRESH=2147483647 SND_CWND=2 ADVMSS=1400 SND_MSS=1400 RCV_MSS=1027
*/

static char* gen_tcpinfo(void)
{
  int ret, i;
  struct tcp_info info;
  socklen_t optlen;
  char slong[FMT_ULONG];
  static char* states[] = {
    "unknown", "TCP_ESTABLISHED", "TCP_SYN_SENT", "TCP_SYN_RECV",
    "TCP_FIN_WAIT1", "TCP_FIN_WAIT2", "TCP_TIME_WAIT", "TCP_CLOSE",
    "TCP_CLOSE_WAIT", "TCP_LAST_ACK", "TCP_LISTEN", "TCP_CLOSING"
  };
  static char* ca_states[]= {
    "TCP_CA_Open", "TCP_CA_Disorder", "TCP_CA_CWR", "TCP_CA_Recovery", "TCP_CA_Loss"
  };
  static stralloc tmpstr = {0};
  struct sockaddr_in sa;
  int dummy;
  int socket_errno = 0;

  if (!stralloc_copys(&tmpstr, "")) die_nomem();
  append_cputime(&tmpstr);
  if (!tcpinfook || flagtlsinit /* work around broken Postfix */ ) {
   if (!stralloc_0(&tmpstr)) die_nomem();
   return tmpstr.s;
  }

  strerr_warn3("qmail-smtpd: id ", qmailid.s, " EXIT_SHUTDOWN", 0);
  /* net/ipv4/tcp_inpuc.c:tcp_update_metrics() called only when TCP enters
     TIME_WAIT or goes from LAST_ACK to CLOSE */
  dummy = sizeof(sa);
  if (getpeername(ssl_rfd, (struct sockaddr *) &sa, &dummy) == -1) {
    socket_errno = errno;
    if (read(ssl_rfd, &dummy, 1) == -1)
      socket_errno = errno;
  }
  shutdown(ssl_rfd, 2);
  /* loop at max ~2.5s while waiting for TCP_CLOSE state */
  for (i = 0; i < 250; i++) {
    optlen = sizeof(info);
    ret = getsockopt(ssl_rfd, SOL_TCP, TCP_INFO, (void*)&info, &optlen);
    if (ret == -1) { if (!stralloc_0(&tmpstr)) die_nomem(); return tmpstr.s; }
    if (info.tcpi_state == TCP_CLOSE)
      break;
    my_nanosleep(0, 10000000);
  }
  optlen = sizeof(info);
  ret = getsockopt(ssl_rfd, SOL_TCP, TCP_INFO, (void*)&info, &optlen);
  if (ret == -1) { if (!stralloc_0(&tmpstr)) die_nomem(); return tmpstr.s; }

  if (!stralloc_cats(&tmpstr, "TCP_INFO ")) die_nomem();
  if (socket_errno) {
    if (!stralloc_cats(&tmpstr, "ERROR=\"")) die_nomem();
    if (!stralloc_cats(&tmpstr, error_str(socket_errno))) die_nomem();
    if (!stralloc_cats(&tmpstr, "\" ")) die_nomem();
  }
  if (!stralloc_cats(&tmpstr, "STATE=")) die_nomem();
  if (!stralloc_cats(&tmpstr, (info.tcpi_state < sizeof(states)/sizeof(states[0]))
                     ? states[info.tcpi_state] : states[0])) die_nomem();
  if (!stralloc_cats(&tmpstr, " CA_STATE=")) die_nomem();
  if (!stralloc_cats(&tmpstr, (info.tcpi_ca_state < sizeof(ca_states)/sizeof(ca_states[0]))
                     ? ca_states[info.tcpi_ca_state] : "unknown")) die_nomem();
  if (!stralloc_cats(&tmpstr, " OPT=(")) die_nomem();
  if (info.tcpi_options & TCPI_OPT_TIMESTAMPS)
    if (!stralloc_cats(&tmpstr, "TIMESTAMPS")) die_nomem();
  if (info.tcpi_options & TCPI_OPT_SACK)
    if (!stralloc_cats(&tmpstr, " SACK")) die_nomem();
  if (info.tcpi_options & TCPI_OPT_ECN)
    if (!stralloc_cats(&tmpstr, " ECN")) die_nomem();
  if (info.tcpi_options & TCPI_OPT_WSCALE) {
    if (!stralloc_cats(&tmpstr, " WSCALE(snd=")) die_nomem();
    slong[fmt_ulong(slong, (info.tcpi_snd_wscale & 15))] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
    if (!stralloc_cats(&tmpstr, ",rcv=")) die_nomem();
    slong[fmt_ulong(slong, (info.tcpi_rcv_wscale & 15))] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
    if (!stralloc_cats(&tmpstr, ")")) die_nomem();
  }
  if (!stralloc_cats(&tmpstr, ") ")) die_nomem();

  if (!stralloc_cats(&tmpstr, "RTO=")) die_nomem();
  slong[fmt_ulong(slong, info.tcpi_rto)] = 0;
  if (!stralloc_cats(&tmpstr, slong)) die_nomem();

  /* ATO is 0 if no data was received.
     However, ATO!=0 could also mean no data was received. */
  if (!stralloc_cats(&tmpstr, " ATO=")) die_nomem();
  slong[fmt_ulong(slong, info.tcpi_ato)] = 0;
  if (!stralloc_cats(&tmpstr, slong)) die_nomem();

  if (!stralloc_cats(&tmpstr, " RTT=")) die_nomem();
  slong[fmt_ulong(slong, info.tcpi_rtt)] = 0;
  if (!stralloc_cats(&tmpstr, slong)) die_nomem();

  if (!stralloc_cats(&tmpstr, " RTTVAR=")) die_nomem();
  slong[fmt_ulong(slong, info.tcpi_rttvar)] = 0;
  if (!stralloc_cats(&tmpstr, slong)) die_nomem();

  if (!stralloc_cats(&tmpstr, " RCV_SSTHRESH=")) die_nomem();
  slong[fmt_ulong(slong, info.tcpi_rcv_ssthresh)] = 0;
  if (!stralloc_cats(&tmpstr, slong)) die_nomem();

  if (!stralloc_cats(&tmpstr, " SND_SSTHRESH=")) die_nomem();
  slong[fmt_ulong(slong, info.tcpi_snd_ssthresh)] = 0;
  if (!stralloc_cats(&tmpstr, slong)) die_nomem();

  if (!stralloc_cats(&tmpstr, " SND_CWND=")) die_nomem();
  slong[fmt_ulong(slong, info.tcpi_snd_cwnd)] = 0;
  if (!stralloc_cats(&tmpstr, slong)) die_nomem();

  if (!stralloc_cats(&tmpstr, " PMTU=")) die_nomem();
  slong[fmt_ulong(slong, info.tcpi_pmtu)] = 0;
  if (!stralloc_cats(&tmpstr, slong)) die_nomem();

  if (!stralloc_cats(&tmpstr, " ADVMSS=")) die_nomem();
  slong[fmt_ulong(slong, info.tcpi_advmss)] = 0;
  if (!stralloc_cats(&tmpstr, slong)) die_nomem();

  if (!stralloc_cats(&tmpstr, " SND_MSS=")) die_nomem();
  slong[fmt_ulong(slong, info.tcpi_snd_mss)] = 0;
  if (!stralloc_cats(&tmpstr, slong)) die_nomem();

  if (!stralloc_cats(&tmpstr, " RCV_MSS=")) die_nomem();
  slong[fmt_ulong(slong, info.tcpi_rcv_mss)] = 0;
  if (!stralloc_cats(&tmpstr, slong)) die_nomem();

  /* this is number of timeout-based retransmit in the network,
     so after connection gets healthy again, it's reset to zero. */
  if (info.tcpi_retransmits) {
    if (!stralloc_cats(&tmpstr, " TIMEOUT_RETRANS=")) die_nomem();
    slong[fmt_ulong(slong, info.tcpi_retransmits)] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
  }
  if (info.tcpi_probes) {
    if (!stralloc_cats(&tmpstr, " PROBES=")) die_nomem();
    slong[fmt_ulong(slong, info.tcpi_probes)] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
  }
  if (info.tcpi_backoff) {
    if (!stralloc_cats(&tmpstr, " BACKOFF=")) die_nomem();
    slong[fmt_ulong(slong, info.tcpi_backoff)] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
  }
  if (info.tcpi_unacked) {
    if (!stralloc_cats(&tmpstr, " UNACKED=")) die_nomem();
    slong[fmt_ulong(slong, info.tcpi_unacked)] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
  }
  if (info.tcpi_sacked) {
    if (!stralloc_cats(&tmpstr, " SACKED=")) die_nomem();
    slong[fmt_ulong(slong, info.tcpi_sacked)] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
  }
  if (info.tcpi_lost) {
    if (!stralloc_cats(&tmpstr, " LOST=")) die_nomem();
    slong[fmt_ulong(slong, info.tcpi_lost)] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
  }
#if 1
  if (info.tcpi_reordering) {
    if (!stralloc_cats(&tmpstr, " REORDERING=")) die_nomem();
    slong[fmt_ulong(slong, info.tcpi_reordering)] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
  }
#endif
  /* these are in >=2.6.10-rc1 kernels */
  if (info.tcpi_rcv_rtt) {
    if (!stralloc_cats(&tmpstr, " RCV_RTT=")) die_nomem();
    slong[fmt_ulong(slong, info.tcpi_rcv_rtt)] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
  }
  if (info.tcpi_rcv_space) {
    if (!stralloc_cats(&tmpstr, " RCV_SPACE=")) die_nomem();
    slong[fmt_ulong(slong, info.tcpi_rcv_space)] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
  }
  if (info.tcpi_total_retrans) {
    if (!stralloc_cats(&tmpstr, " TOTAL_RETRANS=")) die_nomem();
    slong[fmt_ulong(slong, info.tcpi_total_retrans)] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
  }
  if (info.tcpi_retrans) {
    if (!stralloc_cats(&tmpstr, " RETRANS=")) die_nomem();
    slong[fmt_ulong(slong, info.tcpi_retrans)] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
  }
  if (info.tcpi_fackets) {
    if (!stralloc_cats(&tmpstr, " FACKETS=")) die_nomem();
    slong[fmt_ulong(slong, info.tcpi_fackets)] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
  }
  if (info.tcpi_last_data_sent) {
    if (!stralloc_cats(&tmpstr, " LAST_DATA_SENT=")) die_nomem();
    slong[fmt_ulong(slong, info.tcpi_last_data_sent)] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
  }
  /* if ATO=0, tcpi_last_data_recv has undefined value */
  if (info.tcpi_ato) {
    if (info.tcpi_last_data_recv) {
      if (!stralloc_cats(&tmpstr, " LAST_DATA_RECV=")) die_nomem();
      slong[fmt_ulong(slong, info.tcpi_last_data_recv)] = 0;
      if (!stralloc_cats(&tmpstr, slong)) die_nomem();
    }
  }
  if (info.tcpi_last_ack_recv) {
    if (!stralloc_cats(&tmpstr, " LAST_ACK_RECV=")) die_nomem();
    slong[fmt_ulong(slong, info.tcpi_last_ack_recv)] = 0;
    if (!stralloc_cats(&tmpstr, slong)) die_nomem();
  }

  if (!stralloc_0(&tmpstr)) die_nomem();
  return tmpstr.s;
}
#else
static char* gen_tcpinfo(void)
{
  static stralloc tmpstr;

  if (!stralloc_copys(&tmpstr, "")) die_nomem();
  append_cputime(&tmpstr);
  if (!stralloc_0(&tmpstr)) die_nomem();
  return tmpstr.s;
}
#endif

static inline int issafe(unsigned char ch, unsigned int relaxed)
{
  static const unsigned char relax_tbl[3][256] = {
    { ['.'] = 1, ['0'] = 1, ['1'] = 1, ['2'] = 1, ['3'] = 1, ['4'] = 1,
      ['5'] = 1, ['6'] = 1, ['7'] = 1, ['8'] = 1, ['9'] = 1, ['a'] = 1,
      ['b'] = 1, ['c'] = 1, ['d'] = 1, ['e'] = 1, ['f'] = 1, ['g'] = 1,
      ['h'] = 1, ['i'] = 1, ['j'] = 1, ['k'] = 1, ['l'] = 1, ['m'] = 1,
      ['n'] = 1, ['o'] = 1, ['p'] = 1, ['q'] = 1, ['r'] = 1, ['s'] = 1,
      ['t'] = 1, ['u'] = 1, ['v'] = 1, ['w'] = 1, ['x'] = 1, ['y'] = 1,
      ['z'] = 1, ['A'] = 1, ['B'] = 1, ['C'] = 1, ['D'] = 1, ['E'] = 1,
      ['F'] = 1, ['G'] = 1, ['H'] = 1, ['I'] = 1, ['J'] = 1, ['K'] = 1,
      ['L'] = 1, ['M'] = 1, ['N'] = 1, ['O'] = 1, ['P'] = 1, ['Q'] = 1,
      ['R'] = 1, ['S'] = 1, ['T'] = 1, ['U'] = 1, ['V'] = 1, ['W'] = 1,
      ['X'] = 1, ['Y'] = 1, ['Z'] = 1, ['-'] = 1, ['@'] = 1, ['%'] = 1,
      ['+'] = 1, ['/'] = 1, ['='] = 1, ['_'] = 1 },
    { ['.'] = 1, ['0'] = 1, ['1'] = 1, ['2'] = 1, ['3'] = 1, ['4'] = 1,
      ['5'] = 1, ['6'] = 1, ['7'] = 1, ['8'] = 1, ['9'] = 1, ['a'] = 1,
      ['b'] = 1, ['c'] = 1, ['d'] = 1, ['e'] = 1, ['f'] = 1, ['g'] = 1,
      ['h'] = 1, ['i'] = 1, ['j'] = 1, ['k'] = 1, ['l'] = 1, ['m'] = 1,
      ['n'] = 1, ['o'] = 1, ['p'] = 1, ['q'] = 1, ['r'] = 1, ['s'] = 1,
      ['t'] = 1, ['u'] = 1, ['v'] = 1, ['w'] = 1, ['x'] = 1, ['y'] = 1,
      ['z'] = 1, ['A'] = 1, ['B'] = 1, ['C'] = 1, ['D'] = 1, ['E'] = 1,
      ['F'] = 1, ['G'] = 1, ['H'] = 1, ['I'] = 1, ['J'] = 1, ['K'] = 1,
      ['L'] = 1, ['M'] = 1, ['N'] = 1, ['O'] = 1, ['P'] = 1, ['Q'] = 1,
      ['R'] = 1, ['S'] = 1, ['T'] = 1, ['U'] = 1, ['V'] = 1, ['W'] = 1,
      ['X'] = 1, ['Y'] = 1, ['Z'] = 1, ['-'] = 1, ['@'] = 1, ['%'] = 1,
      ['+'] = 1, ['/'] = 1, ['='] = 1, ['_'] = 1, [' '] = 1, ['['] = 1,
      [']'] = 1, [':'] = 1, ['('] = 1, [')'] = 1 },
    { ['.'] = 1, ['0'] = 1, ['1'] = 1, ['2'] = 1, ['3'] = 1, ['4'] = 1,
      ['5'] = 1, ['6'] = 1, ['7'] = 1, ['8'] = 1, ['9'] = 1, ['a'] = 1,
      ['b'] = 1, ['c'] = 1, ['d'] = 1, ['e'] = 1, ['f'] = 1, ['g'] = 1,
      ['h'] = 1, ['i'] = 1, ['j'] = 1, ['k'] = 1, ['l'] = 1, ['m'] = 1,
      ['n'] = 1, ['o'] = 1, ['p'] = 1, ['q'] = 1, ['r'] = 1, ['s'] = 1,
      ['t'] = 1, ['u'] = 1, ['v'] = 1, ['w'] = 1, ['x'] = 1, ['y'] = 1,
      ['z'] = 1, ['A'] = 1, ['B'] = 1, ['C'] = 1, ['D'] = 1, ['E'] = 1,
      ['F'] = 1, ['G'] = 1, ['H'] = 1, ['I'] = 1, ['J'] = 1, ['K'] = 1,
      ['L'] = 1, ['M'] = 1, ['N'] = 1, ['O'] = 1, ['P'] = 1, ['Q'] = 1,
      ['R'] = 1, ['S'] = 1, ['T'] = 1, ['U'] = 1, ['V'] = 1, ['W'] = 1,
      ['X'] = 1, ['Y'] = 1, ['Z'] = 1, ['-'] = 1, ['@'] = 1, ['%'] = 1,
      ['+'] = 1, ['/'] = 1, ['='] = 1, ['_'] = 1, ['['] = 1, [']'] = 1 }
  };
  if (relaxed >= (sizeof(relax_tbl)/sizeof(relax_tbl[0]))) return 0;
  return relax_tbl[relaxed][ch];
}

static void safestr(const char *chin, stralloc* strout, size_t maxlen, int relaxed)
{
  int i;
  unsigned char ch;
  static unsigned char octal[5] = "\\0377";

  /* appending to strout */
  i = 0;
  while ((ch = chin[i])) {
    /* maxlen is for _input_ bytes, 0 means "much" */
    if (maxlen && (i == maxlen)) break;
    i++;
    if (issafe(ch, relaxed)) {
      if (!stralloc_catb(strout, &ch, 1)) die_nomem();
      continue;
    }
    octal[4] = '0' + (ch & 7); ch >>= 3;
    octal[3] = '0' + (ch & 7); ch >>= 3;
    octal[2] = '0' + (ch & 7);
    if (!stralloc_catb(strout, octal, 5)) die_nomem();
  }
}

static char* gen_safestr(const char *chin, size_t maxlen)
{
  static stralloc tmpstr = {0};

  if (!stralloc_copys(&tmpstr, "")) die_nomem();
  safestr(chin, &tmpstr, maxlen, 0);
  if (!stralloc_0(&tmpstr)) die_nomem();
  return tmpstr.s;
}

/* relaxed set of chars */
static char* gen_safestr2(const char *chin, size_t maxlen)
{
  static stralloc tmpstr = {0};

  if (!stralloc_copys(&tmpstr, "")) die_nomem();
  safestr(chin, &tmpstr, maxlen, 1);
  if (!stralloc_0(&tmpstr)) die_nomem();
  return tmpstr.s;
}

/* relaxed set of chars, like gen_safestr, except additional
   safe chars '[' and ']' */
static char* gen_safestr3(const char *chin, size_t maxlen)
{
  static stralloc tmpstr = {0};

  if (!stralloc_copys(&tmpstr, "")) die_nomem();
  safestr(chin, &tmpstr, maxlen, 2);
  if (!stralloc_0(&tmpstr)) die_nomem();
  return tmpstr.s;
}

static const uint8 qptable[256] = {
  128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
  128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
  128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
    0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 128, 128, 128, 128, 128, 128,
  128,  10,  11,  12,  13,  14,  15, 128, 128, 128, 128, 128, 128, 128, 128, 128,
  128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
  128,  10,  11,  12,  13,  14,  15, 128, 128, 128, 128, 128, 128, 128, 128, 128,
  128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
  128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
  128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
  128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
  128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
  128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
  128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
  128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
  128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128 };

/* Reads quoted-printable data from __in of length len and
   writes decoded byte stream to "out".

   Returns: -1 out of memory
             0 OK
             1 nothing to decode
             2 Last char at __in was "soft" line break
 */
static inline int qpdecode(void *__in, unsigned int len, stralloc* out, int append)
{
  unsigned long pos;
  unsigned char ch, ch2;
  unsigned char *in = __in;

  if (!append) if (!stralloc_copys(out, "")) return -1;
  if (len == 0) return 1; /* Empty line */
  if ((len == 1) && in[0] == '=') return 1; /* "soft" line break */

  if (len > 76) {
    flagcontent |= ACL_CONTENT_BADQP;
  }

  pos = 0;
  while (len) {
    ch = in[pos++];
    len--;
    if (ch == '=') {
      if (len == 0) {
        return 2;
      } else if (len == 1) {
        if (!stralloc_catb(out, &in[pos-1], 2)) return -1;
        flagcontent |= ACL_CONTENT_BADQP;
        return 0;
      } else {
        ch = (qptable[in[pos]]);
        ch2 = (qptable[in[pos+1]]);
        if ((ch == 128) || (ch2 == 128)) {
          flagcontent |= ACL_CONTENT_BADQP;
          if (!stralloc_catb(out, &in[pos-1], 3)) return -1;
        } else {
          ch = (ch << 4) | ch2;
          if (!stralloc_append(out, &ch)) return -1;
        }
      }
      len -= 2;
      pos += 2;
    } else {
      if ((len == 0) && ((ch == ' ') || (ch == '\t'))) {
        flagcontent |= ACL_CONTENT_BADQP;
      }
      if (!stralloc_append(out, &ch)) return -1;
    }
  }
  return 0;
}

static void die_control() { out("421 unable to read controls (#4.3.5)\r\n"); flush(); _exit(1); }
static void die_ipme() { out("421 unable to figure out my IP addresses (#4.3.0)\r\n"); flush(); _exit(1); }
static void straynewline() {
  out("451 See http://cr.yp.to/docs/smtplf.html\r\n"); flush();
  strerr_warn4("qmail-smtpd: id ", qmailid.s, " EXIT_STRAYNEWLINE ", gen_tcpinfo(), 0);
  _exit(1); }
void err_looping() {
  strerr_warn3("qmail-smtpd: id ", qmailid.s, " 554 message looping", 0);
  out("554 too many hops, this message is looping (#5.4.6)\r\n"); }
void err_unimpl(arg) char *arg; {
  if (bingocount--) {
    out("502 unimplemented (#5.5.1)\r\n");
  } else {
    out("502 bingo! (#5.5.1)\r\n");
    flush();
    strerr_warn4("qmail-smtpd: id ", qmailid.s,
                 flagnullconn ? " EXIT_BINGO_NULLCONN " : " EXIT_BINGO ",
                 gen_tcpinfo(), 0);
    _exit(1);
  }
}

static void err_syntax() { out("555 syntax error (#5.5.4)\r\n"); }
static void err_wantmail() { out("503 MAIL first (#5.5.1)\r\n"); }
static void err_wantrcpt() { out("503 RCPT first (#5.5.1)\r\n"); }
static void err_noop(arg) char *arg; { out("250 ok\r\n"); }
static void err_vrfy(arg) char *arg; { out("252 send some mail, I'll try my best\r\n"); }
static void err_qqt() { out("451 qqt failure (#4.3.0)\r\n"); }

static int err_child() { out("454 oops, problem with child and I can't auth (#4.3.0)\r\n"); return -1; }
static int err_fork() { out("454 oops, child won't start and I can't auth (#4.3.0)\r\n"); return -1; }
static int err_pipe() { out("454 oops, unable to open pipe and I can't auth (#4.3.0)\r\n"); return -1; }
static int err_write() { out("454 oops, unable to write pipe and I can't auth (#4.3.0)\r\n"); return -1; }
static void err_authd() { out("503 you're already authenticated (#5.5.0)\r\n"); }
static void err_authmail() { out("503 no auth during mail transaction (#5.5.1)\r\n"); }
static int err_noauth() { out("504 unrecognized authentication type (#5.7.0)\r\n"); return -1; }
static int err_authabrt() { out("501 auth exchange cancelled (#5.0.0)\r\n"); return -1; }
static int err_input() { out("501 malformed auth input (#5.5.0)\r\n"); return -1; }
static void err_authrequired() { out("503 you must authenticate first (#5.5.1)\r\n"); }
static void err_nohelo() { out("503 you did not use HELO or EHLO command or argument missing (read RFC2821) (#5.5.1)\r\n"); }

static stralloc greeting = {0};
static stralloc idhost = {0};

static void smtp_greet(code) char *code;
{
  substdio_puts(&ssout, code);
  substdio_put(&ssout, greeting.s, greeting.len);
}

static void smtp_help(arg) char *arg;
{
  out("214 http://www.faqs.org/rfcs/rfc2821.html\r\n");
}

static void smtp_quit(arg) char *arg;
{
  smtp_greet("221 "); out("\r\n"); flush();
  strerr_warn4("qmail-smtpd: id ", qmailid.s,
               flagnullconn ? " EXIT_QUIT_NULLCONN " : " EXIT_QUIT ",
               gen_tcpinfo(), 0);
  _exit(0);
}

static void smtp_notproxy(arg) char *arg;
{
    out("221 so long, sucker. (#5.5.5)\r\n"); flush();
    strerr_warn4("qmail-smtpd: id ", qmailid.s,
                 flagnullconn ? " EXIT_HTTPPROXY_NULLCONN " : " EXIT_HTTPPROXY ",
                 gen_tcpinfo(), 0);
    _exit(0);
}

static char *remotehost;
static char *remoteinfo;
static char *local;
static char *localip;
static char *localport;
static char *relayclient;
static char *requireauth;
static int authd = 0;
static int remotehostresolved = 0;
static int hmackeyok = 0;
static stralloc hmackey = {0};
static stralloc hmackeyline = {0};
static sha1_hmac_state hmactmp;
static int abbskeyok = 0;
static stralloc abbskey = {0};

static stralloc authin = {0};
static stralloc user = {0};
static stralloc pass = {0};
static stralloc resp = {0};
static stralloc slop = {0};
static char *hostname;
static char **childargs;
static substdio ssup;
static char upbuf[128];

static int putinheader;      /* =0 not seen message body yet */
static int linespastheader;  /* =0 after boundary is found in body, until blank line */
static int base64ok;         /* =1 if there's "Content-Transfer-Encoding: base64" in current entity */
static int ct_encbody;       /* =1 if there's "Content-Transfer-Encoding: (base64|quoted-printable)"
                                in message headers */
static char ct_enc;          /* 'Q' for "Content-Transfer-Encoding: quoted-printable"
                                'B' for "Content-Transfer-Encoding: base64"
                               '\0' otherwise */
static char linetype;
static char flagrfc822;
static int rfc822nest;
static char flagtext;

static stralloc protocolinfo = {0};
static stralloc bogoerror = {0};
static stralloc line = {0};
static int linestate = -1;
static stralloc content = {0};
static stralloc msgid = {0};
static stralloc boundary[MAXMIMENESTING] = {{0}}; /* XXX convert to list */
static int mimenesting = 0;
static unsigned char msgiddigest[SHA1DIGESTSIZEHEX + 1];
static sha1_hash_state sha1ctx;

static void dohelo(arg) char *arg; {
  if (!stralloc_copys(&helohost,arg)) die_nomem();
  if (!stralloc_0(&helohost)) die_nomem();
  if (!stralloc_copys(&helohost_rcv, gen_safestr3(helohost.s, 256))) die_nomem();
  if (flagbadhelo & ACL_HELO_IGNORE) {
    if (!stralloc_cats(&helohost_rcv, " (unverified)")) die_nomem();
  } else {
    if (!(flagbadhelo & ACL_HELO_FASCIST_OK)) {
      char *why;

      if (!stralloc_cats(&helohost_rcv, " (verify failed: ")) die_nomem();
      if (flagbadhelo & (ACL_HELO_DNS_HARD | ACL_HELO_NONFQDN)) why = "NXDOMAIN/NODATA)";
      else if (flagbadhelo & (ACL_HELO_SOFT)) why = "temporary error)";
      else if (flagbadhelo & (ACL_HELO_BADIP_OWN | ACL_HELO_BADIP)) why = "IP addr mismatch)";
      else if (flagbadhelo & (ACL_HELO_SPOOF | ACL_HELO_STRICT)) why = "A record mismatch)";
      else why = "unknown error)";
      if (!stralloc_cats(&helohost_rcv, why)) die_nomem();
    } else {
      if (!stralloc_cats(&helohost_rcv, " (verify OK)")) die_nomem();
    }
  }
  if (!stralloc_0(&helohost_rcv)) die_nomem();
  fakehelo = helohost_rcv.s;
}

static int liphostok = 0;
static stralloc liphost = {0};

static int bcsok = 0;
static stralloc bcs = {0};
static struct constmap mapbcs;

static int bsha1ok = 0;
static stralloc bsha1 = {0};
static struct constmap mapbsha1;

static int acl_badsigs_status = 0;
static int badsigsok = 0;
static stralloc badsigs = {0};

static int bmfok = -1;
static stralloc bmf = {0};
static struct constmap mapbmf;

static int fakedok = 0;
static stralloc faked = {0};
static struct constmap mapfaked;

static int smtpauthok = 0;
static stralloc smtpauth = {0};
static struct constmap mapsmtpauth;

static int rblok = 0;

static void greyoptsparse(char *strin, greyopts_t *opts)
{
  char ch;
  uint64 nr;
  unsigned int pos;
  int flagout;

  while (*strin) {
    ch = *strin++;
    switch (ch) {
      case 'A':
        pos = scan_ullong(strin, &nr);
        if (pos) {
          strin += pos;
          opts->initial = nr;
        }
        break;

      case 'B':
        pos = scan_ullong(strin, &nr);
        if (pos) {
          strin += pos;
          opts->window = nr;
        }
        break;

      case 'C':
        pos = scan_ullong(strin, &nr);
        if (pos) {
          strin += pos;
          opts->expire = nr;
        }
        break;

      case 'D':
        pos = scan_ullong(strin, &nr);
        if (pos) {
          strin += pos;
          opts->perminitial = nr;
        }
        break;

      case 'E':
        pos = scan_ullong(strin, &nr);
        if (pos) {
          strin += pos;
          opts->maxrecip = nr;
        }
        break;

      case 'K':
        opts->flags &= ~ACL_GREY_FL_KEYS;
        flagout = 0;
        while (*strin && !flagout) {
          ch = *strin++;
          switch (ch) {
            case ' ':
              flagout = 1;
              break;

            case 'I':
              opts->flags |= ACL_GREY_FL_KEY_IP;
              break;

            case 'S':
              opts->flags |= ACL_GREY_FL_KEY_SIZE;
              break;

            case 'H':
              opts->flags |= ACL_GREY_FL_KEY_HELO;
              break;

            case 'F':
              opts->flags |= ACL_GREY_FL_KEY_FROM;
              break;

            case 'T':
              opts->flags |= ACL_GREY_FL_KEY_TO;
              break;
          }
        }
        if ((opts->flags & ACL_GREY_FL_KEYS) == 0)
          opts->flags |= ACL_GREY_FL_KEY_IP;

        break;

      case 'Z':
        opts->flags |= ACL_GREY_FL_DISINFO;
        break;
    }
  }
}

static void setup()
{
  char *x;
  uint32 u;
  uint64 ul;
  int i;
  char sid[FMT_ULONG];
  int fd;

  clock_gettime(CLOCK_MONOTONIC, &tspec);
  clock_gettime(CLOCK_REALTIME, &tspec_sid);
  remoteip = env_get("TCPREMOTEIP");
  if (!remoteip) {
    remoteip = "unknown";
  } else {
    rblok |= 1;
  }
  remoteport = env_get("TCPREMOTEPORT");
  if (!remoteport) {
    remoteport = "unknown";
  }
  local = env_get("TCPLOCALHOST");
  localip = env_get("TCPLOCALIP");
  if (!local) local = localip;
  if (!local) local = "unknown";
  if (!localip) localip = "unknown";
  localport = env_get("TCPLOCALPORT");
  if (!localport) localport = "unknown";
  remotehost = env_get("TCPREMOTEHOST");
  if (!remotehost) {
    remotehost = "unknown";
  } else {
    remotehostresolved = 1;
  }

  x = env_get("QMAILQUEUE");
  if (x) {
    if (!env_put2("ORIGQQ", x)) die_nomem();
    origqq = env_get("ORIGQQ");
  }

  if (control_rldef(&idhost, "control/idhost", 1, local) != 1)
    die_control();
  x = env_get("QMAILIDHOST");
  if (x) if (!stralloc_copys(&idhost, x)) die_nomem();

  sid[fmt_uint(sid, getpid())] = 0;
  if (!stralloc_copys(&qmailid, sid)) die_nomem();
#ifdef VERBOSEQMAILID
  if (!stralloc_cats(&qmailid, "/")) die_nomem();
  sid[fmt_uint(sid, tspec_sid.tv_sec)] = 0;
  if (!stralloc_cats(&qmailid, sid)) die_nomem();
  if (!stralloc_cats(&qmailid, ".")) die_nomem();
  sid[fmt_uint(sid, tspec_sid.tv_nsec)] = 0;
  if (!stralloc_cats(&qmailid, sid)) die_nomem();
  if (!stralloc_cats(&qmailid, "/")) die_nomem();
  if (!stralloc_cats(&qmailid, localip ? localip : local)) die_nomem();
  if (!stralloc_cats(&qmailid, ":")) die_nomem();
  if (!stralloc_cats(&qmailid, localport)) die_nomem();
  if (!stralloc_cats(&qmailid, "/")) die_nomem();
  if (!stralloc_cats(&qmailid, remoteip)) die_nomem();
  if (!stralloc_cats(&qmailid, ":")) die_nomem();
  if (!stralloc_cats(&qmailid, remoteport)) die_nomem();
#endif
  if (!stralloc_0(&qmailid)) die_nomem();

#ifdef TLS_SMTPD
  if (env_get("TLSENABLE")) tlsok = 1;
  if (env_get("TLSDISABLE")) tlsok = 0;
  if (!stralloc_copys(&cert_status_str, "FAIL: no STARTTLS issued")) die_nomem();
  if (!stralloc_0(&cert_status_str)) die_nomem();
#endif
  if (env_get("TCPINFOENABLE")) tcpinfook = 1;
  if (env_get("TCPINFODISABLE")) tcpinfook = 0;
  if (control_init() == -1) die_control();
  if (control_rldef(&greeting,"control/smtpgreeting",1,(char *) 0) != 1)
    die_control();
  liphostok = control_rldef(&liphost,"control/localiphost",1,(char *) 0);
  if (liphostok == -1) die_control();
  if (control_readint(&timeout,"control/timeoutsmtpd") == -1) die_control();
  if (!timeout) timeout = 15;
  if (control_readint(&abbslifetime, "control/abbslifetime") == -1) die_control();

  /* key is binary-data, not just the first line */
  fd = open_read("control/smtpdkey");
  if (fd == -1) {
    if (errno != error_noent)
      die_control();
  } else {
    if (slurpclose(fd, &hmackey, 64) == -1)
      die_control();
    if (hmackey.len > 0)
      hmackeyok = 1;
  }

  dns_random_init();

  /* key is binary-data, not just the first line */
  fd = open_read("control/abbskey");
  if (fd == -1) {
    if (errno != error_noent)
      die_control();
  } else {
    if (slurpclose(fd, &abbskey, 64) == -1)
      die_control();
    if (abbskey.len > 0)
      abbskeyok = 1;
  }

  if (rcpthosts_init() == -1) die_control();
  if (control_readint(&bingocount, "control/bingocount") == -1) die_control();

#ifdef PCRE_SMTPD
  if (control_readint(&flagpcre, "control/pcre/enable") == -1) die_control();
  x = env_get("PCREENABLE");
  if (x) { if (scan_ulong(x, &u)) flagpcre = u; }
  if (flagpcre) {
    init_list_head(&pcre_active);
    init_list_head(&pcre_inactive);
    if (!stralloc_copys(&pcre_active_h, "")) die_nomem();
    if (!stralloc_copys(&pcre_active_m, "")) die_nomem();
    if (!stralloc_copys(&pcre_active_r, "")) die_nomem();
    if (!stralloc_copys(&pcre_active_H, "")) die_nomem();
    if (!stralloc_copys(&pcre_active_M, "")) die_nomem();
    if (!stralloc_copys(&pcre_active_R, "")) die_nomem();

    switch (control_readline(&pcre_line, "control/pcre/locale")) {
      case -1:
        die_control();
      case 0:
        break;
      case 1:
        if (setlocale(LC_CTYPE, pcre_line.s) != NULL) {
          pcre_tables = pcre_maketables();
          if (pcre_tables == NULL) die_nomem();
        }
    }
    x = env_get("PCREREJECT");
    if (!x || !*x) {
      switch (control_readline(&pcre_reject_dfl, "control/pcre/reject")) {
        case -1:
          die_control();
        case 1:
          if (pcre_reject_dfl.len > 0)
            break;
        case 0:
          if (!stralloc_copys(&pcre_reject_dfl, "(reject)")) die_nomem();
          break;
      }
    } else {
      if (!stralloc_copys(&pcre_reject_dfl, x)) die_nomem();
    }
    if (!stralloc_0(&pcre_reject_dfl)) die_nomem();
    if (control_readint(&pcredebug, "control/pcre/debug") == -1) die_control();
  }

  /* badsigs feature needs PCRE */
  badsigsok = control_readfile(&badsigs, "control/badsigs", 0);
  if (badsigsok == -1) die_control();

  if (!stralloc_copyb(&badhosts_last, "", 2)) die_nomem();

#endif

  if (control_readint(&maxrcpt, "control/maxrcpt") == -1) die_control();
  x = env_get("MAXRCPT");
  if (x) { if (scan_ulong(x, &u)) maxrcpt = u; }

  if (control_readint(&mfchk, "control/mfcheck") == -1) die_control();
  x = env_get("MFCHECK");
  if (x) { if (scan_ulong(x, &u)) mfchk = u; }

  bcsok = control_readfile(&bcs, "control/badcharset", 0);
  if (bcsok == -1) die_control();
  if (bcsok)
    if (!constmap_init(&mapbcs, bcs.s, bcs.len, 0)) die_nomem();

  bsha1ok = control_readfile(&bsha1, "control/badsha1", 0);
  if (bsha1ok == -1) die_control();
  if (bsha1ok) {
    if (!constmap_init(&mapbsha1, bsha1.s, bsha1.len, 0)) die_nomem();
  } else {
    if (!constmap_init(&mapbsha1, "", 0, 0)) die_nomem();
    /* calculate SHA1 anyways for logging purposes */
    bsha1ok = 1;
  }

  fakedok = control_readfile(&faked, "control/faked", 0);
  if (fakedok == -1) die_control();
  if (fakedok) {
    if (!constmap_init(&mapfaked, faked.s, faked.len, 1)) die_nomem();
    if (env_get("FAKEDTEMP")) fakedok = 2;
  }

  smtpauthok = control_readfile(&smtpauth, "control/smtpauth", 0);
  if (smtpauthok == -1) die_control();
  if (smtpauthok)
    if (!constmap_init(&mapsmtpauth, smtpauth.s, smtpauth.len, 1)) die_nomem();

  if (control_readint(&sd_giveuptime, "control/spamdomain/giveuptime") == -1) die_control();
  if (control_readint(&sd_maxdomains, "control/spamdomain/maxdomains") == -1) die_control();
  if (sd_maxdomains > 256) sd_maxdomains = 256;
  if (control_readint(&sd_debug, "control/spamdomain/debug") == -1) die_control();
  if (control_readint(&sd_www, "control/spamdomain/www") == -1) die_control();

  sd_tld_ok = control_readfile(&sd_tld, "control/spamdomain/tld", 0);
  if (sd_tld_ok == -1) die_control();
  if (sd_tld_ok)
    if (!constmap_init(&map_sd_tld, sd_tld.s, sd_tld.len, 0)) die_nomem();

  sd_2ld_ok = control_readfile(&sd_2ld, "control/spamdomain/2ld", 0);
  if (sd_2ld_ok == -1) die_control();
  if (sd_2ld_ok)
    if (!constmap_init(&map_sd_2ld, sd_2ld.s, sd_2ld.len, 0)) die_nomem();

  init_list_head(&sd_active);
  init_list_head(&sd_inactive);

  acl_hdrip_ok = control_readfile(&hdrip, "control/hdrip", 0);
  if (acl_hdrip_ok == -1) die_control();
  if (acl_hdrip_ok)
    if (!constmap_init(&map_hdrip, hdrip.s, hdrip.len, 0)) die_nomem();

  /* initial */
  switch (control_readint(&u, "control/grey/initial")) {
    case -1: die_control();
    case  1: if (u > 0) greyopts.initial = u;
  }
  x = env_get("GREYINITIAL");
  if (x) { if (scan_ulong(x, &u)) greyopts.initial = u; }

  /* window */
  switch (control_readint(&u, "control/grey/window")) {
    case -1: die_control();
    case  1: if (u > 0) greyopts.window = u;
  }
  x = env_get("GREYWINDOW");
  if (x) { if (scan_ulong(x, &u)) greyopts.window = u; }

  /* expire */
  switch (control_readint(&u, "control/grey/expire")) {
    case -1: die_control();
    case  1: if (u > 0) greyopts.expire = u;
  }
  x = env_get("GREYEXPIRE");
  if (x) { if (scan_ulong(x, &u)) greyopts.expire = u; }

  /* perminitial */
  switch (control_readint(&u, "control/grey/perminitial")) {
    case -1: die_control();
    case  1: greyopts.perminitial = u;
  }
  x = env_get("GREYPERMINITIAL");
  if (x) { if (scan_ulong(x, &u)) greyopts.perminitial = u; }

  /* maxrecip */
  switch (control_readint(&u, "control/grey/maxrecip")) {
    case -1: die_control();
    case  1: greyopts.maxrecip = u;
  }
  x = env_get("GREYMAXRECIP");
  if (x) { if (scan_ulong(x, &u)) greyopts.maxrecip = u; }

  x = env_get("GREYOPTS");
  if (x) greyoptsparse(x, &greyopts);

  if ((greyopts.expire < greyopts.initial) ||
      (greyopts.expire < greyopts.window) ||
      (greyopts.initial > greyopts.window)) {
    die_control();
  }
  if (control_readline(&greylistmsg, "control/grey/msg") == -1) die_control();

  flagstray = (env_get("STRAYOK") ? 1 : 0);

  x = env_get("ACL");
  acldfl = env_get("ACLDFL");
  if (!stralloc_copys(&envacl, "")) die_nomem();
  if (x) {
    int flagesc, numcolons, len;
    unsigned char ch;
    static stralloc tmpacl = {0};

    if ((x[0] == '=') && (x[1] != '\0')) {
      i = acl_getcdb(x+1, &tmpacl);
      if ((i != ACL_ERR_NOMATCH) && (i != ACL_ERR_MATCH)) die_control();
      if (i == ACL_ERR_MATCH) {
        if (!stralloc_copy(&envacl, &tmpacl)) die_nomem();
      }
    } else {
      for (flagesc = 0, numcolons = 0, len = str_len(x); len; len--) {
        ch = *x;
        x++;
        if (flagesc) {
          if (!stralloc_append(&envacl, &ch)) die_nomem();
          flagesc = 0;
          continue;
        }
        if (ch == '\\') { flagesc = 1; continue; }
        if (ch == ':') { numcolons++; if (!stralloc_0(&envacl)) die_nomem(); continue; }
        if (!stralloc_append(&envacl, &ch)) die_nomem();
      }
      if (numcolons == 0) die_control();

      while (envacl.len) {
        if (envacl.s[envacl.len - 1] == ' ') { --envacl.len; continue; }
        if (envacl.s[envacl.len - 1] == '\n') { --envacl.len; continue; }
        if (envacl.s[envacl.len - 1] == '\t') { --envacl.len; continue; }
        if (envacl.s[envacl.len - 1] == '\0') { --envacl.len; continue; }
        break;
      }
    }
  }
  if (!stralloc_0(&envacl)) die_nomem();

  if (control_readint64(&databytes,"control/databytes") == -1) die_control();
  x = env_get("DATABYTES");
  if (x) { if (scan_ullong(x, &ul)) databytes = ul; }
  if (!(databytes + 1)) --databytes;
  databytes_s[fmt_ullong(databytes_s, databytes)] = 0;

  remoteinfo = env_get("TCPREMOTEINFO");
  relayclient = env_get("RELAYCLIENT");
  requireauth = env_get("REQUIREAUTH");
  x = env_get("RBLSMTPD");
  if (x) {
    switch (x[0]) {
      case '\0':
        decision = ACL_RBLSMTPD_WL; /* whitelisted */
        if (!acldfl) {
          if (!env_put2("ACLDFL", "wl")) die_nomem();
          acldfl = env_get("ACLDFL");
        }
        break;
      case '-':
        decision = ACL_RBLSMTPD_PERM; /* permanent reject */
        rbltext = x + 1;
        break;
      default:
        decision = ACL_RBLSMTPD_TEMP; /* temporary reject */
        rbltext = x;
        break;
    }
  }

  if (decision != ACL_RBLSMTPD_WL) {
    x = env_get("BANNERDELAY");
    if (x && scan_ulong(x, &u) && u > 0 && is_socket(ssl_rfd)) {
      char snum1[FMT_ULONG];
      char snum2[FMT_ULONG];
      char snum3[FMT_ULONG];
      char snum4[FMT_ULONG];
      int inq, isconn;
      struct timespec tspecend;
      uint64 elapsed;

      if (u > 1800) u = 1800;

      /* delay started counting when we entered setup()... */
      do {
        inq = socket_inqueue(ssl_rfd);
        isconn = is_connected(ssl_rfd);
        if (inq > 0 || !isconn) {
          clock_gettime(CLOCK_MONOTONIC, &tspecend);
          elapsed = tspec_elapsed(&tspecend, &tspec);
          snum1[fmt_uint(snum1, elapsed / 1000000000)] = 0;
          snum2[fmt_uint0(snum2, (elapsed % 1000000000)/10000000, 2)] = 0;
          snum3[fmt_uint(snum3, u)] = 0;
          snum4[fmt_uint(snum4, inq)] = 0;
          if (isconn) {
            out("450 your SMTP client wrote ");
            out(snum4);
            out(" bytes before I sent SMTP greeting (read RFC2821)\r\n");
            flush();
          }
          strerr_warn12("qmail-smtpd: id ", qmailid.s, " EXIT_BANNERDELAY_NULLCONN (",
                        snum1, ".", snum2, "/", snum3, " s, inq=",
                        snum4, ") ", gen_tcpinfo(), 0);
          _exit(1);
        }
        my_nanosleep(0, 100000000);
        clock_gettime(CLOCK_MONOTONIC, &tspecend);
      } while (tspec_elapsed(&tspecend, &tspec) < ((uint64)u * (uint64)1000000000));
    }
  }

  dohelo(remotehost);
}

static int addrparse(arg, reqdomain)
char *arg;
int reqdomain;
{
  unsigned long int i;
  char ch;
  char terminator;
  struct ip_address ip;
  int flagesc;
  int flagquoted;

  terminator = '>';
  i = str_chr(arg,'<');
  if (arg[i])
    arg += i + 1;
  else { /* partner should go read rfc 821 */
    terminator = ' ';
    arg += str_chr(arg,':');
    if (*arg == ':') ++arg;
    if (*arg == '\0') return 0;
    while (*arg == ' ') ++arg;
  }

  /* strip source route */
  if (*arg == '@') while (*arg) if (*arg++ == ':') break;

  if (!stralloc_copys(&addr,"")) die_nomem();
  flagesc = 0;
  flagquoted = 0;
  for (i = 0; (ch = arg[i]); ++i) { /* copy arg to addr, stripping quotes */
    if (flagesc) {
      if (!stralloc_append(&addr,&ch)) die_nomem();
      flagesc = 0;
    }
    else {
      if (!flagquoted && (ch == terminator)) break;
      switch(ch) {
        case '\\': flagesc = 1; break;
        case '"': flagquoted = !flagquoted; break;
        default: if (!stralloc_append(&addr,&ch)) die_nomem();
      }
    }
  }
  /* could check for termination failure here, but why bother? */
  if (!stralloc_append(&addr,"")) die_nomem();

  if (liphostok) {
    i = byte_rchr(addr.s, addr.len, '@');
    if (i < addr.len) { /* if not, partner should go read rfc 821 */
      /* no Domain? */
      if (i == addr.len-2) return 0;
      if (addr.s[i + 1] == '[')
        if (!addr.s[i + 1 + ip_scanbracket(addr.s + i + 1, &ip)])
          if (ipme_is(&ip)) {
            addr.len = i + 1;
            if (!stralloc_cat(&addr,&liphost)) die_nomem();
            if (!stralloc_0(&addr)) die_nomem();
          }
    } else {
      if (addr.len > 900) return 0;
      if (addr.len > 1) return reqdomain ? 0 : 1;
    }
  }

  if (addr.len > 900) return 0;
  return 1;
}

static stralloc parameter = {0};

static char *getparameter(arg, paramname)
char *arg;
char *paramname;
{
  unsigned long int i;
  char ch;
  char terminator;
  int flagesc;
  int flagquoted;

  terminator = '>';
  i = str_chr(arg,'<');
  if (arg[i])
    arg += i + 1;
  else { /* partner should go read rfc 821 */
    terminator = ' ';
    arg += str_chr(arg,':');
    if (*arg == ':') ++arg;
    if (*arg == '\0') return 0;
    while (*arg == ' ') ++arg;
  }

  flagesc = 0;
  flagquoted = 0;
  for (i = 0; (ch = arg[i]) ;++i) { /* skipping addr, respecting quotes */
    if (flagesc) {
      flagesc = 0;
    } else {
      if (!flagquoted && (ch == terminator))
        break;
      switch(ch) {
        case '\\': flagesc = 1;
          break;
        case '"': flagquoted = !flagquoted;
          break;
       default:
          break;
     }
    }
  }

  if (!arg[i++]) return (char *)0; /* no parameters */
  arg += i;
  do {
    while (*arg == ' ') if (!*arg++) return (char *)0;
    if (case_diffb(arg, str_len(paramname), paramname) == 0) {
      arg += str_len(paramname);
      if (*arg++ == '=') {
        i = str_chr(arg, ' ');
        if (!stralloc_copyb(&parameter, arg, i)) die_nomem();
        if (!stralloc_0(&parameter)) die_nomem();
        return parameter.s;
      }
    }
    while (*arg != ' ') if (!*arg++) return (char *)0;
  } while (1);
}

static int smtp_param(arg, val, ret)
char *arg;
char *val;
uint64 *ret;
{
  char *size;
  uint64 sizebytes;

  size = getparameter(arg, val);
  if (!size) return 0;
  if (scan_ullong(size, &sizebytes) == 0) return 0;
  *ret = sizebytes;
  return 1;
}

static int addrmap(struct constmap *addrmap, struct stralloc *addrtest)
{
  unsigned long int dot, j;
  static stralloc addrtmp = {0};

  /* VERP not being taken care of */
  if (constmap(addrmap, addrtest->s, addrtest->len - 1)) return 1;
  j = byte_rchr(addrtest->s, addrtest->len, '@');
  if (j < addrtest->len) {
    /* matches "@something"-line in the control file */
    if (constmap(addrmap, addrtest->s + j, addrtest->len - j - 1)) return 1;
    /* matches "something@"-line in the control file */
    if (constmap(addrmap, addrtest->s, j + 1)) return 1;
    dot = 0;
    j++; /* skip '@' */
    /* now check for input addrtest.s of "offers@mx.01.example.net" if there
       are matches for "@.mx.01.example.net", @.01.example.net", "@.example.net"
       or "@.net" in the addrmap currently being checked. */
    do {
      if (!stralloc_copys(&addrtmp, "@.")) die_nomem();
      if (!stralloc_cats(&addrtmp, addrtest->s + j + dot)) die_nomem();
      if (constmap(addrmap, addrtmp.s, addrtmp.len)) return 1;
      dot += byte_chr(addrtest->s + j + dot, addrtest->len - j - dot, '.') + 1;
    } while (dot < addrtest->len - j);
  }

  return 0;
}

/* with 10000-line badmailfrom the constmap_init takes 0.1s on Pentium 200 MHz */
static int bmfcheck()
{
  if (bmfok == -1) {
    bmfok = control_readfile(&bmf, "control/badmailfrom", 0);
    if (bmfok == -1) die_control();
    if (bmfok)
      if (!constmap_init(&mapbmf, bmf.s, bmf.len, 0)) die_nomem();
  }
  if (!bmfok) return 0;
  return addrmap(&mapbmf, &mailfrom);
}

static int bmfcheck_init()
{
  int ret = ACL_MF_OK;

  if (addr.len == 1) {
    /* deny bounces if NOBOUNCE is set */
    if (env_get("NOBOUNCE")) ret |= ACL_MF_BADBOUNCE;
    return ret;
  }
  return ret;
}

static int mfcheck(int *ips)
{
  long int j = 0;

  *ips = 0;
  if (!mfchk || (addr.len == 1)) return 0;

  j = byte_rchr(addr.s, addr.len, '@') + 1;
  if (j < addr.len) {
    if(!stralloc_copys(&sa_mf, addr.s + j)) die_nomem();
    ia_mf.len = 0;
    j = dns_mxip(&ia_mf, &sa_mf, 0);
    *ips = ia_mf.len;
    if ((j == 0) && (ia_mf.len == 0)) return DNS_HARD;
    return j;
  } else {
    return 0;
  }
}

static int domain_mx_ipcheck(char *dom, char *dnsbl, char **ipmatch, char **rblmatch)
{
  int i, j, rblret;
  static char iptemp[IPFMT];

  if (!stralloc_copys(&sa_mf, dom)) die_nomem();
  ia_mf.len = 0;
  j = dns_mxip(&ia_mf, &sa_mf, 0);
  for (i = 0; i < ia_mf.len; ++i) {
    iptemp[ip_fmt(iptemp, &ia_mf.ix[i].ip)] = 0;
    rblret = rblcheck("rbl", dnsbl, iptemp, rblmatch);
    switch (rblret) {
      case RBL_REJECT:
      case RBL_REJECTTEMP:
        *ipmatch = &iptemp[0];
        return rblret;
      case RBL_ERR:
        return rblret;
      default:
        continue;
    }
  }
  /* return (j == 0) ? RBL_OK : RBL_ERR; */
  return RBL_OK;
}

static int domain_ip_nscheck(char *dom, char *dnsbl, char **ipmatch, char **rblmatch)
{
  int i, rblret;
  static char iptemp[IPFMT];

  if (!stralloc_copys(&sa_mf, dom)) die_nomem();
  if (dns_nsip(&ia_mf, &sa_mf) == DNS_MEM) die_nomem();
  for (i = 0; i < ia_mf.len; ++i) {
    iptemp[ip_fmt(iptemp, &ia_mf.ix[i].ip)] = 0;
    rblret = rblcheck("rbl", dnsbl, iptemp, rblmatch);
    switch (rblret) {
      case RBL_REJECT:
      case RBL_REJECTTEMP:
        *ipmatch = &iptemp[0];
        return rblret;
      case RBL_ERR:
        return rblret;
      default:
        continue;
    }
  }
  return RBL_OK;
}

static int domain_rhs_check(char *dom, char *dnsbl, char **rblmatch, int flags)
{
  int i, j;
  static stralloc rhs_text = {0};
  char iptemp[IPFMT];

  if (!stralloc_copys(&sa_mf, dom)) die_nomem();
  if (!stralloc_cats(&sa_mf, ".")) die_nomem();
  if (!stralloc_cats(&sa_mf, dnsbl)) die_nomem();
  if (flags & RHS_DNS_TXT) {
    j = dns_txt(&rhs_text, &sa_mf);
    if (!stralloc_0(&rhs_text)) die_nomem();
  } else if (flags & RHS_DNS_A) {
    j = dns_ip(&ia_mf, &sa_mf);
    if ((j == DNS_MEM) || !stralloc_copys(&rhs_text, "A records ")) die_nomem();
    for (i = 0; i < ia_mf.len; ++i) {
      iptemp[ip_fmt(iptemp, &ia_mf.ix[i].ip)] = 0;
      if (!stralloc_cats(&rhs_text, iptemp)) die_nomem();
      if (!stralloc_cats(&rhs_text, " ")) die_nomem();
    }
    if (!stralloc_0(&rhs_text)) die_nomem();
  } else
    return RBL_OK;

  switch (j) {
    case DNS_MEM:
    case DNS_SOFT:
      return (flags & RHS_SOFTFAIL) ? RBL_TEMPERROR : RBL_OK;
    case DNS_HARD:
      return RBL_OK;
    default:
      *rblmatch = rhs_text.s;
      return RBL_REJECT;
  }
}

static int addrallowed()
{
  int r;
  r = rcpthosts(addr.s, str_len(addr.s));
  if (r == -1) die_control();
  return r;
}

static stralloc sa_helo = {0};
static ipalloc ia_helo = {0};

static int helohostcheck(char *arg)
{
  unsigned long int i, j;
  char iptemp[IPFMT];
  struct ip_address iptest;
  int ret;
  int aclret;
  static stralloc tmpacl = {0};

  ret = ACL_HELO_OK;

  if (!stralloc_copys(&tmpacl, "h")) die_nomem();
  if (!stralloc_cats(&tmpacl, arg) || !stralloc_0(&tmpacl)) die_nomem();
  aclret = acl_getcdb(tmpacl.s, &acl_helo);
  if ((aclret != ACL_ERR_NOMATCH) && (aclret != ACL_ERR_MATCH)) die_control();

  if (env_get("IGNOREHELO")) {
    return (ret | ACL_HELO_IGNORE);
  }

  /* too many broken spamwares flying around and causing
     useless queries to *.root-servers.net ... */
  i = str_len(arg);
  /* DNS protocol error to be expected anyways */
  if (i > 255) return ACL_HELO_DNS_HARD;

  if (case_diffs(arg, "localhost")) { /* XXX */
    j = byte_chr(arg, i, '.');
    while ((j < i) && (arg[j] == '.')) j++;
    if (j == i) return ACL_HELO_NONFQDN;
  }

  if (!stralloc_copyb(&sa_helo, arg, i)) {
    return ACL_HELO_SOFT;
  }

  /* if spamware uses some other IP than the REAL one it's connecting from... */
  if (((j = ip_scan(sa_helo.s, &iptest)) > 0) ||
      ((j = ip_scanbracket(sa_helo.s, &iptest)) > 0)) {
    if (j == sa_helo.len) {
      iptemp[ip_fmt(iptemp, &iptest)] = 0;
      if (str_equal(iptemp, remoteip)) {
        return (ret | ACL_HELO_FASCIST_OK);
      } else {
        if (str_equal(iptemp, localip)) {
          ret |= ACL_HELO_BADIP_OWN;
        } else {
          ret |= ACL_HELO_BADIP;
        }
      }
      return ret;
    }
  }

  switch (dns_ip(&ia_helo, &sa_helo)) {
    case DNS_HARD:
      return (ret | ACL_HELO_DNS_HARD);
    case DNS_SOFT:
    case DNS_MEM:
      return (ret | ACL_HELO_SOFT);
  }

  /* if spamware or our friend ( :) ) uses _our_ hostname as the helo argument... */
  if (case_diffb(arg, greeting.len, greeting.s) == 0) {
    for (j = 0; j < ia_helo.len; ++j) {
      iptemp[ip_fmt(iptemp, &ia_helo.ix[j].ip)] = 0;
      if (str_equal(iptemp, remoteip)) {
        break;
      }
    }
    /* spanky failed */
    if (j == ia_helo.len) ret |= ACL_HELO_SPOOF;
  }

  /* fascist check */
  for (j = 0; j < ia_helo.len; ++j) {
    iptemp[ip_fmt(iptemp, &ia_helo.ix[j].ip)] = 0;
    if (str_equal(iptemp, remoteip)) {
      return (ret | ACL_HELO_FASCIST_OK);
    }
  }

  /* given helohost did not match connecting IP */
  return (ret | ACL_HELO_STRICT);
}

static void smtp_rset()
{
  seenmail = 0;
  strerr_warn3("qmail-smtpd: id ", qmailid.s, " RSET", 0);
  out("250 flushed\r\n");
}

static void smtp_mail(char *arg)
{
  int ips = 0;
  char s_ips[FMT_ULONG];
  int domainpart;
  uint64 tmpsize;

  if (requireauth && !authd) { err_authrequired(); return; }
  if (!addrparse(arg, 1)) { err_syntax(); return; }
  if (flagbadhelo & ACL_HELO_NOHELO) {
    err_nohelo();
    strerr_warn3("qmail-smtpd: id ", qmailid.s, " 503 ACL_HELO_NOHELO", 0);
    return;
  }

  switch (acl_get(addr.s, &acl_from, 'f', 0)) {
    case ACL_ERR_TEMP:
      out("450 error while accessing ACL (#4.3.0)\r\n"); return;
    case ACL_ERR_PERM:
      out("550 error while accessing ACL (#5.3.0)\r\n"); return;
    case ACL_ERR_MATCH:
      break;
    case ACL_ERR_NOMATCH:
      if (!stralloc_copys(&acl_from, "")) die_nomem();
      if (!stralloc_0(&acl_from)) die_nomem();
      break;
    default:
      out("450 unexpected error while accessing ACL (#4.3.0)\r\n"); return;
  }

  flagbarf = bmfcheck_init();
  switch (mfcheck(&ips)) {
    case DNS_HARD:
      flagbarf |= ACL_MF_DNS_HARD;
      break;
    case 1:
      if (ips > 0) break;
    case DNS_SOFT:
    case DNS_MEM:
      flagbarf |= ACL_MF_SOFT;
      break;
    default:
      break;
  }

  acl_maxsz = 0;
  /* RFC1870 */
  if (smtp_param(arg, "SIZE", &tmpsize))
    smtpsize = tmpsize;
  else
    smtpsize = 0;
  smtpsize_s[fmt_ullong(smtpsize_s, smtpsize)] = 0;
  acl_maxsz_s[fmt_ullong(acl_maxsz_s, acl_maxsz)] = 0;

  seenmail = 1;
  rblflush();
  if (!stralloc_copys(&rcptto,"")) die_nomem();
  if (!stralloc_copys(&mailfrom, addr.s)) die_nomem();
  if (!stralloc_0(&mailfrom)) die_nomem();
  recipcount = 0;
  flaggreyid_ok = 0;
  greydelayed_total = 0;
  greydelayed_min = 0;
  greydelayed_max = 0;

  if (!env_put("BOGOOPT=3")) die_nomem();
  bogowanted = '?';
  if (!env_unset("QMAILQUEUE")) die_nomem();
  if (origqq) {
    if (!env_put2("QMAILQUEUE", origqq)) die_nomem();
  }
  flagaestrap = 0;
  if (!stralloc_copys(&aestrap_info, "")) die_nomem();

  s_ips[fmt_uint(s_ips, ips)] = 0;
  out("250 ok");
  domainpart = byte_rchr(mailfrom.s, mailfrom.len, '@');
  if (mfchk && (ips > 0) && (domainpart < mailfrom.len)) {
    out(", found "); out(s_ips); out(" IP"); if (ips > 1) out("s");
    out(" for "); out(mailfrom.s + domainpart + 1);
  }
  out("\r\n");
}

/*
   returns 1 if mailfrom.s if forged with some probability for some usually
   forged domain.  this also prevents your server from being (ab)used in joejobs etc.
   involving these domains (forging victim's email in MAIL FROM and causing bounces
   to that address when destination mailbox does not exist on your server(s)).
   just watch.  http://basic.wirehub.nl/blocked.yesterday.txt

   ``control/faked'' syntax is:
   --- 8< ---
   example.com:ISEXAMPLE
   .example.com:ISEXAMPLE
   isexample:
   foo.example.net:ISEXAMPLE2
   isexample2:you can't use that envelope sender
   --- >8 ---

   example.com, xyzzy.example.com, test.example.com, all of *.example.com
   are allowed only if ISEXAMPLE env ver is set,
   give the default error if it is not set.

   foo.example.net is accepted in domain part of envelope sender only
   if ISEXAMPLE2 env var is set.

   related stuff:
   http://www.ietf.org/internet-drafts/draft-danisch-dns-rr-smtp-01.txt
   ftp://ftp.rfc-editor.org/in-notes/internet-drafts/draft-fecyk-dsprotocol-02.txt
*/
static int fakemailfrom(char **ret)
{
  char *s;
  char *fakedom;
  char *fakedomerror;
  long int i, j, fakedomlen;
  static stralloc strfake = {0};

  if (!fakedok) return ACL_MF_OK;
  j = byte_rchr(mailfrom.s, mailfrom.len, '@');
  if (j < mailfrom.len) {
    s = mailfrom.s + j + 1; /* skip over '@' */
    fakedom = 0;
    fakedomlen = str_len(s);
    for (i = 0; i <= fakedomlen; ++i)
      if ((i == 0) || (i == fakedomlen) || (s[i] == '.'))
        if ((fakedom = constmap(&mapfaked, s + i, fakedomlen - i)))
          break;
    if (fakedom && !*fakedom) fakedom = 0;
    if (fakedom) {
      /* NOTE: they key in control/faked must be lowercase */
      if (env_get(fakedom) != 0) {
        return ACL_MF_OK;
      } else {
        fakedomerror = constmap(&mapfaked, fakedom, str_len(fakedom));
        if (!fakedomerror) {
          if (!stralloc_copys(&strfake, "421 sorry, local configuration error with fakemailfrom (#4.3.5)")) die_nomem();
          if (!stralloc_0(&strfake)) die_nomem();
          *ret = strfake.s;
          return ACL_MF_MAYBEFAKECFG;
        }
        if (!stralloc_copys(&strfake, (fakedok == 1) ? "553 sorry, " : "450 sorry, ")) die_nomem();
        if (!stralloc_cats(&strfake, !*fakedomerror
                           ? "that envelope sender is not accepted from your IP"
                           : fakedomerror))
          die_nomem();
        if (!stralloc_cats(&strfake, (fakedok == 1) ? " (#5.7.1)" : " (#4.3.0)")) die_nomem();
        if (!stralloc_0(&strfake)) die_nomem();
        *ret = strfake.s;
        return (fakedok == 1) ? ACL_MF_MAYBEFAKE : ACL_MF_MAYBEFAKETEMP;
      }
    }
  }

  return ACL_MF_OK;
}

static aestrap_ctx trapctx = {0};

static int aestrap_verify(stralloc *checkaddr, stralloc *infoheader, char *trapbase,
                   char *scriptname, char **aesdnsbl, datetime_sec *expires,
                   char **remip_ret)
{
  symmetric_key aes_key;
  sha1_hmac_state hmactmp;
  long int j;
  uint8 trapout[25];
  uint8 p[16];
  uint8 traphmac[9];
  char snum[FMT_ULONG];
  static char remip[IPFMT];
  uint32 time_s, time_hs;
  uint32 genid;
  pid_t xpid;
  uid_t xuid;
  static stralloc addrlower = {0};
  datetime_sec expire, expire_sec;
  int expired = 0;
  static stralloc dnsblstr = {0};
  uint8 tmp_enc_key[AESTRAP_KEYSIZE];
  int flaggen;

  if (!stralloc_copy(&addrlower, checkaddr)) die_nomem();
  if (!stralloc_copys(infoheader, "")) die_nomem();
  case_lowerb(addrlower.s, addrlower.len);
  j = byte_rchr(addrlower.s, addrlower.len, '@');
  if ((j < 40) || (j == addrlower.len)) return AESTRAP_FAIL;
  if (aestrap_initkey(&trapctx, trapbase, scriptname) != 1) return AESTRAP_TEMP;
  base32decode(&addrlower.s[j-40], &trapout[0], 40);

  /* verify 72-bit HMAC */
  sha1_hmac_init(&hmactmp, &trapctx.auth_key[0], SHA1DIGESTSIZERAW);
  sha1_hmac_process(&hmactmp, &trapout[0], 16);
  if (j > 40) sha1_hmac_process(&hmactmp, addrlower.s, j-40);
  sha1_hmac_process(&hmactmp, addrlower.s + j, str_len(addrlower.s + j));
  sha1_hmac_done(&hmactmp, &traphmac[0], sizeof(traphmac), SHA1DIGESTRAW);
  if (crypto_verify_9(&trapout[16], &traphmac[0]) != 0) return AESTRAP_FAIL;

  /* prepare decryption key */
  sha1_hmac_init(&hmactmp, &trapctx.enc_key[0], SHA1DIGESTSIZERAW);
  if (j > 40) sha1_hmac_process(&hmactmp, addrlower.s, j-40);
  sha1_hmac_process(&hmactmp, addrlower.s + j, str_len(addrlower.s + j));
  byte_zero(tmp_enc_key, sizeof(tmp_enc_key));
  sha1_hmac_done(&hmactmp, tmp_enc_key, sizeof(tmp_enc_key), SHA1DIGESTRAW);
  aes_setup(tmp_enc_key, AESTRAP_KEYSIZE, &aes_key);
  aes_ecb_decrypt(&trapout[0], p, &aes_key);

  flaggen = ((p[8] & 0x80) == 0x80);
  if (flaggen) {
    genid = (p[0] << 24) | (p[1] << 16) | (p[2] << 8) | (p[3]);
    snum[fmt_ulong(snum, genid)] = 0;
    if (!stralloc_cats(infoheader, "GENID=")) die_nomem();
    if (!stralloc_cats(infoheader, snum)) die_nomem();
    if (!stralloc_cats(infoheader, " UID=")) die_nomem();
    xuid = (p[12] << 24) | (p[13] << 16) | (p[14] << 8) | (p[15]);
    snum[fmt_ulong(snum, xuid)] = 0;
    if (!stralloc_cats(infoheader, snum)) die_nomem();
  } else {
    remip[ip_fmt(remip, (struct ip_address*)p)] = 0;
    if (!stralloc_cats(infoheader, "IPv4=")) die_nomem();
    if (!stralloc_cats(infoheader, remip)) die_nomem();
    if (!stralloc_cats(infoheader, " PID=")) die_nomem();
    xpid = p[12] << 8 | p[13];
    snum[fmt_uint(snum, xpid)] = 0;
    if (!stralloc_cats(infoheader, snum)) die_nomem();
  }

  if (!stralloc_cats(infoheader, " timestamp=")) die_nomem();
  time_s = p[4] << 24 | p[5] << 16 | p[6] << 8 | p[7];
  time_hs = (p[8] & ~0x80);
  expire = p[9] << 16 | p[10] << 8 | p[11];
  snum[fmt_uint(snum, time_s)] = 0;
  if (!stralloc_cats(infoheader, snum)) die_nomem();
  if (!stralloc_cats(infoheader, ".")) die_nomem();
  snum[fmt_uint0(snum, time_hs*10000, 6)] = 0;
  if (!stralloc_cats(infoheader, snum)) die_nomem();
  if (expire) {
    expire_sec = (time_s + (expire * 60));
    if ((now() > expire_sec)) {
      *expires = expire_sec;
      expired = 1;
    }
    if (!stralloc_cats(infoheader, " expires=")) die_nomem();
    snum[fmt_uint(snum, time_s + (expire * 60))] = 0;
    if (!stralloc_cats(infoheader, snum)) die_nomem();
  }
  if (!stralloc_0(infoheader)) die_nomem();

  if (expired) return AESTRAP_EXPIRED;
  if (!flaggen) {
    if (!stralloc_copys(&dnsblstr, scriptname)) die_nomem();
    if (!stralloc_cats(&dnsblstr, "/dnsbl") || !stralloc_0(&dnsblstr)) die_nomem();
    if (rblcheck("aestrap", dnsblstr.s, remip, aesdnsbl) == RBL_REJECT) {
      *remip_ret = remip;
      return AESTRAP_DNSBL;
    }
  }
  return AESTRAP_OK;
}

static int greylist_idgen(char *idout, greyopts_t *gopts, int outlen)
{
  if (!hmackeyok) return 1;
  sha1_hmac_init(&hmactmp, hmackey.s, hmackey.len);

  if (gopts->flags & ACL_GREY_FL_KEY_IP) {
    sha1_hmac_process(&hmactmp, "I", 1);
    sha1_hmac_process(&hmactmp, remoteip, str_len(remoteip)+1);
  }
  if (gopts->flags & ACL_GREY_FL_KEY_SIZE) {
    sha1_hmac_process(&hmactmp, "S", 1);
    sha1_hmac_process(&hmactmp, (uint8*)&smtpsize, sizeof(smtpsize));
  }
  if (gopts->flags & ACL_GREY_FL_KEY_HELO) {
    sha1_hmac_process(&hmactmp, "H", 1);
    sha1_hmac_process(&hmactmp, helohost.s, helohost.len);
  }
  if (gopts->flags & ACL_GREY_FL_KEY_FROM) {
    sha1_hmac_process(&hmactmp, "F", 1);
    sha1_hmac_process(&hmactmp, (uint8*)mailfrom.s, mailfrom.len);
  }
  if (gopts->flags & ACL_GREY_FL_KEY_TO) {
    sha1_hmac_process(&hmactmp, "T", 1);
    sha1_hmac_process(&hmactmp, (uint8*)addr.s, addr.len);
  }

  sha1_hmac_done(&hmactmp, idout, outlen, SHA1DIGESTBASE32);
  return 0;
}

static int acl_match_rcpt(char **ret)
{
  char *cp, *cpend, *fakemsg, *dnsmsg;
  unsigned char ch;
  int flagout = 0;
  long int atpos;
  static stralloc retmsg = {0};
  datetime_sec expiresec;
  char snum[FMT_ULONG];
  int domret;
  char *addy;
  char *rblmatch, *rblmatchaes, *aestrap_ip;
  char *dompos;
  long int domlen, j;
  int rhs_flags;
  char rhs_action;
  static stralloc rhs_err = {0};
  const char *sqlite3error;
  static stralloc linetmp = {0};
  const char *pcreerr;
  int pcreerr_off;
  int pcreret;
  char *badhosts_ch;

  cp = acl_checks.s;
  cpend = acl_checks.s + acl_checks.len;
  while ((cp < cpend) && !flagout) {
    ch = *cp++;
    if (cp >= cpend) break;
    if (ch == '\0') continue;
#if 0
    strerr_warn4("qmail-smtpd: id ", qmailid.s, " DEBUG_ACL_MATCH_RCPT ", cp-1, 0);
#endif
    if (ch == 'M') {
      ch = *cp++;
      if (cp >= cpend) break;
      switch (ch) {
        case '1':
          if (bmfcheck()) {
            strerr_warn8("qmail-smtpd: id ", qmailid.s, " 553 ACL_MF_BADMAILFROM from ",
                         safemailfrom.s, " SIZE=", smtpsize_s,
                         " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "553 your envelope sender is in my badmailfrom list (#5.1.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '2':
          if (flagbarf & ACL_MF_SOFT) {
            strerr_warn8("qmail-smtpd: id ", qmailid.s, " 451 ACL_MF_SOFT from ",
                         safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "451 temporary error while resolving envelope sender (#4.1.8)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '3':
          if (flagbarf & ACL_MF_DNS_HARD) {
            strerr_warn8("qmail-smtpd: id ", qmailid.s, " 553 ACL_MF_DNS_HARD from ",
                         safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "553 permanent error while resolving envelope sender (#5.1.8)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '4':
          if (flagbarf & ACL_MF_DNS_HARD) {
            strerr_warn8("qmail-smtpd: id ", qmailid.s, " 451 ACL_MF_DNS_HARD from ",
                         safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "451 permanent error while resolving envelope "
                                "sender, try again later (#4.1.8)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '6':
          if (flagbarf & ACL_MF_BADBOUNCE) {
            strerr_warn8("qmail-smtpd: id ", qmailid.s, " 553 ACL_MF_BADBOUNCE from ",
                         safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "553 bounces from you are administratively denied (#5.7.1)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '7':
          switch(fakemailfrom(&fakemsg)) {
            case ACL_MF_MAYBEFAKE:
              strerr_warn8("qmail-smtpd: id ", qmailid.s, " 553 ACL_MF_MAYBEFAKE from ",
                           safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
              if (!stralloc_copys(&retmsg, fakemsg)) die_nomem();
              if (!stralloc_0(&retmsg)) die_nomem();
              *ret = retmsg.s;
              return ACL_FAIL;

            case ACL_MF_MAYBEFAKETEMP:
              strerr_warn8("qmail-smtpd: id ", qmailid.s, " 450 ACL_MF_MAYBEFAKETEMP from ",
                           safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
              if (!stralloc_copys(&retmsg, fakemsg)) die_nomem();
              if (!stralloc_0(&retmsg)) die_nomem();
              *ret = retmsg.s;
              return ACL_FAIL;

            case ACL_MF_MAYBEFAKECFG:
              strerr_warn8("qmail-smtpd: id ", qmailid.s, " 421 ACL_MF_MAYBEFAKECFG from ",
                           safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
              if (!stralloc_copys(&retmsg, fakemsg)) die_nomem();
              if (!stralloc_0(&retmsg)) die_nomem();
              *ret = retmsg.s;
              return ACL_FAIL;

            case ACL_MF_OK:
              break;
          }
          break;

        case '8':
          if (env_get("DNSHARD")) {
            strerr_warn8("qmail-smtpd: id ", qmailid.s, " 553 ACL_BROKENDNS_HARD from ",
                         safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "553 please fix your DNS for ")) die_nomem();
            if (!stralloc_cats(&retmsg, remoteip)) die_nomem();
            if (!stralloc_cats(&retmsg, ": could not find ")) die_nomem();
            if (!stralloc_cats(&retmsg, env_get("DNSHARD"))) die_nomem();
            if (!stralloc_cats(&retmsg, ", read RFC1912 for more info (#5.7.1)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '9':
          if (env_get("DNSSOFT")) {
            strerr_warn8("qmail-smtpd: id ", qmailid.s, " 450 ACL_BROKENDNS_SOFT from ",
                         safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "450 please fix your DNS for ")) die_nomem();
            if (!stralloc_cats(&retmsg, remoteip)) die_nomem();
            if (!stralloc_cats(&retmsg, ": could not find ")) die_nomem();
            if (!stralloc_cats(&retmsg, env_get("DNSSOFT"))) die_nomem();
            if (!stralloc_cats(&retmsg, ", read RFC1912 for more info (#4.7.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case 'A':
          if (!abbskeyok) {
            strerr_warn8("qmail-smtpd: id ", qmailid.s, " 421 ACL_ABBS_TEMP from ",
                         safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "421 local configuration error with ABBS (#4.3.5)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          } else {
            char base32digest[ACL_ABBS_LEN + 1];
            long int i, j, k;
            uint32 abbsdate;
            int abbs_expired = 0;

            /* if some stupid thing changes the case, fix it not to do it */
            if (env_get("BADABBS")) break;
            i = str_rchr(addr.s, '@');
            if (!addr.s[i]) goto badabbs;
            j = byte_rchr(addr.s, i, '-');
            if ((j == 0) || (j == i)) goto badabbs;
            sha1_hmac_init(&hmactmp, abbskey.s, abbskey.len);
            sha1_hmac_process(&hmactmp, addr.s, j);
            sha1_hmac_process(&hmactmp, addr.s + i, str_len(addr.s + i));
            sha1_hmac_done(&hmactmp, base32digest, ACL_ABBS_LEN, SHA1DIGESTBASE32);
            base32digest[ACL_ABBS_LEN] = 0;
            if (str_diffn(addr.s + j + 1, base32digest, ACL_ABBS_LEN) != 0) goto badabbs;
            k = byte_rchr(addr.s, j, '-');
            if ((k == 0) || (k == j)) goto badabbs;
            if (!scan_ulong(addr.s + k + 1, &abbsdate)) goto badabbs;
            if ((now() - abbsdate) > abbslifetime) {
              abbs_expired = 1;
              goto badabbs;
            }
            break;

          badabbs:
            strerr_warn10("qmail-smtpd: id ", qmailid.s, " 553 ",
                          abbs_expired ? "ACL_ABBS_PERM_EXPIRED" : "ACL_ABBS_PERM",
                          " from ", safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "553 ABBS check failed, either the HMAC is invalid or recipient address is too old (#5.7.1)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case 'B':
          if ((str_diff(safemailfrom.s, "<>") == 0) && !env_get("BADDSN")) {
            strerr_warn6("qmail-smtpd: id ", qmailid.s, " 553 ACL_MF_BOUNCE from <> SIZE=",
                         smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "553 this recipient does not accept bounces (#5.7.1)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

#ifdef TLS_SMTPD
        case 'C':
          if (cert_status != ACL_CERT_OK) {
            strerr_warn10("qmail-smtpd: id ", qmailid.s, " 553 ACL_CERT from ",
                          safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s,
                          " X509 cert verify: ", cert_status_str.s, 0);
            if (!stralloc_copys(&retmsg, "553 X509 cert verify: ")) die_nomem();
            if (!stralloc_cats(&retmsg, cert_status_str.s)) die_nomem();
            if (!stralloc_cats(&retmsg, " (#5.7.5)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;
#endif

        case 'D':
          if ((str_diff(safemailfrom.s, "<>") != 0) && !env_get("BADDSN")) {
            strerr_warn8("qmail-smtpd: id ", qmailid.s, " 452 ACL_NOTBOUNCE from ",
                         safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "452 this recipient accepts only Delivery Status Notifications with null envelope sender (#4.7.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

#ifdef TLS_SMTPD
        case 'E':
          ch = *cp++;
          if (cp >= cpend) break;
          switch (ch) {
            case 'P':
              /* valid X509 cert with PKCS9 email */
              if ((cert_status == ACL_CERT_OK) && (cert_email.len > 1)) {
                strerr_warn10("qmail-smtpd: id ", qmailid.s, " 250 ACL_CERT_EMAIL_PASS from ",
                              safemailfrom.s, " (PKCS9_EMAIL=", cert_email.s, ") SIZE=",
                              smtpsize_s, " to ", safercptto.s, 0);
                flagout = 1;
                break;
              }
              break;

            case 'F':
              /* invalid X509 cert without PKCS9 email causes rejection */
              if ((cert_status != ACL_CERT_OK) || (cert_email.len == 1)) {
                strerr_warn10("qmail-smtpd: id ", qmailid.s, " 553 ACL_CERT_EMAIL_FAIL from ",
                              safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s,
                              " X509 cert verify: ", cert_status_str.s, 0);
                if (!stralloc_copys(&retmsg, "553 you did not provide valid X509 cert with PKCS9 email address (X509 cert verify: ")) die_nomem();
                if (!stralloc_cats(&retmsg, cert_status_str.s)) die_nomem();
                if (!stralloc_cats(&retmsg, ") (#5.7.5)")) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;
              }
              break;

            default:
              break;
          }
          break;
#endif

#ifdef GREYLIST
        case 'G':
          ch = *cp++;
          if (cp >= cpend) break;
          if (ch == 'I') {
            domlen = 0;
            while (validbase32[(unsigned char)addr.s[domlen]]) domlen++;
            if ((addr.s[domlen] != '@') || (domlen != ACL_GREY_IDLEN)) {
              strerr_warn3("qmail-smtpd: id ", qmailid.s,
                           " ACL_GREY_INFO_BAD", 0);
              if (!stralloc_copys(&retmsg, "550 invalid GREYLIST ID (must be lowercase) (#5.3.0)")) die_nomem();
              if (!stralloc_0(&retmsg)) die_nomem();
              *ret = retmsg.s;
              return ACL_FAIL;
            }

            switch (greylist_getinfo(addr.s, &greyinfo, &sqlite3error)) {
              case ACL_GREY_INFOOK:
                strerr_warn10("qmail-smtpd: id ", qmailid.s, " 550 ACL_GREY_INFOOK helo ",
                              gen_safestr(helohost.s, MAXLOGLEN),
                              " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                              " to ", safercptto.s, 0);
                if (!stralloc_copys(&retmsg, "550 GREYID for ")) die_nomem();
                if (!stralloc_catb(&retmsg, addr.s, ACL_GREY_IDLEN)) die_nomem();
                if (!stralloc_cats(&retmsg, ": created=")) die_nomem();
                if (!stralloc_cats(&retmsg, greyinfo.s_created)) die_nomem();
                if (!stralloc_cats(&retmsg, ", expires=")) die_nomem();
                if (!stralloc_cats(&retmsg, greyinfo.s_expires)) die_nomem();
                if (!stralloc_cats(&retmsg, ", windowstart=")) die_nomem();
                if (!stralloc_cats(&retmsg, greyinfo.s_windowstart)) die_nomem();
                if (!stralloc_cats(&retmsg, ", windowend=")) die_nomem();
                if (!stralloc_cats(&retmsg, greyinfo.s_windowend)) die_nomem();
                if (!stralloc_cats(&retmsg, ", nrblocked=")) die_nomem();
                if (!stralloc_cats(&retmsg, greyinfo.s_nrblocked)) die_nomem();
                if (!stralloc_cats(&retmsg, ", nrpassed=")) die_nomem();
                if (!stralloc_cats(&retmsg, greyinfo.s_nrpassed)) die_nomem();
                if (!stralloc_cats(&retmsg, ", nridqueries=")) die_nomem();
                if (!stralloc_cats(&retmsg, greyinfo.s_nridqueries)) die_nomem();
                if (!stralloc_cats(&retmsg, " (#5.3.0)")) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;

              case ACL_GREY_INTERROR:
              case ACL_GREY_INFOPERMERR:
                strerr_warn12("qmail-smtpd: id ", qmailid.s, " 550 ACL_GREY_PERMERR: ",
                              sqlite3error, ", helo ", gen_safestr(helohost.s, MAXLOGLEN),
                              " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                              " to ", safercptto.s, 0);
                if (!stralloc_copys(&retmsg, "550 failed to fetch GREYID (#5.3.0)")) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;
            }
          } else {
            greyopts_t greyoptstmp = {
              .initial = greyopts.initial,
              .window = greyopts.window,
              .expire = greyopts.expire,
              .perminitial = greyopts.perminitial,
              .maxrecip = greyopts.maxrecip,
              .flags = greyopts.flags,
            };
            uint64 greydelayed;
            int greyret;

            /* update opts for current recipient using defaults as base */
            greyoptsparse(cp-1, &greyoptstmp);
            if (mailfrom.len == 1) greyoptstmp.maxrecip = 1;

            if (greylist_idgen(greyid, &greyoptstmp, ACL_GREY_IDLEN)) {
              strerr_warn3("qmail-smtpd: id ", qmailid.s,
                           " ACL_GREY_INTERROR no HMAC configured", 0);
              if (!stralloc_copys(&retmsg, "451 temporary error with GREYLIST (#4.3.0)")) die_nomem();
              if (!stralloc_0(&retmsg)) die_nomem();
              *ret = retmsg.s;
              return ACL_FAIL;
            }
            greyid[ACL_GREY_IDLEN] = 0;
            flaggreyid = 1;

            greyret = greylist(greyid, &greyoptstmp, &greydelayed, &sqlite3error);
            greydelayed_total += greydelayed;
            if (greydelayed > greydelayed_max) greydelayed_max = greydelayed;
            if (greydelayed < greydelayed_min) greydelayed_min = greydelayed;
            switch (greyret) {
              case ACL_GREY_PASS:
                if (flaggreyid_ok == 0) {
                  greydelayed_min = greydelayed_max;
                }
                flaggreyid_ok++;
                break;

              case ACL_GREY_TEMPFAIL:
                strerr_warn12("qmail-smtpd: id ", qmailid.s, " 451 ACL_GREY_TEMPFAIL ",
                              greyid, " helo ", gen_safestr(helohost.s, MAXLOGLEN),
                              " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                              " to ", safercptto.s, 0);
                if (!stralloc_copys(&retmsg, "451 please try again later. ")) die_nomem();
                if (!stralloc_cat(&retmsg, &greylistmsg)) die_nomem();
                if (!stralloc_cats(&retmsg, " (GREYLIST ID=")) die_nomem();
                if (!stralloc_cats(&retmsg, greyid)) die_nomem();
                if (!stralloc_cats(&retmsg, ") (#4.3.0)")) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;

              case ACL_GREY_PERMFAIL:
                strerr_warn12("qmail-smtpd: id ", qmailid.s, " 550 ACL_GREY_PERMFAIL ",
                              greyid, " helo ", gen_safestr(helohost.s, MAXLOGLEN),
                              " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                              " to ", safercptto.s, 0);
                if (!stralloc_copys(&retmsg, "550 access denied. ")) die_nomem();
                if (!stralloc_cat(&retmsg, &greylistmsg)) die_nomem();
                if (!stralloc_cats(&retmsg, " (GREYLIST ID=")) die_nomem();
                if (!stralloc_cats(&retmsg, greyid)) die_nomem();
                if (!stralloc_cats(&retmsg, ") (#4.3.0)")) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;

              case ACL_GREY_INTERROR:
                strerr_warn4("qmail-smtpd: id ", qmailid.s,
                             " ACL_GREY_INTERROR greylist: ", sqlite3error, 0);
                if (!stralloc_copys(&retmsg, "451 temporary error with GREYLIST (#4.3.0)")) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;
            }
          }
          break;
#endif

        case 'I':
          ch = *cp++;
          if (cp >= cpend) break;
          domret = RBL_OK;

          if (!*cp) break;
          if (ch == 'F')
            dompos = mailfrom.s;
          else if (ch == 'T')
            dompos = addr.s;
          else
            break;

          domlen = str_len(dompos);
          j = byte_rchr(dompos, domlen, '@') + 1;
          if (j < domlen) {
            domret = domain_mx_ipcheck(dompos + j, cp, &addy, &rblmatch);
            switch(domret) {
              /* domain's MX ip listed in some of the queried DNSbls (temporary) */
              case RBL_REJECT:
                strerr_warn12("qmail-smtpd: id ", qmailid.s, " 553 ACL_DOMAINIP ", addy,
                              " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                              " to ", safercptto.s, ": ", rblmatch, 0);
                if (!stralloc_copys(&retmsg, "553 ACL_DOMAINIP ")) die_nomem();
                if (!stralloc_cats(&retmsg, dompos + j)) die_nomem();
                if (!stralloc_cats(&retmsg, " resolves to unwanted IP ")) die_nomem();
                if (!stralloc_cats(&retmsg, addy)) die_nomem();
                if (!stralloc_cats(&retmsg, ": ")) die_nomem();
                if (!stralloc_cats(&retmsg, rblmatch)) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;

              /* domain's MX ip listed in some of the queried DNSbls */
              case RBL_REJECTTEMP:
                strerr_warn12("qmail-smtpd: id ", qmailid.s, " 450 ACL_DOMAINIP_TEMP ", addy,
                              " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                              " to ", safercptto.s, ": ", rblmatch, 0);
                if (!stralloc_copys(&retmsg, "450 ACL_DOMAINIP_TEMP ")) die_nomem();
                if (!stralloc_cats(&retmsg, dompos + j)) die_nomem();
                if (!stralloc_cats(&retmsg, " resolves to unwanted IP ")) die_nomem();
                if (!stralloc_cats(&retmsg, addy)) die_nomem();
                if (!stralloc_cats(&retmsg, ": ")) die_nomem();
                if (!stralloc_cats(&retmsg, rblmatch)) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;

              /* trouble with rbl setup */
              case RBL_ERR:
                strerr_warn10("qmail-smtpd: id ", qmailid.s, " 450 ACL_DOMAINIP setup problem ",
                              cp, " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                              " to ", safercptto.s, 0);
                if (!stralloc_copys(&retmsg, "450 ACL_DOMAINIP problem")) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;
            }
          }
          break;

        case 'N':
          ch = *cp++;
          if (cp >= cpend) break;
          domret = RBL_OK;

          if (!*cp) break;
          if (ch == 'F')
            dompos = mailfrom.s;
          else if (ch == 'T')
            dompos = addr.s;
          else
            break;

          domlen = str_len(dompos);
          j = byte_rchr(dompos, domlen, '@') + 1;
          if (j < domlen) {
            domret = domain_ip_nscheck(dompos + j, cp, &addy, &rblmatch);
            switch(domret) {
              /* domain's nameserver listed in some of the queried DNSbls */
              case RBL_REJECT:
                strerr_warn12("qmail-smtpd: id ", qmailid.s, " 553 ACL_DOMAINIP_NS ", addy,
                              " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                              " to ", safercptto.s, ": ", rblmatch, 0);
                if (!stralloc_copys(&retmsg, "553 ACL_DOMAINIP_NS ")) die_nomem();
                if (!stralloc_cats(&retmsg, dompos + j)) die_nomem();
                if (!stralloc_cats(&retmsg, " nameserver resolves to unwanted IP ")) die_nomem();
                if (!stralloc_cats(&retmsg, addy)) die_nomem();
                if (!stralloc_cats(&retmsg, ": ")) die_nomem();
                if (!stralloc_cats(&retmsg, rblmatch)) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;

              /* domain's nameserver listed in some of the queried DNSbls (temporary) */
              case RBL_REJECTTEMP:
                strerr_warn12("qmail-smtpd: id ", qmailid.s, " 450 ACL_DOMAINIP_NS_TEMP ", addy,
                              " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                              " to ", safercptto.s, ": ", rblmatch, 0);
                if (!stralloc_copys(&retmsg, "450 ACL_DOMAINIP_NS_TEMP ")) die_nomem();
                if (!stralloc_cats(&retmsg, dompos + j)) die_nomem();
                if (!stralloc_cats(&retmsg, " nameserver resolves to unwanted IP ")) die_nomem();
                if (!stralloc_cats(&retmsg, addy)) die_nomem();
                if (!stralloc_cats(&retmsg, ": ")) die_nomem();
                if (!stralloc_cats(&retmsg, rblmatch)) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;

              /* trouble with rbl setup */
              case RBL_ERR:
                strerr_warn10("qmail-smtpd: id ", qmailid.s, " 450 ACL_DOMAINIP_NS setup problem ",
                              cp, " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                              " to ", safercptto.s, 0);
                if (!stralloc_copys(&retmsg, "450 ACL_DOMAINIP_NS problem")) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;
            }
          }
          break;

#ifdef PCRE_SMTPD
        /* PCRE check for remotehost */
        case 'P':
          ch = *cp++;
          if (cp >= cpend) break;
          if (!*cp) break;
          if (ch == 'T')
            badhosts_ch = "450";
          else if (ch == 'P')
            badhosts_ch = "550";
          else
            break;

          if (!remotehostresolved) break;

          if (!badhostsok || str_diff(badhosts_last.s, cp)) {
            if (badhosts_pcre_pattern) {
              alloc_free(badhosts_pcre_pattern);
              badhosts_pcre_pattern = 0;
            }
            if (badhosts_pcre_hints) {
              alloc_free(badhosts_pcre_hints);
              badhosts_pcre_hints = 0;
            }
            if (!stralloc_copys(&linetmp, "control/pcre/")) die_nomem();
            if (!stralloc_cats(&linetmp, cp) || !stralloc_0(&linetmp)) die_nomem();
            if (!stralloc_copys(&badhosts_last, cp) ||
                !stralloc_0(&badhosts_last)) die_nomem();
            badhostsok = control_readfile(&badhosts, linetmp.s, 0);
            if (badhostsok == -1) die_control();
            if (badhosts.len == 0) badhostsok = 0;
            if (badhostsok) {
              errno = 0;
              badhosts_pcre_pattern = pcre_compile(badhosts.s, 0,
                                                   &pcreerr, &pcreerr_off, 0);
              if (!badhosts_pcre_pattern) {
                if (errno == error_nomem) die_nomem();
                badhostsok = 0;
                if (pcredebug > 1) {
                  strerr_warn7("qmail-smtpd: id ", qmailid.s,
                               " DEBUG_ACL_PCRE pcre_compile error in ", linetmp.s,
                               ": \"", pcreerr, "\"", 0);
                }
                if (!stralloc_copys(&retmsg, "450 ACL_BADHOSTS problem")) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;
              }

              errno = 0;
              badhosts_pcre_hints = pcre_study(badhosts_pcre_pattern, 0, &pcreerr);
              if (pcreerr != NULL) {
                if (errno == error_nomem) die_nomem();
                badhostsok = 0;
                alloc_free(badhosts_pcre_pattern);
                badhosts_pcre_pattern = 0;
                if (pcredebug > 1) {
                  strerr_warn7("qmail-smtpd: id ", qmailid.s,
                               " DEBUG_ACL_PCRE pcre_study error in ", linetmp.s,
                               ": \"", pcreerr, "\"", 0);
                }
                if (!stralloc_copys(&retmsg, "450 ACL_BADHOSTS problem")) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;
              }
            }
          }

          if (badhostsok) {
            pcreret = pcre_exec(badhosts_pcre_pattern, badhosts_pcre_hints,
                                remotehost, str_len(remotehost), 0,
                                PCRE_NOTEMPTY, ovec, OVECSZ);
            if (pcreret >= 0) {
              strerr_warn12("qmail-smtpd: id ", qmailid.s, " ", badhosts_ch,
                             " ACL_BADHOSTS ", badhosts_last.s,
                              " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                              " to ", safercptto.s, 0);
              if (!stralloc_copys(&retmsg, badhosts_ch)) die_nomem();
              if (!stralloc_cats(&retmsg, " ACL_BADHOSTS ")) die_nomem();
              if (!stralloc_cats(&retmsg, badhosts_last.s)) die_nomem();
              if (!stralloc_cats(&retmsg, " ")) die_nomem();
              if (!stralloc_cats(&retmsg, remotehost)) die_nomem();
              if (!stralloc_0(&retmsg)) die_nomem();
              *ret = retmsg.s;
              return ACL_FAIL;
            }
          }

          break;
#endif

        /* RHS (Right-Hand-Side) */
        case 'R':
          ch = *cp++;
          if (cp >= cpend) break;
          domret = RBL_OK;

          /* Temporary / Permanent */
          if (!*cp) break;
          if (!stralloc_copys(&rhs_err, "")) die_nomem();
          rhs_flags = 0;
          if (ch == 'T' || ch == 't') {
            if (ch == 't') rhs_flags = RHS_SOFTFAIL;
            if (!stralloc_copys(&rhs_err, "450 ACL_RHS_")) die_nomem();
          } else if (ch == 'P' || ch == 'p') {
            if (ch == 'p') rhs_flags = RHS_SOFTFAIL;
            if (!stralloc_copys(&rhs_err, "553 ACL_RHS_")) die_nomem();
          } else
            break;

          /* Mail_from / Rcpt_to / tcp_remote_Host */
          ch = *cp++;
          if (cp >= cpend) break;
          if (!*cp) break;

          if (ch == 'M') {
            rhs_action = 'M';
            if (!stralloc_cats(&rhs_err, "MAIL_FROM ")) die_nomem();
            dompos = mailfrom.s;
          } else if (ch == 'R') {
            rhs_action = 'R';
            if (!stralloc_cats(&rhs_err, "RCPT_TO ")) die_nomem();
            dompos = addr.s;
          } else if (ch == 'H') {
            rhs_action = 'H';
            if (!stralloc_cats(&rhs_err, "REMOTEHOST ")) die_nomem();
            if (remotehostresolved) {
              dompos = remotehost;
            } else {
              break;
            }
          } else
            break;

          ch = *cp++;
          if (cp >= cpend) break;
          if (!*cp) break;

          if (ch == 'A')
            rhs_flags |= RHS_DNS_A;
          else if (ch == 'T')
            rhs_flags |= RHS_DNS_TXT;
          else
            break;

          domlen = str_len(dompos);
          if (rhs_action != 'H')
            j = byte_rchr(dompos, domlen, '@') + 1;
          else
            j = 0;
          if (j < domlen) {
            domret = domain_rhs_check(dompos + j, cp, &rblmatch, rhs_flags);
            switch(domret) {
              /* domain listed in some of the queried DNSbls */
              case RBL_REJECT:
                if (!stralloc_cats(&rhs_err, dompos + j)) die_nomem();
                if (!stralloc_cats(&rhs_err, " is listed on RHSBL ")) die_nomem();
                if (!stralloc_cats(&rhs_err, cp)) die_nomem();
                if (!stralloc_0(&rhs_err)) die_nomem();
                strerr_warn12("qmail-smtpd: id ", qmailid.s, " ", rhs_err.s,
                              " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                              " to ", safercptto.s, ": ", rblmatch, 0);
                if (!stralloc_copys(&retmsg, rhs_err.s)) die_nomem();
                if (!stralloc_cats(&retmsg, ": ")) die_nomem();
                if (!stralloc_cats(&retmsg, rblmatch)) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;

              /* domain listed in some of the queried DNSbls (temporary) */
              case RBL_TEMPERROR:
                strerr_warn12("qmail-smtpd: id ", qmailid.s, " 450 ACL_RHS_SOFTFAIL ",
                              dompos + j, " ", cp,
                              " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                              " to ", safercptto.s, 0);
                if (!stralloc_copys(&retmsg, "450 ACL_RHS_SOFTFAIL ")) die_nomem();
                if (!stralloc_cats(&retmsg, dompos + j)) die_nomem();
                if (!stralloc_cats(&retmsg, " ")) die_nomem();
                if (!stralloc_cats(&retmsg, cp)) die_nomem();
                if (!stralloc_0(&retmsg)) die_nomem();
                *ret = retmsg.s;
                return ACL_FAIL;
            }
            break;
          }
          break;

        case 'S':
          if (recipcount != 0) {
            strerr_warn8("qmail-smtpd: id ", qmailid.s, " 452 ACL_SINGLERECIP from ",
                         safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "452 RCPT TO limit exceeded (#4.7.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case 'T':
          switch(aestrap_verify(&addr, &aestrap_info_curr, "control/aestrap",
                 *cp ? cp : "default", &rblmatchaes, &expiresec, &aestrap_ip)) {
            case AESTRAP_TEMP:
              strerr_warn8("qmail-smtpd: id ", qmailid.s, " 451 ACL_AESTRAP_DENY_TEMP from ",
                           safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
              if (!stralloc_copys(&retmsg, "451 temporary error (#4.2.0)")) die_nomem();
              if (!stralloc_0(&retmsg)) die_nomem();
              *ret = retmsg.s;
              return ACL_FAIL;

            case AESTRAP_FAIL:
              strerr_warn8("qmail-smtpd: id ", qmailid.s, " 553 ACL_AESTRAP_DENY_HARD from ",
                           safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
              if (!stralloc_copys(&retmsg, "553 bad recipient (#5.7.1)")) die_nomem();
              if (!stralloc_0(&retmsg)) die_nomem();
              *ret = retmsg.s;
              return ACL_FAIL;

            case AESTRAP_EXPIRED:
              snum[fmt_uint(snum, (now() - expiresec))] = 0;
              strerr_warn11("qmail-smtpd: id ", qmailid.s, " 553 ACL_AESTRAP_DENY_HARD_EXPIRED from ",
                            safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s,
                            " (", aestrap_info_curr.s, ")", 0);
              if (!stralloc_copys(&retmsg, "553 sorry, mailbox expired ")) die_nomem();
              if (!stralloc_cats(&retmsg, snum)) die_nomem();
              if (!stralloc_cats(&retmsg, " seconds ago (#5.7.1)")) die_nomem();
              if (!stralloc_0(&retmsg)) die_nomem();
              *ret = retmsg.s;
              return ACL_FAIL;

            case AESTRAP_DNSBL:
              strerr_warn12("qmail-smtpd: id ", qmailid.s,
                            " 553 ACL_AESTRAP_DENY_HARD_DNSBL ",
                            aestrap_ip, " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                            " to ", safercptto.s, ": ", rblmatchaes, 0);
              if (!stralloc_copys(&retmsg, "553 bad recipient (#5.7.1)")) die_nomem();
              if (!stralloc_0(&retmsg)) die_nomem();
              *ret = retmsg.s;
              return ACL_FAIL;

            case AESTRAP_OK:
              if (!stralloc_cats(&aestrap_info, "X-aestrap: ")) die_nomem();
              if (!stralloc_cats(&aestrap_info, safercptto.s)) die_nomem();
              if (!stralloc_cats(&aestrap_info, " ")) die_nomem();
              if (!stralloc_cats(&aestrap_info, aestrap_info_curr.s)) die_nomem();
              if (!stralloc_cats(&aestrap_info, "\n")) die_nomem();
              flagaestrap = 1;
              break;

            default:
              break;
          }

        case 'V':
          break;

        default:
          break;
      }
    } else if ((ch == 'C')) {
      int matched = 0;

      while ((cp < cpend) && !matched) {
        unsigned char which;

        ch = *cp++;
        if (cp >= cpend) break;
        if (!*cp) break;
        if (ch == ' ') break;

        which = *cp++;
        if (cp >= cpend) break;
        if (!*cp) break;
        if (which == ' ') break;
        which -= '0';
        if (which >= 2) continue;

        switch (ch) {
          case 'D':
            if (which == 0) {
              /* DNS broken? */
              if (env_get("DNSSOFT") || env_get("DNSHARD")) {
                matched = 1;
              }
            } else if (which == 1) {
              /* DNS OK? */
              if (!env_get("DNSSOFT") && !env_get("DNSHARD")) {
                matched = 1;
              }
            }
            break;

          case 'H':
            if (which == 0) {
              /* HELO not OK? */
              if (!(flagbadhelo & (ACL_HELO_FASCIST_OK | ACL_HELO_IGNORE))) {
                matched = 1;
              }
            } else if (which == 1) {
              /* HELO OK? */
              if (flagbadhelo & (ACL_HELO_FASCIST_OK | ACL_HELO_IGNORE)) {
                matched = 1;
              }
            }
            break;

          case 'S':
            if (which == 0) {
              /* SMTP SIZE == 0? */
              if (smtpsize == 0) {
                matched = 1;
              }
            } else if (ch == '1') {
              /* SMTP SIZE != 0? */
              if (smtpsize != 0) {
                matched = 1;
              }
            }
            break;
        }
        if (matched) {
          while ((cp < cpend) && (*cp != ' ') && (*cp != 0)) {
            *cp++ = 0;
          }
          *cp = 0;
        }
      }
    } else if ((ch == 'H')) {
      ch = *cp++;
      if (cp >= cpend) break;
      switch (ch) {
        case '2':
          if (flagbadhelo & ACL_HELO_SOFT) {
            strerr_warn10("qmail-smtpd: id ", qmailid.s, " 451 ACL_HELO_SOFT ",
                          gen_safestr(helohost.s, MAXLOGLEN),
                          " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                          " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "451 temporary error while "
               "resolving HELO domain ")) die_nomem();
            if (!stralloc_cats(&retmsg, helohost.s)) die_nomem();
            if (!stralloc_cats(&retmsg, " (#4.0.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '3':
          if (flagbadhelo & ACL_HELO_DNS_HARD) {
            strerr_warn10("qmail-smtpd: id ", qmailid.s, " 550 ACL_HELO_DNS_HARD ",
                          gen_safestr(helohost.s, MAXLOGLEN),
                          " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                          " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "550 permanent error while "
               "resolving HELO domain ")) die_nomem();
            if (!stralloc_cats(&retmsg, helohost.s)) die_nomem();
            if (!stralloc_cats(&retmsg, " (#5.0.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case 'O':
        case '4':
          if (flagbadhelo & (ACL_HELO_BADIP | ACL_HELO_BADIP_OWN)) {
            strerr_warn10("qmail-smtpd: id ", qmailid.s, (flagbadhelo & ACL_HELO_BADIP_OWN)
                            ? " 550 ACL_HELO_BADIP_OWN "
                            : " 550 ACL_HELO_BADIP ",
                          gen_safestr(helohost.s, MAXLOGLEN),
                          " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                          " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "550 bad IP ")) die_nomem();
            if (!stralloc_cats(&retmsg, helohost.s)) die_nomem();
            if (!stralloc_cats(&retmsg, " in HELO argument (#5.0.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '5':
          if (flagbadhelo & ACL_HELO_NONFQDN) {
            strerr_warn10("qmail-smtpd: id ", qmailid.s, " 550 ACL_HELO_NONFQDN ",
                          gen_safestr(helohost.s, MAXLOGLEN),
                          " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                          " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "550 must give fully-qualified "
               "HELO domain (#5.0.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '6':
          if (flagbadhelo & ACL_HELO_STRICT) {
            strerr_warn10("qmail-smtpd: id ", qmailid.s, " 550 ACL_HELO_STRICT ",
                          gen_safestr(helohost.s, MAXLOGLEN),
                          " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                          " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "550 your HELO domain ")) die_nomem();
            if (!stralloc_cats(&retmsg, helohost.s)) die_nomem();
            if (!stralloc_cats(&retmsg, " does not have A record ")) die_nomem();
            if (!stralloc_cats(&retmsg, remoteip)) die_nomem();
            if (!stralloc_cats(&retmsg, " (#5.0.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '7':
          if (flagbadhelo & ACL_HELO_SPOOF) {
            strerr_warn10("qmail-smtpd: id ", qmailid.s, " 550 ACL_HELO_SPOOF ",
                          gen_safestr(helohost.s, MAXLOGLEN),
                          " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                          " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "550 HELO domain must be your "
               "hostname, not mine (#5.0.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        /* helohost == mailbox */
        /* careful: do not enable this check for mailbox called "hotmail.com"... :) */
        case '8':
          atpos = byte_rchr(addr.s, addr.len, '@');
          if ((atpos < addr.len) && (atpos == helohost.len-1) &&
              !(flagbadhelo & (ACL_HELO_FASCIST_OK | ACL_HELO_IGNORE)) &&
              (case_diffb(addr.s, atpos, helohost.s) == 0)) {
            strerr_warn8("qmail-smtpd: id ", qmailid.s, " 550 ACL_HELO_MAILBOX from ",
                         safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "550 HELO domain must be your "
               "hostname, not victim's mailbox (#5.0.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        /* invalid use of SMTP PIPELINING */
        case '9':
          if (flagbadhelo & ACL_HELO_PIPELINE) {
            strerr_warn8("qmail-smtpd: id ", qmailid.s, " 550 ACL_PIPELINE_HELO from ",
                         safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "550 invalid use of SMTP PIPELINING with HELO/EHLO (#5.4.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        default:
          break;
      }
    } else if (ch == 'L') {
      ch = *cp++;
      if (cp >= cpend) break;
      switch (ch) {
        case '1':
          if (decision & ACL_RBLSMTPD_TEMP) {
            if (!stralloc_copys(&retmsg, "451 RBLSMTPD_TEMP match for ")) die_nomem();
            if (!stralloc_cats(&retmsg, remoteip)) die_nomem();
            if (!stralloc_cats(&retmsg, ": ")) die_nomem();
            if (!stralloc_cats(&retmsg, rbltext)) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            strerr_warn10("qmail-smtpd: id ", qmailid.s, " 451 ACL_RBLSMTPD_TEMP from ",
                          safemailfrom.s, " SIZE=", smtpsize_s,
                          " to ", safercptto.s, ": ", rbltext, 0);
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '2':
          if (decision & ACL_RBLSMTPD_PERM) {
            if (!stralloc_copys(&retmsg, "553 RBLSMTPD_PERM match for ")) die_nomem();
            if (!stralloc_cats(&retmsg, remoteip)) die_nomem();
            if (!stralloc_cats(&retmsg, ": ")) die_nomem();
            if (!stralloc_cats(&retmsg, rbltext)) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            strerr_warn10("qmail-smtpd: id ", qmailid.s, " 553 ACL_RBLSMTPD_PERM from ",
                          safemailfrom.s, " SIZE=", smtpsize_s,
                          " to ", safercptto.s, ": ", rbltext, 0);
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '3':
          if (decision & ACL_RBLSMTPD_WL) {
            flagout = 1;
          }
          break;

         default:
           break;
      }
    } else if (ch == 'S') {
      uint64 tmpsz;

      if (scan_ullong(cp, &tmpsz)) {
        if ((tmpsz < acl_maxsz) || !acl_maxsz) acl_maxsz = tmpsz;
        acl_maxsz_s[fmt_ullong(acl_maxsz_s, acl_maxsz)] = 0;
        if (smtpsize > acl_maxsz) {
          strerr_warn10("qmail-smtpd: id ", qmailid.s, " 552 ACL_SIZELIMIT from ",
                        safemailfrom.s, " to ", safercptto.s, ": ",
                        smtpsize_s, " > ", acl_maxsz_s, 0);
          if (!stralloc_copys(&retmsg, "552 MAIL SIZE=")) die_nomem();
          if (!stralloc_cats(&retmsg, smtpsize_s)) die_nomem();
          if (!stralloc_cats(&retmsg, " exceeds my limit of ")) die_nomem();
          if (!stralloc_cats(&retmsg, acl_maxsz_s)) die_nomem();
          if (!stralloc_cats(&retmsg, " (#5.3.4)")) die_nomem();
          if (!stralloc_0(&retmsg)) die_nomem();
          *ret = retmsg.s;
          return ACL_FAIL;
        }
      }
    } else if (ch == '2') {
      if (!stralloc_copys(&retmsg, cp-1)) die_nomem();
      if (!stralloc_0(&retmsg)) die_nomem();
      *ret = retmsg.s;
      goto checksmtpsize;
    } else if ((ch == '4') || (ch == '5')) {
      char *tmpcp = cp-1;

      if ((tmpcp[1] >= '0') && (tmpcp[1] <= '9') && (tmpcp[2] >= '0') && (tmpcp[2] <= '9')) {
        if ((tmpcp[3] == '\0') && rbltext && rbltext[0]) tmpcp = rbltext;
        strerr_warn10("qmail-smtpd: id ", qmailid.s,
                      (ch == '4') ? " 400 ACL_TEMP_RCPT from " : " 500 ACL_PERM_RCPT from ",
                      safemailfrom.s, " SIZE=", smtpsize_s,
                      " to ", safercptto.s, ": ", tmpcp, 0);
        if (tmpcp == rbltext) {
          if (!stralloc_copyb(&retmsg, cp-1, 3)) die_nomem();
          if (!stralloc_cats(&retmsg, " ")) die_nomem();
          if (!stralloc_cats(&retmsg, tmpcp)) die_nomem();
        } else {
          if (!stralloc_copys(&retmsg, tmpcp)) die_nomem();
        }
        if (!stralloc_0(&retmsg)) die_nomem();
        *ret = retmsg.s;
        return ACL_FAIL;
      }
    } else if (ch == 'D') {
      /* unknown and not whitelisted or unrbl'ed, do rblcheck */
      if ((rblok & 1)) {
        switch(rblcheck("rbl", cp, remoteip, &dnsmsg)) {
          /* remoteip is whitelisted */
          case RBL_WHITELIST:
          case RBL_WHITELISTP:
            flagout = 1;
            break;

          /* remoteip is listed in some of the queried DNSbls (temporary) */
          case RBL_REJECT:
            strerr_warn12("qmail-smtpd: id ", qmailid.s, " 553 ACL_DNS denied ", remoteip,
                          " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                          " to ", safercptto.s, ": ", dnsmsg, 0);
            if (!stralloc_copys(&retmsg, "553 DNSBL match for ")) die_nomem();
            if (!stralloc_cats(&retmsg, remoteip)) die_nomem();
            if (!stralloc_cats(&retmsg, ": ")) die_nomem();
            if (!stralloc_cats(&retmsg, dnsmsg)) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;

          /* remoteip is listed in some of the queried DNSbls */
          case RBL_REJECTTEMP:
            strerr_warn12("qmail-smtpd: id ", qmailid.s, " 450 ACL_DNS denied ", remoteip,
                          " from ", safemailfrom.s, " SIZE=", smtpsize_s,
                          " to ", safercptto.s, ": ", dnsmsg, 0);
            if (!stralloc_copys(&retmsg, "450 DNSBL match for ")) die_nomem();
            if (!stralloc_cats(&retmsg, remoteip)) die_nomem();
            if (!stralloc_cats(&retmsg, ": ")) die_nomem();
            if (!stralloc_cats(&retmsg, dnsmsg)) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;

          /* soft error, listed on whitelisted basedomain or not found in any DNSbl/wl */
          case RBL_OK:
            break;

          /* trouble with rbl setup */
          case RBL_ERR:
            strerr_warn12("qmail-smtpd: id ", qmailid.s, " 450 ACL_DNS setup problem ",
                          cp, " ", remoteip, " from ", safemailfrom.s,
                          " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "450 DNSBL problem")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;

          case RBL_WHITELISTF:
            strerr_warn12("qmail-smtpd: id ", qmailid.s, " 450 ACL_DNS_WHITELISTF ",
                          cp, " ", remoteip, " from ", safemailfrom.s,
                          " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
            if (!stralloc_copys(&retmsg, "450 DNSBL problem")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
        }
      }
    } else if (ch == 'Q') {
      bogowanted = *cp++;
      if (cp >= cpend) break;
    }
    /* skip to next candidate */
    while (*cp++);
    if (cp >= cpend) break;
  }

  if (!stralloc_copys(&retmsg, "250 ok")) die_nomem();
  if (!stralloc_0(&retmsg)) die_nomem();
  *ret = retmsg.s;

checksmtpsize:
  if (databytes && (smtpsize > databytes)) {
    strerr_warn10("qmail-smtpd: id ", qmailid.s, " 552 ACL_DATABYTES from ",
                  safemailfrom.s, " to ", safercptto.s, ": ",
                  smtpsize_s, " > ", databytes_s, 0);
    if (!stralloc_copys(&retmsg, "552 MAIL SIZE=")) die_nomem();
    if (!stralloc_cats(&retmsg, smtpsize_s)) die_nomem();
    if (!stralloc_cats(&retmsg, " exceeds my limit of ")) die_nomem();
    if (!stralloc_cats(&retmsg, databytes_s)) die_nomem();
    if (!stralloc_cats(&retmsg, " (#5.3.4)")) die_nomem();
    if (!stralloc_0(&retmsg)) die_nomem();
    *ret = retmsg.s;
    return ACL_FAIL;
  }

  return ACL_OK;
}

static int contentscan_disabled(char ch)
{
  char *cp;
  char *cpend;
  char chtmp;

  cp = acl_checks.s;
  cpend = acl_checks.s + acl_checks.len;
  while ((cp < cpend)) {
    chtmp = *cp++;
    if (cp >= cpend) break;
    if (chtmp == '\0') continue;
    if (chtmp == 'L') {
      chtmp = *cp++;
      if (cp >= cpend) break;
      if (chtmp == '\0') continue;
      if (chtmp == '3') {
        if (decision & ACL_RBLSMTPD_WL) return 1;
      }
    } else if (chtmp == 'X') {
      chtmp = *cp++;
      if (cp >= cpend) break;
      if (chtmp == '\0') continue;
      if (chtmp == ch) return 0;
    }
    /* skip to next candidate */
    while (*cp) cp++;
  }
  /* no X[ch] found */
  return 1;
}

#ifdef PCRE_SMTPD

static int acl_pcre_exec(int headertype, stralloc* matchline, char **retmsg)
{
  int ret;
  static stralloc str_ret = {0};
  char *tmperr;
  int sublen;
  char str_num1[FMT_ULONG];
  uint64 tick_start, tick_end;
  char htype;
  struct acl_pcre_state_t *pcreptr;
  stralloc *str_pcre;
  size_t pcres;
  char *p;

  if (pcredebug > 2) tick_start = rdtscl();

  switch (headertype & ACL_PCRE_MATCHMASK) {
    case ACL_PCRE_BODY:
      str_pcre = &pcre_active_h;
      break;
    case ACL_PCRE_BODYB:
      str_pcre = &pcre_active_H;
      break;
    case ACL_PCRE_MIME:
      str_pcre = &pcre_active_m;
      break;
    case ACL_PCRE_MIMEB:
      str_pcre = &pcre_active_M;
      break;
    case ACL_PCRE_RFC822:
      str_pcre = &pcre_active_r;
      break;
    case ACL_PCRE_RFC822B:
      str_pcre = &pcre_active_R;
      break;
    default:
      return ACL_PCRE_NOMATCH;
  }

  pcres = str_pcre->len / sizeof(pcreptr);
  p = str_pcre->s;
  while (pcres) {
    byte_copy(&pcreptr, sizeof(pcreptr), p);
    p += sizeof(pcreptr);
    pcres--;

    if ((pcreptr->matchon & headertype) != headertype) continue;
    ret = pcre_exec(pcreptr->pcre_pattern, pcreptr->pcre_hints,
                    matchline->s, matchline->len, 0, 0, ovec, OVECSZ);
    if (ret >= 0) {
      if (pcreptr->matchon & ACL_PCRE_WL) {
        /* whitelist pattern, bail out */
        acl_pcre_status = 0;
        return ACL_PCRE_NOMATCH;
      }
      if (pcreptr->matchon & ACL_PCRE_WL_REQ) {
        /* required pattern matched, mark as seen and continue */
        pcreptr->matchon = 0;
        continue;
      }
      if (!stralloc_copys(&str_ret, "")) die_nomem();
      if (pcredebug > 0) {
        if (!stralloc_cats(&str_ret, "\"")) die_nomem();
        if (ret == 0) {
          if (!stralloc_cats(&str_ret, "?")) die_nomem();
        } else {
          /* log only first match */
          sublen = (ovec[1] - ovec[0]);
          if (sublen > ACL_PCRE_MAX_LOGLEN)
            sublen = ACL_PCRE_MAX_LOGLEN;
          if (!stralloc_cats(&str_ret, gen_safestr2(matchline->s + ovec[0], sublen)))
            die_nomem();
          if (!stralloc_cats(&str_ret, "\" ")) die_nomem();
        }
      }
      if (pcreptr->flags.dfl_match) {
        if (!stralloc_catb(&str_ret, pcre_reject_dfl.s,
                           pcre_reject_dfl.len-1)) die_nomem();
      } else {
        if (!stralloc_catb(&str_ret, pcreptr->matchtext.s,
                           pcreptr->matchtext.len)) die_nomem();
      }
      if (!stralloc_0(&str_ret)) die_nomem();
      *retmsg = str_ret.s;
      if (pcredebug > 2) {
        strerr_warn4("qmail-smtpd: id ", qmailid.s,
                     " DEBUG_ACL_PCRE pcre_exec matched ", str_ret.s, 0);
      }
      return (pcreptr->matchon & ACL_PCRE_KILL) ?
              ACL_PCRE_MATCH_KILL : ACL_PCRE_MATCH;
    }

    switch (ret) {
      case PCRE_ERROR_UNKNOWN_NODE: tmperr = "internal PCRE error"; break;
      case PCRE_ERROR_NOMEMORY: tmperr = "out of memory";           break;
      case PCRE_ERROR_MATCHLIMIT: tmperr = "match limit reached";   break;
      case PCRE_ERROR_NOMATCH:
        continue;
      default:
        if (pcredebug > 1) {
          /* flood prevention */
          static datetime_sec last_pcrelog = 0;

          if ((last_pcrelog == 0) || (now() != last_pcrelog)) {
            last_pcrelog = now();
            str_num1[fmt_ulong(str_num1, -ret)] = 0;
            sublen = matchline->len;
            if (sublen > ACL_PCRE_MAX_LOGLEN)
              sublen = ACL_PCRE_MAX_LOGLEN;
            strerr_warn7("qmail-smtpd: id ", qmailid.s,
                         " DEBUG_ACL_PCRE pcre_exec error -", str_num1,
                         ", string=\"",
                         gen_safestr2(matchline->s, sublen), "\"", 0);
          }
        }
        continue;
    }
    if (!stralloc_copys(&str_ret, tmperr)) die_nomem();
    if (!stralloc_0(&str_ret)) die_nomem();
    *retmsg = str_ret.s;
    return ACL_PCRE_TEMP;
  }
  if (pcredebug > 2) {
    tick_end = rdtscl();
    str_num1[fmt_ullong(str_num1, tick_end-tick_start)] = 0;
    switch (headertype) {
      case ACL_PCRE_BODY:    htype = 'h'; break;
      case ACL_PCRE_BODYB:   htype = 'H'; break;
      case ACL_PCRE_MIME:    htype = 'm'; break;
      case ACL_PCRE_MIMEB:   htype = 'M'; break;
      case ACL_PCRE_RFC822:  htype = 'r'; break;
      case ACL_PCRE_RFC822B: htype = 'R'; break;
    }
    substdio_puts(subfderr, "qmail-smtpd: id ");
    substdio_puts(subfderr, qmailid.s);
    substdio_puts(subfderr, " DEBUG_ACL_PCRE acl_pcre_match ");
    substdio_put(subfderr, &htype, 1);
    substdio_puts(subfderr, " took ");
    substdio_puts(subfderr, str_num1);
    substdio_puts(subfderr, " ticks [");
    sublen = matchline->len;
    if (sublen > ACL_PCRE_MAX_LOGLEN)
      sublen = ACL_PCRE_MAX_LOGLEN;
    /* '\0' truncates output... */
    substdio_puts(subfderr, gen_safestr(matchline->s, sublen));
    substdio_puts(subfderr, "]\n");
    substdio_flush(subfderr);
  }

  return ACL_PCRE_NOMATCH;
}

static void acl_pcre_match(int headertype, stralloc* matchline)
{
  char *retmsg;
  char *strheader;
  int ret;

  if (acl_pcre_status != 1) return;

  ret = acl_pcre_exec(headertype, matchline, &retmsg);
  switch (ret) {
    case ACL_PCRE_TEMP:
      flagcontent |= ACL_CONTENT_PCRE_TEMP;
      if (!stralloc_copys(&pcre_line_match, "ACL_CONTENT_PCRE_TEMP: ")) die_nomem();
      if (!stralloc_cats(&pcre_line_match, retmsg)) die_nomem();
      if (!stralloc_0(&pcre_line_match)) die_nomem();
      break;

    case ACL_PCRE_MATCH_KILL:
    case ACL_PCRE_MATCH:
      flagcontent |= ACL_CONTENT_PCRE_MATCH;
      switch (headertype) {
        case ACL_PCRE_BODY:    strheader = "ACL_CONTENT_PCRE_MATCH_BODY";    break;
        case ACL_PCRE_BODYB:   strheader = "ACL_CONTENT_PCRE_MATCH_BODYB";   break;
        case ACL_PCRE_MIME:    strheader = "ACL_CONTENT_PCRE_MATCH_MIME";    break;
        case ACL_PCRE_MIMEB:   strheader = "ACL_CONTENT_PCRE_MATCH_MIMEB";   break;
        case ACL_PCRE_RFC822:  strheader = "ACL_CONTENT_PCRE_MATCH_RFC822";  break;
        case ACL_PCRE_RFC822B: strheader = "ACL_CONTENT_PCRE_MATCH_RFC822B"; break;
        default: strheader = "ACL_CONTENT_PCRE_MATCH_UNKNOWN"; break;
      }
      if (!stralloc_copys(&pcre_line_match, strheader)) die_nomem();
      if (!stralloc_cats(&pcre_line_match, " matched ")) die_nomem();
      if (!stralloc_cats(&pcre_line_match, retmsg)) die_nomem();
      if (!stralloc_0(&pcre_line_match)) die_nomem();

      if (ret == ACL_PCRE_MATCH_KILL) {
        strerr_warn4("qmail-smtpd: id ", qmailid.s, "EXIT_PCRE_MATCH_KILL ",
                     pcre_line_match.s, 0);
        _exit(1);
      } else {
        break;
      }

    default:
      break;
  }
  return;
}

static char pcreinbuf[1024];
static substdio pcressin;

/* gathers 'P' ACLs and initializes corresponding control/pcre/ files
   (file ``control/pcre/foo'' for 'Pfoo' ACL) */
static int acl_pcre_setup(void)
{
  char *cp, *cpend, *cpln, *quotdst;
  const char *pcreerr;
  int pcreerr_off;
  unsigned char ch, chtmp;
  int fd, match, flagesc;
  unsigned long int len;
  int pcreopt, linenr, states;
  unsigned long matchon;
  static stralloc pcrefn = {0};
  static stralloc ln = {0};
  char str_num1[FMT_ULONG];
  char str_num2[FMT_ULONG];
  uint64 tick_start, tick_end;
  struct list_head *pos1, *pos2;
  struct acl_pcre_state_t *pcreptr;

  if (!flagpcre) return 0;

  tick_start = rdtscl();
  list_for_each_safe(pos1, pos2, &pcre_active) {
    pcreptr = list_entry(pos1, struct acl_pcre_state_t, list);
    alloc_free(pcreptr->pcre_pattern);
    pcreptr->pcre_pattern = 0;
    if (pcreptr->pcre_hints) {
      alloc_free(pcreptr->pcre_hints);
      pcreptr->pcre_hints = 0;
    }
    if (!stralloc_copys(&pcreptr->matchtext, "")) die_nomem();
    list_move_tail(&pcreptr->list, &pcre_inactive);
  }

  if (contentscan_disabled('P')) {
    if (pcredebug > 1) {
      strerr_warn3("qmail-smtpd: id ", qmailid.s, " DEBUG_ACL_PCRE disabled", 0);
    }
    return 0;
  }

  pcre_active_h.len = 0;
  pcre_active_H.len = 0;
  pcre_active_m.len = 0;
  pcre_active_M.len = 0;
  pcre_active_r.len = 0;
  pcre_active_R.len = 0;

  cp = acl_checks.s;
  cpend = acl_checks.s + acl_checks.len;
  while ((cp < cpend)) {
    ch = *cp++;
    if (cp >= cpend) break;
    if (ch == '\0') continue;

    if (ch == 'P') {
      if (cp >= cpend) break;
      if (ch == '\0') continue;

      /* P0 disables PCRE completely */
      if (*cp == '0') return 0;
      if (!stralloc_copys(&pcrefn, "control/pcre/")) return -1;
      if (!stralloc_cats(&pcrefn, cp)) return -1;
      if (!stralloc_0(&pcrefn)) return -1;

      fd = open_read(pcrefn.s);
      if (fd == -1) {
        if (errno == error_noent)
          continue;
        else
          return -1;
      }

      linenr = 0;
      states = 0;
      substdio_fdbuf(&pcressin, read, fd, pcreinbuf, sizeof(pcreinbuf));
      for (;;) {
        if (getln(&pcressin, &ln, &match, '\n') != 0) { (void)close(fd); return -1; }
        linenr++;
        if (!match) break;
        if (byte_chr(ln.s, ln.len, '\0') < ln.len) break;
        if ((ln.len <= 5) || (ln.s[0] != '/')) continue;

        /* start and end of pattern -- use \/ to include / in pattern,
           otherwise string is not altered */
        cpln = ln.s+1;
        len = ln.len-1;
        flagesc = 0;
        quotdst = cpln;
        while (len) {
          chtmp = *cpln;
          cpln++;
          len--;
          if (flagesc) {
            if (chtmp != '/') { *quotdst = '\\'; quotdst++; }
            *quotdst = chtmp;
            quotdst++;
            flagesc = 0;
            continue;
          }
          if (chtmp == '\\') { flagesc = 1; continue; }
          if (chtmp == '/') { *quotdst = '\0'; break; }
          *quotdst = chtmp;
          quotdst++;
        }

        /* PCRE options */
        pcreopt = 0;
        while (len && ((*cpln != '\t') && (*cpln != ' '))) {
          switch (*cpln) {
            case 'i': pcreopt |= PCRE_CASELESS; break;
            case 'u': pcreopt |= PCRE_UTF8;     break;
            case 's': pcreopt |= PCRE_DOTALL;   break;
            case 'x': pcreopt |= PCRE_EXTENDED; break;
            case 'U': pcreopt |= PCRE_UNGREEDY; break;
            default:
              if (pcredebug > 2) {
                char badopt[2];

                badopt[0] = *cpln;
                badopt[1] = '\0';
                str_num1[fmt_ulong(str_num1, linenr)] = 0;
                strerr_warn8("qmail-smtpd: id ", qmailid.s,
                             " DEBUG_ACL_PCRE ignored bad option ",
                             gen_safestr(badopt, 1),
                             " in ", pcrefn.s, ":", str_num1, 0);
              }
              break;
          }
          len--;
          cpln++;
        }
        while (len && (*cpln == '\t' || *cpln == ' ')) { len--; cpln++; }
        if (len == 0) continue;

        /* matchon */
        matchon = 0;
        while (len && ((*cpln != '\t') && (*cpln != ' '))) {
          switch (*cpln) {
            case 'h': matchon |= ACL_PCRE_BODY;    break;
            case 'H': matchon |= ACL_PCRE_BODYB;   break;
            case 'm': matchon |= ACL_PCRE_MIME;    break;
            case 'M': matchon |= ACL_PCRE_MIMEB;   break;
            case 'r': matchon |= ACL_PCRE_RFC822;  break;
            case 'R': matchon |= ACL_PCRE_RFC822B; break;
            case 'k': matchon |= ACL_PCRE_KILL;    break;
            /* if this pattern matches, further PCRE patterns are skipped */
            case 'w': matchon |= ACL_PCRE_WL;      break;
            /* this pattern is REQUIRED to be seen or message is rejected.
               this does not imply 'w' */
            case 'W': matchon |= ACL_PCRE_WL_REQ;  break;
            default:
              if (pcredebug > 2) {
                char badmatchon[2];

                badmatchon[0] = *cpln;
                badmatchon[1] = '\0';
                str_num1[fmt_ulong(str_num1, linenr)] = 0;
                strerr_warn8("qmail-smtpd: id ", qmailid.s,
                             " DEBUG_ACL_PCRE ignored bad matchon ",
                             gen_safestr(badmatchon, 1),
                             " in ", pcrefn.s, ":", str_num1, 0);
              }
              break;
          }
          len--;
          cpln++;
        }
        /* Require at least one of hmrHMR */
        if ((matchon & ACL_PCRE_MATCHMASK) == 0) continue;

        if (list_empty(&pcre_inactive)) {
          pcreptr = (struct acl_pcre_state_t*) alloc(sizeof(struct acl_pcre_state_t));
          if (!pcreptr) die_nomem();
          byte_zero(pcreptr, sizeof(*pcreptr));
          if (!stralloc_copys(&pcreptr->matchtext, "")) die_nomem();
          list_add_tail(&pcreptr->list, &pcre_inactive);
        }
        pcreptr = list_entry(pcre_inactive.next, struct acl_pcre_state_t, list);
        pcreptr->matchon = matchon;

        while (len && (*cpln == '\t' || *cpln == ' ')) { len--; cpln++; }
        while (len) {
          if (cpln[len-1] == ' ') { len--; continue; }
          if (cpln[len-1] == '\t') { len--; continue; }
          if (cpln[len-1] == '\r') { len--; continue; }
          if (cpln[len-1] == '\n') { len--; continue; }
          if (cpln[len-1] == '\0') { len--; continue; }
          break;
        }

        /* rejection message for logging */
        if (len) {
          if (!stralloc_copyb(&pcreptr->matchtext, cpln, len)) die_nomem();
          pcreptr->flags.dfl_match = 0;
        } else {
          pcreptr->flags.dfl_match = 1;
        }

        errno = 0;
        pcreptr->pcre_pattern = pcre_compile(&ln.s[1], pcreopt, &pcreerr,
                                             &pcreerr_off, pcre_tables);
        if (!pcreptr->pcre_pattern) {
          /* ICK! */
          if (errno == error_nomem) die_nomem();
          if (pcredebug > 1) {
            str_num1[fmt_ulong(str_num1, linenr)] = 0;
            str_num2[fmt_ulong(str_num2, pcreerr_off)] = 0;
            strerr_warn11("qmail-smtpd: id ", qmailid.s,
                         " DEBUG_ACL_PCRE pcre_compile error in ", pcrefn.s, ":",
                         str_num1, ":", str_num2, ": \"", pcreerr, "\"", 0);
          }
          continue;
        }
        errno = 0;
        pcreptr->pcre_hints = pcre_study(pcreptr->pcre_pattern, 0, &pcreerr);
        if (pcreerr != NULL) {
          if (errno == error_nomem) die_nomem();
          alloc_free(pcreptr->pcre_pattern);
          if (pcredebug > 1) {
            str_num1[fmt_ulong(str_num1, linenr)] = 0;
            strerr_warn9("qmail-smtpd: id ", qmailid.s,
                         " DEBUG_ACL_PCRE pcre_study error in ", pcrefn.s, ":",
                         str_num1, ": \"", pcreerr, "\"", 0);
          }
          continue;
        }

        list_move_tail(&pcreptr->list, &pcre_active);

        if (matchon & ACL_PCRE_BODY)
          if (!stralloc_catb(&pcre_active_h, &pcreptr, sizeof(pcreptr))) die_nomem();
        if (matchon & ACL_PCRE_BODYB)
          if (!stralloc_catb(&pcre_active_H, &pcreptr, sizeof(pcreptr))) die_nomem();
        if (matchon & ACL_PCRE_MIME)
          if (!stralloc_catb(&pcre_active_m, &pcreptr, sizeof(pcreptr))) die_nomem();
        if (matchon & ACL_PCRE_MIMEB)
          if (!stralloc_catb(&pcre_active_M, &pcreptr, sizeof(pcreptr))) die_nomem();
        if (matchon & ACL_PCRE_RFC822)
          if (!stralloc_catb(&pcre_active_r, &pcreptr, sizeof(pcreptr))) die_nomem();
        if (matchon & ACL_PCRE_RFC822B)
          if (!stralloc_catb(&pcre_active_R, &pcreptr, sizeof(pcreptr))) die_nomem();

        states++;
      }

      tick_end = rdtscl();
      (void)close(fd);
      if ((pcredebug > 1) && (states >= 1)) {
        str_num1[fmt_ulong(str_num1, states)] = 0;
        str_num2[fmt_ullong(str_num2, tick_end-tick_start)] = 0;
        strerr_warn9("qmail-smtpd: id ", qmailid.s,
                     " DEBUG_ACL_PCRE acl_pcre_setup accepted ", str_num1,
                     (states == 1) ? " PCRE from " : " PCREs from ", pcrefn.s,
                     " (", str_num2, " ticks)", 0);
      }
    }
    /* skip to next candidate */
    while (*cp) cp++;
    if (cp >= cpend) break;
  }

  return (!list_empty(&pcre_active));
}

#if PCRE_PUT_DEBUG
static void num_puts(char *num_name, long num, int padlen)
{
  char str_num[FMT_ULONG];
  char ch_sign;

  if (num < 0) {
    ch_sign = '-';
    num = -num;
  } else {
    ch_sign = ' ';
  }
  str_num[fmt_uint0(str_num, num, padlen)] = 0;
  substdio_puts(subfderr, num_name);
  substdio_puts(subfderr, "=");
  substdio_put(subfderr, &ch_sign, 1);
  substdio_puts(subfderr, str_num);
  substdio_puts(subfderr, " ");
}

static void put_debug(long infopos)
{
  if (pcredebug <= 3) return;
  substdio_puts(subfderr, "qmail-smtpd: id ");
  substdio_puts(subfderr, qmailid.s);
  substdio_puts(subfderr, " VERBOSE_DEBUG_ACL_PCRE put_debug: ");
  num_puts("pos", infopos, 3);
  num_puts("mimenesting", mimenesting, 3);
  num_puts("rfc822nest", rfc822nest, 3);
  num_puts("linestate", linestate, 1);
  num_puts("putinheader", putinheader, 1);
  num_puts("linespastheader", linespastheader, 1);
  num_puts("base64ok", base64ok, 1);
  num_puts("ct_encbody", ct_encbody, 1);
  substdio_puts(subfderr, " ct_enc=");
  if (ct_enc == 'Q') substdio_puts(subfderr, "Q");
  else if (ct_enc == 'B') substdio_puts(subfderr, "B");
  else substdio_puts(subfderr, "?");
  substdio_puts(subfderr, " linetype=");
  substdio_puts(subfderr, (linetype == 'C') ? "C" : "?");
  substdio_puts(subfderr, " ");
  num_puts("flagrfc822", flagrfc822, 1);
  num_puts("flagtext", flagtext, 1);
  num_puts("line.len", line.len, 6);
  num_puts("pcre_line.len", pcre_line.len, 6);
  substdio_puts(subfderr, "\n");
  substdio_flush(subfderr);
}
#else
static inline void put_debug(long infopos)
{
    return;
}
#endif

#else /* !PCRE_SMTPD */

static inline void acl_pcre_match(int headertype, stralloc* matchline)
{
  return;
}

static inline void put_debug(long infopos)
{
  return;
}

#endif /* PCRE_SMTPD */

static char *iprev(char *ipaddy, unsigned int iplen)
{
  static stralloc ip_reverse = {0};
  unsigned int j;

  if (!stralloc_copys(&ip_reverse, "")) die_nomem();

  while (iplen) {
    for (j = iplen; j > 0; --j) {
      if (ipaddy[j - 1] == '.') break;
    }
    if (!stralloc_catb(&ip_reverse, ipaddy + j, iplen - j)) die_nomem();
    if (!stralloc_cats(&ip_reverse, ".")) die_nomem();
    if (!j) break;
    iplen = j - 1;
  }

  if (!stralloc_0(&ip_reverse)) die_nomem();
  return ip_reverse.s;
}

static int acl_sd_domain_lookup(char *dom, unsigned long domlen, datetime_sec giveout,
                                unsigned long matchflags)
{
  int j;
  int ret;
  int nsdomlen;
  int ips;
  static stralloc domainq = {0};
  static stralloc domainout = {0};
  static stralloc nsdomainout = {0};
  unsigned long flags;
  static char iptemp[IPFMT];
  static stralloc domlookfor = {0};
  struct acl_sd_state_t *sdptr;
  struct acl_sd_state_t *sdtmp;
  uint32 domhash;
  stralloc *domptr;
  uint8 *domstart;
  uint8 *domend;
  uint8 tmplen;

  if (domlen > 255) return 0;
  if (domlen == 0) return 0;
  if (!stralloc_copyb(&domlookfor, dom, domlen)) die_nomem();
  case_lowerb(domlookfor.s, domlookfor.len);
  domhash = cdb_hash(domlookfor.s, domlookfor.len);
  domptr = &domcache_htable[domhash & DOMCACHE_HMASK];
  domstart = domptr->s;
  domend = domptr->s + domptr->len;
  while(domstart < domend) {
    tmplen = *domstart;
    domstart++;
    if (tmplen == 0) break;
    if (tmplen != domlen) {
      domstart += tmplen;
      continue;
    }
    if (str_diffn(domlookfor.s, domstart, domlen) == 0) return 0;
    domstart += tmplen; 
  }
  domcache_count++;
  tmplen = domlen;
  if (!stralloc_catb(domptr, &tmplen, 1)) die_nomem();
  if (!stralloc_cat(domptr, &domlookfor)) die_nomem();

  list_for_each_entry_safe(sdptr, sdtmp, &sd_active, list) {
    if (now() > giveout) return -1;
    flags = sdptr->flags;
    if ((flags & matchflags) == 0) continue;

    if (sd_debug > 1) {
      static stralloc currdom = {0};

      if (!stralloc_copyb(&currdom, dom, domlen)) die_nomem();
      if (!stralloc_cats(&currdom, " ")) die_nomem();
      if (!stralloc_cat(&currdom,  &sdptr->nameserver)) die_nomem();
      if (!stralloc_0(&currdom)) die_nomem();
      strerr_warn4("qmail-smtpd: id ", qmailid.s, " DEBUG_SD ", currdom.s, 0);
    }

    switch (flags) {
      case ACL_SD_DOMAIN:
      case ACL_SD_SKIP_DOMAIN:
        if (!stralloc_copyb(&domainq, dom, domlen)) die_nomem();
        if (!stralloc_catb(&domainq, ".", 1)) die_nomem();
        if (!stralloc_cat(&domainq, &sdptr->nameserver)) die_nomem();
        ret = dns_txt(&domainout, &domainq);
        if (!stralloc_0(&domainout)) die_nomem();
        switch (ret) {
          case DNS_SOFT:
          case DNS_MEM:
          case DNS_HARD:
            break;
          default:
            if (flags == ACL_SD_SKIP_DOMAIN) {
              return 0;
            }
            if (!stralloc_copyb(&spamdomain, dom, domlen)) die_nomem();
            if (!stralloc_0(&spamdomain)) die_nomem();
            if (domainout.len > 2) {
              if (!stralloc_cats(&sdptr->comment, " (")) die_nomem();
              if (!stralloc_cats(&sdptr->comment, domainout.s)) die_nomem();
              if (!stralloc_cats(&sdptr->comment, ")")) die_nomem();
            }
            if (!stralloc_0(&sdptr->comment)) die_nomem();
            spamdomain_comment = sdptr->comment;
            flagcontent |= ACL_CONTENT_SD_DOMAIN;
            return 1;
        }
        break;

      case ACL_SD_DOMAIN_IP:
      case ACL_SD_SKIP_DOMAIN_IP:
        if (!stralloc_copyb(&domainq, dom, domlen)) die_nomem();
        ret = dns_ip(&ia_mf, &domainq);
        for (ips = 0; ret == 0 && ips < ia_mf.len; ips++) {
          unsigned int iplen;

          iplen = ip_fmt(iptemp, &ia_mf.ix[ips].ip);
          iptemp[iplen] = 0;
          if (!stralloc_copys(&domainq, iprev(iptemp, iplen))) die_nomem();
          if (!stralloc_catb(&domainq, ".", 1)) die_nomem();
          if (!stralloc_cat(&domainq, &sdptr->nameserver)) die_nomem();
          ret = dns_txt(&nsdomainout, &domainq);
          switch (ret) {
            case DNS_SOFT:
            case DNS_MEM:
            case DNS_HARD:
              break;
            default:
              if (flags == ACL_SD_SKIP_DOMAIN_IP) {
                return 0;
              }
              if (!stralloc_copyb(&spamdomain, dom, domlen)) die_nomem();
              if (!stralloc_0(&spamdomain)) die_nomem();
              spamdomainip = iptemp;
              if (nsdomainout.len > 2) {
                if (!stralloc_cats(&sdptr->comment, " (")) die_nomem();
                if (!stralloc_cat(&sdptr->comment, &nsdomainout)) die_nomem();
                if (!stralloc_cats(&sdptr->comment, ")")) die_nomem();
              }
              if (!stralloc_0(&sdptr->comment)) die_nomem();
              spamdomain_comment = sdptr->comment;
              flagcontent |= ACL_CONTENT_SD_DOMAIN_IP;
              return 1;
          }
          if (now() > giveout) return -1;
        }
        break;

      case ACL_SD_NS:
      case ACL_SD_SKIP_NS:
        if (!stralloc_copyb(&domainq, dom, domlen)) die_nomem();
        ret = dns_ns(&domainout, &domainq);
        switch (ret) {
          case DNS_SOFT:
          case DNS_MEM:
          case DNS_HARD:
            break;
          default:
            j = 0;
            while (j < domainout.len) {
              nsdomlen = str_len(domainout.s + j);
              if (!stralloc_copyb(&domainq, domainout.s + j, nsdomlen)) die_nomem();
              if (!stralloc_catb(&domainq, ".", 1)) die_nomem();
              if (!stralloc_cat(&domainq, &sdptr->nameserver)) die_nomem();
              ret = dns_txt(&nsdomainout, &domainq);
              switch (ret) {
                case DNS_SOFT:
                case DNS_MEM:
                case DNS_HARD:
                  break;
                default:
                  if (flags == ACL_SD_SKIP_NS) {
                    return 0;
                  }
                  if (!stralloc_copyb(&spamdomain, dom, domlen)) die_nomem();
                  if (!stralloc_0(&spamdomain)) die_nomem();
                  if (!stralloc_copyb(&spamdomain_ns,
                      domainout.s + j, nsdomlen)) die_nomem();
                  if (!stralloc_0(&spamdomain_ns)) die_nomem();
                  if (nsdomainout.len > 2) {
                    if (!stralloc_cats(&sdptr->comment, " (")) die_nomem();
                    if (!stralloc_cat(&sdptr->comment, &nsdomainout)) die_nomem();
                    if (!stralloc_cats(&sdptr->comment, ")")) die_nomem();
                  }
                  if (!stralloc_0(&sdptr->comment)) die_nomem();
                  spamdomain_comment = sdptr->comment;
                  flagcontent |= ACL_CONTENT_SD_NS;
                  return 1;
              }
              if (now() > giveout) return -1;
              while (domainout.s[j] != '\0') j++;
              j++;
            }
            break;
        }
        break;

      case ACL_SD_NS_IP:
      case ACL_SD_SKIP_NS_IP:
        if (!stralloc_copyb(&domainq, dom, domlen)) die_nomem();
        ret = dns_nsip(&ia_mf, &domainq);
        for (ips = 0; ret == 0 && ips < ia_mf.len; ips++) {
          unsigned int iplen;

          iplen = ip_fmt(iptemp, &ia_mf.ix[ips].ip);
          iptemp[iplen] = 0;
          if (!stralloc_copys(&domainq, iprev(iptemp, iplen))) die_nomem();
          if (!stralloc_catb(&domainq, ".", 1)) die_nomem();
          if (!stralloc_cat(&domainq, &sdptr->nameserver)) die_nomem();
          ret = dns_txt(&nsdomainout, &domainq);
          switch (ret) {
            case DNS_SOFT:
            case DNS_MEM:
            case DNS_HARD:
              break;
            default:
              if (flags == ACL_SD_SKIP_NS_IP) {
                return 0;
              }
              if (!stralloc_copyb(&spamdomain, dom, domlen)) die_nomem();
              if (!stralloc_0(&spamdomain)) die_nomem();
              spamdomainip = iptemp;
              if (nsdomainout.len > 2) {
                if (!stralloc_cats(&sdptr->comment, " (")) die_nomem();
                if (!stralloc_cat(&sdptr->comment, &nsdomainout)) die_nomem();
                if (!stralloc_cats(&sdptr->comment, ")")) die_nomem();
              }
              if (!stralloc_0(&sdptr->comment)) die_nomem();
              spamdomain_comment = sdptr->comment;
              flagcontent |= ACL_CONTENT_SD_NS_IP;
              return 1;
          }
          if (now() > giveout) return -1;
        }
        break;
    }
  }

  return 0;
}

static inline int sd_start(unsigned char ch, int pos)
{
   if ((ch == "http://"[pos]) || (ch == "HTTP://"[pos]))
     return (pos == 6) ? 2 : 1;
   if (sd_www) {
     if ((ch == "www."[pos]) || (ch == "WWW."[pos]))
       return (pos == 3) ? 2 : 1;
   }
   return 0;
}

static inline int domainch(unsigned char domch)
{
  static const unsigned char validchars[256] = {
      ['.'] = 1, ['0'] = 1, ['1'] = 1, ['2'] = 1, ['3'] = 1, ['4'] = 1,
      ['5'] = 1, ['6'] = 1, ['7'] = 1, ['8'] = 1, ['9'] = 1, ['a'] = 1,
      ['b'] = 1, ['c'] = 1, ['d'] = 1, ['e'] = 1, ['f'] = 1, ['g'] = 1,
      ['h'] = 1, ['i'] = 1, ['j'] = 1, ['k'] = 1, ['l'] = 1, ['m'] = 1,
      ['n'] = 1, ['o'] = 1, ['p'] = 1, ['q'] = 1, ['r'] = 1, ['s'] = 1,
      ['t'] = 1, ['u'] = 1, ['v'] = 1, ['w'] = 1, ['x'] = 1, ['y'] = 1,
      ['z'] = 1, ['A'] = 1, ['B'] = 1, ['C'] = 1, ['D'] = 1, ['E'] = 1,
      ['F'] = 1, ['G'] = 1, ['H'] = 1, ['I'] = 1, ['J'] = 1, ['K'] = 1,
      ['L'] = 1, ['M'] = 1, ['N'] = 1, ['O'] = 1, ['P'] = 1, ['Q'] = 1,
      ['R'] = 1, ['S'] = 1, ['T'] = 1, ['U'] = 1, ['V'] = 1, ['W'] = 1,
      ['X'] = 1, ['Y'] = 1, ['Z'] = 1, ['-'] = 1 };

  return validchars[domch];
}

static int acl_sd_check(stralloc *ln)
{
  unsigned char *cp, *cpend;
  unsigned char *domend, *domstart, *domend_real;
  unsigned char ch;
  unsigned long matchflags;
  int pos = 0;
  int ret;
  datetime_sec sd_limit;
  struct ip_address tmpip;
  int iplen;

  sd_limit = now() + sd_giveuptime;

  cp = ln->s;
  cpend = ln->s + ln->len;

  while (cp < cpend) {
    ch = *cp++;
    if (cp > cpend) break;
    if (domcache_count >= sd_maxdomains) return ACL_SD_NOMATCH;
    switch (sd_start(ch, pos)) {
      case 2:
        pos = 0;
        domstart = cp;
        iplen = ip_scan(domstart, &tmpip);
        if (iplen) {
          /* XXX http://1.2.3.4.biz.info/ */
          ret = acl_sd_domain_lookup(cp, iplen, sd_limit, ACL_SD_DOMAIN | ACL_SD_SKIP_DOMAIN |
                                     ACL_SD_DOMAIN_IP | ACL_SD_SKIP_DOMAIN_IP);
          cp += iplen;
          switch (ret) {
          case -1:
            return ACL_SD_TIMEOUT;
          case 1:
            return ACL_SD_MATCH;
          }
        } else {
          while ((cp < cpend) && domainch(*cp)) cp++;
          domend_real = domend = cp;
          if (cp > domstart) cp--;
          while ((cp > domstart) && *cp == '.') { cp--; domend--; }
          if (cp == domstart) { cp = domend_real; continue; }
          while (cp > domstart && *cp != '.') cp--;
          if (cp == domstart) { cp = domend_real; continue; };
          if (cp == (domend - 1)) { cp = domend_real; continue; };
          if (sd_tld_ok) {
            if (!constmap(&map_sd_tld, cp+1, domend-cp-1)) {
              continue;
            }
          }
          cp--;
          while (cp >= domstart && *cp != '.' && domainch(*cp)) cp--;
          cp++;
          if (sd_2ld_ok) {
            if (constmap(&map_sd_2ld, cp, domend-cp)) {
              if (cp > domstart) {
                cp--;
                while (cp > domstart && *cp == '.') cp--;
              }
              while (cp >= domstart && *cp != '.' && domainch(*cp)) cp--;
              cp++;
            }
          }
          if (cp == domstart)
            matchflags = ACL_SD_ANY;
          else
            matchflags = (ACL_SD_ANY & ~(ACL_SD_DOMAIN_IP | ACL_SD_SKIP_DOMAIN_IP));
          ret = acl_sd_domain_lookup(cp, domend-cp, sd_limit, matchflags);
          switch (ret) {
            case -1:
              return ACL_SD_TIMEOUT;
            case 1:
              return ACL_SD_MATCH;
          }
          if (cp != domstart) {
            ret = acl_sd_domain_lookup(domstart, domend-domstart, sd_limit,
                                       (ACL_SD_DOMAIN_IP | ACL_SD_SKIP_DOMAIN_IP));
            switch (ret) {
              case -1:
                return ACL_SD_TIMEOUT;
              case 1:
                return ACL_SD_MATCH;
            }
          }
          cp = domend_real;
        }
        break;

      case 1:
        pos++;
        break;

      case 0:
        pos = 0;
        break;
    }
  }

  return ACL_SD_NOMATCH;
}

static void acl_sd_match(int headertype, stralloc* matchline)
{
  int ret;
  uint64 tick_start;
  uint64 tick_end;
  char strtick[FMT_ULONG];

  if ((acl_sd_status != 1)) return;
  if ((headertype != ACL_PCRE_BODYB) && (headertype != ACL_PCRE_MIMEB)) return;

  if (sd_debug > 1) tick_start = rdtscl();
  ret = acl_sd_check(matchline);
  if (sd_debug > 1) tick_end = rdtscl();
  switch(ret) {
    case ACL_SD_NOMATCH:
      break;
    case ACL_SD_MATCH:
      break;
    case ACL_SD_TIMEOUT:
      acl_sd_status = 2;
      break;
  }
  if (sd_debug > 1) {
    strtick[fmt_ullong(strtick, tick_end - tick_start)] = 0;
    strerr_warn5("qmail-smtpd: id ", qmailid.s, " DEBUG_ACL_SPAMDOMAIN match took ",
                 strtick, " cycles", 0);
  }
}

static int acl_hdrip_setup(void)
{
  char *cp;
  char *cpend;
  char ch;

  if (!acl_hdrip_ok) return 0;
  if (contentscan_disabled('H')) return 0;
  if (!stralloc_copys(&hdrip_servers, "")) die_nomem();

  cp = acl_checks.s;
  cpend = acl_checks.s + acl_checks.len;
  while ((cp < cpend)) {
    ch = *cp++;
    if (cp >= cpend) break;
    if (ch == '\0') continue;

    if (ch == 'I') {
      if (cp >= cpend) break;
      if (ch == '\0') continue;

      /* I0 disables hdrip completely */
      if (*cp == '0') return 0;
      if (!stralloc_cats(&hdrip_servers, cp)) return -1;
      if (!stralloc_0(&hdrip_servers)) return -1;
    }
    /* skip to next candidate */
    while (*cp) cp++;
  }
  return (hdrip_servers.len > 0);
}

static int acl_hdrip_checkip(char *ipaddy, unsigned int iplen)
{
  char *cp;
  char *cpend;
  int ret;
  static stralloc hdrip_domain = {0};
  static stralloc hdrip_txt = {0};

  cp = hdrip_servers.s;
  cpend = hdrip_servers.s + hdrip_servers.len;
  while (cp < cpend) {
    if (!stralloc_copys(&hdrip_domain, iprev(ipaddy, iplen))) die_nomem();
    if (!stralloc_catb(&hdrip_domain, ".", 1)) die_nomem();
    if (!stralloc_cats(&hdrip_domain, cp)) die_nomem();
    ret = dns_txt(&hdrip_txt, &hdrip_domain);
    switch (ret) {
      case DNS_SOFT:
      case DNS_MEM:
      case DNS_HARD:
        break;
      default:
        if (!stralloc_copyb(&hdrip_err, ipaddy, iplen)) die_nomem();
        if (!stralloc_cats(&hdrip_err, " found on ")) die_nomem();
        if (!stralloc_cats(&hdrip_err, cp)) die_nomem();
        if (hdrip_txt.len > 2) {
          if (!stralloc_cats(&hdrip_err, " (")) die_nomem();
          if (!stralloc_cat(&hdrip_err, &hdrip_txt)) die_nomem();
          if (!stralloc_cats(&hdrip_err, ")")) die_nomem();
        }
        if (!stralloc_0(&hdrip_err)) die_nomem();
        return 1;
    }
    while (*cp) cp++;
    cp++;
  }
  return 0;
}

static int acl_hdrip_check(stralloc* matchline)
{
  unsigned long int j;
  unsigned int iplen;
  char *cp;
  char *cpend;
  static struct ip_address iptest;

  j = byte_chr(matchline->s, matchline->len, ':');
  if (j == matchline->len) return ACL_HDRIP_NOMATCH;
  if (!constmap(&map_hdrip, matchline->s, j)) return ACL_HDRIP_NOMATCH;
  if (matchline->len - j < 8) return ACL_HDRIP_NOMATCH;
  if (!stralloc_0(matchline)) die_nomem();
  matchline->len--;
  cp = matchline->s + j + 1;
  cpend = matchline->s + matchline->len;
  while (cp < cpend) {
    if (*cp >= '0' && *cp <= '9') {
      iplen = ip_scan(cp, &iptest);
      if (iplen) {
        if (acl_hdrip_checkip(cp, iplen)) return ACL_HDRIP_MATCH;
        cp += iplen;
      }
    }
    cp++;
  }
  return ACL_HDRIP_NOMATCH;
}

static void acl_hdrip_match(int headertype, stralloc* matchline)
{
  int ret;

  if ((acl_hdrip_status != 1)) return;
  if (headertype != ACL_PCRE_BODY) return;

  ret = acl_hdrip_check(matchline);
  switch (ret) {
    case ACL_HDRIP_MATCH:
      flagcontent |= ACL_CONTENT_HDRIP;
      break;
    case ACL_HDRIP_NOMATCH:
      break;
  }
}

static void process_line(int headertype, stralloc *matchline)
{
  if ((flagcontent & (ACL_CONTENT_PCRE_MATCH | ACL_CONTENT_PCRE_TEMP)) == 0) {
    acl_pcre_match(headertype, matchline);
  }
  if ((flagcontent & (ACL_CONTENT_SD_ANY)) == 0) {
    acl_sd_match(headertype, matchline);
  }
  if ((flagcontent & (ACL_CONTENT_HDRIP)) == 0) {
    acl_hdrip_match(headertype, matchline);
  }
}

static void acl_line_process(int headertype, stralloc* matchline, int line_enc)
{
  static stralloc line_decoded = {0};
  static int qp_softlb; /* Previous one was "soft" line break? */
  static int lastheadertype = -1;

  if (lastheadertype != headertype) {
    if (qp_softlb) {
      qp_softlb = 0;
      process_line(lastheadertype, &line_decoded);
    }
    lastheadertype = headertype;
  }

  if (matchline->len == 0) return;

  switch (line_enc) {
    case 0:
      break;

    case 'B':
      switch (b64decode(matchline->s, matchline->len, &line_decoded)) {
        case -1:
          die_nomem();
          break;
        case 0:
        case 1:
          matchline = &line_decoded;
          break;
        case 2:
          break;
      }
      break;

    case 'Q':
      switch (qpdecode(matchline->s, matchline->len, &line_decoded, qp_softlb)) {
        case -1:
          die_nomem();
          break;
        case 1:
          if (qp_softlb == 0) return;
        case 0:
          qp_softlb = 0;
          matchline = &line_decoded;
          break;
        case 2:
          qp_softlb = 1;
          return;
      }
      break;
  }

  process_line(headertype, matchline);
}

static char sdinbuf[1024];
static substdio sdssin;

/* gathers 'R' ACLs and initializes corresponding control/spamdomain/ files
   (file ``control/spamdomain/foo'' for 'Rfoo' ACL) */
static int acl_sd_setup(void)
{
  char *cp, *cpend, *cpln, *cptmp;
  unsigned char ch;
  int fd, match;
  unsigned long int len;
  int linenr, states;
  unsigned long flags;
  static stralloc sdfn = {0};
  static stralloc ln = {0};
  char str_num1[FMT_ULONG];
  struct acl_sd_state_t *sdptr;
  struct acl_sd_state_t *sdtmp;

  list_for_each_entry_safe(sdptr, sdtmp, &sd_active, list) {
    if (!stralloc_copys(&sdptr->nameserver, "")) die_nomem();
    if (!stralloc_copys(&sdptr->comment, "")) die_nomem();
    list_move_tail(&sdptr->list, &sd_inactive);
  }

  for (match = 0; match < DOMCACHE_HSIZE; match++) {
    if (!stralloc_copys(&domcache_htable[match], "")) die_nomem();
  }
  domcache_count = 0;

  if (contentscan_disabled('S')) {
    if (sd_debug > 0) {
      strerr_warn3("qmail-smtpd: id ", qmailid.s, " DEBUG_ACL_SPAMDOMAIN disabled", 0);
    }
    return 0;
  }

  cp = acl_checks.s;
  cpend = acl_checks.s + acl_checks.len;
  while ((cp < cpend)) {
    ch = *cp++;
    if (cp >= cpend) break;
    if (ch == '\0') continue;

    if (ch == 'R') {
      if (cp >= cpend) break;
      if (ch == '\0') continue;

      /* R0 disables spamdomain completely */
      if (*cp == '0') return 0;
      if (!stralloc_copys(&sdfn, "control/spamdomain/")) return -1;
      if (!stralloc_cats(&sdfn, cp)) return -1;
      if (!stralloc_0(&sdfn)) return -1;

      fd = open_read(sdfn.s);
      if (fd == -1) {
        if (errno == error_noent)
          continue;
        else
          return -1;
      }

      linenr = 0;
      states = 0;
      substdio_fdbuf(&sdssin, read, fd, sdinbuf, sizeof(sdinbuf));
      for (;;) {
        if (getln(&sdssin, &ln, &match, '\n') != 0) { (void)close(fd); return -1; }
        linenr++;
        if (!match) break;
        if (byte_chr(ln.s, ln.len, '\0') < ln.len) break;

        cpln = ln.s;
        len = ln.len;

        switch (*cpln) {
          case 'd': flags = ACL_SD_DOMAIN;           break;
          case 's': flags = ACL_SD_SKIP_DOMAIN;      break;
          case 'n': flags = ACL_SD_NS;               break;
          case 'N': flags = ACL_SD_SKIP_NS;          break;
          case 'i': flags = ACL_SD_NS_IP;            break;
          case 'I': flags = ACL_SD_SKIP_NS_IP;       break;
          case 'o': flags = ACL_SD_DOMAIN_IP;        break;
          case 'O': flags = ACL_SD_SKIP_DOMAIN_IP;   break;
          default:                                   continue;
        }

        if (list_empty(&sd_inactive)) {
          sdptr = (struct acl_sd_state_t*) alloc(sizeof(struct acl_sd_state_t));
          if (!sdptr) die_nomem();
          byte_zero(sdptr, sizeof(*sdptr));
          if (!stralloc_copys(&sdptr->nameserver, "")) die_nomem();
          if (!stralloc_copys(&sdptr->comment, "")) die_nomem();
          list_add_tail(&sdptr->list, &sd_inactive);
        }
        sdptr = list_entry(sd_inactive.next, struct acl_sd_state_t, list);

        sdptr->flags = flags;
        cpln++;
        len--;
        if (len == 0) continue;

        while (len && (*cpln == '\t' || *cpln == ' ')) { len--; cpln++; }
        if (len == 0) continue;
        cptmp = cpln;

        while (len && (*cpln != '\t' && *cpln != ' ')) { len--; cpln++; }
        if (len == 0) continue;
        if (!stralloc_copyb(&sdptr->nameserver, cptmp, cpln-cptmp)) die_nomem();

        while (len && (*cpln == '\t' || *cpln == ' ')) { len--; cpln++; }
        while (len) {
          if (cpln[len-1] == ' ') { len--; continue; }
          if (cpln[len-1] == '\t') { len--; continue; }
          if (cpln[len-1] == '\r') { len--; continue; }
          if (cpln[len-1] == '\n') { len--; continue; }
          if (cpln[len-1] == '\0') { len--; continue; }
          break;
        }

        if (len)
          if (!stralloc_copyb(&sdptr->comment, cpln, len)) die_nomem();

        list_move_tail(&sdptr->list, &sd_active);

        states++;
      }

      (void)close(fd);
      if ((sd_debug > 0) && (states >= 1)) {
        str_num1[fmt_ulong(str_num1, states)] = 0;
        strerr_warn6("qmail-smtpd: id ", qmailid.s,
                     " DEBUG_ACL_SPAMDOMAIN acl_sd_setup accepted ", str_num1,
                     (states == 1) ? " line from " : " lines from ",
                     sdfn.s, 0);
      }
    }
    /* skip to next candidate */
    while (*cp) cp++;
    if (cp >= cpend) break;
  }

  return (!list_empty(&sd_active));
}

static int acl_match_data(char **ret)
{
  char *cp, *cpend;
  unsigned char ch;
  int flagout = 0;
  static stralloc retmsg = {0};
#ifdef PCRE_SMTPD
  struct list_head *pos1, *pos2;
  struct acl_pcre_state_t *pcreptr;
#endif

  cp = acl_checks.s;
  cpend = acl_checks.s + acl_checks.len;
  while ((cp < cpend) && !flagout) {
    ch = *cp++;
    if (cp >= cpend) break;
    if (ch == '\0') continue;
    /* strerr_warn4("qmail-smtpd: id ", qmailid.s, " DEBUG_ACL_MATCH_DATA ", cp-1, 0); */
    if (ch == 'L') {
      ch = *cp++;
      if (cp >= cpend) break;
      if (ch == '\0') continue;
      if (ch == '3') {
        if (decision & ACL_RBLSMTPD_WL) {
          flagout = 1;
          break;
        }
      }
    } else if (ch == '!') {
      ch = *cp++;
      if (cp >= cpend) break;
      if (ch == '\0') continue;
      if (ch == '2') {
        if (!stralloc_copys(&retmsg, cp-1)) die_nomem();
        if (!stralloc_0(&retmsg)) die_nomem();
        *ret = retmsg.s;
        goto checkdatabytes;
      } else if (ch == '4') {
        strerr_warn6("qmail-smtpd: id ", qmailid.s, " 400 ACL_TEMP_DATA: mail bytes=",
                      bytesread_s, ": ", cp-1, 0);
        if (!stralloc_copys(&retmsg, cp-1)) die_nomem();
        if (!stralloc_0(&retmsg)) die_nomem();
        *ret = retmsg.s;
        return ACL_FAIL;
      } else if (ch == '5') {
        strerr_warn6("qmail-smtpd: id ", qmailid.s, " 500 ACL_PERM_DATA: mail bytes=",
                      bytesread_s, ": ", cp-1, 0);
        if (!stralloc_copys(&retmsg, cp-1)) die_nomem();
        if (!stralloc_0(&retmsg)) die_nomem();
        *ret = retmsg.s;
        return ACL_FAIL;
      }
    } else if (ch == 'T') {
      int ctmatch;

      ch = *cp++;
      if (cp >= cpend) break;
      if (ch == '\0') continue;
      ctmatch = 0;
      if (ch == 'G') ctmatch = ACL_CONTENT_CT_GOOD;
      if (ch == 'B') ctmatch = ACL_CONTENT_CT_BAD;
      if (*cp) {
        unsigned long int j, len;

        flagcontent |= ACL_CONTENT_CT;
        if (case_diffs(cp, str_ct.s) == 0) {
          flagcontent |= ctmatch;
        } else if (case_diffs(cp, "*") == 0) {
          flagcontent |= ctmatch;
        } else {
          len = str_len(cp);
          j = byte_chr(cp, len, '*');
          if (j < len) {
            if (case_diffb(cp, j, str_ct.s) == 0) {
              cp[j] = 0;
              flagcontent |= ctmatch;
            }
          }
        }
      }
    } else if (ch == 'X') {
      ch = *cp++;
      if (cp >= cpend) break;
      switch (ch) {
        case '1':
          if (flagcontent & ACL_CONTENT_MSEXE) {
            strerr_warn6("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_MSEXE: mail bytes=",
                         bytesread_s, ", attachment SHA1=",
                         bsha1ok ? str_sha1.s : "unknown", 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: MS executables unwanted (#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '2':
          if (flagcontent & ACL_CONTENT_MSWORD) {
            strerr_warn6("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_MSWORD: mail bytes=",
                         bytesread_s, ", attachment SHA1=",
                         bsha1ok ? str_sha1.s : "unknown", 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: MS Word unwanted - http://www.fsf.org/philosophy/no-word-attachments.html (#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '3':
          if (flagcontent & ACL_CONTENT_CHARSET) {
            strerr_warn6("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_CHARSET: mail bytes=",
                         bytesread_s, ", charset=", str_charset.s, 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: unwanted charset ")) die_nomem();
            if (!stralloc_cats(&retmsg, str_charset.s)) die_nomem();
            if (!stralloc_cats(&retmsg, " (#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '4':
          if (flagcontent & ACL_CONTENT_RFC2047) {
            strerr_warn4("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_RFC2047: mail bytes=",
                         bytesread_s, 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: message does not conform to RFC2047 and RFC2821 (non-us-ascii character(s) in headers) (#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '5':
          if (flagcontent & ACL_CONTENT_BADSHA1) {
            strerr_warn6("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_SHA1: mail bytes=",
                         bytesread_s, ", attachment SHA1=", str_sha1.s, 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: unwanted attachment SHA1=")) die_nomem();
            if (!stralloc_cats(&retmsg, str_sha1.s)) die_nomem();
            if (!stralloc_cats(&retmsg, " (#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '6':
          if (flagcontent & (ACL_CONTENT_MIMENEST | ACL_CONTENT_MIMENB |
              ACL_CONTENT_MIMEBB | ACL_CONTENT_MIMEBL |
              ACL_CONTENT_BADBASE64 | ACL_CONTENT_BASE64LEN | ACL_CONTENT_BADQP)) {
            static stralloc mimeviol = {0};

            if (!stralloc_copys(&mimeviol, "")) die_nomem();
            if (flagcontent & ACL_CONTENT_MIMENEST)
              if (!stralloc_cats(&mimeviol, "MIME_NESTING_EXCEEDED ")) die_nomem();
            if (flagcontent & ACL_CONTENT_MIMENB)
              if (!stralloc_cats(&mimeviol, "MIME_NO_BOUNDARY_IN_MULTIPART_MESSAGE ")) die_nomem();
            if (flagcontent & ACL_CONTENT_MIMEBB)
              if (!stralloc_cats(&mimeviol, "NON_RFC2046_BOUNDARYCHARS ")) die_nomem();
            if (flagcontent & ACL_CONTENT_MIMEBL)
              if (!stralloc_cats(&mimeviol, "NON_RFC2046_BOUNDARYLEN ")) die_nomem();
            if (flagcontent & ACL_CONTENT_BADBASE64)
              if (!stralloc_cats(&mimeviol, "ILLEGAL_BASE64_DATA ")) die_nomem();
            if (flagcontent & ACL_CONTENT_BASE64LEN)
              if (!stralloc_cats(&mimeviol, "TOO_LONG_BASE64_DATA ")) die_nomem();
            if (flagcontent & ACL_CONTENT_BADQP)
              if (!stralloc_cats(&mimeviol, "NON_RFC2045_QP ")) die_nomem();
            if (!stralloc_0(&mimeviol)) die_nomem();
            strerr_warn6("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_MIME: mail bytes=",
                         bytesread_s, ", violations=", mimeviol.s, 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: malformed MIME-message, violations=")) die_nomem();
            if (!stralloc_cats(&retmsg, mimeviol.s)) die_nomem();
            if (!stralloc_cats(&retmsg, "(#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case '7':
          if (flagcontent & ACL_CONTENT_NOMSGID) {
            strerr_warn4("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_NOMSGID: mail bytes=",
                         bytesread_s, 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: no Message-ID (#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case 'Z':
          if (flagcontent & ACL_CONTENT_ZIP) {
            strerr_warn6("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_ZIP: mail bytes=",
                         bytesread_s, ", attachment SHA1=",
                         bsha1ok ? str_sha1.s : "unknown", 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: ZIP unwanted - use bzip2 http://sources.redhat.com/bzip2/ or if you are a virus, die (#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case 'T':
          if (flagcontent & ACL_CONTENT_TNEF) {
            strerr_warn6("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_TNEF: mail bytes=",
                         bytesread_s, ", attachment SHA1=",
                         bsha1ok ? str_sha1.s : "unknown", 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: MS-TNEF attachments unwanted (#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

#ifdef PCRE_SMTPD
        case 'P':
          if (acl_pcre_status != 1) break;

          if (flagcontent & ACL_CONTENT_PCRE_TEMP) {
            strerr_warn6("qmail-smtpd: id ", qmailid.s, " 451 ", pcre_line_match.s,
                         ", mail bytes=", bytesread_s, 0);
            if (!stralloc_copys(&retmsg, "451 ACL PCRE temporary error (#4.3.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }

          if (flagcontent & ACL_CONTENT_PCRE_MATCH) {
            strerr_warn6("qmail-smtpd: id ", qmailid.s, " 553 ", pcre_line_match.s,
                         ", mail bytes=", bytesread_s, 0);
            if (!stralloc_copys(&retmsg, "553 ACL PCRE content match (#5.6.0)"))
              die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }

          list_for_each_safe(pos1, pos2, &pcre_active) {
            pcreptr = list_entry(pos1, struct acl_pcre_state_t, list);
            if ((pcreptr->matchon & ACL_PCRE_WL_REQ)) {
              char *textptr;

              if (pcreptr->flags.dfl_match) {
                textptr = pcre_reject_dfl.s;
              } else {
                if (!stralloc_0(&pcreptr->matchtext)) die_nomem();
                textptr = pcreptr->matchtext.s;
              }

              strerr_warn6("qmail-smtpd: id ", qmailid.s,
                           " 553 ACL_CONTENT_PCRE_REQ mail bytes=",
                           bytesread_s, ": ", textptr, 0);
              if (!stralloc_copys(&retmsg, "553 ACL PCRE no required content found: ")) die_nomem();
              if (!stralloc_cats(&retmsg, textptr)) die_nomem();
              if (!stralloc_cats(&retmsg, " (#5.6.0)") || !stralloc_0(&retmsg)) die_nomem();
              *ret = retmsg.s;
              return ACL_FAIL;
            }
          }
          break;
#endif

        case 'S':
          if ((flagcontent & ACL_CONTENT_SD_ANY) == 0) break;

          if (flagcontent & ACL_CONTENT_SD_DOMAIN) {
            strerr_warn8("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_SD_DOMAIN ",
                         spamdomain.s, ": ", spamdomain_comment.s,
                         ", mail bytes=", bytesread_s, 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: spamdomain (#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }

          if (flagcontent & ACL_CONTENT_SD_DOMAIN_IP) {
            strerr_warn10("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_SD_DOMAIN_IP ",
                         spamdomain.s, " has IP ", spamdomainip, ": ", spamdomain_comment.s,
                         ", mail bytes=", bytesread_s, 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: spamdomain (#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }

          if (flagcontent & ACL_CONTENT_SD_NS) {
            strerr_warn10("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_SD_NS ",
                         spamdomain.s, " has nameserver ", spamdomain_ns.s,
                         ": ", spamdomain_comment.s, ", mail bytes=", bytesread_s, 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: spamdomain (#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }

          if (flagcontent & ACL_CONTENT_SD_NS_IP) {
            strerr_warn10("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_SD_NS_IP ",
                         spamdomain.s, " has nameserver at IP ",
                         spamdomainip, ": ", spamdomain_comment.s,
                         ", mail bytes=", bytesread_s, 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: spamdomain (#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

        case 'H':
          if (flagcontent & ACL_CONTENT_HDRIP) {
            strerr_warn6("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_HDRIP ",
                         hdrip_err.s, ", mail bytes=", bytesread_s, 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: header IP (#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;

#ifdef PCRE_SMTPD
        case 'I':
          if (flagcontent & ACL_CONTENT_BADSIG) {
            strerr_warn8("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_BADSIG: mail bytes=",
                         bytesread_s, ", attachment SHA1=",
                         bsha1ok ? str_sha1.s : "unknown", ", sig=", str_badsigs.s, 0);
            if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: Unwanted content in attachment (#5.6.0)")) die_nomem();
            if (!stralloc_0(&retmsg)) die_nomem();
            *ret = retmsg.s;
            return ACL_FAIL;
          }
          break;
#endif

        default:
          break;
      }
    }
    /* skip to next candidate */
    while (*cp) cp++;
    if (cp >= cpend) break;
  }

  if (flagcontent & ACL_CONTENT_CT) {
    if (flagcontent & ACL_CONTENT_CT_BAD) {
      strerr_warn6("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_CT_BAD: mail bytes=",
                   bytesread_s, " bad Content-Type ", gen_safestr(str_ct.s, 511), 0);
    } else if (!(flagcontent & ACL_CONTENT_CT_GOOD)) {
      strerr_warn7("qmail-smtpd: id ", qmailid.s, " 553 ACL_CONTENT_CT_GOOD: mail bytes=",
                   bytesread_s, " Content-Type ", gen_safestr(str_ct.s, 511), " not allowed", 0);
    } else {
      goto checkdatabytes;
    }
      if (!stralloc_copys(&retmsg, "553 ACL CONTENT violation: unwanted Content-Type ")) die_nomem();
      if (!stralloc_cats(&retmsg, str_ct.s)) die_nomem();
      if (!stralloc_cats(&retmsg, " (#5.6.0)")) die_nomem();
      if (!stralloc_0(&retmsg)) die_nomem();
      *ret = retmsg.s;
      return ACL_FAIL;
  }

checkdatabytes:
  if ((acl_maxsz && (bytesread > acl_maxsz)) ||
      (databytes && (bytesread > databytes))) {
    strerr_warn8("qmail-smtpd: id ", qmailid.s, " 552 ACL_DATABYTES: mail bytes=",
                 bytesread_s, " acl_maxsz=", acl_maxsz_s, " databytes=", databytes_s, 0);
    if (!stralloc_copys(&retmsg, "552 mail bytes=")) die_nomem();
    if (!stralloc_cats(&retmsg, bytesread_s)) die_nomem();
    if (!stralloc_cats(&retmsg, " exceeds my limit of ")) die_nomem();
    if (!stralloc_cats(&retmsg, (acl_maxsz && (acl_maxsz < bytesread)) ?
                                 acl_maxsz_s : databytes_s)) die_nomem();
    if (!stralloc_cats(&retmsg, " (#5.3.4)")) die_nomem();
    if (!stralloc_0(&retmsg)) die_nomem();
    *ret = retmsg.s;
    return ACL_FAIL;
  }
  return ACL_OK;
}

static void smtp_rcpt(char *arg)
{
  char *aclmsg;
  int flagallowed;
  char *useracl;
  unsigned char chbogo[2];

  if (!seenmail) { err_wantmail(); return; }
  if (!addrparse(arg, 0)) { err_syntax(); return; }

  if (!stralloc_copys(&safemailfrom, "<")) die_nomem();
  if (!stralloc_cats(&safemailfrom, gen_safestr(mailfrom.s, MAXLOGLEN))) die_nomem();
  if (!stralloc_cats(&safemailfrom, ">")) die_nomem();
  if (!stralloc_0(&safemailfrom)) die_nomem();
  if (!stralloc_copys(&safercptto, "<")) die_nomem();
  if (!stralloc_cats(&safercptto, gen_safestr(addr.s, MAXLOGLEN))) die_nomem();
  if (!stralloc_cats(&safercptto, ">")) die_nomem();
  if (!stralloc_0(&safercptto)) die_nomem();

  if (addr.len == 1) {
    strerr_warn6("qmail-smtpd: id ", qmailid.s, " 553 ACL_NULL_RECIPIENT from ",
                 safemailfrom.s, " SIZE=", smtpsize_s, 0);
    out("553 sorry, null recipient is not allowed (#5.7.1)\r\n");
    flagnullconn = 0;
    return;
  }

  flagallowed = addrallowed();
  if (relayclient) {
    --addr.len;
    if (!stralloc_cats(&addr, relayclient)) die_nomem();
    if (!stralloc_0(&addr)) die_nomem();
  } else {
    if (!flagallowed) {
      strerr_warn8("qmail-smtpd: id ", qmailid.s, " 553 ACL_RELAY_DENIED from ",
                   safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
      out("553 sorry, that domain isn't in my list of allowed rcpthosts (#5.7.1)\r\n");
      flagnullconn = 0;
      return;
    }
  }

  switch (acl_get(addr.s, &acl_to, 't', (flagallowed == 1))) {
    case ACL_ERR_TEMP:
      out("450 error while accessing ACL (#4.3.0)\r\n"); return;
    case ACL_ERR_PERM:
      out("550 error while accessing ACL (#5.3.0)\r\n"); return;
    case ACL_ERR_MATCH:
      break;
    case ACL_ERR_NOMATCH:
      if (!stralloc_copys(&acl_to, "")) die_nomem();
      if (!stralloc_0(&acl_to)) die_nomem();
      break;
    default:
      out("450 unknown error while accessing ACL (#4.3.0)\r\n"); return;
  }

  if (!stralloc_copyb(&acl_checks, envacl.s, envacl.len)) die_nomem();
  if (!stralloc_0(&acl_checks)) die_nomem();

  if (authd) {
    useracl = user.s;
  } else if (relayclient) {
    useracl = relayclient;
  } else {
    useracl = 0;
  }
  if (useracl) {
    static stralloc tmpacl = {0};
    int ret;

    if (!stralloc_copys(&tmpacl, "u")) die_nomem();
    if (!stralloc_cats(&tmpacl, useracl) || !stralloc_0(&tmpacl)) die_nomem();
    ret = acl_getcdb(tmpacl.s, &acl_user);
    if ((ret != ACL_ERR_NOMATCH) && (ret != ACL_ERR_MATCH)) die_control();
    if (ret == ACL_ERR_MATCH) {
      if (!stralloc_cat(&acl_checks, &acl_user)) die_nomem();
    }
  }

  if (!stralloc_cat(&acl_checks, &acl_helo)) die_nomem();
  if (!stralloc_cat(&acl_checks, &acl_from)) die_nomem();
  if (!stralloc_cat(&acl_checks, &acl_to)) die_nomem();

  /* RFC2821 4.5.3.1 */
  if ((recipcount >= maxrcpt) || ((mailfrom.len == 1) && (recipcount == 1))) {
    strerr_warn8("qmail-smtpd: id ", qmailid.s, " 452 ACL_MAXRCPT from ",
                 safemailfrom.s, " SIZE=", smtpsize_s, " to ", safercptto.s, 0);
    out("452 RCPT TO limit exceeded (#4.7.0)\r\n");
    flagnullconn = 0;
    return;
  }

  flaggreyid = 0;
  if (!stralloc_copys(&aestrap_info_curr, "")) die_nomem();
  if (acl_match_rcpt(&aclmsg) == ACL_FAIL) {
    out(aclmsg);
    out("\r\n");
    flagnullconn = 0;
    return;
  } else {
    out(aclmsg);
    out("\r\n");
  }

  chbogo[0] = bogowanted;
  chbogo[1] = 0;
  switch (chbogo[0]) {
    case '1':
      if (!env_put2("QMAILQUEUE", "bin/qmail-queue")) die_nomem();
      break;

    case '2': /* reject suspect spam with 554, keep copy of the spam at qfilter temp dir */
    case '3': /* don't reject suspect spam, add X-Bogosity header field */
    case '4': /* reject suspect spam with 554, no copy at qfilter temp dir */
    case '5': /* accept spam with 250 but discard the spam */
      if (!env_put2("BOGOOPT", chbogo)) die_nomem();
      if (!env_put2("QMAILQUEUE", "bin/bogorun")) die_nomem();
      break;

    default:
      break;
  }

  if (!stralloc_copys(&str_grey, " (HELO=")) die_nomem();
  if (!stralloc_cats(&str_grey, gen_safestr(helohost.s, MAXLOGLEN))) die_nomem();
  if (!stralloc_cats(&str_grey, " GREYID=")) die_nomem();
  /* GREYID passed for current recipient? */
  if (flaggreyid) {
    char s_greydelayed[FMT_ULONG];

    if (!stralloc_cats(&str_grey, greyid)) die_nomem();

    if (!stralloc_copys(&str_greydelays, " delays avg=")) die_nomem();
    s_greydelayed[fmt_ullong(s_greydelayed, greydelayed_total/flaggreyid_ok)] = 0;
    if (!stralloc_cats(&str_greydelays, s_greydelayed)) die_nomem();
    if (!stralloc_cats(&str_greydelays, " min=")) die_nomem();
    s_greydelayed[fmt_ullong(s_greydelayed, greydelayed_min)] = 0;
    if (!stralloc_cats(&str_greydelays, s_greydelayed)) die_nomem();
    if (!stralloc_cats(&str_greydelays, " max=")) die_nomem();
    s_greydelayed[fmt_ullong(s_greydelayed, greydelayed_max)] = 0;
    if (!stralloc_cats(&str_greydelays, s_greydelayed)) die_nomem();
    if (!stralloc_cat(&str_grey, &str_greydelays)) die_nomem();
  } else {
    if (!stralloc_cats(&str_grey, "none")) die_nomem();
  }
  if (!stralloc_cats(&str_grey, ")") || !stralloc_0(&str_grey)) die_nomem();

  if (aestrap_info_curr.len) {
    strerr_warn12("qmail-smtpd: id ", qmailid.s,
                  " MAIL FROM ", safemailfrom.s, " SIZE=", smtpsize_s,
                  " RCPT TO ", safercptto.s,
                  " (AESTRAP ", aestrap_info_curr.s, ")", str_grey.s, 0);
  } else {
    strerr_warn9("qmail-smtpd: id ", qmailid.s,
                  " MAIL FROM ", safemailfrom.s, " SIZE=", smtpsize_s,
                  " RCPT TO ", safercptto.s, str_grey.s, 0);
  }
  if (!stralloc_cats(&rcptto,"T")) die_nomem();
  if (!stralloc_cats(&rcptto, addr.s)) die_nomem();
  if (!stralloc_0(&rcptto)) die_nomem();
  flagnullconn = 2;
  recipcount++;
}

/* check Content-Type: field, check for boundary and charset
   NOTE: maximum MIME nesting level is defined in MAXMIMENESTING
     returns:
       0: no multipart/* or message/*
       1: multipart/*
       2: message/rfc822{,-headers}
       3: text/*
*/
static int checkct(int putct);
int checkct(int putct)
{
  char *cp, *cpstart, *cpafter;
  unsigned long int len;
  int flagtype;
  int foundb = 0;
  int bpos;
  static const unsigned char bchars[256] = {
    ['0'] = 1, ['1'] = 1, ['2'] = 1, ['3'] = 1, ['4'] = 1, ['5'] = 1, ['6'] = 1,
    ['7'] = 1, ['8'] = 1, ['9'] = 1, ['a'] = 1, ['b'] = 1, ['c'] = 1, ['d'] = 1,
    ['e'] = 1, ['f'] = 1, ['g'] = 1, ['h'] = 1, ['i'] = 1, ['j'] = 1, ['k'] = 1,
    ['l'] = 1, ['m'] = 1, ['n'] = 1, ['o'] = 1, ['p'] = 1, ['q'] = 1, ['r'] = 1,
    ['s'] = 1, ['t'] = 1, ['u'] = 1, ['v'] = 1, ['w'] = 1, ['x'] = 1, ['y'] = 1,
    ['z'] = 1, ['A'] = 1, ['B'] = 1, ['C'] = 1, ['D'] = 1, ['E'] = 1, ['F'] = 1,
    ['G'] = 1, ['H'] = 1, ['I'] = 1, ['J'] = 1, ['K'] = 1, ['L'] = 1, ['M'] = 1,
    ['N'] = 1, ['O'] = 1, ['P'] = 1, ['Q'] = 1, ['R'] = 1, ['S'] = 1, ['T'] = 1,
    ['U'] = 1, ['V'] = 1, ['W'] = 1, ['X'] = 1, ['Y'] = 1, ['Z'] = 1, ['\''] = 1,
    ['('] = 1, [')'] = 1, ['+'] = 1, ['_'] = 1, [','] = 1, ['-'] = 1, ['.'] = 1,
    ['/'] = 1, [':'] = 1, ['='] = 1, ['?'] = 1, [' '] = 1
  };

  cp = content.s;
  len = content.len;
  cpafter = content.s + content.len;
  while (len && ((*cp == ' ') || (*cp == '\t'))) { ++cp; --len; }
  cpstart = cp;

  if (case_startb(cp, cpafter-cp, "multipart/"))
    flagtype = 1;
  else if (case_startb(cp, cpafter-cp, "message/rfc822"))
    flagtype = 2;
  else if (case_startb(cp, cpafter-cp, "message/rfc822-headers"))
    flagtype = 2;
  else if (case_startb(cp, cpafter-cp, "text/"))
    flagtype = 3;
  else
    flagtype = 0;

  if (len && (*cp == '"')) { /* might be commented */
    ++cp; --len; cpstart = cp;
    while (len && *cp != '"') { ++cp; --len; }
  } else {
    while (len && (*cp != ' ') && (*cp != '\t') && (*cp != ';')) { ++cp; --len; }
  }

  if (putct) {
    if (!stralloc_copyb(&str_ct, cpstart, cp-cpstart)) die_nomem();
    if (!stralloc_0(&str_ct)) die_nomem();
    case_lowerb(str_ct.s, str_ct.len);
  }
  while((cp += byte_chr(cp, cpafter-cp, ';')) != cpafter) {
    ++cp;
    while (cp < cpafter && ((*cp == ' ') || (*cp == '\t'))) ++cp;
    if (flagtype == 1) {
      if (case_startb(cp, cpafter-cp, "boundary") && !foundb) {
        cp += 8; /* after boundary */
        while (cp < cpafter && (*cp == ' ' || *cp == '\t')) cp++;
        if (cp == cpafter) continue;
        if (*cp != '=') continue;
        cp++;
        while (cp < cpafter && (*cp == ' ' || *cp == '\t')) cp++;
        if (cp == cpafter) continue;
        if (cp < cpafter && *cp == '"') {
          ++cp;
          cpstart = cp;
          while (cp < cpafter && *cp != '"') ++cp;
          if (*cp == '"') {
            --cp;
            while ((cp > cpstart) && ((*cp == ' ') || (*cp == '\t'))) --cp;
            ++cp;
          }
        } else {
          cpstart = cp;
          while (cp < cpafter && (*cp != ';') && (*cp != ' ') && (*cp != '\t')) ++cp;
        }
        if (mimenesting < MAXMIMENESTING) {
          /* RFC2046
             If a boundary delimiter line appears to end with white space,
             the white space must be presumed to have been added by a gateway,
             and must be deleted. */
          if (cp-cpstart > 0) {
            foundb = 1;
            if (cp-cpstart > 70) {
              flagcontent |= ACL_CONTENT_MIMEBL;
            }
            for (bpos = 0; bpos < cp-cpstart; bpos++) {
              if (bchars[(unsigned char)cpstart[bpos]] == 0) {
                flagcontent |= ACL_CONTENT_MIMEBB;
                break;
              }
            }
            if (!stralloc_copys(&boundary[mimenesting], "--")) die_nomem();
            if (!stralloc_catb(&boundary[mimenesting], cpstart, cp-cpstart)) die_nomem();
            mimenesting++;
          } else {
            flagcontent |= ACL_CONTENT_MIMEBL;
          }
        } else {
          flagcontent |= ACL_CONTENT_MIMENEST;
        }
      }
    }

    if (bcsok && case_startb(cp, cpafter-cp, "charset")) {
      cp += 7; /*after charset */
      while (cp < cpafter && (*cp == ' ' || *cp == '\t')) cp++;
      if (cp == cpafter) continue;
      if (*cp != '=') continue;
      cp++;
      while (cp < cpafter && (*cp == ' ' || *cp == '\t')) cp++;
      if (cp == cpafter) continue;
      if (cp < cpafter && *cp == '"') {
        ++cp;
        cpstart = cp;
        while (cp < cpafter && *cp != '"') ++cp;
      } else {
        cpstart = cp;
        while (cp < cpafter && (*cp != ';') && (*cp != ' ') && (*cp != '\t')) ++cp;
      }
      if ((cp-cpstart >= 1) && constmap(&mapbcs, cpstart, cp-cpstart)) {
        flagcontent |= ACL_CONTENT_CHARSET;
        if (!stralloc_copyb(&str_charset, cpstart, cp-cpstart)) die_nomem();
        if (!stralloc_0(&str_charset)) die_nomem();
      }
    }
  }
  return flagtype;
}

static int acl_badsigs_setup(void)
{
  if (badsigsok != 1) return 0;
  if (contentscan_disabled('I')) return 0;
  return (badsigs.len > 0);
}

#ifdef PCRE_SMTPD
#define MAXSIGLEN (64)
static void check_badsigs(stralloc* tmpline)
{
  int ret;
  int i;
  unsigned long j, k;
  unsigned char hextmp[MAXSIGLEN*2 + 1];
  char *cp;
  char *cpafter;
  const char *pcreerr;
  int pcreerr_off;
  pcre *pcre_pattern;
  static stralloc sigtemp = {0};

  if (acl_badsigs_status != 1) return;
  /* Found match already, do not search */
  if ((flagcontent & ACL_CONTENT_BADSIG) != 0) return;

  ret = b64decode(tmpline->s, tmpline->len-1, &lineout);
  if (ret == -1) die_nomem();
  else if (ret == 0 || ret == 1) {
    if (lineout.len >= 3) {
      if (lineout.len > MAXSIGLEN) lineout.len = MAXSIGLEN;
      for (i = 0; i < lineout.len; i++) {
        hextmp[i*2+0] = hextbl[(((unsigned char)lineout.s[i]) >> 4)];
        hextmp[i*2+1] = hextbl[(((unsigned char)lineout.s[i]) & 15)];
      }
      hextmp[i*2] = 0;
      if (!stralloc_copyb(&sigtemp, badsigs.s, badsigs.len)) die_nomem();
      cp = sigtemp.s;
      cpafter = sigtemp.s + sigtemp.len;
      while (cp < cpafter) {
        j = byte_chr(cp, cpafter-cp, 0);
        k = byte_rchr(cp, j, ':');
        if (j == k) {
          /* no ':' found */
          cp += j + 1;
          continue;
        }
        cp[k] = 0;
        errno = 0;
        pcre_pattern = pcre_compile(cp, PCRE_ANCHORED, &pcreerr,
                                    &pcreerr_off, pcre_tables);
        if (!pcre_pattern) {
          if (errno == error_nomem) die_nomem();
          if (pcredebug > 2) {
            strerr_warn6("qmail-smtpd: id ", qmailid.s,
                         " DEBUG_ACL_PCRE pcre_compile error in control/badsigs line \"",
                         gen_safestr(cp, 128), "\": ", pcreerr, 0);
          }
          cp += j + 1;
          continue;
        }
        ret = pcre_exec(pcre_pattern, NULL, hextmp, i*2, 0,
                        PCRE_NOTEMPTY, ovec, OVECSZ);
        alloc_free(pcre_pattern);
        if (ret >= 0) {
          if (!stralloc_copyb(&str_badsigs, hextmp, i*2)) die_nomem();
          if (!stralloc_cats(&str_badsigs, " (")) die_nomem();
          if (!stralloc_catb(&str_badsigs, cp+k+1, j-k-1)) die_nomem();
          if (!stralloc_catb(&str_badsigs, ")", 2)) die_nomem();
          flagcontent |= ACL_CONTENT_BADSIG;
          return;
        }
        cp += j + 1;
      }
    }
  }
}
#else
static void check_badsigs(stralloc* tmpline)
{
}
#endif

static void put_str_sha1(void)
{
  char attachlen[FMT_ULONG];
  uint8 hexdigest[SHA1DIGESTSIZEHEX];

  attachlen[fmt_ullong(attachlen, ((sha1ctx.length/8) + (sha1ctx.curlen)))] = 0;
  sha1_done(&sha1ctx, hexdigest, SHA1DIGESTSIZEHEX, SHA1DIGESTHEX);
  sha1_init(&sha1ctx);
  if (!stralloc_copyb(&str_sha1, hexdigest, SHA1DIGESTSIZEHEX) ||
      !stralloc_cats(&str_sha1, " (") ||
      !stralloc_cats(&str_sha1, attachlen) ||
      !stralloc_cats(&str_sha1, " bytes)") ||
      !stralloc_0(&str_sha1)) die_nomem();
  if (constmap(&mapbsha1, hexdigest, SHA1DIGESTSIZEHEX)) {
    flagcontent |= ACL_CONTENT_BADSHA1;
  }
}

/* returns -1 if field name is not content-transfer-encoding,
   'Q' if it is quoted-printable,
   'B' if it is base64,
     0 if something else. */
static int get_ct(stralloc*);
int get_ct(stralloc* tmpline)
{
  char *cp, *cpafter, *cpstart;

  if (case_startb(tmpline->s, tmpline->len, "content-transfer-encoding:")) {
    cp = tmpline->s + 26;
    cpafter = tmpline->s + tmpline->len;

    while (cp < cpafter && ((*cp == ' ') || (*cp == '\t'))) ++cp;
    if (cp < cpafter && *cp == '"') {
      ++cp;
      cpstart = cp;
      while (cp < cpafter && *cp != '"') ++cp;
    } else {
      cpstart = cp;
      while (cp < cpafter && (*cp != ';') && (*cp != ' ') && (*cp != '\t')) ++cp;
    }
    if (case_startb(cpstart, cp-cpstart, "base64")) return 'B';
    if (case_startb(cpstart, cp-cpstart, "quoted-printable")) return 'Q';
    return 0;
  } else {
    return -1;
  }
}

static void put(ch)
char *ch;
{
  int i;

  /* XXX */
  /* if (line.len < 1024) */
    if (!stralloc_append(&line,ch)) die_nomem();

  if (*ch == '\n') {
    if (putinheader) {
      if (line.len == 1) {
        /* end of message header */
        putinheader = 0;
        if (content.len) {
          i = checkct(1);
          if ((i == 1) && (mimenesting != 1)) {
            /* found multipart-message but no valid boundary */
            flagcontent |= ACL_CONTENT_MIMENB;
          }
          if (i == 3) flagtext = 1;
        } else {
          /* text/plain */
          flagtext = 1;
        }
        if (bsha1ok) sha1_init(&sha1ctx);
        if (linestate >= 0) acl_line_process(ACL_PCRE_BODY, &pcre_line, 0);
        pcre_line.len = 0;
        linestate = 0;
        linetype = ' ';
      } else { /* line.len > 1 */
        put_debug(1);
        if ((*line.s == ' ' || *line.s == '\t')) {
          switch(linetype) {
            case 'C': if (!stralloc_catb(&content, line.s, line.len-1)) die_nomem(); break;
            default: break;
          }
          if (!stralloc_catb(&pcre_line, line.s, line.len-1)) die_nomem();
          linestate = 1;
        } else {
          switch (linestate) {
            case -1:
            case 0:
              linestate = 1;
              if (!stralloc_copyb(&pcre_line, line.s, line.len-1)) die_nomem();
              break;
            case 1:
              acl_line_process(ACL_PCRE_BODY, &pcre_line, 0);
              if (!stralloc_copyb(&pcre_line, line.s, line.len-1)) die_nomem();
              break;
          }
          if (case_startb(line.s, line.len, "content-type:")) {
            if (!stralloc_copyb(&content, line.s+13, line.len-14)) die_nomem();
            linetype = 'C';
          } else {
            linetype = ' ';
          }
          if (case_startb(line.s, line.len, "message-id:")) {
            unsigned long int i, j;

            if (!stralloc_copys(&msgid, "")) die_nomem();
            i = byte_chr(line.s+11, line.len-12, '<');
            j = byte_chr(line.s+11, line.len-12, '>');
            if ((i < j) && (j < line.len-12)) {
              if (!stralloc_catb(&msgid, line.s+i+11+1, j-i-1)) die_nomem();
              flagcontent &= ~ACL_CONTENT_NOMSGID;
            }
            if (!stralloc_0(&msgid)) die_nomem();
          } else {
            switch (get_ct(&line)) {
            case -1:
              break;
            case 0:
              ct_encbody = 0; base64ok = 0; ct_enc = 0;
              break;
            case 'Q':
              ct_encbody = 1; base64ok = 0; ct_enc = 'Q';
              break;
            case 'B':
              ct_encbody = 1; base64ok = 1; ct_enc = 'B';
              break;
            }
          }
        }
        put_debug(7);
      }
    } else { /* not putinheader */
      put_debug(8);
      if (linespastheader == -1) {
        put_debug(9);
        if (mimenesting > 0 && boundary[mimenesting-1].len &&
            (*line.s == '-') && (line.len > boundary[mimenesting-1].len) &&
            !str_diffn(line.s, boundary[mimenesting-1].s, boundary[mimenesting-1].len)) {
          if ((line.len > (boundary[mimenesting-1].len + 2)) &&
              !str_diffn(line.s + boundary[mimenesting-1].len, "--", 2)) {
            mimenesting--;
            if (flagrfc822) {
              if (rfc822nest == mimenesting) {
                flagrfc822 = 0;
                rfc822nest = 0;
              }
            }
          } else {
            linespastheader = 0;
            flagrfc822 = 0;
          }
          linetype = ' ';
          linestate = 0;
          pcre_line.len = 0;
          /* in the first header line of multipart entity */
          if (!stralloc_copys(&content, "")) die_nomem();
          put_debug(10);
        } else {
          put_debug(11);

          if ((ct_encbody == 1) && base64ok && bsha1ok) {
            int ret;

            put_debug(12);
            ret = b64decode(line.s, line.len-1, &lineout);
            if (line.len-1 > 76) flagcontent |= ACL_CONTENT_BASE64LEN;
            switch (ret) {
              case -1:
                die_nomem();
                break;
              case 1:
                flagcontent |= ACL_CONTENT_BADBASE64;
              case 0:
                sha1_process(&sha1ctx, lineout.s, lineout.len);
                if (flagtext) {
                  acl_line_process(ACL_PCRE_BODYB, &lineout, 0);
                }
                break;
              case 2:
                flagcontent |= ACL_CONTENT_BADBASE64;
                if (flagtext) {
                  line.len--;
                  /* allows to match invalid base64 in case it's useful... */
                  acl_line_process(ACL_PCRE_BODYB, &line, 0);
                  line.len++;
                }
                break;
            }
          } else {
            /* MIME multipart's preamble/epilogue not matched */
            if (flagtext) {
              line.len--;
              acl_line_process(ACL_PCRE_BODYB, &line, ct_enc);
              line.len++;
            }
          }
        }
      } else if ((linespastheader == 0) && (line.len > 1)) {
        put_debug(13);
        if ((*line.s == ' ' || *line.s == '\t')) {
          switch(linetype) {
            case 'C': if (!stralloc_catb(&content, line.s, line.len-1)) die_nomem(); break;
            default: break;
          }
          if (!stralloc_catb(&pcre_line, line.s, line.len-1)) die_nomem();
          linestate = 1;
          put_debug(14);
        } else {
          put_debug(15);
          if (case_startb(line.s, line.len, "content-type:")) {
            if (!stralloc_copyb(&content, line.s+13, line.len-14)) die_nomem();
            linetype = 'C';
          } else {
            linetype = ' ';
            /* content-transfer-encoding in multipart MIME attachments
             * overrides the one specified in body part */
            switch (get_ct(&line)) {
              case 0:   ct_enc = 0;   base64ok = 0; break;
              case 'Q': ct_enc = 'Q'; base64ok = 0; break;
              case 'B': ct_enc = 'B'; base64ok = 1; break;
            }
          }
          switch (linestate) {
            case 0:
              linestate = 1;
              if (!stralloc_copyb(&pcre_line, line.s, line.len-1)) die_nomem();
              break;
            case 1:
              acl_line_process(flagrfc822 ? ACL_PCRE_RFC822 : ACL_PCRE_MIME, &pcre_line, 0);
              if (!stralloc_copyb(&pcre_line, line.s, line.len-1)) die_nomem();
              break;
          }
          put_debug(16);
        }
        put_debug(17);
      } else if ((linespastheader == 0) && (line.len == 1)) {
        put_debug(18);
        acl_line_process(flagrfc822 ? ACL_PCRE_RFC822 : ACL_PCRE_MIME, &pcre_line, 0);
        pcre_line.len = 0;
        linestate = 0;
        linespastheader = 1;
        linetype = ' ';
        if (content.len) {
          i = checkct(0);
          if (i == 2) {
            flagrfc822 = 1;
            rfc822nest = mimenesting;
            if (!stralloc_copys(&content, "")) die_nomem();
            linespastheader = 0;
          } else if (i == 3) {
            flagtext = 1;
          } else {
            flagtext = 0;
          }
        } else {
          /* text/plain */
          flagtext = 1;
        }
        put_debug(19);
      } else if ((linespastheader >= 1)) {
        put_debug(20);
        if (linespastheader == 1 || linespastheader == 3) {
          put_debug(21);
          if (base64ok) {
            if (line.len >= 4) {
              linespastheader = 2;
              check_badsigs(&line);
            } else {
              /* We want to check sigs also after empty base64-line */
              linespastheader = 3;
            }
            if (line.len >= 6) {
              if (!str_diffn(line.s, "UEsDBA", 6) ||
                  !str_diffn(line.s, "UEsDBB", 6)) {
                flagcontent |= ACL_CONTENT_ZIP;
              }
            }
            if (line.len >= 9) {
              if (!str_diffn(line.s, "TV", 2)) {
                if (!str_diffn(line.s, "TVoAAAAAA", 9) ||
                    !str_diffn(line.s, "TVoAAAEAA", 9) ||
                    !str_diffn(line.s, "TVoAAAQAA", 9) ||
                    !str_diffn(line.s, "TVoFAQUAA", 9) ||
                    !str_diffn(line.s, "TVoIARMAA", 9) ||
                    !str_diffn(line.s, "TVouARsAA", 9) ||
                    !str_diffn(line.s, "TVpAALQAc", 9) ||
                    !str_diffn(line.s, "TVpCAQEAA", 9) ||
                    !str_diffn(line.s, "TVpOAAcAA", 9) ||
                    !str_diffn(line.s, "TVpQAAIAA", 9) ||
                    !str_diffn(line.s, "TVpyAXkAX", 9) ||
                    !str_diffn(line.s, "TVqAAAEAA", 9) ||
                    !str_diffn(line.s, "TVoAAD8AA", 9) ||
                    !str_diffn(line.s, "TVqQAAIAA", 9) ||
                    !str_diffn(line.s, "TVqQAAMAA", 9) ||
                    !str_diffn(line.s, "TVqaAAMAA", 9) ||
                    !str_diffn(line.s, "TVqiAAQAA", 9) ||
                    !str_diffn(line.s, "TVr1AR4AA", 9) ||
                    !str_diffn(line.s, "TVrNABIAN", 9) ||
                    !str_diffn(line.s, "TVrQAT8AA", 9) ||
                    !str_diffn(line.s, "TVrhARwAk", 9) ||
                    !str_diffn(line.s, "TVrmAU4AA", 9) ||
                    !str_diffn(line.s, "TVryAAgAB", 9))
                      flagcontent |= ACL_CONTENT_MSEXE;
              } else {
                /* TVqQAAMAA encoded again and again! */
                if (!str_diffn(line.s, "VFZxUUFBT", 9) ||
                    !str_diffn(line.s, "VkZaeFVVR", 9))
                  flagcontent |= ACL_CONTENT_MSEXE;
              }
            }
            if ((line.len >= 20)) {
              if (!str_diffn(line.s, "0M8R4KGxGuEAAAAAAAAA", 20)) {
                flagcontent |= ACL_CONTENT_MSWORD;
              }
            }
            if ((line.len >= 5)) {
              if (!str_diffn(line.s, "eJ8+I", 5)) {
                flagcontent |= ACL_CONTENT_TNEF;
              }
            }
            put_debug(22);
          } else {
            linespastheader = 2;
          }
          put_debug(23);
        }
        if (mimenesting > 0 && boundary[mimenesting-1].len && (*line.s == '-') &&
            (line.len > boundary[mimenesting-1].len) &&
            !str_diffn(line.s, boundary[mimenesting-1].s, boundary[mimenesting-1].len)) {

          put_debug(24);
          if ((line.len > (boundary[mimenesting-1].len + 2)) &&
              !str_diffn(line.s + boundary[mimenesting-1].len, "--", 2)) {
            mimenesting--;
            if (flagrfc822) {
              if (rfc822nest == mimenesting) {
                flagrfc822 = 0;
                rfc822nest = 0;
              }
            }
            linespastheader = -1;
            /* ignore MIME preamble/epilogue */
            flagtext = 0;
          } else {
            linespastheader = 0;
            linestate = 0;
            if (ct_encbody <= 0) ct_enc = 0;
            if (flagrfc822) {
              if (rfc822nest == mimenesting) {
                flagrfc822 = 0;
                rfc822nest = 0;
              }
            }
          }
          if (!stralloc_copys(&content, "")) die_nomem();
          put_debug(25);
          /* NOTE: if unwanted attachment has been detected,
             don't calculate SHA1 for the other attachments after that */
          if (!(flagcontent & ACL_CONTENT_BADSHA1) && base64ok && bsha1ok) {
            put_str_sha1();
          }
          if (ct_encbody == -1) base64ok = 0;
          put_debug(26);
        } else if (!(flagcontent & ACL_CONTENT_BADSHA1) && base64ok && bsha1ok) {
          int ret;

          put_debug(27);
          ret = b64decode(line.s, line.len-1, &lineout);
          if (line.len-1 > 76) flagcontent |= ACL_CONTENT_BASE64LEN;
          switch (ret) {
            case -1:
              die_nomem();
              break;
            case 1:
              flagcontent |= ACL_CONTENT_BADBASE64;
            case 0:
              sha1_process(&sha1ctx, lineout.s, lineout.len);
              if (flagtext) {
                acl_line_process(flagrfc822 ? ACL_PCRE_RFC822B : ACL_PCRE_MIMEB, &lineout, 0);
              }
              break;
            case 2:
              flagcontent |= ACL_CONTENT_BADBASE64;
              if (flagtext) {
                line.len--;
                /* allows to match invalid base64 in case it's useful... */
                acl_line_process(flagrfc822 ? ACL_PCRE_RFC822B : ACL_PCRE_MIMEB, &line, 0);
                line.len++;
              }
              break;
          }
        } else {
          put_debug(28);
          if (flagtext) {
            line.len--;
            acl_line_process(flagrfc822 ? ACL_PCRE_RFC822B : ACL_PCRE_MIMEB, &line, ct_enc);
            line.len++;
          }
        }
      }
      put_debug(29);
    }
    line.len = 0;
  }
  if ((putinheader == 1) && ((unsigned char)*ch > 127)) {
    flagcontent |= ACL_CONTENT_RFC2047;
  }
  qmail_put(&qqt,ch,1);
  bytesread++;
}

static void msgidhmac(void)
{
  uint8 base32digest[SHA1DIGESTSIZEBASE32];
  sha1_hash_state sha1ctxtmp;
  static uint8 counter[8];

  if ((flagcontent & ACL_CONTENT_NOMSGID)) {
    /* we have now seen the message headers with no Message-ID,
       append Message-ID */
    if (hmackeyok) {
      if (!stralloc_copy(&msgid, &hmackeyline)) die_nomem();
      msgid.len--;
      if (!stralloc_cats(&msgid, ":msgid")) die_nomem();
      sha1_hmac_memory(hmackey.s, hmackey.len, msgid.s, msgid.len,
                       base32digest, SHA1DIGESTSIZEBASE32, SHA1DIGESTBASE32);
    } else { /* hopefully you're using VERBOSEQMAILID */
      sha1_hmac_memory((uint8*)&counter, sizeof(counter),
                       qmailid.s, qmailid.len-1, base32digest,
                       SHA1DIGESTSIZEBASE32, SHA1DIGESTBASE32);
      if (!++counter[0]) if (!++counter[1]) if (!++counter[2]) if (!++counter[3])
        if (!++counter[4]) if (!++counter[5]) if (!++counter[6]) ++counter[7];
    }
    if (!stralloc_copyb(&msgid, base32digest, SHA1DIGESTSIZEBASE32)) die_nomem();
    if (!stralloc_cats(&msgid, "@")) die_nomem();
    if (!stralloc_catb(&msgid, idhost.s, idhost.len)) die_nomem();
    if (!stralloc_0(&msgid)) die_nomem();
    qmail_puts(&qqt, "Message-ID: <");
    qmail_puts(&qqt, msgid.s);
    qmail_puts(&qqt, ">\n");
  }

  sha1_init(&sha1ctxtmp);
  sha1_process(&sha1ctxtmp, msgid.s, msgid.len-1);
  sha1_done(&sha1ctxtmp, msgiddigest, SHA1DIGESTSIZEHEX, SHA1DIGESTHEX);
  msgiddigest[SHA1DIGESTSIZEHEX] = 0;
}

static void blast(int *hops)
{
  char ch;
  int state;
  int flaginheader;
  int pos; /* number of bytes since most recent \n, if fih */
  int flagmaybex; /* 1 if this line might match RECEIVED, if fih */
  int flagmaybey; /* 1 if this line might match \r\n, if fih */
  int flagmaybez; /* 1 if this line might match DELIVERED, if fih */

  state = 1;
  *hops = 0;
  flaginheader = 1;
  pos = 0; flagmaybex = flagmaybey = flagmaybez = 1;
  for (;;) {
    substdio_get(&ssin,&ch,1);
    if (flaginheader) {
      if (pos < 9) {
        if (ch != "delivered"[pos]) if (ch != "DELIVERED"[pos]) flagmaybez = 0;
        if (flagmaybez) if (pos == 8) ++*hops;
        if (pos < 8)
          if (ch != "received"[pos]) if (ch != "RECEIVED"[pos]) flagmaybex = 0;
        if (flagmaybex) if (pos == 7) ++*hops;
        if (pos < 2) if (ch != "\r\n"[pos]) flagmaybey = 0;
        if (flagmaybey) if (pos == 1) if (flaginheader) {
          flaginheader = 0;
          msgidhmac();
        }
        ++pos;
      }
      if (ch == '\n') { pos = 0; flagmaybex = flagmaybey = flagmaybez = 1; }
    }
    switch(state) {
      case 0:
        if (ch == '\n') {
          if (!flagstray) {
            straynewline();
          }  else {
            state = 1;
            foundstray = 1;
            break;
          }
        }
        if (ch == '\r') { state = 4; continue; }
        break;
      case 1: /* \r\n */
        if (ch == '\n') {
          if (!flagstray) {
            straynewline();
          }  else {
            foundstray = 1;
          }
        }
        if (ch == '.') { state = 2; continue; }
        if (ch == '\r') { state = 4; continue; }
        if (!flagstray) {
          state = 0;
        }  else {
          if (ch != '\n') state = 0;
        }
        break;
      case 2: /* \r\n + . */
        if (ch == '\n') {
          if (!flagstray) {
            straynewline();
          } else {
            if (flaginheader) msgidhmac();
            foundstray = 1;
            return;
          }
        }
        if (ch == '\r') { state = 3; continue; }
        state = 0;
        break;
      case 3: /* \r\n + .\r */
        if (ch == '\n') {
          /* no msgbody? */
          if (flaginheader) msgidhmac();
          /* Final touch if qp_softlb==1 */
          line.len = 0;
          acl_line_process(-1, &line, 0);
          return;
        }
        put(".");
        put("\r");
        if (ch == '\r') { state = 4; continue; }
        state = 0;
        break;
      case 4: /* + \r */
        if (ch == '\n') { state = 1; break; }
        if (ch != '\r') { put("\r"); state = 0; }
    }
    put(&ch);
  }
}

static void acceptmessage(qp, authid) unsigned long qp; char* authid;
{
  datetime_sec when;
  char accept_buf[FMT_ULONG];

  when = now();
  out("250 ok ");
  accept_buf[fmt_ulong(accept_buf,(unsigned long) when)] = 0;
  out(accept_buf);
  out(" qp ");
  accept_buf[fmt_ulong(accept_buf,qp)] = 0;
  out(accept_buf);
  if (authid) { out(" "); out(authid); }
  if (foundstray) out(" (found stray LF)");

#if USE_TCP_INFO
  {
    struct tcp_info info;
    socklen_t optlen;
    char slong[FMT_ULONG];

    optlen = sizeof(info);
    if (tcpinfook &&
        getsockopt(ssl_rfd, SOL_TCP, TCP_INFO, (void*)&info, &optlen) == 0) {
      if (!stralloc_copys(&str_tcpi, " (RTT=")) die_nomem();
      slong[fmt_ulong(slong, info.tcpi_rtt)] = 0;
      if (!stralloc_cats(&str_tcpi, slong)) die_nomem();
      if (!stralloc_cats(&str_tcpi, "us LAST_DATA_RECV=")) die_nomem();
      slong[fmt_ulong(slong, info.tcpi_last_data_recv)] = 0;
      if (!stralloc_cats(&str_tcpi, slong)) die_nomem();
      if (!stralloc_cats(&str_tcpi, "ms TOTAL_RETRANS=")) die_nomem();
      slong[fmt_ulong(slong, info.tcpi_total_retrans)] = 0;
      if (!stralloc_cats(&str_tcpi, slong)) die_nomem();
      if (!stralloc_cats(&str_tcpi, ")")) die_nomem();
      if (!stralloc_0(&str_tcpi)) die_nomem();
      out(str_tcpi.s);
    }
  }
#endif
  out("\r\n");
}

#ifdef TLS_SMTPD

static void addtlsinfo(stralloc *sslstr)
{
  char *cp;

  cp = env_get("SSL_PROTOCOL");
  if (cp) if (!stralloc_cats(sslstr, cp)) die_nomem();
  if (!stralloc_cats(sslstr, ":")) die_nomem();
  cp = env_get("SSL_CIPHER");
  if (cp) if (!stralloc_cats(sslstr, cp)) die_nomem();
  cp = env_get("SSL_PUBKEY_SIZE");
  if (cp) {
    if (!stralloc_cats(sslstr, " client pubkey=")) die_nomem();
    if (!stralloc_cats(sslstr, cp)) die_nomem();
    if (!stralloc_cats(sslstr, "b")) die_nomem();
  }
}

#endif

static void smtp_data(char *arg)
{
  int hops;
  int wstat;
  unsigned long qp;
  unsigned char base32digest[SHA1DIGESTSIZEBASE32 + 1];
  char *qqx;
  char *datamsg;
  int aclret;
  struct timespec ts_start, ts_end;
  int inq;

  if (!seenmail) { err_wantmail(); return; }
  inq = socket_inqueue(ssl_rfd);
  if (ssin.p || (inq > 0)) {
    char snum[FMT_ULONG];

    out("503 invalid DATA PIPELINING (#5.5.1)\r\n");
    flush();
    snum[fmt_ulong(snum, inq)] = 0;
    strerr_warn7("qmail-smtpd: id ", qmailid.s, " EXIT_PIPELINE_DATA",
                 (!rcptto.len) ? "_NORCPT inq=" : " inq=", snum, " ",
                 gen_tcpinfo(), 0);
    _exit(1);
  }
  if (!rcptto.len) { err_wantrcpt(); return; }

  seenmail = 0;
  bytesread = 0;
  flagnullconn = 1;
  for (mimenesting = 0; mimenesting < MAXMIMENESTING; mimenesting++)
    boundary[mimenesting].len = 0;
  mimenesting = 0;
  rfc822nest = 0;
  if (!stralloc_copys(&content, "")) die_nomem();
  if (!stralloc_copys(&line, "")) die_nomem();
  if (!stralloc_copys(&pcre_line, "")) die_nomem();
  linestate = -1;
  putinheader = 1;
  linespastheader = -1;
  flagcontent = ACL_CONTENT_NOMSGID;
  base64ok = 0;
  ct_encbody = -1;
  ct_enc = 0;
  flagrfc822 = 0;
  flagtext = 0;
  linetype = ' ';
  foundstray = 0;
  if (!stralloc_copys(&str_ct, "text/plain")) die_nomem();
  if (!stralloc_0(&str_ct)) die_nomem();

  if (!stralloc_copys(&protocolinfo, "")) die_nomem();

#ifdef PCRE_SMTPD
  acl_pcre_status = acl_pcre_setup();
  if (acl_pcre_status == -1) {
    strerr_warn3("qmail-smtpd: id ", qmailid.s, " 451 ACL_PCRE_SETUP", 0);
    out("451 temporary error with PCRE (#4.3.0)\r\n");
    return;
  }
#endif

  acl_sd_status = acl_sd_setup();
  if (acl_sd_status == -1) {
    strerr_warn3("qmail-smtpd: id ", qmailid.s, " 451 ACL_SPAMDOMAIN_SETUP", 0);
    out("451 temporary error with spamdomain feature (#4.3.0)\r\n");
    return;
  }

  acl_hdrip_status = acl_hdrip_setup();
  if (acl_hdrip_status == -1) {
    strerr_warn3("qmail-smtpd: id ", qmailid.s, " 451 ACL_HDRIP_SETUP", 0);
    out("451 temporary error with hdrip feature (#4.3.0)\r\n");
    return;
  }

  acl_badsigs_status = acl_badsigs_setup();

#ifdef TLS_SMTPD
  if(flagtlsinit) {
#define X509NAMELEN 256
    char *str;

    if (!stralloc_cats(&protocolinfo, "(")) die_nomem();
    addtlsinfo(&protocolinfo);
    if (!stralloc_cats(&protocolinfo, ")")) die_nomem();

    /* RFC3848 */
    if (!stralloc_cats(&protocolinfo, (authd) ?
        " ESMTPSA\n    " : " ESMTPS\n    ")) die_nomem();

  } else {
    if (authd) {
      if (!stralloc_copys(&protocolinfo, "ESMTPA")) die_nomem();
    } else {
      if (!stralloc_copys(&protocolinfo, (flagbadhelo & ACL_HELO_HELO) ?
          "SMTP" : "ESMTP")) die_nomem();
    }
  }
#else
  if (authd) {
    if (!stralloc_copys(&protocolinfo, "ESMTPA")) die_nomem();
  } else {
    if (!stralloc_copys(&protocolinfo, (flagbadhelo & ACL_HELO_HELO) ?
        "SMTP" : "ESMTP")) die_nomem();
  }
#endif
  if (!stralloc_0(&protocolinfo)) die_nomem();

  if (hmackeyok) {
    char str[FMT_ULONG];
    static unsigned int ctr = 1;
    long int i, j;

    if (!stralloc_copys(&hmackeyline, "")) die_nomem();
    if (!stralloc_cats(&hmackeyline, qmailid.s)) die_nomem();
    if (authd) {
      if (!stralloc_cats(&hmackeyline, ":AUTH=")) die_nomem();
      safestr(user.s, &hmackeyline, 64, 0);
      if (!stralloc_cats(&hmackeyline, ":")) die_nomem();
    } else {
      if (!stralloc_cats(&hmackeyline, ":NOAUTH:")) die_nomem();
    }

    if (helohost.len)
      safestr(helohost.s, &hmackeyline, 64, 0);
    if (!stralloc_cats(&hmackeyline, ":")) die_nomem();
    if (remoteinfo)
      safestr(remoteinfo, &hmackeyline, 64, 0);
    if (!stralloc_cats(&hmackeyline, ":FROM=")) die_nomem();
    safestr(mailfrom.s, &hmackeyline, MAXLOGLEN, 0);
    if (!stralloc_cats(&hmackeyline, ":SIZE=")) die_nomem();
    if (!stralloc_cats(&hmackeyline, smtpsize_s)) die_nomem();
    if (!stralloc_cats(&hmackeyline, ":TO=")) die_nomem();
    i = 0;
    while (1) {
      if ((rcptto.s[i]) != 'T') break;
      i++;
      j = str_len(rcptto.s + i);
      safestr(rcptto.s + i, &hmackeyline, j, 0);
      i += j + 1;
      if (i >= rcptto.len) break;
      if (!stralloc_cats(&hmackeyline, "\\000")) die_nomem();
    }
    if (!stralloc_cats(&hmackeyline, ":")) die_nomem();
    str[fmt_ulong(str, ctr)] = 0;
    ctr++;
    if (!stralloc_cats(&hmackeyline, str)) die_nomem();
    if (!stralloc_cats(&hmackeyline, ":")) die_nomem();
    str[fmt_ullong(str, rdtscl())] = 0;
    if (!stralloc_cats(&hmackeyline, str)) die_nomem();
    if (!stralloc_0(&hmackeyline)) die_nomem();
    sha1_hmac_memory(hmackey.s, hmackey.len, hmackeyline.s, hmackeyline.len-1,
                     base32digest, SHA1DIGESTSIZEBASE32, SHA1DIGESTBASE32);
    base32digest[SHA1DIGESTSIZEBASE32] = 0;
    if (!stralloc_copys(&smtpauthid, protocolinfo.s)) die_nomem();
    if (!stralloc_cats(&smtpauthid, " ID ")) die_nomem();
    if (!stralloc_catb(&smtpauthid, base32digest, SHA1DIGESTSIZEBASE32)) die_nomem();
    if (!stralloc_0(&smtpauthid)) die_nomem();
  }

  if (!stralloc_copys(&bogoerror, "Dbogofilter suspects the mail to be spam - ID=") ||
      !stralloc_cats(&bogoerror, base32digest) ||
      !stralloc_cats(&bogoerror, " (#5.6.0)") ||
      !stralloc_0(&bogoerror)) {
    die_nomem();
  }
  if (!env_put2("BOGOERROR", bogoerror.s)) die_nomem();
  if (!env_put2("BOGOID", base32digest)) die_nomem();

  if (qmail_open(&qqt) == -1) { err_qqt(); return; }
  qp = qmail_qp(&qqt);

  /* wait for 0.01s if qq happens to die/exit */
  my_nanosleep(0, 10000000);
  if ((waitpid(qp, &wstat, WNOHANG) == qp)) {
    char num[FMT_ULONG];

    close(qqt.fde);
    close(qqt.fdm);
    flagnullconn = 0;
    num[fmt_ulong(num, wait_crashed(wstat) ? wait_crashed(wstat) : wait_exitcode(wstat))] = 0;
    strerr_warn5("qmail-smtpd: id ", qmailid.s, " 451 qq trouble: ",
                 (wait_crashed(wstat) ? "sig " : "exitcode "), num, 0);
    out("451 qqt premature failure (#4.3.0)\r\n");
    return;
  }

  out("354 make my day\r\n"); flush();
  clock_gettime(CLOCK_MONOTONIC, &ts_start);
  if (flagaestrap) qmail_put(&qqt, aestrap_info.s, aestrap_info.len);
  if (flaggreyid_ok) {
    qmail_puts(&qqt, "X-greylist:");
    qmail_put(&qqt, str_greydelays.s, str_greydelays.len);
    qmail_puts(&qqt, "\n");
  }
  if (rblok) rblheader(&qqt);
  strerr_warn3("qmail-smtpd: id ", qmailid.s, " DATA", 0);
  received(&qqt, hmackeyok ? smtpauthid.s : protocolinfo.s, local,
           remoteip, remoteport, remotehost, remoteinfo, fakehelo);

  blast(&hops);

  if ((ct_encbody == 1) && base64ok && bsha1ok) {
    put_str_sha1();
  }

  bytesread_s[fmt_ullong(bytesread_s, bytesread)] = 0;
  clock_gettime(CLOCK_MONOTONIC, &ts_end);
  flagnullconn = 0;
  hops = (hops >= MAXHOPS);
  if (hops) qmail_fail(&qqt);
  aclret = acl_match_data(&datamsg);
  if (aclret == ACL_FAIL) {
    qmail_fail(&qqt);
  }
  qmail_from(&qqt, mailfrom.s);
  qmail_put(&qqt, rcptto.s, rcptto.len);
  qqx = qmail_close(&qqt);
  if (aclret == ACL_FAIL) {
    out(datamsg);
    out("\r\n");
    return;
  }
  if (!*qqx) {
    if (hmackeyok) {
      uint64 nsecs;
      uint64 res;
      char bps_s[FMT_ULONG];

      nsecs = tspec_elapsed(&ts_end, &ts_start);
      res = (uint64_t)(((double)bytesread*(double)1000000000ULL) / (double)nsecs);
      bps_s[fmt_ullong(bps_s, res)] = 0;
      strerr_warn12("qmail-smtpd: id ", qmailid.s,
                    foundstray ? " FOUNDSTRAY MSGID-SHA1=" : " NOSTRAY MSGID-SHA1=",
                    msgiddigest, " BYTESREAD=", bytesread_s, " BPS=", bps_s,
                    " AUTHID=", hmackeyline.s, " ", base32digest, 0);
    }
    acceptmessage(qp, hmackeyok ? base32digest : 0);
#if USE_TCP_INFO
    if (tcpinfook)
      strerr_warn4("qmail-smtpd: id ", qmailid.s, " TCP_INFO", str_tcpi.s, 0);
#endif
    return;
  }
  if (hops) {
    err_looping();
    return;
  }
  if (*qqx == 'D') {
    strerr_warn6("qmail-smtpd: id ", qmailid.s, " 554 ACL_DATA_REJECT_PERM: mail bytes=",
                 bytesread_s, " ID=", base32digest, 0);
    out("554 ");
  } else {
    strerr_warn4("qmail-smtpd: id ", qmailid.s, " 451 ACL_DATA_REJECT_TEMP: mail bytes=",
                 bytesread_s, 0);
    out("451 ");
  }
  out(qqx + 1);
  out("\r\n");
}

#ifdef TLS_SMTPD

/* RFC2246 RFC3207 RFC3546 */
static void smtp_tls(char *arg)
{
  uint32 ctlfd;
  int ctlret;
  char ctlbuf[8192];
  static stralloc sslenv = {0};
  char *cp;
  char *ema;

  if (!tlsok) { err_unimpl(0); return; }
  if (flagtlsinit) { err_syntax(); return; }

  if (*arg) {
    out("501 Syntax error (no parameters allowed) (#5.5.4)\r\n");
    return;
  }

  out("220 ready for TLS\r\n"); flush();

  if (!ucspitls(&ctlfd)) {
   strerr_warn3("qmail-smtpd: id ", qmailid.s, " failed to initialize TLS", 0);
   _exit(1);
  }

  if (!stralloc_copys(&sslenv, "")) die_nomem();
  /* Wait 60 seconds for TLS to initialize */
  while ((ctlret = timeoutread(60, ctlfd, ctlbuf, sizeof(ctlbuf))) > 0) {
    if (!stralloc_catb(&sslenv, ctlbuf, ctlret)) die_read();
    if (sslenv.len >= 2 && sslenv.s[sslenv.len-2]==0 && sslenv.s[sslenv.len-1]==0)
      break;
  }
  if (ctlret < 0) {
    if (errno == error_timeout) die_alarm();
    die_read();
  }
  cp = sslenv.s;
  while (*cp) {
    if (!env_put(cp)) die_read();
    cp += str_len(cp) + 1;
  }

  if (!stralloc_copys(&protocolinfo, "")) die_read();
  addtlsinfo(&protocolinfo);

  if (!stralloc_copys(&cert_email, "")) die_read();
  cp = env_get("SSL_VERIFY_STATUS");
  if (!cp) die_read();
  if (!str_diff(cp, "OK")) {
    if (!stralloc_copys(&cert_status_str, "OK")) die_read();
    cert_status = ACL_CERT_OK;
    ema = env_get("SSL_CLIENT_S_DN_Email");
    if (ema) {
      if (!stralloc_cats(&cert_email, ema)) die_read();
      if (!stralloc_cats(&cert_status_str, ", email=")) die_read();
      if (!stralloc_cats(&cert_status_str, ema)) die_read();
    }
  } else if (!str_diff(cp, "NOCERT")) {
    if (!stralloc_copys(&cert_status_str, "FAIL: no cert received")) die_read();
    cert_status = ACL_CERT_NOCERT;
  } else {
    if (!stralloc_copys(&cert_status_str, "FAIL: ")) die_read();
    if (!stralloc_cats(&cert_status_str, env_get("SSL_VERIFY_STRING"))) die_read();
    cert_status = ACL_CERT_INVALIDCERT;
  }
  cp = env_get("SSL_RFD");
  if (!scan_ulong(cp, &ssl_rfd)) die_read();
  cp = env_get("SSL_WFD");
  if (!scan_ulong(cp, &ssl_wfd)) die_read();

  if (!stralloc_0(&cert_status_str)) die_read();
  if (!stralloc_0(&cert_email)) die_read();
  if (!stralloc_0(&protocolinfo)) die_read();

  strerr_warn4("qmail-smtpd: id ", qmailid.s, " TLS connection established: ",
               protocolinfo.s, 0);
  authd = 0;
  flagbadhelo = ACL_HELO_NOHELO;
  seenmail = 0;
  flagtlsinit = 1;
  dohelo(remotehost);
}
#endif

static int authgetl(void) {
  int i;

  if (!stralloc_copys(&authin, "")) die_nomem();

  for (;;) {
    if (!stralloc_readyplus(&authin,1)) die_nomem(); /* XXX */
    i = substdio_get(&ssin, authin.s + authin.len, 1);
    if (i != 1) die_read();
    if (authin.s[authin.len] == '\n') break;
    ++authin.len;
  }

  if (authin.len > 0) if (authin.s[authin.len - 1] == '\r') --authin.len;
  authin.s[authin.len] = 0;

  if (*authin.s == '*' && *(authin.s + 1) == 0) { return err_authabrt(); }
  if (authin.len == 0) { return err_input(); }
  return authin.len;
}

static int authenticate(void)
{
  int child;
  int wstat;
  int pi[2];

  if (!stralloc_0(&user)) die_nomem();
  if (!stralloc_0(&pass)) die_nomem();
  if (!stralloc_0(&resp)) die_nomem();

  if (pipe(pi) == -1) return err_pipe();

  switch(child = fork()) {
    case -1:
      return err_fork();
    case 0:
      close(pi[1]);
      close(0);
      close(1);
      if (fd_copy(3, pi[0]) == -1)
        kill(getpid(), SIGTERM);
      sig_pipedefault();
      (void) setsid();
      execvp(*childargs, childargs);
      my_nanosleep(3, 0);
      kill(getpid(), SIGTERM);
  }

  close(pi[0]);
  substdio_fdbuf(&ssup, write, pi[1], upbuf, sizeof(upbuf));
  if (substdio_put(&ssup, user.s, user.len) == -1) { close(pi[1]); return err_write(); }
  if (substdio_put(&ssup, pass.s, pass.len) == -1) { close(pi[1]); return err_write(); }
  if (substdio_put(&ssup, resp.s, resp.len) == -1) { close(pi[1]); return err_write(); }
  if (substdio_flush(&ssup) == -1) { close(pi[1]); return err_write(); }

  close(pi[1]);
  if (wait_pid(&wstat,child) == -1) return err_child();
  if (wait_crashed(wstat)) return err_child();
  if (wait_exitcode(wstat)) { my_nanosleep(3, 0); return 1; } /* no */
  return 0; /* yes */
}

static int auth_login(arg) char *arg;
{
  int r;

  if (*arg) {
    r = b64decode(arg, str_len(arg), &user);
  } else {
    out("334 VXNlcm5hbWU6\r\n"); flush(); /* Username: */
    if (authgetl() < 0) return -1;
    r = b64decode(authin.s, authin.len, &user);
  }
  if (r == 1 || r == 2) return err_input();
  if (r == -1) die_nomem();

  out("334 UGFzc3dvcmQ6\r\n"); flush(); /* Password: */
  if (authgetl() < 0) return -1;

  r = b64decode(authin.s, authin.len, &pass);
  if (r == 1 || r == 2) return err_input();
  if (r == -1) die_nomem();

  if (!user.len || !pass.len) return err_input();
  return authenticate();
}

/* RFC2595 */
static int auth_plain(arg) char *arg;
{
  int r;
  unsigned long int id = 0;

  if (*arg) {
    r = b64decode(arg,str_len(arg),&slop);
  } else {
    out("334 \r\n"); flush();
    if (authgetl() < 0) return -1;
    r = b64decode(authin.s, authin.len, &slop);
  }
  if (r == 1 || r == 2) return err_input();
  if (r == -1 || !stralloc_0(&slop)) die_nomem();
  while (slop.s[id]) id++; /* ignore authorize-id */

  if (slop.len > id + 1) {
    if (!stralloc_copys(&user, slop.s + id + 1)) die_nomem();
  }
  if (slop.len > id + user.len + 2) {
    if (!stralloc_copys(&pass, slop.s + id + user.len + 2)) die_nomem();
  }

  if (!user.len || !pass.len) return err_input();
  return authenticate();
}

/* RFC2195 */
static int auth_cram()
{
  unsigned long int i;
  int r;
  char *s;
  struct timespec tscram;
  uint64 ticks;
  unsigned char base32digest[SHA1DIGESTSIZEBASE32];
  static unsigned long int ctr = 1;
  static char unique[(FMT_ULONG + 1)*5 + 1];

  clock_gettime(CLOCK_MONOTONIC, &tscram);
  s = unique;
  s += fmt_uint(s, getpid());
  *s++ = '.';
  s += fmt_ulong(s,(unsigned long) tscram.tv_sec);
  *s++ = '.';
  s += fmt_ulong(s,(unsigned long) tscram.tv_nsec);
  *s++ = '.';
  s += fmt_uint(s, charcrandom_u32());
  *s++ = '.';
  s += fmt_ulong(s, ctr);
  *s++ = '.';
  *s++ = 0;
  ctr++;

  if (!stralloc_copys(&pass, "<")) die_nomem();
  ticks = rdtscl();
  if (hmackeyok) {
    sha1_hmac_init(&hmactmp, hmackey.s, hmackey.len);
  } else {
    sha1_hmac_init(&hmactmp, (uint8*)&ticks, sizeof(ticks));
  }
  sha1_hmac_process(&hmactmp, "cram", 5);
  sha1_hmac_process(&hmactmp, (uint8*)&ticks, sizeof(ticks));
  sha1_hmac_process(&hmactmp, unique, str_len(unique));
  sha1_hmac_process(&hmactmp, remoteip, str_len(remoteip));
  sha1_hmac_process(&hmactmp, ".", 1);
  sha1_hmac_process(&hmactmp, remoteport, str_len(remoteport));
  sha1_hmac_done(&hmactmp, base32digest, SHA1DIGESTSIZEBASE32, SHA1DIGESTBASE32);
  if (!stralloc_catb(&pass, base32digest, SHA1DIGESTSIZEBASE32)) die_nomem();
  if (!stralloc_cats(&pass, "@")) die_nomem();
  if (!stralloc_cats(&pass, hostname)) die_nomem();
  if (!stralloc_cats(&pass, ">")) die_nomem();
  if (b64encode(&pass, &slop)  == -1) die_nomem(); /* challenge base64 encoded */
  if (!stralloc_0(&slop)) die_nomem();

  out("334 ");
  out(slop.s);
  out("\r\n");
  flush();

  if (authgetl() < 0) return -1;  /* got message digest */
  r = b64decode(authin.s, authin.len, &slop);
  if (r == 1 || r == 2) return err_input();
  if (r == -1 || !stralloc_0(&slop)) die_nomem();

  i = str_chr(slop.s,' ');
  s = slop.s + i;
  while (*s == ' ') ++s;
  slop.s[i] = 0;
  if (!stralloc_copys(&user, slop.s)) die_nomem();
  if (!stralloc_copys(&resp, s)) die_nomem();

  if (!user.len || !resp.len) return err_input();
  return authenticate();
}

static struct authcmd {
  char *text;
  int (*fun)();
} authcmds[] = {
  { "cram-md5", auth_cram },
  { "plain", auth_plain },
  { "login", auth_login },
  { 0, err_noauth }
};

/* RFC2554 */
static void smtp_auth(arg)
char *arg;
{
  unsigned long int i;
  char *authtype;
  char *cmd = arg;

  if (seenmail) { err_authmail(); return; }
  if (authd) { err_authd(); return; }
  if (!hostname || !*childargs) {
    flagnullconn = 0;
    out("503 auth not available (#5.3.3)\r\n");
    return;
  }

  if (!stralloc_copys(&user,"")) die_nomem();
  if (!stralloc_copys(&pass,"")) die_nomem();
  if (!stralloc_copys(&resp,"")) die_nomem();

  i = str_chr(cmd,' ');
  arg = cmd + i;
  while (*arg == ' ') ++arg;
  cmd[i] = 0;

  for (i = 0; authcmds[i].text; ++i)
    if (case_equals(authcmds[i].text, cmd)) break;

  if (authcmds[i].text) {
    if (smtpauthok && (authtype = constmap(&mapsmtpauth, authcmds[i].text, str_len(authcmds[i].text)))) {
#ifdef TLS_SMTPD
      if ((case_diffs(authtype, "encryption") == 0) && !flagtlsinit) {
        /* strerr_warn4("qmail-smtpd: id ", qmailid.s, " ACL_AUTH_NOTLS ", authcmds[i].text, 0); */
        flagnullconn = 0;
        out("538 STARTTLS needed with SMTP AUTH ");
        out(authcmds[i].text);
        out(" (#5.7.0)\r\n");
        return;
      }
#else
      if ((case_diffs(authtype, "encryption") == 0)) {
        /* strerr_warn4("qmail-smtpd: id ", qmailid.s, " ACL_AUTH_TLSDISABLED ", authcmds[i].text, 0); */
        flagnullconn = 0;
        out("538 STARTTLS is disabled, cannot use SMTP AUTH ");
        out(authcmds[i].text);
        out(" (#5.7.0)\r\n");
        return;
      }
#endif
    } else {
      /* strerr_warn4("qmail-smtpd: id ", qmailid.s, " ACL_AUTH_DISABLED ", authcmds[i].text, 0); */
      flagnullconn = 0;
      out("504 authentication method ");
      out(authcmds[i].text);
      out(" is disabled (#5.7.0)\r\n");
      return;
    }
  }

  switch (authcmds[i].fun(arg)) {
    case 0:
      authd = 1;
      if (!relayclient)
        relayclient = "";
      strerr_warn6("qmail-smtpd: id ", qmailid.s, " ACL_AUTH_OK ", authcmds[i].text, " ",
                   gen_safestr(user.s, 256), 0);
      out("235 ok, go ahead (#2.0.0)\r\n");
      break;
    case 1:
      flagnullconn = 0;
      strerr_warn6("qmail-smtpd: id ", qmailid.s, " ACL_AUTH_FAIL ", authcmds[i].text, " ",
                   gen_safestr(user.s, 256), 0);
      out("535 authentication failed (#5.7.0)\r\n");
      break;
  }
  byte_zero(pass.s, pass.len);
  byte_zero(upbuf, sizeof upbuf);
}

static void smtp_helo(arg) char *arg;
{
  if (*arg) flagbadhelo = ACL_HELO_OK;
  if (ssin.p || (socket_inqueue(ssl_rfd) > 0)) flagbadhelo |= ACL_HELO_PIPELINE;
  smtp_greet("250 "); out("\r\n");
  seenmail = 0;
  flagbadhelo |= helohostcheck(arg) | ACL_HELO_HELO;
  dohelo(arg);
}

static void smtp_ehlo(arg) char *arg;
{
  if (*arg) flagbadhelo = ACL_HELO_OK;
  if (ssin.p || (socket_inqueue(ssl_rfd) > 0)) flagbadhelo |= ACL_HELO_PIPELINE;
  smtp_greet("250-"); out("\r\n");
  out("250-PIPELINING\r\n");
  out("250-SIZE "); out(databytes_s);
  if (smtpauthok) {
    int i;

    for (i = 0; authcmds[i].text; ++i) {
      if (constmap(&mapsmtpauth, authcmds[i].text, str_len(authcmds[i].text))) {
        out("\r\n250-AUTH ");
        out(authcmds[i].text);
        out("\r\n250-AUTH=");
        out(authcmds[i].text);
      }
    }
  }
#ifdef TLS_SMTPD
  if (tlsok && !flagtlsinit)
  out("\r\n250-STARTTLS");
#endif
  out("\r\n250 8BITMIME\r\n");

  seenmail = 0;
  flagbadhelo |= helohostcheck(arg) | ACL_HELO_EHLO;
  dohelo(arg);
}

static struct commands smtpcommands[] = {
  { "rcpt", smtp_rcpt, 0 }
, { "mail", smtp_mail, 0 }
, { "data", smtp_data, flush }
, { "auth", smtp_auth, flush }
, { "quit", smtp_quit, flush }
, { "helo", smtp_helo, flush }
, { "ehlo", smtp_ehlo, flush }
, { "rset", smtp_rset, 0 }
, { "help", smtp_help, flush }
#ifdef TLS_SMTPD
, { "starttls", smtp_tls, 0 }
#endif
, { "noop", err_noop, flush }
, { "vrfy", err_vrfy, flush }
, { "get", smtp_notproxy, flush }
, { "head", smtp_notproxy, flush }
, { "post", smtp_notproxy, flush }
, { 0, err_unimpl, flush }
} ;

int main(argc,argv)
int argc;
char **argv;
{
  hostname = argv[1];
  childargs = argv + 2;

  sig_pipeignore();
  if (chdir(auto_qmail) == -1) die_control();
  setup();
  if (ipme_init() != 1) die_ipme();
  smtp_greet("220-");
  out(" ESMTP ; you are ");
  if(remoteinfo && remoteinfo[0]) { out(remoteinfo); out(" from "); }
  out(remoteip);
  if(remotehostresolved) { out(" ("); out(remotehost); out(")"); }
  out("\r\n220-senders of unsolicited bulk email and viruses/other malware"
      "\r\n220 (hopefully) get punished to the maximum extent of the law\r\n");
  flush();
  if (commands(&ssin,smtpcommands) == 0) die_read();
  die_nomem();
  return 1;
}


#include "substdio.h"

void substdio_fdbuf(s,op,fd,buf,len)
substdio *s;
ssize_t (*op)(int, void*, size_t);
long int fd;
char *buf;
long int len;
{
  s->x = buf;
  s->fd = fd;
  s->op = op;
  s->p = 0;
  s->n = len;
}

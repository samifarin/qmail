#include "stralloc.h"
#include "dns.h"

int dns_domain_todot_cat(stralloc *out,const char *d)
{
  char ch;
  unsigned char ch2;
  unsigned char ch3;
  static char buf[4] = "\\777";
  static const unsigned char chlookup[256] = {
      ['A'] = 1, ['B'] = 1, ['C'] = 1, ['D'] = 1, ['E'] = 1, ['F'] = 1,
      ['G'] = 1, ['H'] = 1, ['I'] = 1, ['J'] = 1, ['K'] = 1, ['L'] = 1,
      ['M'] = 1, ['N'] = 1, ['O'] = 1, ['P'] = 1, ['Q'] = 1, ['R'] = 1,
      ['S'] = 1, ['T'] = 1, ['U'] = 1, ['V'] = 1, ['W'] = 1, ['X'] = 1,
      ['Y'] = 1, ['Z'] = 1, 
      ['a'] = 2, ['b'] = 2, ['c'] = 2, ['d'] = 2, ['e'] = 2, ['f'] = 2,
      ['g'] = 2, ['h'] = 2, ['i'] = 2, ['j'] = 2, ['k'] = 2, ['l'] = 2,
      ['m'] = 2, ['n'] = 2, ['o'] = 2, ['p'] = 2, ['q'] = 2, ['r'] = 2,
      ['s'] = 2, ['t'] = 2, ['u'] = 2, ['v'] = 2, ['w'] = 2, ['x'] = 2,
      ['y'] = 2, ['z'] = 2, ['0'] = 2, ['1'] = 2, ['2'] = 2, ['3'] = 2,
      ['4'] = 2, ['5'] = 2, ['6'] = 2, ['7'] = 2, ['8'] = 2, ['9'] = 2,
      ['-'] = 2, ['_'] = 2
  };

  if (!*d)
    return stralloc_append(out,".");

  for (;;) {
    ch = *d++;
    while (ch--) {
      ch2 = *d++;
      switch (chlookup[ch2]) {
        case 1:
          ch2 += 'a' - 'A';
        case 2:
          if (!stralloc_append(out,&ch2)) return 0;
          break;
        default:
          ch3 = ch2;
          buf[3] = '0' + (ch3 & 7); ch3 >>= 3;
          buf[2] = '0' + (ch3 & 7); ch3 >>= 3;
          buf[1] = '0' + (ch3 & 7);
          if (!stralloc_catb(out,buf,4)) return 0;
          break;
      }
    }
    if (!*d) return 1;
    if (!stralloc_append(out,".")) return 0;
  }
}

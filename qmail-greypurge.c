/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

#include <unistd.h>

#include "qmail-acl.h"
#include "stralloc.h"
#include "fmt.h"
#include "now.h"
#include "str.h"
#include "byte.h"
#include "strerr.h"
#include "scan.h"
#include "auto_qmail.h"
#include "substdio.h"
#include "helper.h"

#include "qmail-sqlite3.h"

#define BEGIN \
  "BEGIN;"

#define BEGINEXCL \
  "BEGIN EXCLUSIVE;"

#define COMMIT \
  "COMMIT;"

#define ROLLBACK \
  "ROLLBACK;"

#define STAT_DELETE \
  "DELETE FROM qmailgrey WHERE windowend < strftime('%s','now') - ?;"

#define TABLECHECK \
  "SELECT name FROM sqlite_master WHERE type='table' AND name='qmailgrey';"

static char ssoutbuf[512];
static substdio ssout = SUBSTDIO_FDBUF(write, 1, ssoutbuf, sizeof(ssoutbuf));

static const char greydbfn[] = "control/grey/sqlite.db";
static const char oomtext[] = "out of memory";
static char *sqlite3error;
static int flagoom;
static stralloc errmsg = {0};
sqlite3_stmt *st_delete;
sqlite3 *db;

void die_temp(char *die1, char *die2)
{
  strerr_die3x(111, "qmail-greypurge: ", die1, die2);
}

static sqlite3_stmt *sqlprepare(const char *sqlcommand)
{
  const char *tail;
  sqlite3_stmt *prep;

  if (sqlite3_prepare_v2(db, sqlcommand, str_len(sqlcommand),
      &prep, &tail) != SQLITE_OK) {
    if (!stralloc_copys(&errmsg, sqlite3_errmsg(db)) ||
        !stralloc_0(&errmsg))
      flagoom = 1;
    return 0;
  }
  return prep;
}

static int sqlexec(const char *sqlcommand)
{
  char *err = 0;
  int ret;

  ret = sqlite3_exec(db, sqlcommand, 0, 0, &err);
  if (ret != SQLITE_OK) {
    if (!stralloc_copys(&errmsg, sqlite3_errmsg(db)) ||
        !stralloc_0(&errmsg)) {
      flagoom = 1;
    }
    if (err)
      sqlite3_free(err);
  }
  return ret;
}

static int sql_ops(sqlite3_stmt *statem)
{
  int ret;
  int found = 0;

  while(1) {
    ret = sqlite3_step(statem);
    switch (ret) {
      case SQLITE_ROW:
        found = 1;
        break;

      case SQLITE_BUSY:
        if (!stralloc_copys(&errmsg, sqlite3_errmsg(db)) ||
            !stralloc_0(&errmsg)) {
          sqlite3error = oomtext;
        } else {
          sqlite3error = errmsg.s;
        }
        sqlite3_reset(statem);
        if (sqlexec(ROLLBACK) != SQLITE_OK) {
          if (flagoom) {
            sqlite3error = oomtext;
          } else {
            sqlite3error = errmsg.s;
          }
        }
        return -1;

      case SQLITE_DONE:
        sqlite3_reset(statem);
        return found;

      default:
        if (!stralloc_copys(&errmsg, sqlite3_errmsg(db)) ||
            !stralloc_0(&errmsg)) {
          sqlite3error = oomtext;
        } else {
          sqlite3error = errmsg.s;
        }
        sqlite3_reset(statem);
        return -1;
    }
  }
}

static int db_delete(uint64 age)
{
  int ret;

  if (sqlexec(BEGINEXCL) != SQLITE_OK) {
    if (flagoom) {
      sqlite3error = oomtext;
    } else {
      sqlite3error = errmsg.s;
    }
    return -1;
  }
  sqlite3_bind_int64(st_delete, 1, age);
  ret = sql_ops(st_delete);
  if (ret == -1) return ret;

  if (sqlexec(COMMIT) != SQLITE_OK) {
    if (flagoom) {
      sqlite3error = oomtext;
    } else {
      sqlite3error = errmsg.s;
    }
    return -1;
  }
  return ret;
}

static void db_close(void)
{
  int ret;

  if (st_delete) sqlite3_finalize(st_delete);
  st_delete = 0;
  ret = sqlite3_close(db);
  if (ret != SQLITE_OK) {
    /* crap! this must suck. */
  }
}

static int grey_checktable(void)
{
  int ret;
  int found = 0;
  int loop;
  const char *tail;
  sqlite3_stmt *statem;

  ret = sqlite3_prepare_v2(db, TABLECHECK, str_len(TABLECHECK),
                        &statem, &tail);
  if (ret != SQLITE_OK) {
    if (!stralloc_copys(&errmsg, sqlite3_errmsg(db)) ||
        !stralloc_0(&errmsg)) {
      sqlite3error = oomtext;
    } else {
      sqlite3error = errmsg.s;
    }
    sqlite3_finalize(statem);
    return ret;
  }
  loop = 1;
  while (loop) {
    ret = (sqlite3_step(statem));
    switch (ret) {
      case SQLITE_ROW:
        found = 1;
        break;
      case SQLITE_DONE:
        sqlite3_reset(statem);
        loop = 0;
        break;
      default:
        if (!stralloc_copys(&errmsg, sqlite3_errmsg(db)) ||
            !stralloc_0(&errmsg)) {
          sqlite3error = oomtext;
        } else {
          sqlite3error = errmsg.s;
        }
        sqlite3_finalize(statem);
        return ret;
    }
  }
  sqlite3_finalize(statem);
  return found ? SQLITE_OK : -1;
}

static int dbopen(void)
{
  if (sqlite3_open_v2(greydbfn, &db, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK)
    goto failed;
  sqlite3_extended_result_codes(db, 1);
  if (sqlite3_busy_timeout(db, SQLITE_BUSYWAIT) != SQLITE_OK)      
    goto failed;
  if (sqlexec(BEGIN) != SQLITE_OK)
    goto failedclose;

  switch (grey_checktable()) {
    case SQLITE_OK:
      if (sqlexec(COMMIT) != SQLITE_OK)
        goto failedclose;
      break;
    default:
      goto failedclose;
      break;
  }
  st_delete = sqlprepare(STAT_DELETE);
  if (!st_delete) goto failedclose;

  return ACL_GREY_PASS;

failed:
  if (!stralloc_copys(&errmsg, sqlite3_errmsg(db)) ||
      !stralloc_0(&errmsg)) {
    flagoom = 1;
  }
failedclose:
  db_close();
  if (flagoom) {
    sqlite3error = oomtext;
  } else {
    sqlite3error = errmsg.s;
  }
  return ACL_GREY_INTERROR;
}

static int dbpurge(uint64 secs, uint64 *nrpurged)
{
  switch (db_delete(secs)) {
    case -1:
      return ACL_GREY_INTERROR;
    case 0:
    case 1:
      *nrpurged = sqlite3_total_changes(db);
      return ACL_GREY_PASS;
  }
  return ACL_GREY_INTERROR;
}

int main (int argc, char *argv[])
{
  uint64 secint;
  uint64 nrpurged;
  char stpurge[FMT_ULONG];

  if (argc != 2)
    die_temp("usage: [age (seconds)]", "");
  if (!scan_ullong(argv[1], &secint))
    die_temp("seconds should be decimal number", "");
  if (chdir(auto_qmail) == -1)
    die_temp("chdir ", auto_qmail);
  if (dbopen() != ACL_GREY_PASS)
    die_temp("open database ", errmsg.s);
  if (dbpurge(secint, &nrpurged) != ACL_GREY_PASS)
    die_temp("purge database ", errmsg.s);

  stpurge[fmt_ullong(stpurge, nrpurged)] = 0;
  substdio_puts(&ssout, stpurge);
  substdio_puts(&ssout, "\n");
  substdio_flush(&ssout);

  return 0;
}


#include "fmt.h"
#include "qmail.h"
#include "now.h"
#include "datetime.h"
#include "date822fmt.h"
#include "received.h"

static inline int issafe(unsigned char ch)
{
  static const unsigned char safechars[256] = {
    ['0'] = 1, ['1'] = 1, ['2'] = 1, ['3'] = 1, ['4'] = 1, ['5'] = 1,
    ['6'] = 1, ['7'] = 1, ['8'] = 1, ['9'] = 1, ['a'] = 1, ['b'] = 1,
    ['c'] = 1, ['d'] = 1, ['e'] = 1, ['f'] = 1, ['g'] = 1, ['h'] = 1,
    ['i'] = 1, ['j'] = 1, ['k'] = 1, ['l'] = 1, ['m'] = 1, ['n'] = 1,
    ['o'] = 1, ['p'] = 1, ['q'] = 1, ['r'] = 1, ['s'] = 1, ['t'] = 1,
    ['u'] = 1, ['v'] = 1, ['w'] = 1, ['x'] = 1, ['y'] = 1, ['z'] = 1,
    ['A'] = 1, ['B'] = 1, ['C'] = 1, ['D'] = 1, ['E'] = 1, ['F'] = 1,
    ['G'] = 1, ['H'] = 1, ['I'] = 1, ['J'] = 1, ['K'] = 1, ['L'] = 1,
    ['M'] = 1, ['N'] = 1, ['O'] = 1, ['P'] = 1, ['Q'] = 1, ['R'] = 1,
    ['S'] = 1, ['T'] = 1, ['U'] = 1, ['V'] = 1, ['W'] = 1, ['X'] = 1,
    ['Y'] = 1, ['Z'] = 1, ['.'] = 1, ['@'] = 1, ['%'] = 1, ['+'] = 1,
    ['/'] = 1, ['='] = 1, [':'] = 1, ['-'] = 1
  };

  return safechars[ch];
}

static inline void safeput(qqt,s)
struct qmail *qqt;
char *s;
{
  unsigned char ch;

  while ((ch = *s++)) {
    if (!issafe(ch)) ch = '?';
    qmail_put(qqt,&ch,1);
  }
}

static char buf[DATE822FMT];

/* "Received: from relay1.uu.net (HELO uunet.uu.net) (7@192.48.96.5)\n" */
/* "  by silverton.berkeley.edu with SMTP; 26 Sep 1995 04:46:54 -0000\n" */

void received(qqt,protocol,local,remoteip,remoteport,remotehost,remoteinfo,helo)
struct qmail *qqt;
char *protocol;
char *local;
char *remoteip;
char *remoteport;
char *remotehost;
char *remoteinfo;
char *helo;
{
  struct datetime dt;

  qmail_puts(qqt,"Received: from ");
  safeput(qqt,remotehost);
  if (helo) {
    qmail_puts(qqt," (HELO ");
    qmail_puts(qqt,helo);
    qmail_puts(qqt,")");
  }
  qmail_puts(qqt," (");
  if (remoteinfo) {
    safeput(qqt,remoteinfo);
    qmail_puts(qqt,"@");
  }
  qmail_puts(qqt, "[");
  safeput(qqt,remoteip);
  qmail_puts(qqt, "]");
  qmail_puts(qqt, ":");
  safeput(qqt,remoteport);
  qmail_puts(qqt,")\n  by ");
  safeput(qqt,local);
  qmail_puts(qqt," with ");
  qmail_puts(qqt,protocol);
  qmail_puts(qqt,";\n  ");
  datetime_tai(&dt,now());
  qmail_put(qqt,buf,date822fmt(buf,&dt));
}

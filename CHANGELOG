20040317
 - first public release

20040319
 - changed input buffer in aestrapgen to four bytes
 - using idhost in qmail-smtpd.c as the Message-ID host

20040324
 - ipme.c / HASSALEN fix

20040331
 - qmail-send.c / generate Message-ID fields

20040402
 - qmail-smtpd.c / ACL 'HO': matches when helohost == localip
   and remoteip != localip
 - qmail-smtpd.c / PCRE support added for header/body lines.
   see file README.safari .
 - improved base64decoder behavior on invalid & short inputs
 - ditched DJB's alloc stuffs, using malloc/realloc/free only
   - with alloc_re copying old data is not necessary anymore,
     so with long PCRE files this gives about five times better
     performance.

20040404
 - qmail-smtpd.c / put() fixes

20040408
 - fixed bug in setup_qqargs (QMAILQUEUE patch) which resulted into
   possibly freed memory being used as binqqargs[0].
   funny effect caused by it: qmail-smtpd was trying to execve()
   whatever garbage was in that memory position.
 - PCRE matchopt 'k' to immediately kill connection on first match.

20040425
 - renamed log2 to qslog2 because log2 is already defined in ISO/IEC 9899:1999
   (also log1, log3 renamed)

20040506
 - removed 3s pause from bingo (err_unimpl).
   evidently some spamware ignores 5xx-error for RCPT, then
   sends DATA _and_ the spam.

20040510
 - qmail-smtpd.c / smtp_tls(): verify callback now always returns success.

20040629
 - qmail-cdbacl.c: non-comment-lines now need to be correctly delimited
   with ':', so fix your users/assignacl.

20040702
 - Makefile: add uint32.h to nughde.o deps.

20041111
 - qmail-smtpd.c: TCP_INFO: show all the values in struct tcp_info.
   also show tcpi_last_data_recv only when tcpi_ato>0.
   if your glibc does not have the new additions found in kernel 2.6.9+,
   add these to /usr/include/netinet/tcp.h at the end of struct tcp_info:
     u_int32_t     tcpi_rcv_rtt;
     u_int32_t     tcpi_rcv_space;
     u_int32_t     tcpi_total_retrans;

20041216
 - support 64 bit st_ino in struct stat and delid (delivery id)
   in qmail-send.c and qmail-qread.c

20041218
 - qmail-queue.c: delete created files in intd and mess if qmail-queue
   gets invalid input from stdin
 - README.safari: add recommended file permissions for {smtpd,abbs,send}key

20050201
 - qmail-smtpd.c: add 'M4' ACL which is like 'M3' except that 451 error
   is given out
 - update various man pages

20050214
 - convert DNS to use djbdns library. no more of this thingy:
       $ size /lib/libresolv-2.3.4.so
          text    data     bss     dec     hex filename
         60988    1844    9960   72792   11c58 /lib/libresolv-2.3.4.so

   NOTE: you can set env var DNSCACHEIP so looking at /etc/resolv.conf
         is not needed.  little test showed djbdns library is about 2.3
         times faster than glibc getaddrinfo.
 - qmail-smtpd.c: fix problem in helocheck when localip env var is not set
 - use uint32 instead of unsigned long in scan_ulong.c and fmt_ulong.c
 - received.c: record remoteport in Received header field
   and put IP inside []
 - qmail-remote.c: the first IP which is tried to be connected is selected
   based on outgoingip/sender/recipient/time combination to play nicely
   with greylist-enabled SMTP servers.  use control/remoterekey to change
   the time (seconds) after which time interval the IPs are shuffled.
   control/remotekey contains the key used with HMAC-SHA1.
   dns_shuffle does the shuffling.

20050216
 - change DNS timeouts to 25 seconds [3+8+14 second timeouts]
   as a comparison, glibc-2.3.4 has 20 seconds [5+5+5+5 seconds]

20050225
 - qmail-smtpd.c: if env var BANNERDELAY is set and RBLSMTPD is not set
   to empty string ('whitelisted'), sleep BANNERDELAY seconds
   before showing the SMTP banner.  This deters many trojans.
   BANNERDELAY can be set in tcprules.
   To set delays otherwise, set env var BANNERDELAY_DNSOK and
   BANNERDELAY_DNSERROR.  BANNERDELAY_DNSOK is number of seconds
   to wait if there are no DNS errors (soft or hard),
   BANNERDELAY_DNSERROR is number of seconds to wait if there
   are DNS errors.
   Connection state is checked every second if USE_TCP_INFO is set to 1.
   Also TCP socket's In-Queue size is checked on Linux to detect invalid
   pipelining.  Start playing with 35 or 65 seconds...
   RFC2821 says about timeouts: ``Initial 220 Message: 5 minutes''.
   Note also that DNS and ident take time, too.
   I do not recommend using more than 240 seconds delay.
   Remember to increase your concurrency limits.
   One disadvantage: you won't see which mailboxes get spammed if
   bannerdelay made the spammer go away.
 - qmail-smtpd.c: require HELO or EHLO.  Remove corresponding ACL 'H1'.
   Require also argument for HELO or EHLO.
 - qmail-smtpd.c: require Domain in MAIL FROM.  Remove corresponding
   ACL 'M5'.
 - qmail-smtpd.c received.c: always log HELO/EHLO and tell if it was
   spoofed.

20050301
 - qmail-smtpd.c dns_qmail.c: add 'MNF'/'MNT' ACL, which acts like
   'MIF'/'MIT' except checks for the domain's nameservers' IPs.
 - aestrap-cgi.c: change generation logic of Last-Modified header.
 - use GCC 3.4+ feature warn_unused_result if available
 - implement RHSBL, ACL 'MR'.  It takes three chars as parameters,
   after them the rhsbl domain.
   - meaning of characters:
     - char 1: T: give temporary 450 error if match found
               t: likewise, except give temporary 450 error also
                  if there was temporary DNS/memory problem
               P: give permanent 553 error if match found
               p: likewise, except give temporary 450 error
                  if there was temporary DNS/memory problem
    - char 2:  M: use domain in MAIL FROM
               R: use domain in RCPT TO
               H: use $TCPREMOTEHOST.  This does nothing if
                  that environment variable is not set.
    - char 3:  A: search for A records
               T: search for TXT records
  - for example, MRTHAdynamic.rhs.mailpolice.com

20050303
 - qmail-smtpd.c: print invalid options and matchons for PCRE files.
   check your files.  pcredebug must be >=3.

20050307
 - implement URI scanning for message bodies, ACL 'R' (ACL 'XS' checks
   the status).  'R0' disables URI scanning (spamdomains feature).
   'Rfoo' reads configuration file from control/spamdomain/foo.
   For more info read README.safari.
 - implement hdrip content scanning for headers (not multipart or rfc822).
   when you use this you don't need to hack long PCRE-patterns.
   usage: put header fields where you see IPv4 addresses into ``control/hdrip''.
   then use ACL 'Ifoo.bl.local' to check TXT records from DNSbl foo.bl.local
   for each found IP address.  ACL 'XH' checks hdrip status.
   ACL 'I0' disables adding any more nameservers.
   I recommend that you only keep a local list of bad IPs (no SBL,...).

20050321
 - RFC3848 support

20050515
 - Take helohost length into account in ACL H8.

20050530
 - Better errors for DNS checks M8 and M9.
 - Attempt at fixing qmail for 64bit systems, where sizeof(void*) != sizeof(int).

20050605
 - qmail-smtpd.c: when looking for boundary and charset in content-type header
   field, skip whitespace around the '=' character.
 - add ACL support for HELO/EHLO hostname,
   =hxyz in assignacl matches "HELO xyz" or "EHLO xyz".

20050615
 - qmail-smtpd.c: null-terminate char passed to env_put2("BOGOOPT"...
   to quieten valgrind.
 - qmail-smtpd.c: include TCPLOCALPORT in qmailid

20050616
 - README.safari: add timings for spamdomain feature (tested on Celeron 366MHz).
 - qmail-smtpd.c: add timing support (using rdtscl()) for spamdomain feature.

20050622
 - qmail-smtpd.c: add badsigs feature.  Put unwanted signatures (first bytes
   of the file encoded in hex), followed by colon and description into
   control/badsigs.  Signature is treated as a PCRE pattern.
   Pattern is compiled with PCRE_ANCHORED flag, so ^ is not needed.
   For example:
   4749463839618.02e.01f0..000.0.00ffffff21f90404000010002c000000007f02e3010002ff8c8fa9cbed0fa39cb4:college_degree_1-206-984-0021
   The corresponding ACL is 'XI'.  Signatures are checked on every non-text
   base64-encoded line (only the first line of an attachment).

20050623
 - qmail-smtpd.c qmail-remote.c: to work around lameness in freebsd,
   move "#include <sys/types.h>" as the first include file.

20050918
 - Convert to use ucspi-ssl-0.70-ucspitls in qmail-smtpd.c.
   This gives really nice CPU usage savings with STARTTLS.
   Also, OpenSSL stuff can run chrooted and with lowest privileges.

    Then set env var CRLDIR=/var/qmail/control/crl for sslserver.
    It's recommended to update the CRL files frequently.
    If they're more than a month old, OpenSSL reports FAILURE for
    SSL verify.

    Needed patch:
    https://gitlab.com/samifarin/qmail/blob/master/data/ucspi-ssl-0.70-ucspitls-latest.diff
    It's required that all env vars are set correctly.
    So use sslserver -sne.

 - DH keys needed: generate the keys dh1024.pem and dh512.pem
   or waste minute(s) of CPU time when they're generated for each
   STARTTLS invocation.

20051005
 - qmail-remote.c: apply http://untroubled.org/qmail/qmail-1.03-fastremote-3.patch

20051021
 - rbl.c qmail-smtpd.c: add actions "whitelistp" and whitelistf".
   They act like "whitelist", when there are no temporary DNS or memory errors,
   but when there's a temporary error, "whitelistp" ('p' as in 'pass') skips
   further RBL checks, acting like "whitelist" when a match is found.
   Likewise, "whitelistf" returns 450 error on temporary errors ('f' as in 'fail').
 - cleanup dns_Txt: return DNS_HARD instead of -1 if zero-length TXT record was found

20051128
 - qmail-smtpd.c: if pcre_compile fails, show also the offset in the pattern
   (remember that you quote '/' char...).

20051130
  - qmail-smtpd.c: (re)set env vars BOGOOPT and QMAILQUEUE only after
    smtp_rcpt() succeeds.  Latest valid 'Qx' (Q1, Q2, Q3, Q4 or Q5) ACL
    is in effect in smtp_data().

20051213
 - qmail-smtpd.c: the content-transfer-encoding in multipart MIME
   attachments always overrides the one specified in body part

20051215
 - qmail-smtpd.c: treat helohost len>255 as ACL_HELO_DNS_HARD.
   treat helohost with only one label (e.g. ends with dot(s), and no
   dots elsewhere) as ACL_HELO_NONFQDN.
 - qmail-smtpd.c: restore QMAILQUEUE env var to its original value
   after each MAIL FROM command.

20051217
 - qmail-smtpd.c: greylist support with sqlite3-3.2.6+.
   ACL 'MG' enables greylisting for a particular mailbox.
   control/grey dir holds the data files:
   'msg' is extra info given with the 452-greylist rejection,
   'initial' is initial delay needed before accepting,
   'window' is timewindow during which mail is accepted after
   initial time (window does not include initial, so make sure
   initial is bigger then window;
   finally, expire is max lifetime of the record.
   sqlite3 database is created as 'sqlite.db'.
   It's recreated when needed if it does not exist.
   For more info, see README.safari.

20060118
 - qmail-smtpd.c: log number of bytes read if mail is rejected by qmail-queue

20060222
 - qmail-smtpd.c: feature: add possibility to choose which values are
   keyed into the greyid -- by default IP address, SMTP SIZE, HELO,
   MAIL FROM and RCPT TO.  This can be configured by flag 'K' into
   GREYOPTS env var, or ACL 'MGK', where after 'K' you can give
   following chars:
     I remote IP address
     S SMTP SIZE
     H HELO/EHLO value
     F MAIL FROM
     T RCPT TO
   (Use K as the last option, or add ' ' after the final char.)
   NOTE: generated greyids are different than in earlier versions
   because of this feature.  You may delete sqlite.db before upgrading.

20060309
 - qmail-smtpd.c: add skip feature to spamdomain-feature for
   domain IPs, nameservers and spamnameserver IPs.
   See README.safari for current list of options.
   Do not use these features if you don't need them: it just causes
   more useless DNS queries.  Remember to check rbldnsd loads OK
   if you start using these features.

20060326
 - qmail-smtpd.c: new control file control/spamdomain/www:
   if it has integer != 0, apply spamdomain checks for www.* domains
   (otherwise always require http://)

20060423
 - upgraded qmail-smtpd.c to use doubly-linked circular lists,
   taken from Linux 2.6.16 (used with PCRE and spamdomain feature).
   Sorry dudes, also license changed.
   qmail-1.03-safari-20060403T102030Z was the latest non-GPL.

20060511
 - general: change some functions to take void* pointers instead of char* etc.
 - byte_*.c byte.h builtinhacks.h: optimize operations for constant-size
   operations of 0-16 bytes for byte_zero byte_copy and byte_diff.
   also add const as appropriate.

20060525
 - qmail-smtpd.c base64.c: ACL 'X6' now also matches when _some_ chars
   in base64 data were invalid (earlier all had to be invalid),
   and it also matches if 76 chars line length was exceeded (RFC2045).
   Gotcha: 'X6' does not match if badsha1 matched earlier.

20060725
 - qmail-smtpd.c: skip empty base64 lines before checking for ACLs
   'XI' and 'XZ' etc.

20070120
 - qmail-smtpd.c: new ACL 'CD0 xyz' matches if DNS is broken
   and executes ACL xyz.  ACL 'CD1 xyz' matches if DNS is OK
   and executes ACL xyz.  For example, 'CD0 MG:' executes
   greylisting ACL 'MG' if DNS is broken for the connecting
   SMTP client.  Likewise 'CH0' for invalid HELO and 'CH1'
   for OK HELO, and 'CS0' for missing SMTP SIZE parameter,
   and 'CS1' matches if SMTP SIZE feature was used.
   You can also do 'CD0H0S0 MG' in which case 'MG' ACL is executed
   if ANY of 'D0', 'H0' or 'S0' matched.
 - qmail-smtpd.c: show TCP info after client has sent one message.
   Also log the line.  Also show system error in case it is returned
   if remote client disconnects.
     2007-01-29 22:36:15.932879500 qmail-smtpd: id 9194/1170102975.929913/80.223.106.128:25/87.219.215.65:2713 EXIT_BANNERDELAY_NULLCONN (0.00/16 s, inq=0) TCP_INFO ERROR="connection reset" STATE=TCP_CLOSE CA_STATE=TCP_CA_Open OPT=( SACK) RTO=3000000 ATO=0 RTT=0 RTTVAR=750000 RCV_SSTHRESH=5720 SND_SSTHRESH=100 SND_CWND=2 ADVMSS=1430 SND_MSS=512 RCV_MSS=512 RCV_SPACE=5720 LAST_DATA_SENT=1013 LAST_ACK_RECV=1013
   Note that some versions of Postfix have buggy STARTTLS implementation
   and retry emails infinitely when we send FIN packet _after_ Postfix
   has already sent QUIT.
 - qmail-smtpd.c: replace ioctl SIOCINQ with FIONREAD.
 - qmail-smtpd.c: use lookup tables when generating safe strings.
 - qmail-smtpd.c: log client's pubkey size if STARTTLS is used.

20070201
 - rbl.c: show $RBLINFO only when rejecting, not when writing X-RBL header.
 - qmail-smtpd.c: if SMTP clients sends data before we send SMTP greeting,
   tell it so, if it is still connected.

20070228
 - qmail.smtpd.c: log IP address in ACL_AESTRAP_DENY_HARD_DNSBL
   (the just decrypted one).

20070306
 - dns_random.c: use DJB's salsa20.
 - builtinhacks.h: update from djbdns-1.05-epoll patch.
 - case.h: inline.

20070309
 - qmail-smtpd.c: s/\<sleep\>/my_nanosleep/g .
 - qmail-remote.c: if control/remotekeyfstat contains integer different than 0,
   take st.{st_size,st_mtime,st_ino} returned by fstat() into account when
   generating HMAC.  This allows selecting different first IP address to try
   when having the same sender/recipient(s) list.

20070503
 - *.safari: fix some typos.
 - qmail-smtpd.c: do not do TCPINFO if SSL is being used to work around
   buggy Postfix.

20070806
 - qmail-smtpd.c: add quoted-printable (RFC2045) validity checks for ACL 'X6'.
 - qmail-smtpd.c: support "soft" line breaks in quoted-printable (RFC2045) messages.

20070829
 - taia_now.c, qmail-local.c, qmail-send.c, qmail-smtpd.c: use clock_gettime.

20070911
 - make sure users of scan_ulong pass pointer to uint32.
 - rdtsc.c: add support for __x86_64__.

20071016
 - qmail-smtpd.c: show CPU time used after client does QUIT.
 - fmt_ulong0.c: new function fmt_ulong0.
 - newfield.c: modify output produced by msgidfmt.
 - qmail-smtpd.c: record reason why HELO/EHLO check failed in Received header field.

20071018
 - qmail-smtpd.c: ACL 'MP' support.  Allows PCRE matching for remotehost.

20071020
 - qmail-smtpd.c: optimize PCRE lookups.

20071021
 - qmail-smtpd.c: forbid null recipient.

20071123
 - qmail-greypurge.c qmail-sqlite3.c: update to sqlite3 _v2 functions.

20080424
 - qmail-smtpd.c: limit spamdomain/maxdomains to 256.
 - qmail-smtpd.c: use hash table in caching already checked spamdomains.

20080426
 - various: check bounds for size parameter for alloc() function

20080427
 - qmail-remote.c: show reason for failed connect()
 - qmail-send.c: allow configurable retry schedule (for remote deliveries),
   file control/retryschedule .
   Default is 20.  Reloading this config file with SIGHUP is supported.
   Smaller means more aggressive retry schedule.  Valid range is 10..100.
   Also little randomization is done for the times.
 - djb-nextretry.c: new file.  For trying retry schedules.
   ./djb-nextretry 0 10 | tailocal

20080428
 - qmail-remote.c: show IP address we tried to connect, or say "TCP timeout"
   if every IP address tried was listed by qmail-tcpto, or say "protocol error"
   if greeting, HELO or EHLO resulted in error 400..499.
 - qmail-remote.c: also use EHOSTUNREACH and ENETUNREACH as error cases to tcpto_err.

20080828
 - qmail-smtpd.c: fix bannerdelay delay loop by using 64bit math

20090129
 - dns_transmit.c: support 4096 byte UDP DNS packets

20101003
 - qmail-smtpd.c: use constant time verify in security–critical AES operations

20101126
 - remove builtinhacks.h, use memset/memcpy/memcmp

20140726
 - rbl.c: prevent buffer overflow if you have >= 32 rbls configured

20141015
 - qmail-inject.c: fix sieve vacation message issue

20150223
 - dns_transmit.c: timeouts now 1,3,6,10 s—We're not in Kansas anymore
 - qmail-smtpd.c: show SSL_CLIENT_DIGEST_SHA256.  Also, md5 is not shown anymore.
 - cdb uses siphash.

20151112
 - fix links to point to gitlab and also include some configs and patches in
   data/ dir


#ifndef FD_H
#define FD_H

#include "helper.h"

extern int fd_copy(int, int);
extern int fd_move(int, int);

#endif

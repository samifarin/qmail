/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

#include <unistd.h>
#include "fd.h"
#include "wait.h"
#include "prot.h"
#include "substdio.h"
#include "stralloc.h"
#include "scan.h"
#include "fork.h"
#include "error.h"
#include "cdb.h"
#include "case.h"
#include "slurpclose.h"
#include "auto_qmail.h"
#include "auto_uids.h"
#include "qlx.h"
#include "open.h"
#include "byte.h"
#include "qmail-acl.h"
#include "strerr.h"
#include "str.h"
#include "fmt.h"

static stralloc lower = {0};
static stralloc wildchars = {0};
static stralloc tmpstr = {0};

extern char *acldfl;

int acl_getcdb(mailbox, mailboxacl)
char *mailbox;
stralloc *mailboxacl;
{
  int r;
  int fd;
  uint32 dlen;
  unsigned long int i;
  int flagwild;

#if 0
  strerr_warn3("qmail-smtpd acl_getcdb: [", mailbox, "]", 0);
#endif
  if (!stralloc_copys(&lower, "!")) return ACL_ERR_TEMP;
  if (!stralloc_cats(&lower, mailbox)) return ACL_ERR_TEMP;
  if (!stralloc_0(&lower)) return ACL_ERR_TEMP;
  case_lowerb(lower.s, lower.len);

  if (!stralloc_copys(mailboxacl, "")) return ACL_ERR_TEMP;

  fd = open_read("users/cdbacl");
  if (fd == -1) {
    return (errno == error_noent) ? ACL_ERR_NOMATCH : ACL_ERR_TEMP;
  }
  r = cdb_seek(fd, "", 0, &dlen);
  if (r != 1) {
    (void)close(fd);
    return ACL_ERR_TEMP;
  }
  if (!stralloc_ready(&wildchars, (unsigned int) dlen)) {
    (void)close(fd);
    return ACL_ERR_TEMP;
  }
  wildchars.len = dlen;
  if (cdb_bread(fd,wildchars.s,wildchars.len) == -1) {
    (void)close(fd);
    return ACL_ERR_TEMP;
  }

  i = lower.len;
  flagwild = 0;

  do {
    /* i > 0 */
    if (!flagwild || (i == 1) || (byte_chr(wildchars.s, wildchars.len,
                                  lower.s[i - 1]) < wildchars.len)) {
      r = cdb_seek(fd,lower.s,i,&dlen);
      if (r == -1) {
        (void)close(fd);
        return ACL_ERR_TEMP;
      }
      if (r == 1) {
        if (!stralloc_ready(mailboxacl, (unsigned int) dlen)) {
          (void)close(fd);
          return ACL_ERR_TEMP;
        }
        mailboxacl->len = dlen;
        if (cdb_bread(fd, mailboxacl->s, mailboxacl->len) == -1) {
          (void)close(fd);
          return ACL_ERR_TEMP;
        }
        if (flagwild)
          if (!stralloc_cats(mailboxacl, mailbox + i - 1)) {
            (void)close(fd);
            return ACL_ERR_TEMP;
          }
        (void)close(fd);
        if (!stralloc_0(mailboxacl)) return ACL_ERR_TEMP;
        return ACL_ERR_MATCH;
      }
    }
    --i;
    flagwild = 1;
  } while (i);

  (void)close(fd);
  return ACL_ERR_NOMATCH;
}

static int acl_getpreset(stralloc *tmp, stralloc *dest)
{
  unsigned long int colon;
  int ret;

  colon = byte_chr(dest->s, dest->len, ':');
  if (colon == 1) return ACL_ERR_PERM;
  if (!stralloc_copyb(tmp, &dest->s[1], colon-1)) return ACL_ERR_TEMP;
  if (!stralloc_0(tmp)) return ACL_ERR_TEMP;
  ret = acl_getcdb(tmp->s, dest);
  return ret;
}

int acl_get(mailbox, mailboxacl, type, flagrcpt)
  char *mailbox;        /* ACL to search for */
  stralloc *mailboxacl; /* fetched data is stored here */
  unsigned char type;   /* 'f' = MAIL FROM, 't' = RCPT TO */
  int flagrcpt;         /* 1=domain in rcpthosts */
{
  int i;
  unsigned long int j, len, domaindot;
  int ret;
  int flagover;
  char *str_flagover[2] = {"o", ""};

  len = str_len(mailbox);
  j = byte_rchr(mailbox, len, '@');
  if (type != 't') flagrcpt = 0;

  for (flagover = 0; flagover <= 1; flagover++) {
    for (i = 0; i < (1 + flagrcpt); i++) {
      if (!stralloc_copys(&tmpstr, str_flagover[flagover])) return ACL_ERR_TEMP;
      if (!stralloc_cats(&tmpstr, (i == 0 && flagrcpt) ? "r" : "")) return ACL_ERR_TEMP;
      if (!stralloc_catb(&tmpstr, &type, 1)) return ACL_ERR_TEMP;
      if (!stralloc_cats(&tmpstr, mailbox)) return ACL_ERR_TEMP;
      if (!stralloc_0(&tmpstr)) return ACL_ERR_TEMP;
      /* box@example.com */
      ret = acl_getcdb(tmpstr.s, mailboxacl);
      if ((ret != ACL_ERR_NOMATCH) && (ret != ACL_ERR_MATCH)) return ret;
      /* redirect/alias to some other pre-set */
      if (ret == ACL_ERR_MATCH) {
        if (mailboxacl->s[0] == '=')
          return acl_getpreset(&tmpstr, mailboxacl);
        else
          return ACL_ERR_MATCH;
      }
    }

    if (j < len) {
      /* we have '@' */
      /* wildcard domain + '@' + mailbox */
      for (domaindot = j + 1; domaindot < len; domaindot++) {
        if ((domaindot == j + 1) || (mailbox[domaindot] == '.')) {
          if (!stralloc_copys(&tmpstr, str_flagover[flagover])) return ACL_ERR_TEMP;
          if (!stralloc_catb(&tmpstr, "w", 1)) return ACL_ERR_TEMP;
          if (!stralloc_catb(&tmpstr, &type, 1)) return ACL_ERR_TEMP;
          if (!stralloc_cats(&tmpstr, mailbox + domaindot)) return ACL_ERR_TEMP;
          if (!stralloc_catb(&tmpstr, "@", 1)) return ACL_ERR_TEMP;
          if (!stralloc_catb(&tmpstr, mailbox, j)) return ACL_ERR_TEMP;
          if (!stralloc_0(&tmpstr)) return ACL_ERR_TEMP;
          ret = acl_getcdb(tmpstr.s, mailboxacl);
          if ((ret != ACL_ERR_NOMATCH) && (ret != ACL_ERR_MATCH)) return ret;
          if (ret == ACL_ERR_MATCH) {
            if (mailboxacl->s[0] == '=')
              return acl_getpreset(&tmpstr, mailboxacl);
            else
              return ACL_ERR_MATCH;
          }
        }
      }

      /* box@ */
      if (!stralloc_copys(&tmpstr, str_flagover[flagover])) return ACL_ERR_TEMP;
      if (!stralloc_catb(&tmpstr, &type, 1)) return ACL_ERR_TEMP;
      if (!stralloc_catb(&tmpstr, mailbox, j + 1)) return ACL_ERR_TEMP;
      if (!stralloc_0(&tmpstr)) return ACL_ERR_TEMP;
      ret = acl_getcdb(tmpstr.s, mailboxacl);
      if ((ret != ACL_ERR_NOMATCH) && (ret != ACL_ERR_MATCH)) return ret;
      if (ret == ACL_ERR_MATCH) {
        if (mailboxacl->s[0] == '=')
          return acl_getpreset(&tmpstr, mailboxacl);
        else
          return ACL_ERR_MATCH;
      }

      /* @foo.example.com or @.example.com or @.com */
      for (domaindot = j + 1; domaindot < len; domaindot++) {
        if ((domaindot == j + 1) || (mailbox[domaindot] == '.')) {
          if (!stralloc_copys(&tmpstr, str_flagover[flagover])) return ACL_ERR_TEMP;
          if (!stralloc_catb(&tmpstr, &type, 1)) return ACL_ERR_TEMP;
          if (!stralloc_catb(&tmpstr, "@", 1)) return ACL_ERR_TEMP;
          if (!stralloc_cats(&tmpstr, mailbox + domaindot)) return ACL_ERR_TEMP;
          if (!stralloc_0(&tmpstr)) return ACL_ERR_TEMP;
          ret = acl_getcdb(tmpstr.s, mailboxacl);
          if ((ret != ACL_ERR_NOMATCH) && (ret != ACL_ERR_MATCH)) return ret;
          if (ret == ACL_ERR_MATCH) {
            if (mailboxacl->s[0] == '=')
              return acl_getpreset(&tmpstr, mailboxacl);
            else
              return ACL_ERR_MATCH;
          }
        }
      }
    }
  }

  /* get default - NOTE: you can't (shouldn't) use "+t:something" because it would
   * always be matched in the box@example.com check */
  if (!stralloc_copys(&tmpstr, "d")) return ACL_ERR_TEMP;
  if (!stralloc_catb(&tmpstr, &type, 1)) return ACL_ERR_TEMP;
  if (acldfl) if (!stralloc_cats(&tmpstr, acldfl)) return ACL_ERR_TEMP;
  if (!stralloc_0(&tmpstr)) return ACL_ERR_TEMP;
  ret = acl_getcdb(tmpstr.s, mailboxacl);
  if ((ret != ACL_ERR_NOMATCH) && (ret != ACL_ERR_MATCH)) return ret;
  if (ret == ACL_ERR_MATCH) {
    if (mailboxacl->s[0] == '=')
      return acl_getpreset(&tmpstr, mailboxacl);
    else
      return ACL_ERR_MATCH;
  }

  return ACL_ERR_NOMATCH;
}


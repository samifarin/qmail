/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>

#include "alloc.h"
#include "control.h"
#include "constmap.h"
#include "base32.h"
#include "scan.h"
#include "stralloc.h"
#include "open.h"
#include "byte.h"
#include "substdio.h"
#include "getln.h"
#include "str.h"
#include "sha1.h"
#include "aestrap.h"
#include "ip.h"
#include "datetime.h"
#include "case.h"
#include "rcpthosts.h"
#include "strerr.h"
#include "aes.h"
#include "fmt.h"
#include "auto_qmail.h"
#include "nughde.h"
#include "error.h"
#include "helper.h"

void die_usage(void)
{
  strerr_die1x(100, "aestrapgen: usage: aestrapgen mailboxprefix domain id expire");
}

void die_temp(char *die1, char *die2)
{
  strerr_die3x(111, "aestrapgen: ", die1, die2);
}

void die_tempmem(void)
{
  strerr_die1x(111, "aestrapgen: out of memory");
}

void die_perm(char *die1, char *die2)
{
  strerr_die3x(100, "aestrapgen: ", die1, die2);
}

static stralloc tmpstr = {0};
static stralloc baduid = {0};
struct constmap mapbaduid;
static stralloc genuid = {0};
struct constmap mapgenuid;
struct stralloc nughde_luser = {0};
char s_uid[FMT_ULONG];
char ssinbuf[4];
substdio ssin = SUBSTDIO_FDBUF(read, 0, ssinbuf, sizeof(ssinbuf));
char ssoutbuf[1024];
substdio ssout = SUBSTDIO_FDBUF(write, 1, ssoutbuf, sizeof(ssoutbuf));
aestrap_ctx trapctx = {0};

int main(int argc, char* argv[])
{
  uint32 genid;
  struct timeval tv;
  pid_t uid;
  uint32 time_s, time_hs; /* hundreds of a second */
  uint8 *p;
  uint32 expire = 0;
  int uidtest;
  int genuidtest;
  symmetric_key aes_key;
  sha1_hmac_state hmactmp;
  uint8 trapout[25];
  uint8 trapout_b32[40];
  uint8 tmp_enc_key[AESTRAP_KEYSIZE];
  int i;

  if (argc != 5) die_usage();
  if (str_len(argv[1]) > 900) die_perm("silly mailbox", "");
  if (chdir(auto_qmail) == -1) die_temp("chdir ", auto_qmail);
  uid = getuid();
  uidtest = control_readfile(&baduid, "control/baduid", 0);
  if (uidtest == -1) die_temp("reading control/baduid", "");
  s_uid[fmt_ulong(s_uid, uid)] = 0;
  if (uidtest) {
    if (!constmap_init(&mapbaduid, baduid.s, baduid.len, 0)) die_tempmem();
    if (constmap(&mapbaduid, s_uid, str_len(s_uid))) die_perm("baduid ", s_uid);
  }

  genuidtest = control_readfile(&genuid, "control/genuid", 0);
  if (genuidtest == -1) die_temp("reading control/genuid", "");
  if (genuidtest) {
    if (!constmap_init(&mapgenuid, genuid.s, genuid.len, 0)) die_tempmem();
  }

  if (aestrap_initkey(&trapctx, "control/aestrap", "gen") != 1)
    die_temp("aestrap_initkey failed", "");
  if (setuid(uid)) die_temp("setuid: ", error_str(errno));
  if (rcpthosts_init() == -1) die_temp("rcpthosts_init failed", "");
  if (!stralloc_copys(&tmpstr, "x@")) die_tempmem();
  if (!stralloc_cats(&tmpstr, argv[2])) die_tempmem();
  if (!rcpthosts(tmpstr.s, tmpstr.len)) die_perm("rcpthosts error", "");
  if (scan_ulong(argv[3], &genid) == 0) die_usage();
  if (genid == 0) {
    for (i = 0; i < 4; i++) {
      int ret;
      uint8 ch;

      ret = substdio_get(&ssin, &ch, 1);
      if (ret == 0) break;
      if (ret == -1) die_temp("reading input: ", error_str(errno));
      genid <<= 8UL;
      genid |= ch;
    }
  }
  if (scan_ulong(argv[4], &expire) == 0) die_usage();
  if ((expire > 1) && (expire < 60)) expire = 60;
  expire /= 60;
  if (expire >= (1UL<<24)) expire = (1UL<<24)-1; /* at max 31.9 years */
  if (gettimeofday(&tv, NULL) != 0) die_temp("gettimeofday: ", error_str(errno));
  time_s = tv.tv_sec;
  time_hs = tv.tv_usec / 10000;

  p = &trapctx.trapdata[0];
  p[0] = genid >> 24;
  p[1] = genid >> 16;
  p[2] = genid >> 8;
  p[3] = genid;
  p[4] = time_s >> 24;
  p[5] = time_s >> 16;
  p[6] = time_s >> 8;
  p[7] = time_s;
  /* highest bit 1: we have control over generated emails */
  p[8] = (time_hs | 0x80);
  p[9] = expire >> 16;
  p[10] = expire >> 8;
  p[11] = expire;
  p[12] = uid >> 24;
  p[13] = uid >> 16;
  p[14] = uid >> 8; 
  p[15] = uid; 

  /* init encryption key */
  sha1_hmac_init(&hmactmp, &trapctx.enc_key[0], SHA1DIGESTSIZERAW);
  sha1_hmac_process(&hmactmp, argv[1], str_len(argv[1]));
  sha1_hmac_process(&hmactmp, "@", 1);
  sha1_hmac_process(&hmactmp, argv[2], str_len(argv[2]));
  byte_zero(tmp_enc_key, sizeof(tmp_enc_key));
  sha1_hmac_done(&hmactmp, tmp_enc_key, sizeof(tmp_enc_key), SHA1DIGESTRAW);
  aes_setup(tmp_enc_key, AESTRAP_KEYSIZE, &aes_key);
  aes_ecb_encrypt(p, &trapout[0], &aes_key);

  /* encrypt-then-authenticate */
  sha1_hmac_init(&hmactmp, &trapctx.auth_key[0], SHA1DIGESTSIZERAW);
  sha1_hmac_process(&hmactmp, &trapout[0], 16);
  sha1_hmac_process(&hmactmp, argv[1], str_len(argv[1]));
  sha1_hmac_process(&hmactmp, "@", 1);
  sha1_hmac_process(&hmactmp, argv[2], str_len(argv[2]));
  sha1_hmac_done(&hmactmp, &trapout[16], 9, SHA1DIGESTRAW);

  base32encode(&trapout[0], &trapout_b32[0], 25);
  if (!stralloc_copys(&tmpstr, argv[1])) die_tempmem();
  if (!stralloc_catb(&tmpstr, trapout_b32, 40)) die_tempmem();
  if (!stralloc_0(&tmpstr)) die_tempmem();

#if 1
  nughde_get(tmpstr.s, &nughde_luser);
  i = byte_chr(nughde_luser.s, nughde_luser.len, 0);
  if (i++ == nughde_luser.len) die_perm("malformed users/cdb", "");
  if (str_diff(s_uid, &nughde_luser.s[i])) {
    if ((genuidtest != 1) || !constmap(&mapgenuid, s_uid, str_len(s_uid))) {
      die_perm("sorry, but this mailbox is controlled by: ", nughde_luser.s);
    }
  }
#endif

  tmpstr.len--;
  if (!stralloc_cats(&tmpstr, "@")) die_tempmem();
  if (!stralloc_cats(&tmpstr, argv[2])) die_tempmem();
  if (!stralloc_0(&tmpstr)) die_tempmem();
  substdio_puts(&ssout, tmpstr.s);
  substdio_putsflush(&ssout, "\n");
  /* taking care of uppercase chars left as an exercise for the reader */

  _exit(0);
}


#ifndef BASE64_H
#define BASE64_H

#include "stralloc.h"

extern int b64encode(stralloc*, stralloc*);
extern int b64decode(void*, unsigned long int, stralloc*);

#endif


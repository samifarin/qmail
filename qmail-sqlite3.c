/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

#include <unistd.h>

#include "qmail-acl.h"
#include "stralloc.h"
#include "fmt.h"
#include "now.h"
#include "str.h"
#include "byte.h"
#include "strerr.h"

#ifdef GREYLIST

#include "qmail-sqlite3.h"

#define BEGIN \
  "BEGIN;"

#define BEGINEXCL \
  "BEGIN EXCLUSIVE;"

#define COMMIT \
  "COMMIT;"

#define ROLLBACK \
  "ROLLBACK;"

#define STAT_SELECT_GREY \
  "SELECT * FROM qmailgrey where key=?;"

#define STAT_INSERT_GREY \
  "REPLACE INTO qmailgrey VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"

#define LAYOUT \
  "CREATE TABLE qmailgrey (      " \
  "  key CHAR(16) PRIMARY KEY,   " \
  "  created int64,              " \
  "  expires int64,              " \
  "  windowstart int64,          " \
  "  windowend int64,            " \
  "  nrblocked int64,            " \
  "  nrpassed int64,             " \
  "  lastblocked int64,          " \
  "  lastpassed int64,           " \
  "  nridqueries int64,          " \
  "  flags int64,                " \
  "  to_initial int64,           " \
  "  to_window int64,            " \
  "  to_expire int64,            " \
  "  perminitial int64,          " \
  "  maxrecip int64,             " \
  "  recipleft int64 );          "

#define TABLECHECK \
  "SELECT name FROM sqlite_master WHERE type='table' AND name='qmailgrey';"

static const char unknerr[] = "unknown error";
static const char not_found[] = "not found";
static const char oomtext[] = "out of memory";
static const char infodis[] = "greyid info disabled";
static const char greydbfn[] = "control/grey/sqlite.db";
typedef struct {
  sqlite3 *db;
  sqlite3_stmt *st_select_grey;
  sqlite3_stmt *st_insert_grey;
  int opened;
  int flagoom;
  stralloc errmsg;
} greydb_t;
static greydb_t greydb;

static unsigned int iso8601_fmt(char *s, struct datetime *dt)
{
  unsigned int i;
  unsigned int len;
  len = 0;

  i = fmt_uint(s,dt->year + 1900); len += i; if (s) s += i;
  i = fmt_uint0(s,dt->mon + 1,2); len += i; if (s) s += i;
  i = fmt_uint0(s,dt->mday,2); len += i; if (s) s += i;
  i = fmt_str(s,"T"); len += i; if (s) s += i;
  i = fmt_uint0(s,dt->hour,2); len += i; if (s) s += i;
  i = fmt_uint0(s,dt->min,2); len += i; if (s) s += i;
  i = fmt_uint0(s,dt->sec,2); len += i; if (s) s += i;
  i = fmt_str(s,"Z"); len += i; if (s) s += i;
  return len;
}

static sqlite3_stmt *sqlprepare(greydb_t *greybar, const char *sqlcommand)
{
  const char *tail;
  sqlite3_stmt *prep;

  if (sqlite3_prepare_v2(greybar->db, sqlcommand, str_len(sqlcommand),
      &prep, &tail) != SQLITE_OK) {
    if (!stralloc_copys(&greybar->errmsg, sqlite3_errmsg(greybar->db)) ||
        !stralloc_0(&greybar->errmsg))
      greybar->flagoom = 1;
    return 0;
  }
  return prep;
}

static int sqlexec(greydb_t *greybar, const char *sqlcommand)
{
  char *err = 0;
  int ret;

  ret = sqlite3_exec(greybar->db, sqlcommand, 0, 0, &err);
  if (ret != SQLITE_OK) {
    if (!stralloc_copys(&greybar->errmsg, sqlite3_errmsg(greybar->db)) ||
        !stralloc_0(&greybar->errmsg)) {
      greybar->flagoom = 1;
    }
    if (err)
      sqlite3_free(err);
  }
  return ret;
}

static void fillgrey(greyinfo_t *out, int iCol, int datatype, void* in)
{
  switch (datatype) {
    case SQLITE_INTEGER:
      switch(iCol) {
        case 0:
          break;
        case 1:
          byte_copy(&out->created, 8, in);
          break;
        case 2:
          byte_copy(&out->expires, 8, in);
          break;
        case 3:
          byte_copy(&out->windowstart, 8, in);
          break;
        case 4:
          byte_copy(&out->windowend, 8, in);
          break;
        case 5:
          byte_copy(&out->nrblocked, 8, in);
          break;
        case 6:
          byte_copy(&out->nrpassed, 8, in);
          break;
        case 7:
          byte_copy(&out->lastblocked, 8, in);
          break;
        case 8:
          byte_copy(&out->lastpassed, 8, in);
          break;
        case 9:
          byte_copy(&out->nridqueries, 8, in);
          break;
        case 10:
          byte_copy(&out->flags, 8, in);
          break;
        case 11:
          byte_copy(&out->to_initial, 8, in);
          break;
        case 12:
          byte_copy(&out->to_window, 8, in);
          break;
        case 13:
          byte_copy(&out->to_expire, 8, in);
          break;
        case 14:
          byte_copy(&out->perminitial, 8, in);
          break;
        case 15:
          byte_copy(&out->maxrecip, 8, in);
          break;
        case 16:
          byte_copy(&out->recipleft, 8, in);
          break;
      }
      break;

    case SQLITE_TEXT:
      switch(iCol) {
        case 0:
          byte_copy(&out->greyid[0], ACL_GREY_IDLEN, in);
          out->greyid[ACL_GREY_IDLEN] = 0;
      }
      break;
  }
}

static int sql_ops(greydb_t *greybar, sqlite3_stmt *statem, greyinfo_t *out, const char **sqlite3error)
{
  int ret;
  int found = 0;
  uint64 tmp64;
  int iCol;

  while(1) {
    ret = sqlite3_step(statem);
    switch (ret) {
      case SQLITE_ROW:
        if (out) {
          fillgrey(out, 0, SQLITE_TEXT, sqlite3_column_text(statem, 0));
          for (iCol = 1; iCol < sqlite3_column_count(statem); iCol++) {
              tmp64 = sqlite3_column_int64(statem, iCol);
              fillgrey(out, iCol, SQLITE_INTEGER, &tmp64);
          }
        }
        found = 1;
        break;

      case SQLITE_BUSY:
        if (!stralloc_copys(&greybar->errmsg, sqlite3_errmsg(greybar->db)) ||
            !stralloc_0(&greybar->errmsg)) {
          *sqlite3error = oomtext;
        } else {
          *sqlite3error = greybar->errmsg.s;
        }
        sqlite3_reset(statem);
        if (sqlexec(greybar, ROLLBACK) != SQLITE_OK) {
          if (greybar->flagoom) {
            *sqlite3error = oomtext;
          } else {
            *sqlite3error = greybar->errmsg.s;
          }
        }
        return -1;

      case SQLITE_DONE:
        sqlite3_reset(statem);
        return found;

      default:
        if (!stralloc_copys(&greybar->errmsg, sqlite3_errmsg(greybar->db)) ||
            !stralloc_0(&greybar->errmsg)) {
          *sqlite3error = oomtext;
        } else {
          *sqlite3error = greybar->errmsg.s;
        }
        sqlite3_reset(statem);
        return -1;
    }
  }
}

static int db_getgrey(greydb_t *greybar, char *greyid, greyinfo_t *infoout, const char **sqlite3error)
{
  int ret;

  if (sqlexec(greybar, BEGINEXCL) != SQLITE_OK) {
    if (greybar->flagoom) {
      *sqlite3error = oomtext;
    } else {
      *sqlite3error = greybar->errmsg.s;
    }
    return -1;
  }

  sqlite3_bind_text(greybar->st_select_grey, 1, greyid, ACL_GREY_IDLEN, SQLITE_TRANSIENT);
  ret = sql_ops(greybar, greybar->st_select_grey, infoout, sqlite3error);
  return ret;
}

static int db_setgrey(greydb_t *greybar, char *greyid, greyinfo_t *infoin, const char **sqlite3error)
{
  int ret;

  sqlite3_bind_text(greybar->st_insert_grey, 1, greyid, ACL_GREY_IDLEN, SQLITE_TRANSIENT);
  sqlite3_bind_int64(greybar->st_insert_grey, 2, infoin->created);
  sqlite3_bind_int64(greybar->st_insert_grey, 3, infoin->expires);
  sqlite3_bind_int64(greybar->st_insert_grey, 4, infoin->windowstart);
  sqlite3_bind_int64(greybar->st_insert_grey, 5, infoin->windowend);
  sqlite3_bind_int64(greybar->st_insert_grey, 6, infoin->nrblocked);
  sqlite3_bind_int64(greybar->st_insert_grey, 7, infoin->nrpassed);
  sqlite3_bind_int64(greybar->st_insert_grey, 8, infoin->lastblocked);
  sqlite3_bind_int64(greybar->st_insert_grey, 9, infoin->lastpassed);
  sqlite3_bind_int64(greybar->st_insert_grey, 10, infoin->nridqueries);
  sqlite3_bind_int64(greybar->st_insert_grey, 11, infoin->flags);
  sqlite3_bind_int64(greybar->st_insert_grey, 12, infoin->to_initial);
  sqlite3_bind_int64(greybar->st_insert_grey, 13, infoin->to_window);
  sqlite3_bind_int64(greybar->st_insert_grey, 14, infoin->to_expire);
  sqlite3_bind_int64(greybar->st_insert_grey, 15, infoin->perminitial);
  sqlite3_bind_int64(greybar->st_insert_grey, 16, infoin->maxrecip);
  sqlite3_bind_int64(greybar->st_insert_grey, 17, infoin->recipleft);
  ret = sql_ops(greybar, greybar->st_insert_grey, 0, sqlite3error);
  if (ret == -1) return ret;

  if (sqlexec(greybar, COMMIT) != SQLITE_OK) {
    if (greybar->flagoom) {
      *sqlite3error = oomtext;
    } else {
      *sqlite3error = greybar->errmsg.s;
    }
    return -1;
  }
  return ret;
}

static void database_close(greydb_t *greybar)
{
  int ret;

  if (greybar->opened) {
    if (greybar->st_insert_grey) sqlite3_finalize(greybar->st_insert_grey);
    greybar->st_insert_grey = 0;
    if (greybar->st_select_grey) sqlite3_finalize(greybar->st_select_grey);
    greybar->st_select_grey = 0;
    ret = sqlite3_close(greybar->db);
    greybar->opened = 0;
    if (ret != SQLITE_OK) {
      /* crap! this must suck. */
    }
  }
}

static int checktables(greydb_t *greybar)
{
  int ret;
  int found = 0;
  int loop;
  const char *tail;
  sqlite3_stmt *statem;

  ret = sqlite3_prepare_v2(greybar->db, TABLECHECK, str_len(TABLECHECK),
                           &statem, &tail);
  if (ret != SQLITE_OK) {
    if (!stralloc_copys(&greybar->errmsg, sqlite3_errmsg(greybar->db)) ||
        !stralloc_0(&greybar->errmsg)) {
      greybar->flagoom = 1;
    }
    sqlite3_finalize(statem);
    return ret;
  }
  loop = 1;
  while (loop) {
    ret = (sqlite3_step(statem));
    switch (ret) {
      case SQLITE_ROW:
        found = 1;
        break;
      case SQLITE_DONE:
        sqlite3_reset(statem);
        loop = 0;
        break;
      default:
        if (!stralloc_copys(&greybar->errmsg, sqlite3_errmsg(greybar->db)) ||
            !stralloc_0(&greybar->errmsg)) {
          greybar->flagoom = 1;
        }
        sqlite3_finalize(statem);
        return ret;
    }
  }
  sqlite3_finalize(statem);
  return found ? SQLITE_OK : -1;
}

static int databaseinit(greydb_t *greybar, const char **sqlite3error)
{
  if (!greybar->opened) {
    if (sqlite3_open(greydbfn, &greybar->db) != SQLITE_OK)
      goto failed;
    sqlite3_extended_result_codes(greybar->db, 1);
    if (sqlite3_busy_timeout(greybar->db, SQLITE_BUSYWAIT) != SQLITE_OK)
      goto failed;
    if (sqlexec(greybar, BEGIN) != SQLITE_OK)
      goto failedclose;

    switch (checktables(greybar)) {
      case SQLITE_OK:
        if (sqlexec(greybar, COMMIT) != SQLITE_OK)
          goto failedclose;
        break;
      case -1:
        if (sqlexec(greybar, LAYOUT) != SQLITE_OK)
          goto failedclose;
        if (sqlexec(greybar, COMMIT) != SQLITE_OK)
          goto failedclose;
        break;
    }
    greybar->st_select_grey = sqlprepare(greybar, STAT_SELECT_GREY);
    if (!greybar->st_select_grey) goto failedclose;
    greybar->st_insert_grey = sqlprepare(greybar, STAT_INSERT_GREY);
    if (!greybar->st_insert_grey) goto failedclose;

    greybar->opened = 1;
    greybar->flagoom = 0;
  }
  return ACL_GREY_PASS;

failed:
  if (!stralloc_copys(&greybar->errmsg, sqlite3_errmsg(greybar->db)) ||
      !stralloc_0(&greybar->errmsg)) {
    greybar->flagoom = 1;
  }
failedclose:
  database_close(greybar);
  if (greybar->flagoom) {
    *sqlite3error = oomtext;
  } else {
    *sqlite3error = greybar->errmsg.s;
  }
  return ACL_GREY_INTERROR;
}

int greylist(char *greyid, greyopts_t *opts, uint64 *greydelayed, const char **sqlite3error)
{
  int ret;
  greyinfo_t greyinfo;
  datetime_sec creation;

  *greydelayed = 0;
  ret = databaseinit(&greydb, sqlite3error);
  if (ret != ACL_GREY_PASS)
    return ACL_GREY_INTERROR;

  creation = now();
  switch (db_getgrey(&greydb, greyid, &greyinfo, sqlite3error)) {
    case -1:
      return ACL_GREY_INTERROR;
    case 0:
      /* no previous match for this id */
      byte_copy(greyinfo.greyid, ACL_GREY_IDLEN, greyid);
      greyinfo.greyid[ACL_GREY_IDLEN] = 0;
      greyinfo.created = creation;
      greyinfo.expires = creation + opts->expire;
      greyinfo.windowstart = creation + opts->initial;
      greyinfo.windowend = creation + opts->window;
      greyinfo.nrblocked = 1;
      greyinfo.nrpassed = 0;
      greyinfo.lastblocked = 0;
      greyinfo.lastpassed = 0;
      greyinfo.nridqueries = 0;
      greyinfo.flags = opts->flags;
      greyinfo.to_initial = opts->initial;
      greyinfo.to_window = opts->window;
      greyinfo.to_expire = opts->expire;
      greyinfo.perminitial = opts->perminitial;
      greyinfo.maxrecip = opts->maxrecip;
      greyinfo.recipleft = opts->maxrecip;
      switch (db_setgrey(&greydb, greyid, &greyinfo, sqlite3error)) {
        case -1:
          return ACL_GREY_INTERROR;
        case 0:
        case 1:
          return ACL_GREY_TEMPFAIL;
      }
      break;

    /* this greyid is old friend of ours */
    case 1:
      if ((greyinfo.windowend > creation) && (greyinfo.windowstart <= creation) &&
          (greyinfo.expires > creation)) {
        if (!--greyinfo.recipleft) {
          greyinfo.recipleft = greyinfo.maxrecip;
          *greydelayed = creation - greyinfo.created;
          greyinfo.created = creation;
          greyinfo.expires = creation + greyinfo.to_expire;
          greyinfo.windowstart = creation + greyinfo.to_initial;
          greyinfo.windowend = creation + greyinfo.to_window;
        } else {
          if (greyinfo.lastpassed >= greyinfo.windowstart) {
            *greydelayed = 0;
          } else {
            *greydelayed = creation - greyinfo.created;
          }
          greyinfo.windowend = creation + greyinfo.to_window;
        }
        greyinfo.nrpassed++;
        greyinfo.lastpassed = creation;
        switch (db_setgrey(&greydb, greyid, &greyinfo, sqlite3error)) {
          case -1:
            return ACL_GREY_INTERROR;
          case 0:
          case 1:
            return ACL_GREY_PASS;
        }
      } else if ((greyinfo.expires < creation) || (greyinfo.windowend < creation)) {
        /* created is updated to this "fresh" entry */
        greyinfo.created = creation;
        greyinfo.expires = creation + greyinfo.to_expire;
        greyinfo.windowstart = creation + greyinfo.to_initial;
        greyinfo.windowend = creation + greyinfo.to_window;
        greyinfo.nrblocked++;
        greyinfo.lastblocked = creation;
        switch (db_setgrey(&greydb, greyid, &greyinfo, sqlite3error)) {
          case -1:
            return ACL_GREY_INTERROR;
          case 0:
          case 1:
            return ACL_GREY_TEMPFAIL;
        }
      } else if ((greyinfo.windowstart > creation)) {
        greyinfo.nrblocked++;
        if (greyinfo.perminitial && (greyinfo.nrblocked > greyinfo.perminitial)) {
          if ((creation - greyinfo.lastblocked) > 0) {
            /* decrease maxrecip by at max one per second when trying to
             * spew too quickly */
            greyinfo.maxrecip--;
            if (greyinfo.maxrecip == 0) greyinfo.maxrecip = 1;
            if (greyinfo.recipleft > greyinfo.maxrecip)
              greyinfo.recipleft = greyinfo.maxrecip;
            /* modify time windows now? */
          }
        }
        greyinfo.lastblocked = creation;
        switch (db_setgrey(&greydb, greyid, &greyinfo, sqlite3error)) {
          case -1:
            return ACL_GREY_INTERROR;
          case 0:
          case 1:
            return ACL_GREY_TEMPFAIL;
        }
      }
  }
  *sqlite3error = unknerr;
  return ACL_GREY_INTERROR;
}

int greylist_getinfo(char *greyid, greyinfo_t* greyinfo, const char **sqlite3error)
{
  struct datetime dt;
  int ret;

  ret = databaseinit(&greydb, sqlite3error);
  if (ret != ACL_GREY_PASS)
    return ACL_GREY_INTERROR;

  ret = ACL_GREY_INFOOK;

  switch (db_getgrey(&greydb, greyid, greyinfo, sqlite3error)) {
    case -1:
      ret = ACL_GREY_INTERROR;
      break;
    case 0:
      *sqlite3error = not_found;
      ret = ACL_GREY_INFOPERMERR;
      break;
    case 1:
      if (greyinfo->flags & ACL_GREY_FL_DISINFO) {
        *sqlite3error = infodis;
        ret = ACL_GREY_INFOPERMERR;
      }
      break;
  }

  if (ret == ACL_GREY_INFOOK) {
    datetime_tai(&dt, greyinfo->created);
    greyinfo->s_created[iso8601_fmt(greyinfo->s_created, &dt)] = 0;

    datetime_tai(&dt, greyinfo->expires);
    greyinfo->s_expires[iso8601_fmt(greyinfo->s_expires, &dt)] = 0;

    datetime_tai(&dt, greyinfo->windowstart);
    greyinfo->s_windowstart[iso8601_fmt(greyinfo->s_windowstart, &dt)] = 0;

    datetime_tai(&dt, greyinfo->windowend);
    greyinfo->s_windowend[iso8601_fmt(greyinfo->s_windowend, &dt)] = 0;

    greyinfo->s_nrblocked[fmt_ullong(greyinfo->s_nrblocked, greyinfo->nrblocked)] = 0;

    greyinfo->s_nrpassed[fmt_ullong(greyinfo->s_nrpassed, greyinfo->nrpassed)] = 0;

    greyinfo->nridqueries++;
    greyinfo->s_nridqueries[fmt_ullong(greyinfo->s_nridqueries, greyinfo->nridqueries)] = 0;

    switch (db_setgrey(&greydb, greyid, greyinfo, sqlite3error)) {
      case -1:
        return ACL_GREY_INTERROR;
      case 0:
      case 1:
        return ACL_GREY_INFOOK;
    }
    *sqlite3error = unknerr;
    return ACL_GREY_INTERROR;
  } else {
    if (sqlexec(&greydb, COMMIT) != SQLITE_OK) {
      if (greydb.flagoom) {
        *sqlite3error = oomtext;
      } else {
        *sqlite3error = greydb.errmsg.s;
      }
      return ACL_GREY_INTERROR;
    }
    return ret;
  }
}

#endif


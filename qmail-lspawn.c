#include <unistd.h>
#include "fd.h"
#include "wait.h"
#include "prot.h"
#include "substdio.h"
#include "stralloc.h"
#include "scan.h"
#include "exit.h"
#include "fork.h"
#include "error.h"
#include "cdb.h"
#include "case.h"
#include "slurpclose.h"
#include "auto_qmail.h"
#include "auto_uids.h"
#include "qlx.h"
#include "open.h"
#include "byte.h"
#include "nughde.h"
#include "helper.h"

char *aliasempty;

void initialize(argc,argv)
int argc;
char **argv;
{
  aliasempty = argv[1];
  if (!aliasempty) _exit(100);
}

int truncreport = 3000;

void report(ss,wstat,s,len)
substdio *ss;
int wstat;
char *s;
int len;
{
 int i;
 if (wait_crashed(wstat))
  { substdio_puts(ss,"Zqmail-local crashed.\n"); return; }
 switch(wait_exitcode(wstat))
  {
   case QLX_CDB:
     substdio_puts(ss,"ZTrouble reading users/cdb in qmail-lspawn.\n"); return;
   case QLX_NOMEM:
     substdio_puts(ss,"ZOut of memory in qmail-lspawn.\n"); return;
   case QLX_SYS:
     substdio_puts(ss,"ZTemporary failure in qmail-lspawn.\n"); return;
   case QLX_NOALIAS:
     substdio_puts(ss,"ZUnable to find alias user!\n"); return;
   case QLX_ROOT:
     substdio_puts(ss,"ZNot allowed to perform deliveries as root.\n"); return;
   case QLX_USAGE:
     substdio_puts(ss,"ZInternal qmail-lspawn bug.\n"); return;
   case QLX_NFS:
     substdio_puts(ss,"ZNFS failure in qmail-local.\n"); return;
   case QLX_EXECHARD:
     substdio_puts(ss,"DUnable to run qmail-local.\n"); return;
   case QLX_EXECSOFT:
     substdio_puts(ss,"ZUnable to run qmail-local.\n"); return;
   case QLX_EXECPW:
     substdio_puts(ss,"ZUnable to run qmail-getpw.\n"); return;
   case 111: case 71: case 74: case 75:
     substdio_put(ss,"Z",1); break;
   case 0:
     substdio_put(ss,"K",1); break;
   case 100:
   default:
     substdio_put(ss,"D",1); break;
  }

 for (i = 0;i < len;++i) if (!s[i]) break;
 substdio_put(ss,s,i);
}

static stralloc nughde = {0};

int spawn(fdmess,fdout,s,r,at)
int fdmess; int fdout;
char *s; char *r; int at;
{
 int f;

 if (!(f = fork()))
  {
   char *(args[11]);
   uint32 u;
   int n;
   int uid;
   int gid;
   char *x;
   unsigned int xlen;

   r[at] = 0;
   if (!r[0]) _exit(0); /* <> */

   if (chdir(auto_qmail) == -1) _exit(QLX_USAGE);

   nughde_get(r, &nughde);

   x = nughde.s;
   xlen = nughde.len;

   args[0] = "bin/qmail-local";
   args[1] = "--";
   args[2] = x;
   n = byte_chr(x,xlen,0); if (n++ == xlen) _exit(QLX_USAGE); x += n; xlen -= n;

   scan_ulong(x,&u);
   uid = u;
   n = byte_chr(x,xlen,0); if (n++ == xlen) _exit(QLX_USAGE); x += n; xlen -= n;

   scan_ulong(x,&u);
   gid = u;
   n = byte_chr(x,xlen,0); if (n++ == xlen) _exit(QLX_USAGE); x += n; xlen -= n;

   args[3] = x;
   n = byte_chr(x,xlen,0); if (n++ == xlen) _exit(QLX_USAGE); x += n; xlen -= n;

   args[4] = r;
   args[5] = x;
   n = byte_chr(x,xlen,0); if (n++ == xlen) _exit(QLX_USAGE); x += n; xlen -= n;

   args[6] = x;
   n = byte_chr(x,xlen,0); if (n++ == xlen) _exit(QLX_USAGE); x += n; xlen -= n;

   args[7] = r + at + 1;
   args[8] = s;
   args[9] = aliasempty;
   args[10] = 0;

   if (fd_move(0,fdmess) == -1) _exit(QLX_SYS);
   if (fd_move(1,fdout) == -1) _exit(QLX_SYS);
   if (fd_copy(2,1) == -1) _exit(QLX_SYS);
   if (prot_gid(gid) == -1) _exit(QLX_USAGE);
   if (prot_uid(uid) == -1) _exit(QLX_USAGE);
   if (!getuid()) _exit(QLX_ROOT);

   execv(*args,args);
   if (error_temp(errno)) _exit(QLX_EXECSOFT);
   _exit(QLX_EXECHARD);
  }
 return f;
}

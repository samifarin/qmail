#ifndef SUBSTDIO_H
#define SUBSTDIO_H

#include <unistd.h>

typedef struct substdio {
  char *x;
  long int p;
  long int n;
  long int fd;
  ssize_t (*op)(int, void*, size_t);
} substdio;

#define SUBSTDIO_FDBUF(op,fd,buf,len) { (buf), 0, (len), (fd), (op) }

extern void substdio_fdbuf(substdio*, ssize_t (*op)(int, void*, size_t), long int, char*, long int);

extern int substdio_flush(substdio*);
extern int substdio_put(substdio*, char*, long int);
extern int substdio_bput(substdio*, char*, long int);
extern int substdio_putflush(substdio*, char*, long int);
extern int substdio_puts(substdio*, char*);
extern int substdio_bputs(substdio*, char*);
extern int substdio_putsflush(substdio*, char*);

extern long int substdio_get(substdio*, char*, long int);
extern long int substdio_bget(substdio*, char*, long int);
extern long int substdio_feed(substdio*);

extern char *substdio_peek(substdio*);
extern void substdio_seek(substdio*, long int);

#define substdio_fileno(s) ((s)->fd)

#define SUBSTDIO_INSIZE 8192
#define SUBSTDIO_OUTSIZE 8192

#define substdio_PEEK(s) ( (s)->x + (s)->n )
#define substdio_SEEK(s,len) ( ( (s)->p -= (len) ) , ( (s)->n += (len) ) )

#define substdio_BPUTC(s,c) \
  ( ((s)->n != (s)->p) \
    ? ( (s)->x[(s)->p++] = (c), 0 ) \
    : substdio_bput((s),&(c),1) \
  )

extern int substdio_copy(substdio*, substdio*);

#endif


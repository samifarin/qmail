#ifndef TIMEOUTWRITE_H
#define TIMEOUTWRITE_H

extern ssize_t timeoutwrite(int, int, void*, size_t);

#endif

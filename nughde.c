#include <unistd.h>
#include "fd.h"
#include "wait.h"
#include "prot.h"
#include "stralloc.h"
#include "fork.h"
#include "error.h"
#include "cdb.h"
#include "case.h"
#include "slurpclose.h"
#include "auto_uids.h"
#include "qlx.h"
#include "open.h"
#include "byte.h"
#include "nughde.h"

void nughde_get(char *local, stralloc *str_nughde)
{
 char *(args[3]);
 int pi[2];
 int gpwpid;
 int gpwstat;
 int r;
 int fd;
 int flagwild;
 static stralloc wildchars = {0};
 static stralloc lower = {0};

 if (!stralloc_copys(&lower,"!")) _exit(QLX_NOMEM);
 if (!stralloc_cats(&lower,local)) _exit(QLX_NOMEM);
 if (!stralloc_0(&lower)) _exit(QLX_NOMEM);
 case_lowerb(lower.s,lower.len);

 if (!stralloc_copys(str_nughde,"")) _exit(QLX_NOMEM);

 fd = open_read("users/cdb");
 if (fd == -1)
   if (errno != error_noent)
     _exit(QLX_CDB);

 if (fd != -1)
  {
   uint32 dlen;
   unsigned long int i;

   r = cdb_seek(fd,"",0,&dlen);
   if (r != 1) _exit(QLX_CDB);
   if (!stralloc_ready(&wildchars,(unsigned int) dlen)) _exit(QLX_NOMEM);
   wildchars.len = dlen;
   if (cdb_bread(fd,wildchars.s,wildchars.len) == -1) _exit(QLX_CDB);

   i = lower.len;
   flagwild = 0;

   do
    {
     /* i > 0 */
     if (!flagwild || (i == 1) || (byte_chr(wildchars.s,wildchars.len,lower.s[i - 1]) < wildchars.len))
      {
       r = cdb_seek(fd,lower.s,i,&dlen);
       if (r == -1) _exit(QLX_CDB);
       if (r == 1)
        {
         if (!stralloc_ready(str_nughde,(unsigned int) dlen)) _exit(QLX_NOMEM);
         str_nughde->len = dlen;
         if (cdb_bread(fd,str_nughde->s,str_nughde->len) == -1) _exit(QLX_CDB);
         if (flagwild)
	   if (!stralloc_cats(str_nughde,local + i - 1)) _exit(QLX_NOMEM);
         if (!stralloc_0(str_nughde)) _exit(QLX_NOMEM);
         close(fd);
         return;
        }
      }
     --i;
     flagwild = 1;
    }
   while (i);

   close(fd);
  }

 if (pipe(pi) == -1) _exit(QLX_SYS);
 args[0] = "bin/qmail-getpw";
 args[1] = local;
 args[2] = 0;
 switch(gpwpid = vfork())
  {
   case -1:
     _exit(QLX_SYS);
   case 0:
     if (prot_gid(auto_gidn) == -1) _exit(QLX_USAGE);
     if (prot_uid(auto_uidp) == -1) _exit(QLX_USAGE);
     close(pi[0]);
     if (fd_move(1,pi[1]) == -1) _exit(QLX_SYS);
     execv(*args,args);
     _exit(QLX_EXECPW);
  }
 close(pi[1]);

 if (slurpclose(pi[0],str_nughde,128) == -1) _exit(QLX_SYS);

 if (wait_pid(&gpwstat,gpwpid) != -1)
  {
   if (wait_crashed(gpwstat)) _exit(QLX_SYS);
   if (wait_exitcode(gpwstat) != 0) _exit(wait_exitcode(gpwstat));
  }
}


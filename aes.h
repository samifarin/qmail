#ifndef _AES_H_
#define _AES_H_

/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

#include "helper.h"

#define AES_OK    (0)
#define AES_ERROR (1)

struct rijndael_key {
  uint32 eK[64], dK[64];
  int Nr;
};

typedef union Symmetric_key {
  struct rijndael_key rijndael;
} symmetric_key;

/* A block cipher ECB structure */
typedef struct {
  int                 cipher, blocklen;
  symmetric_key       key;
} symmetric_ECB;

extern  struct _cipher_descriptor {
  int  min_key_length, max_key_length, block_length, default_rounds;
  int  (*setup)(const void *key, int keylength, symmetric_key *skey);
  void (*ecb_encrypt)(const void *pt, void *ct, symmetric_key *key);
  void (*ecb_decrypt)(const void *ct, void *pt, symmetric_key *key);
  int  (*test)(void);
} cipher_descriptor[];

extern int aes_setup(const void *key, int keylen, symmetric_key *skey);
extern void aes_ecb_encrypt(const void *pt, void *ct, symmetric_key *key);
extern void aes_ecb_decrypt(const void *ct, void *pt, symmetric_key *key);
extern int aes_test(void);
extern const struct _cipher_descriptor aes_desc;

#endif


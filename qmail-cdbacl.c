/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "stralloc.h"
#include "subfd.h"
#include "getln.h"
#include "substdio.h"
#include "cdbmss.h"
#include "exit.h"
#include "readwrite.h"
#include "open.h"
#include "error.h"
#include "case.h"
#include "auto_qmail.h"
#include "byte.h"
#include "fmt.h"

void die_temp() { _exit(111); }

void die_chdir()
{
  substdio_putsflush(subfderr,"qmail-cdbacl: fatal: unable to chdir\n");
  die_temp();
}
void die_nomem()
{
  substdio_putsflush(subfderr,"qmail-cdbacl: fatal: out of memory\n");
  die_temp();
}
void die_opena()
{
  substdio_putsflush(subfderr,"qmail-cdbacl: fatal: unable to open users/assignacl\n");
  die_temp();
}
void die_reada()
{
  substdio_putsflush(subfderr,"qmail-cdbacl: fatal: unable to read users/assignacl\n");
  die_temp();
}
int linenr;
void die_format()
{
  char str_num[FMT_ULONG];

  str_num[fmt_ulong(str_num, linenr)] = 0;              
  substdio_puts(subfderr,"qmail-cdbacl: fatal: bad format in users/assignacl on line ");
  substdio_puts(subfderr,str_num);
  substdio_putsflush(subfderr,"\n");
  die_temp();
}
void die_opent()
{
  substdio_putsflush(subfderr,"qmail-cdbacl: fatal: unable to open users/cdbacl.tmp\n");
  die_temp();
}
void die_writet()
{
  substdio_putsflush(subfderr,"qmail-cdbacl: fatal: unable to write users/cdbacl.tmp\n");
  die_temp();
}
void die_rename()
{
  substdio_putsflush(subfderr,"qmail-cdbacl: fatal: unable to move users/cdbacl.tmp to users/cdbacl\n");
  die_temp();
}

struct cdbmss cdbmss;
stralloc key = {0};
stralloc data = {0};

char inbuf[1024];
substdio ssin;

int fd;
int fdtemp;

stralloc line = {0};
int match;

stralloc wildchars = {0};

int main()
{
  long int i, cplen;
  int flagesc, numcolons;
  unsigned char ch;
  unsigned char *cp;

  umask(033);
  if (chdir(auto_qmail) == -1) die_chdir();

  fd = open_read("users/assignacl");
  if (fd == -1) die_opena();

  substdio_fdbuf(&ssin, read, fd, inbuf, sizeof(inbuf));

  fdtemp = open_trunc("users/cdbacl.tmp");
  if (fdtemp == -1) die_opent();

  if (cdbmss_start(&cdbmss,fdtemp) == -1) die_writet();

  if (!stralloc_copys(&wildchars,"")) die_nomem();

  for (;;) {
    if (getln(&ssin,&line,&match,'\n') != 0) die_reada();
    linenr++;
    if (line.len && (line.s[0] == '.')) break;
    if (!match) die_format();

    if (byte_chr(line.s, line.len, '\0') < line.len) die_format();
    i = byte_chr(line.s, line.len, ':');
    if (i == 0) die_format();
    if ((line.s[0] == '#') || (line.len == 1)) continue;
    if (i == line.len) die_format();
    if (!stralloc_copys(&key, "!")) die_nomem();
    if (line.s[0] == '+') {
      if (!stralloc_catb(&key, line.s + 1, i - 1)) die_nomem();
      case_lowerb(key.s,key.len);
      if (i >= 2) {
        if (byte_chr(wildchars.s, wildchars.len, line.s[i - 1]) == wildchars.len)
      	  if (!stralloc_append(&wildchars, line.s + i - 1)) die_nomem();
      }
    } else {
      if (!stralloc_catb(&key, line.s + 1, i - 1)) die_nomem();
      if (!stralloc_0(&key)) die_nomem();
      case_lowerb(key.s, key.len);
    }

    cp = line.s + i + 1;
    cplen = line.len - i - 1;
    if (!stralloc_copys(&data, "")) die_nomem();
    for (flagesc = 0, numcolons = 0; cplen; cplen--) {
      ch = *cp;
      cp++;
      if (flagesc) {
        if (!stralloc_append(&data, &ch)) die_nomem();
        flagesc = 0;
        continue;
      }
      if (ch == '\\') { flagesc = 1; continue; }
      if (ch == ':') { numcolons++; if (!stralloc_0(&data)) die_nomem(); continue; }
      if (!stralloc_append(&data, &ch)) die_nomem();
    }
    if (numcolons == 0) die_format();

    while (data.len) {
      if (data.s[data.len - 1] == ' ') { --data.len; continue; }
      if (data.s[data.len - 1] == '\n') { --data.len; continue; }
      if (data.s[data.len - 1] == '\t') { --data.len; continue; }
      if (data.s[data.len - 1] == '\0') { --data.len; continue; }
      if (!stralloc_0(&data)) die_nomem();
      break;
    }
    if (cdbmss_add(&cdbmss, key.s, key.len, data.s, data.len) == -1) die_writet();
  }

  if (cdbmss_add(&cdbmss, "", 0, wildchars.s, wildchars.len) == -1) die_writet();

  if (cdbmss_finish(&cdbmss) == -1) die_writet();
  if (fsync(fdtemp) == -1) die_writet();
  if (close(fdtemp) == -1) die_writet(); /* NFS stupidity */
  if (rename("users/cdbacl.tmp","users/cdbacl") == -1) die_rename();

  _exit(0);
}


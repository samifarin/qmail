#ifndef GETLN_H
#define GETLN_H

#include "stralloc.h"
#include "substdio.h"
#include "helper.h"

extern int __must_check getln(substdio*, stralloc*, int*, int);
extern int __must_check getln2(substdio*, stralloc*, char**, unsigned int*, int);

#endif

#ifndef _DNS_H_
#define _DNS_H_

#include "ipalloc.h"
#include "stralloc.h"
#include "dns_djbdns.h"
#include "helper.h"

#define DNS_SOFT -1
#define DNS_HARD -2
#define DNS_MEM -3

extern int dns_qmail_resolve(stralloc *,const stralloc *, const char [2]);

extern int dns_cname(stralloc *);
extern int dns_ip(ipalloc *,stralloc *);
extern int dns_mxip(ipalloc *,stralloc *, unsigned int);
extern int dns_nsip(ipalloc *,stralloc *);
extern int dns_ns(stralloc *,stralloc *);
extern int dns_ptr(stralloc*, struct ip_address*);
extern void dns_shuffle(ipalloc *);
#endif


#ifndef QSUTIL_H
#define QSUTIL_H

extern void qslog1(char*);
extern void qslog2(char*, char*);
extern void qslog3(char*, char*, char*);
extern void logsa(stralloc*);
extern void nomem(void);
extern void pausedir(char*);
extern void logsafe(char*);

#endif

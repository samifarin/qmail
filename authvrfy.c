/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

#include <unistd.h>

#include "byte.h"
#include "stralloc.h"
#include "control.h"
#include "sha1.h"
#include "str.h"
#include "subfd.h"
#include "auto_qmail.h"
#include "slurpclose.h"
#include "open.h"
#include "base32.h"

stralloc hmackey = {0};

int main(int argc, char* argv[])
{
  char base32digest[SHA1DIGESTSIZEBASE32 + 1];
  int i;

  if (chdir(auto_qmail) == -1) _exit(1);
  i = open_read("control/smtpdkey");
  if (i == -1) _exit(1);
  if (slurpclose(i, &hmackey, 64) == -1) _exit(1);

  if (argc != 3)
    _exit(1);
  sleep(1);
  sha1_hmac_memory(hmackey.s, hmackey.len, argv[1], str_len(argv[1]),
                   base32digest, SHA1DIGESTSIZEBASE32, SHA1DIGESTBASE32);
  base32digest[SHA1DIGESTSIZEBASE32] = 0;
  byte_zero(hmackey.s, hmackey.len);
#if 0
  substdio_putsflush(subfdout, base32digest, 32);
  substdio_putsflush(subfdout, "\n", 1);
#endif
  _exit(str_diff(argv[2], base32digest) ? 1 : 0);
}


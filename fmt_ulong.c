#include "fmt.h"

unsigned int fmt_ulong(s,u) register char *s; register uint32 u;
{
  register unsigned int len;
  register uint32 q;

  len = 1; q = u;
  while (q > 9) { ++len; q /= 10; }
  if (s) {
    s += len;
    do { *--s = '0' + (u % 10); u /= 10; } while(u); /* handles u == 0 */
  }
  return len;
}

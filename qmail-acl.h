/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

#ifndef _QMAIL_ACL_H_
#define _QMAIL_ACL_H_

#include "stralloc.h"
#include "helper.h"

#define ACL_ERR_MATCH 0
#define ACL_ERR_NOMATCH 1
#define ACL_ERR_PERM 2
#define ACL_ERR_TEMP 3

extern int __must_check acl_get(char*, stralloc*, unsigned char, int);
extern int __must_check acl_getcdb(char*, stralloc*);

#define ACL_MF_OK             0
#define ACL_MF_BADMAILFROM    (1 << 0)
#define ACL_MF_SOFT           (1 << 1)
#define ACL_MF_DNS_HARD       (1 << 2)
#define ACL_MF_BADMXIP        (1 << 3)
#define ACL_MF_BADBOUNCE      (1 << 5)
#define ACL_MF_MAYBEFAKE      (1 << 6)
#define ACL_MF_MAYBEFAKETEMP  (1 << 7)
#define ACL_MF_MAYBEFAKECFG   (1 << 8)

#define ACL_HELO_OK           0
#define ACL_HELO_NOHELO       (1 << 0)
#define ACL_HELO_SOFT         (1 << 1)
#define ACL_HELO_DNS_HARD     (1 << 2)
#define ACL_HELO_BADIP        (1 << 3)
#define ACL_HELO_NONFQDN      (1 << 4)
#define ACL_HELO_STRICT       (1 << 5)
#define ACL_HELO_SPOOF        (1 << 6)
#define ACL_HELO_PIPELINE     (1 << 7)
#define ACL_HELO_BADIP_OWN    (1 << 8)
#define ACL_HELO_IGNORE       (1 << 9)
#define ACL_HELO_FASCIST_OK   (1 << 10)
#define ACL_HELO_HELO         (1 << 11)
#define ACL_HELO_EHLO         (1 << 12)

#define ACL_RBLSMTPD_OK       0
#define ACL_RBLSMTPD_TEMP     (1 << 0)
#define ACL_RBLSMTPD_PERM     (1 << 1)
#define ACL_RBLSMTPD_WL       (1 << 2)

#define ACL_CONTENT_OK           0
#define ACL_CONTENT_MSEXE        (1 << 0)
#define ACL_CONTENT_MSWORD       (1 << 1)
#define ACL_CONTENT_CHARSET      (1 << 2)
#define ACL_CONTENT_RFC2047      (1 << 3)
#define ACL_CONTENT_BADSHA1      (1 << 4)
#define ACL_CONTENT_MIMENEST     (1 << 5)
#define ACL_CONTENT_MIMENB       (1 << 6)
#define ACL_CONTENT_BADBASE64    (1 << 7)
#define ACL_CONTENT_BASE64LEN    (1 << 8)
#define ACL_CONTENT_NOMSGID      (1 << 9)
#define ACL_CONTENT_CT           (1 << 10)
#define ACL_CONTENT_CT_GOOD      (1 << 11)
#define ACL_CONTENT_CT_BAD       (1 << 12)
#define ACL_CONTENT_MIMEBB       (1 << 13)
#define ACL_CONTENT_MIMEBL       (1 << 14)
#define ACL_CONTENT_ZIP          (1 << 15)
#define ACL_CONTENT_TNEF         (1 << 16)
#define ACL_CONTENT_PCRE_MATCH   (1 << 17)
#define ACL_CONTENT_PCRE_TEMP    (1 << 18)
#define ACL_CONTENT_SD_NS        (1 << 19)
#define ACL_CONTENT_SD_NS_IP     (1 << 20)
#define ACL_CONTENT_SD_DOMAIN    (1 << 21)
#define ACL_CONTENT_SD_DOMAIN_IP (1 << 22)
#define ACL_CONTENT_SD_ANY       (ACL_CONTENT_SD_NS | ACL_CONTENT_SD_NS_IP | \
                                  ACL_CONTENT_SD_DOMAIN | ACL_CONTENT_SD_DOMAIN_IP)
#define ACL_CONTENT_HDRIP        (1 << 23)
#define ACL_CONTENT_BADSIG       (1 << 24)
#define ACL_CONTENT_BADQP        (1 << 25)

#define ACL_CERT_OK           0
#define ACL_CERT_NOSTARTTLS   (1 << 0)
#define ACL_CERT_NOCERT       (1 << 1)
#define ACL_CERT_INVALIDCERT  (1 << 2)

#define ACL_PCRE_NONE         0
#define ACL_PCRE_BODY         (1 << 0)
#define ACL_PCRE_BODYB        (1 << 1)
#define ACL_PCRE_MIME         (1 << 2)
#define ACL_PCRE_MIMEB        (1 << 3)
#define ACL_PCRE_RFC822       (1 << 4)
#define ACL_PCRE_RFC822B      (1 << 5)
#define ACL_PCRE_KILL         (1 << 6)
#define ACL_PCRE_WL           (1 << 7)
#define ACL_PCRE_WL_REQ       (1 << 8)
#define ACL_PCRE_MATCHMASK    (ACL_PCRE_BODY | ACL_PCRE_BODYB | ACL_PCRE_MIME | \
                               ACL_PCRE_MIMEB | ACL_PCRE_RFC822 | ACL_PCRE_RFC822B)

#define ACL_PCRE_TEMP         (1 << 9)
#define ACL_PCRE_MATCH        (1 << 10)
#define ACL_PCRE_MATCH_KILL   (1 << 11)
#define ACL_PCRE_NOMATCH      (1 << 12)

/* in the beginning this many structs for PCRE stuffs are allocated,
   and this many new structs as neded */
#define ACL_PCRE_ALLOC_CHUNK  (256)
/* log at max this many bytes of matched substrings */
#define ACL_PCRE_MAX_LOGLEN   (256)

#define ACL_SD_DOMAIN         (1 << 0)
#define ACL_SD_SKIP_DOMAIN    (1 << 1)
#define ACL_SD_NS             (1 << 2)
#define ACL_SD_SKIP_NS        (1 << 3)
#define ACL_SD_NS_IP          (1 << 4)
#define ACL_SD_SKIP_NS_IP     (1 << 5)
#define ACL_SD_DOMAIN_IP      (1 << 6)
#define ACL_SD_SKIP_DOMAIN_IP (1 << 7)
#define ACL_SD_ANY            (ACL_SD_DOMAIN | ACL_SD_SKIP_DOMAIN | \
    ACL_SD_NS | ACL_SD_SKIP_NS | ACL_SD_NS_IP | ACL_SD_SKIP_NS_IP | \
    ACL_SD_DOMAIN_IP | ACL_SD_SKIP_DOMAIN_IP)
#define ACL_SD_ALLOC_CHUNK    (32)

#define ACL_SD_MATCH          (1)
#define ACL_SD_NOMATCH        (0)
#define ACL_SD_TIMEOUT        (-1)

#define ACL_HDRIP_MATCH      (1)
#define ACL_HDRIP_NOMATCH    (0)

#define ACL_GREY_PASS        (0)
#define ACL_GREY_TEMPFAIL    (1)
#define ACL_GREY_PERMFAIL    (2)
#define ACL_GREY_INTERROR    (3)
#define ACL_GREY_INFOOK      (0)
#define ACL_GREY_INFOPERMERR (1)
#define ACL_GREY_FL_DISINFO  (1<<0)
#define ACL_GREY_FL_KEY_IP   (1<<1)
#define ACL_GREY_FL_KEY_SIZE (1<<2)
#define ACL_GREY_FL_KEY_HELO (1<<3)
#define ACL_GREY_FL_KEY_FROM (1<<4)
#define ACL_GREY_FL_KEY_TO   (1<<5)
#define ACL_GREY_FL_KEYS (ACL_GREY_FL_KEY_IP | ACL_GREY_FL_KEY_SIZE | \
        ACL_GREY_FL_KEY_HELO | ACL_GREY_FL_KEY_FROM | ACL_GREY_FL_KEY_TO)
#define ACL_GREY_IDLEN       (16)

/* number of chars to use of the SHA1 HMAC (base32-encoded), 1-32 */
#define ACL_ABBS_LEN          16

#define ACL_OK                0
#define ACL_FAIL              1

#endif


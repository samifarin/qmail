#ifndef _AESTRAP_H_
#define _AESTRAP_H_

#include "stralloc.h"
#include "sha1.h"
#include "helper.h"

extern unsigned char* firstnames[256];
extern unsigned char* surnames[256];

#define AESTRAP_TEMP    (-1)
#define AESTRAP_FAIL    ( 0)
#define AESTRAP_EXPIRED ( 1)
#define AESTRAP_DNSBL   ( 2)
#define AESTRAP_OK      ( 3)

/* 16, 24 or 32 -- 32 recommended
   feature: HMAC passes even if you use different keysizes in aestrap.c
   and qmail-smtpd.c but there's not much room in those 16 bytes to store
   anything extra */
#define AESTRAP_KEYSIZE (32)

typedef struct {
  int fd_html;
  int numdomains;
  int quiet;
  int nopid;
  uint8 auth_key[AESTRAP_KEYSIZE];
  uint8 enc_key[AESTRAP_KEYSIZE];
  stralloc trapdomains;
  stralloc mboxprefix;
  stralloc aname;
  stralloc lineprepend;
  char **domainpos;
  uint8 trapdata[16];
} aestrap_ctx;

extern int aestrap_init(aestrap_ctx*, char*, char*, char*);
extern int aestrap_initkey(aestrap_ctx*, char*, char*);

#endif


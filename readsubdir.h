#ifndef READSUBDIR_H
#define READSUBDIR_H

#include "direntry.h"
#include "helper.h"

typedef struct readsubdir
 {
  DIR *dir;
  int pos;
  char *name;
  void (*pause)();
 }
readsubdir;

extern void readsubdir_init(readsubdir*, char*, void(*pause)());
extern int readsubdir_next(readsubdir*, uint64*);

#define READSUBDIR_NAMELEN 10

#endif

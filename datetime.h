#ifndef DATETIME_H
#define DATETIME_H

#include <time.h>

#include "helper.h"

struct datetime {
  int hour;
  int min;
  int sec;
  int wday;
  int mday;
  int yday;
  int mon;
  int year;
} ;

typedef long datetime_sec;

extern void datetime_tai(struct datetime*, datetime_sec);
extern datetime_sec datetime_untai(struct datetime*);

static inline uint64 tspec_elapsed(struct timespec *tvend, struct timespec *tv)
{
  return ((tvend->tv_sec*1000000000ULL + tvend->tv_nsec) -
          (tv->tv_sec*1000000000ULL + tv->tv_nsec));
}

#endif

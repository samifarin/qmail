#include "fmt.h"

unsigned int fmt_ulong0(s,u,n) char *s; unsigned long u; unsigned int n;
{
  unsigned int len;
  len = fmt_ulong(FMT_LEN,u);
  while (len < n) { if (s) *s++ = '0'; ++len; }
  if (s) fmt_ulong(s,u);
  return len;
}

#include "rdtscl.h"
#include "helper.h"

uint64 rdtscl(void)
{
    uint64 val;
    uint32 valhigh, vallow;

#if defined(__GNUC__)
#if defined(__i386__)
    __asm__ volatile ( "rdtsc" : "=A" (val));
#elif defined(__x86_64__)
    asm volatile("rdtsc" : "=a" (valhigh), "=d" (vallow));
    val = ((unsigned long)valhigh) | (((unsigned long)vallow)<<32);
#elif defined(__ia64__)
    __asm__ volatile ("mov %0=ar.itc" : "=r" (val));
#elif defined(__alpha__)
    __asm__ volatile ("rpcc %0" : "=r" (val));
#elif defined(__mips__)
    __asm__ volatile (".set noreorder\n\t"
                      "mfc0 %0, $9\n\t" ".set reorder"
                      : "=r" (val));
#elif defined(__parisc__)
    __asm__ volatile ("mfctl 16, %0" : "=r" (val));
#elif defined(__powerpc__)
    __asm__ volatile ("mftb %0" : "=r" (val));
#elif defined(__s390__) || defined(__s390x__)
    __asm__ volatile ("stck 0(%0)" : : "a" (&(val))
                      :"memory", "cc");
    /* add "-Wa,-Av9a" or "-Wa,-xarch=v8plus" to cc flags */
#elif defined(__sparc__)
    __asm__ volatile ("rd %%tick, %0; clruw %0, %1; srlx %0, 32, %0"
                      : "=r" (valhigh), "=r" (vallow));
    val = (uint64)((uint64)valhigh << 32 | vallow);
#else
    val = 0;
#endif
#endif
    return val;
}


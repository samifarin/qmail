/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>

#include "alloc.h"
#include "control.h"
#include "constmap.h"
#include "base32.h"
#include "scan.h"
#include "stralloc.h"
#include "slurpclose.h"
#include "open.h"
#include "byte.h"
#include "substdio.h"
#include "getln.h"
#include "str.h"
#include "aestrap.h"
#include "aes.h"
#include "sha1.h"
#include "base32.h"
#include "env.h"
#include "datetime.h"
#include "now.h"
#include "fmt.h"

stralloc line = {0};
char htmlinbuf[4096];
substdio htmlin;
char ssoutbuf[4096];
substdio ssout = SUBSTDIO_FDBUF(write, 1, ssoutbuf, sizeof(ssoutbuf));

static char *daytab[7] = {
  "Sun","Mon","Tue","Wed","Thu","Fri","Sat"
};
static char *montab[12] = {
  "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
};

unsigned int httpdatefmt(char *s, struct datetime *dt)
{
  unsigned int i;
  unsigned int len;
  len = 0;

  i = fmt_str(s, daytab[dt->wday]); len += i; if (s) s+= i;
  i = fmt_str(s,", "); len += i; if (s) s += i;
  i = fmt_uint(s,dt->mday); len += i; if (s) s += i;
  i = fmt_str(s," "); len += i; if (s) s += i;
  i = fmt_str(s,montab[dt->mon]); len += i; if (s) s += i;
  i = fmt_str(s," "); len += i; if (s) s += i;
  i = fmt_uint(s,dt->year + 1900); len += i; if (s) s += i;
  i = fmt_str(s," "); len += i; if (s) s += i;
  i = fmt_uint0(s,dt->hour,2); len += i; if (s) s += i;
  i = fmt_str(s,":"); len += i; if (s) s += i;
  i = fmt_uint0(s,dt->min,2); len += i; if (s) s += i;
  i = fmt_str(s,":"); len += i; if (s) s += i;
  i = fmt_uint0(s,dt->sec,2); len += i; if (s) s += i;
  i = fmt_str(s," GMT\r\n"); len += i; if (s) s += i;
  return len;
}

int main(void)
{
  uint64 domainrand;
  uint8 trapout[25];
  uint8 trapout_b32[40];
  int foundmark = 0;
  aestrap_ctx htmltrap = {0};
  int numemails;
  int match;
  uint8 *p;
  char *pdom;
  char *ptr;
  char *remaddr;
  char *scr;
  char *sname;
  char *sport;
  symmetric_key aes_key;
  sha1_hmac_state hmactmp;
  uint8 tmp_enc_key[AESTRAP_KEYSIZE];
  struct datetime dt;
  datetime_sec currsecs, timemod, lastmod;

  remaddr = env_get("REMOTE_ADDR");
  scr = env_get("SCRIPT_NAME");
  sname = env_get("SERVER_NAME");
  sport = env_get("SERVER_PORT");
  if (aestrap_init(&htmltrap, "control/aestrap", remaddr, scr) != 1) _exit(1);
  p = &htmltrap.trapdata[0];
  numemails = (p[14] << 8) | p[15];

  substdio_fdbuf(&htmlin, read, htmltrap.fd_html, htmlinbuf, sizeof(htmlinbuf));
  ptr = env_get("SERVER_SOFTWARE");
  if (!stralloc_copys(&line, "Server: ")) _exit(1);
  if (!stralloc_cats(&line, ptr ? ptr : "Apache")) _exit(1);
  if (!stralloc_cats(&line, "\r\nContent-Type: text/html; charset=iso-8859-1\r\n"
                     "Cache-Control: private, max-age=43200\r\nDate: ")) _exit(1);
  timemod = currsecs = now();
  datetime_tai(&dt, currsecs);
  if (!stralloc_readyplus(&line, httpdatefmt(0, &dt))) _exit(1);
  line.len += httpdatefmt(line.s + line.len, &dt);

  sha1_hmac_init(&hmactmp, &htmltrap.enc_key[0], SHA1DIGESTSIZERAW);
  sha1_hmac_process(&hmactmp, "time", 4);
  /* for same combination of remote addr/script name/server name/server port
     rekey so different Last-Modified header gets generated every 21600 seconds */
  timemod /= 7200;
  sha1_hmac_process(&hmactmp, (uint8*)&timemod, sizeof(timemod));
  timemod /= 3;
  sha1_hmac_process(&hmactmp, (uint8*)&timemod, sizeof(timemod));
  sha1_hmac_process(&hmactmp, remaddr, str_len(remaddr));
  sha1_hmac_process(&hmactmp, "", 1);
  sha1_hmac_process(&hmactmp, scr, str_len(scr));
  sha1_hmac_process(&hmactmp, "", 1);
  sha1_hmac_process(&hmactmp, sname ? sname : "", sname ? str_len(sname) : 1);
  sha1_hmac_process(&hmactmp, "", 1);
  sha1_hmac_process(&hmactmp, sport ? sport : "", sport ? str_len(sport) : 1); 
  sha1_hmac_done(&hmactmp, (uint8*)&domainrand, sizeof(domainrand), SHA1DIGESTRAW);
  lastmod = (currsecs/7200*7200) - 3600 - (domainrand % 7200);
  datetime_tai(&dt, lastmod); 
  if (!stralloc_cats(&line, "Last-Modified: ")) _exit(1);
  if (!stralloc_readyplus(&line, httpdatefmt(0, &dt))) _exit(1);
  line.len += httpdatefmt(line.s + line.len, &dt);

  datetime_tai(&dt, lastmod + 43200);
  if (!stralloc_cats(&line, "Expires: ")) _exit(1);
  if (!stralloc_readyplus(&line, httpdatefmt(0, &dt))) _exit(1);
  line.len += httpdatefmt(line.s + line.len, &dt);

  substdio_put(&ssout, line.s, line.len);
  substdio_puts(&ssout, "\r\n");

  for (;;) {
    if (getln(&htmlin, &line, &match, '\n') == -1) _exit(1);
    if (!match && !line.len) break;
    if (!foundmark && (line.len >= 4) && (str_diffn(&line.s[0], "<!--", 4) == 0)) {
      foundmark = 1;
      while (numemails) {
        p[14] = numemails >> 8;
        p[15] = numemails;
        numemails--;

        sha1_hmac_init(&hmactmp, &htmltrap.enc_key[0], SHA1DIGESTSIZERAW);
        sha1_hmac_process(&hmactmp, "rnd", 3);
        sha1_hmac_process(&hmactmp, p, 16);
        sha1_hmac_process(&hmactmp, &htmltrap.mboxprefix.s[0], htmltrap.mboxprefix.len);
        sha1_hmac_process(&hmactmp, "@", 1);
        sha1_hmac_done(&hmactmp, (uint8*)&domainrand, sizeof(domainrand), SHA1DIGESTRAW);

        pdom = htmltrap.domainpos[domainrand % htmltrap.numdomains];
        sha1_hmac_init(&hmactmp, &htmltrap.enc_key[0], SHA1DIGESTSIZERAW);
        sha1_hmac_process(&hmactmp, &htmltrap.mboxprefix.s[0], htmltrap.mboxprefix.len);
        sha1_hmac_process(&hmactmp, "@", 1);
        sha1_hmac_process(&hmactmp, pdom, str_len(pdom));
        byte_zero(tmp_enc_key, sizeof(tmp_enc_key));
        sha1_hmac_done(&hmactmp, tmp_enc_key, sizeof(tmp_enc_key), SHA1DIGESTRAW);
        aes_setup(tmp_enc_key, AESTRAP_KEYSIZE, &aes_key);
        aes_ecb_encrypt(p, &trapout[0], &aes_key);

        sha1_hmac_init(&hmactmp, &htmltrap.auth_key[0], SHA1DIGESTSIZERAW);
        sha1_hmac_process(&hmactmp, &trapout[0], 16);
        sha1_hmac_process(&hmactmp, &htmltrap.mboxprefix.s[0], htmltrap.mboxprefix.len);
        sha1_hmac_process(&hmactmp, "@", 1);
        sha1_hmac_process(&hmactmp, pdom, str_len(pdom));
        sha1_hmac_done(&hmactmp, &trapout[16], 9, SHA1DIGESTRAW);
        base32encode(&trapout[0], &trapout_b32[0], 25);
        if (htmltrap.lineprepend.len) {
          substdio_put(&ssout, htmltrap.lineprepend.s, htmltrap.lineprepend.len);
        } else {
          substdio_puts(&ssout, "    <BR><A href=\"mailto:");
        }
        substdio_put(&ssout, htmltrap.mboxprefix.s, htmltrap.mboxprefix.len);
        substdio_put(&ssout, trapout_b32, 40);
        substdio_puts(&ssout, "@");
        substdio_puts(&ssout, pdom);
        substdio_puts(&ssout, "\">");
        if(!htmltrap.quiet) {
          if (htmltrap.aname.len) {
            substdio_put(&ssout, htmltrap.aname.s, htmltrap.aname.len);
          } else {
            substdio_puts(&ssout, firstnames[(domainrand >> 56) & 255]);
            substdio_puts(&ssout, " ");
            substdio_puts(&ssout, surnames[(domainrand >> 48) & 255]);
            if (!((domainrand >> 40) & 63)) substdio_puts(&ssout, " Jr.");
          }
          substdio_puts(&ssout, "</A>\n");
        }
      }
    } else {
      substdio_put(&ssout, line.s, line.len);
    }
    if (!match) break;
  }
  substdio_flush(&ssout);
  return 0;
}


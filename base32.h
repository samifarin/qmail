#ifndef BASE32_H
#define BASE32_H

void base32encode(void*, void*, unsigned long int);
void base32decode(void*, void*, unsigned long int);
extern const unsigned char base32[];
extern const unsigned char base32tbl[];
extern const unsigned char validbase32[];

#endif


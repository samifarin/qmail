#ifndef TIMEOUTREAD_H
#define TIMEOUTREAD_H

#include <unistd.h>

extern ssize_t timeoutread(int, int, void*, size_t);

#endif

/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>

#include "alloc.h"
#include "control.h"
#include "constmap.h"
#include "base32.h"
#include "scan.h"
#include "stralloc.h"
#include "slurpclose.h"
#include "open.h"
#include "byte.h"
#include "substdio.h"
#include "getln.h"
#include "str.h"
#include "sha1.h"
#include "aestrap.h"
#include "ip.h"
#include "datetime.h"
#include "case.h"

unsigned char *firstnames[256] = {
  "Michael", "Christopher", "Matthew", "Joshua", "David", "Daniel", "James",
  "Robert", "John", "Joseph", "Jason", "Justin", "Andrew", "Ryan", "William",
  "Brian", "Jonathan", "Brandon", "Nicholas", "Anthony", "Eric", "Adam",
  "Kevin", "Steven", "Thomas", "Timothy", "Richard", "Jeremy", "Kyle",
  "Jeffrey", "Benjamin", "Aaron", "Mark", "Charles", "Jacob", "Stephen",
  "Jose", "Patrick", "Scott", "Paul", "Nathan", "Sean", "Zachary", "Travis",
  "Dustin", "Gregory", "Kenneth", "Alexander", "Jesse", "Tyler", "Bryan",
  "Samuel", "Derek", "Bradley", "Chad", "Shawn", "Edward", "Jared", "Juan",
  "Luis", "Cody", "Jordan", "Peter", "Carlos", "Corey", "Keith", "Donald",
  "Marcus", "Joel", "Ronald", "Phillip", "George", "Cory", "Antonio", "Shane",
  "Douglas", "Raymond", "Brett", "Alex", "Gary", "Nathaniel", "Craig",
  "Derrick", "Casey", "Ian", "Philip", "Gabriel", "Victor", "Erik",
  "Christian", "Frank", "Evan", "Jesus", "Larry", "Seth", "Austin", "Dennis",
  "Vincent", "Brent", "Jeffery", "Wesley", "Randy", "Todd", "Curtis",
  "Miguel", "Jeremiah", "Adrian", "Jorge", "Alan", "Angel", "Mario", "Luke",
  "Russell", "Jerry", "Trevor", "Carl", "Ricardo", "Johnny", "Blake", "Lucas",
  "Shaun", "Mitchell", "Tony", "Cameron", "Terry", "Francisco", "Martin",
  "Troy", "Jessica", "Jennifer", "Amanda", "Ashley", "Sarah", "Stephanie",
  "Melissa", "Nicole", "Elizabeth", "Heather", "Tiffany", "Michelle", "Amber",
  "Megan", "Rachel", "Amy", "Lauren", "Kimberly", "Christina", "Brittany",
  "Crystal", "Rebecca", "Laura", "Emily", "Danielle", "Samantha", "Angela",
  "Erin", "Kelly", "Sara", "Lisa", "Katherine", "Andrea", "Mary", "Jamie",
  "Erica", "Courtney", "Kristen", "Shannon", "April", "Maria", "Kristin",
  "Katie", "Lindsey", "Alicia", "Vanessa", "Lindsay", "Christine", "Allison",
  "Kathryn", "Julie", "Tara", "Anna", "Natalie", "Kayla", "Victoria",
  "Jacqueline", "Monica", "Holly", "Cassandra", "Patricia", "Kristina",
  "Catherine", "Cynthia", "Brandy", "Whitney", "Chelsea", "Veronica",
  "Brandi", "Leslie", "Kathleen", "Stacy", "Diana", "Erika", "Natasha",
  "Meghan", "Dana", "Carrie", "Krystal", "Karen", "Jenna", "Leah", "Melanie",
  "Valerie", "Alexandra", "Brooke", "Jasmine", "Julia", "Alyssa", "Caitlin",
  "Hannah", "Brittney", "Stacey", "Sandra", "Margaret", "Susan", "Candice",
  "Bethany", "Casey", "Katrina", "Latoya", "Tracy", "Misty", "Kelsey", "Kara",
  "Nichole", "Alison", "Molly", "Tina", "Denise", "Heidi", "Alexis",
  "Jillian", "Brenda", "Candace", "Nancy", "Rachael", "Pamela", "Morgan",
  "Renee", "Gina", "Jill", "Kendra", "Teresa", "Sabrina", "Miranda", "Krista",
  "Felicia" };

unsigned char *surnames[256] = {
  "Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller",
  "Wilson", "Moore", "Taylor", "Anderson", "Thomas", "Jackson", "White",
  "Harris", "Martin", "Thompson", "Garcia", "Martinez", "Robinson", "Clark",
  "Rodriguez", "Lewis", "Lee", "Walker", "Hall", "Allen", "Young",
  "Hernandez", "King", "Wright", "Lopez", "Hill", "Scott", "Green", "Adams",
  "Baker", "Gonzalez", "Nelson", "Carter", "Mitchell", "Perez", "Roberts",
  "Turner", "Phillips", "Campbell", "Parker", "Evans", "Edwards", "Collins",
  "Stewart", "Sanchez", "Morris", "Rogers", "Reed", "Cook", "Morgan", "Bell",
  "Murphy", "Bailey", "Rivera", "Cooper", "Richardson", "Cox", "Howard",
  "Ward", "Torres", "Peterson", "Gray", "Ramirez", "James", "Watson",
  "Brooks", "Kelly", "Sanders", "Price", "Bennett", "Wood", "Barnes", "Ross",
  "Henderson", "Coleman", "Jenkins", "Perry", "Powell", "Long", "Patterson",
  "Hughes", "Flores", "Washington", "Butler", "Simmons", "Foster", "Gonzales",
  "Bryant", "Alexander", "Russell", "Griffin", "Diaz", "Hayes", "Myers",
  "Ford", "Hamilton", "Graham", "Sullivan", "Wallace", "Woods", "Cole",
  "West", "Jordan", "Owens", "Reynolds", "Fisher", "Ellis", "Harrison",
  "Gibson", "Mcdonald", "Cruz", "Marshall", "Ortiz", "Gomez", "Murray",
  "Freeman", "Wells", "Webb", "Simpson", "Stevens", "Tucker", "Porter",
  "Hunter", "Hicks", "Crawford", "Henry", "Boyd", "Mason", "Morales",
  "Kennedy", "Warren", "Dixon", "Ramos", "Reyes", "Burns", "Gordon", "Shaw",
  "Holmes", "Rice", "Robertson", "Hunt", "Black", "Daniels", "Palmer",
  "Mills", "Nichols", "Grant", "Knight", "Ferguson", "Rose", "Stone",
  "Hawkins", "Dunn", "Perkins", "Hudson", "Spencer", "Gardner", "Stephens",
  "Payne", "Pierce", "Berry", "Matthews", "Arnold", "Wagner", "Willis", "Ray",
  "Watkins", "Olson", "Carroll", "Duncan", "Snyder", "Hart", "Cunningham",
  "Bradley", "Lane", "Andrews", "Ruiz", "Harper", "Fox", "Riley", "Armstrong",
  "Carpenter", "Weaver", "Greene", "Lawrence", "Elliott", "Chavez", "Sims",
  "Austin", "Peters", "Kelley", "Franklin", "Lawson", "Fields", "Gutierrez",
  "Ryan", "Schmidt", "Carr", "Vasquez", "Castillo", "Wheeler", "Chapman",
  "Oliver", "Montgomery", "Richards", "Williamson", "Johnston", "Banks",
  "Meyer", "Bishop", "Mccoy", "Howell", "Alvarez", "Morrison", "Hansen",
  "Fernandez", "Garza", "Harvey", "Little", "Burton", "Stanley", "Nguyen",
  "George", "Jacobs", "Reid", "Kim", "Fuller", "Lynch", "Dean", "Gilbert",
  "Garrett", "Romero", "Welch", "Larson", "Frazier", "Burke", "Hanson", "Day",
  "Mendoza", "Moreno", "Bowman", "Medina", "Fowler", "Brewer", "Hoffman",
  "Carlson", "Silva", "Pearson", "Holland" };

static int slurpdata(char *slurpname, stralloc *slurpstr)
{
  int fd;

  fd = open_read(slurpname);
  if (fd == -1) return -1;
  if (slurpclose(fd, slurpstr, 64) != 0) return -1;
  return 1;
}

static stralloc aestrapkey = {0};
static stralloc trapbasedir = {0};
static stralloc tmpstr = {0};

int aestrap_initkey(aestrap_ctx *trapctx, char *trapbase, char *scriptname)
{
  sha1_hmac_state sha1hmactmp;

  if (!trapctx || !trapbase || !scriptname) return -1;
  if (!stralloc_copys(&trapbasedir, trapbase)) return -1;
  if (!stralloc_cats(&trapbasedir, "/")) return -1;
  if (case_diffs(scriptname, "/") == 0) {
    if (!stralloc_cats(&trapbasedir, "_")) return -1;
  } else {
    if (!stralloc_cats(&trapbasedir, scriptname)) return -1;
  }
  if (!stralloc_catb(&trapbasedir, "/", 2)) return -1;
  if (!stralloc_copys(&tmpstr, trapbasedir.s)) return -1;
  if (!stralloc_cats(&tmpstr, "key") || !stralloc_0(&tmpstr)) return -1;
  if (slurpdata(tmpstr.s, &aestrapkey) != 1) return -1;
  byte_zero(trapctx->auth_key, sizeof(trapctx->auth_key));
  byte_zero(trapctx->enc_key, sizeof(trapctx->enc_key));    
  sha1_hmac_init(&sha1hmactmp, aestrapkey.s, aestrapkey.len);
  sha1_hmac_process(&sha1hmactmp, "auth", 4);
  sha1_hmac_done(&sha1hmactmp, trapctx->auth_key, sizeof(trapctx->auth_key), SHA1DIGESTRAW);
  sha1_hmac_init(&sha1hmactmp, aestrapkey.s, aestrapkey.len);
  sha1_hmac_process(&sha1hmactmp, "enc", 3);
  sha1_hmac_done(&sha1hmactmp, trapctx->enc_key, sizeof(trapctx->enc_key), SHA1DIGESTRAW);
  byte_zero(aestrapkey.s, aestrapkey.len);
  if (!stralloc_copys(&aestrapkey, "")) return -1;
  return 1;
}

int aestrap_init(aestrap_ctx *trapctx, char *trapbase, char *remip, char *scriptname)
{
  struct ip_address ip_v4;
  struct timeval tv;
  uint32 timesplit;
  uint32 numemails;
  uint32 u;
  int i, j;
  pid_t currpid;
  uint32 time_s, time_hs; /* hundreds of a second */
  int numdomains;
  uint8 *p;
  datetime_sec expire = 0;

  if (!trapctx || !trapbase || !remip || !scriptname) return 0;
  if (aestrap_initkey(trapctx, trapbase, scriptname) != 1) return -1;
  if (!stralloc_copys(&tmpstr, trapbasedir.s)) return -1;
  if (!stralloc_cats(&tmpstr, "domains") || !stralloc_0(&tmpstr)) return -1;
  if (control_readfile(&trapctx->trapdomains, tmpstr.s, 0) != 1) return -1;
  for (numdomains = 0, j = 0; j < trapctx->trapdomains.len; j++)
    if (!trapctx->trapdomains.s[j]) ++numdomains;
  if (!numdomains) return 0;
  trapctx->numdomains = numdomains;
  if (numdomains > (SIZE_MAX / sizeof(char*))) return -1;
  trapctx->domainpos = (char**) alloc(sizeof(char*) * numdomains);
  if (trapctx->domainpos == (char**)0) return -1;
  for (i = 0, j = 0; i < numdomains; i++) {
    /* populate pointers to the domains */
    trapctx->domainpos[i] = &trapctx->trapdomains.s[j];
    while(trapctx->trapdomains.s[j]) j++;
    j++;
  }
  if (!stralloc_copys(&tmpstr, trapbasedir.s)) return -1;
  if (!stralloc_cats(&tmpstr, "timesplit") || !stralloc_0(&tmpstr)) return -1;
  if (control_readint(&timesplit, tmpstr.s) != 1) timesplit = 1;
  if (!stralloc_copys(&tmpstr, trapbasedir.s)) return -1;
  if (!stralloc_cats(&tmpstr, "numemails") || !stralloc_0(&tmpstr)) return -1;
  if (control_readint(&numemails, tmpstr.s) != 1) {
    numemails = 10;
  } else {
    if (numemails > 65535) numemails = 65535;
  }
  if (!stralloc_copys(&tmpstr, trapbasedir.s)) return -1;
  if (!stralloc_cats(&tmpstr, "mboxprefix") || !stralloc_0(&tmpstr)) return -1;
  if (control_readline(&trapctx->mboxprefix, tmpstr.s) != 1)
    if (!stralloc_copys(&trapctx->mboxprefix, "")) return -1;
  if (!stralloc_copys(&tmpstr, trapbasedir.s)) return -1;
  if (!stralloc_cats(&tmpstr, "aname") || !stralloc_0(&tmpstr)) return -1;
  if (control_readline(&trapctx->aname, tmpstr.s) != 1)
    if (!stralloc_copys(&trapctx->aname, "")) return -1;
  if (!stralloc_copys(&tmpstr, trapbasedir.s)) return -1;
  if (!stralloc_cats(&tmpstr, "lineprepend") || !stralloc_0(&tmpstr)) return -1;
  if (control_readline(&trapctx->lineprepend, tmpstr.s) != 1)
    if (!stralloc_copys(&trapctx->lineprepend, "")) return -1;
  if (!stralloc_copys(&tmpstr, trapbasedir.s)) return -1;
  if (!stralloc_cats(&tmpstr, "quiet") || !stralloc_0(&tmpstr)) return -1;
  i = open_read(tmpstr.s);
  if (i != -1) {
    trapctx->quiet = 1;
    close(i);
  } else {
    trapctx->quiet = 0;
  }
  if (!stralloc_copys(&tmpstr, trapbasedir.s)) return -1;
  if (!stralloc_cats(&tmpstr, "nopid") || !stralloc_0(&tmpstr)) return -1;
  i = open_read(tmpstr.s);
  if (i != -1) {
    trapctx->nopid = 1;
    close(i);
  } else {
    trapctx->nopid = 0;
  }
  if (!stralloc_copys(&tmpstr, trapbasedir.s)) return -1;
  if (!stralloc_cats(&tmpstr, "htmlcode") || !stralloc_0(&tmpstr)) return -1;
  trapctx->fd_html = open_read(tmpstr.s);
  if (trapctx->fd_html == -1) return 0;
  if (!ip_scan(remip, &ip_v4)) return 0;
  if (!stralloc_copys(&tmpstr, trapbasedir.s)) return -1;
  if (!stralloc_cats(&tmpstr, "expire") || !stralloc_0(&tmpstr)) return -1;
  if (control_readint(&u, tmpstr.s) == 1) {
    if ((u > 1) && (u < 60)) u = 60;
    expire = u / 60;
    if (expire >= (1UL<<24)) expire = (1UL<<24)-1; /* at max 31.9 years */
  }
  if (gettimeofday(&tv, NULL) != 0) return -1;
  if (timesplit == 0) {
    time_s = tv.tv_sec;
    time_hs = tv.tv_usec / 10000;
  } else {
    time_s = tv.tv_sec / timesplit * timesplit;
    time_hs = 0;
  }
  p = &trapctx->trapdata[0];
  byte_copy(&p[0], 4, (char*)&ip_v4);
  p[4] = time_s >> 24;
  p[5] = time_s >> 16;
  p[6] = time_s >> 8;
  p[7] = time_s;
  /* bit8 == 0 */
  p[8] = (time_hs & ~0x80);
  p[9] = expire >> 16;
  p[10] = expire >> 8;
  p[11] = expire;
  currpid = (trapctx->nopid) ? 0 : getpid();
  p[12] = currpid >> 8;
  p[13] = currpid;
  p[14] = numemails >> 8;
  p[15] = numemails;

  return 1;
}


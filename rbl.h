#ifndef RBL_H
#define RBL_H

#define MAXRBL 32
/* truncate TXT records longer than this */
#define TXTLEN 250

typedef struct {
  stralloc baseaddr;
  stralloc action;
  stralloc matchon;
  stralloc message;
  int origlen;
} rbl_t;

extern void rblheader(struct qmail*);
extern int rblcheck(char*, char*, char*, char**);
extern void rblflush(void);

#define RBL_ERR        (-1)
#define RBL_OK         ( 0)
#define RBL_REJECT     ( 1)
#define RBL_REJECTTEMP ( 2)
#define RBL_WHITELIST  ( 3)
#define RBL_WHITELISTP ( 4)
#define RBL_WHITELISTF ( 5)
#define RBL_TEMPERROR  ( 6)

#define RHS_SOFTFAIL   (1 << 0)
#define RHS_DNS_A      (1 << 1)
#define RHS_DNS_TXT    (1 << 2)

#endif

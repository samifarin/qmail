#ifndef ENV_H
#define ENV_H

extern int env_isinit;

extern int env_init(void);
extern int env_put(char*);
extern int env_put2(char*, char*);
extern int env_unset(char*);
extern /*@null@*/char *env_get(char*);
extern char *env_pick(void);
extern void env_clear(void);
extern char *env_findeq(char*);

extern char **environ;

#endif

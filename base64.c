/*
vim:tw=76:ts=2:sw=2:cindent:expandtab
*/

/* base64 code from libtomcrypt */

#include "stralloc.h"
#include "base64.h"

static const unsigned char codes[] =
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static const unsigned char chmap[256] = {
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255,  62, 255, 255, 255,  63,
 52,  53,  54,  55,  56,  57,  58,  59,  60,  61, 255, 255,
255, 254, 255, 255, 255,   0,   1,   2,   3,   4,   5,   6,
  7,   8,   9,  10,  11,  12,  13,  14,  15,  16,  17,  18,
 19,  20,  21,  22,  23,  24,  25, 255, 255, 255, 255, 255,
255,  26,  27,  28,  29,  30,  31,  32,  33,  34,  35,  36,
 37,  38,  39,  40,  41,  42,  43,  44,  45,  46,  47,  48,
 49,  50,  51, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
255, 255, 255, 255 };

/* returns
    -1 out of memory 
     0 all of the input chars encoded ok
*/
int b64encode(stralloc* in, stralloc* out)
{
  unsigned long i, leven;
  unsigned char *src, *dst;

  if (in->len == 0) {
    if (!stralloc_copys(out, "")) return -1;
    return 0;
  }

  if (!stralloc_ready(out, (in->len / 3 * 4) + 4)) return -1;

  src = in->s;
  dst = out->s;
  leven = 3*(in->len / 3);
  out->len = in->len / 3 * 4;
  for (i = 0; i < leven; i += 3) {
    *dst++ = codes[src[0] >> 2];
    *dst++ = codes[((src[0] & 3) << 4) + (src[1] >> 4)];
    *dst++ = codes[((src[1] & 0xf) << 2) + (src[2] >> 6)];
    *dst++ = codes[src[2] & 0x3f];
    src += 3;
  }
  /* Pad it if necessary...  */
  if (i < in->len) {
    unsigned int a = src[0];
    unsigned int b = (i+1 < in->len) ? src[1] : 0;
    unsigned int c = 0;

    *dst++ = codes[a >> 2];
    *dst++ = codes[((a & 3) << 4) + (b >> 4)];
    *dst++ = (i+1 < in->len) ? codes[((b & 0xf) << 2) + (c >> 6)] : '=';
    *dst++ = '=';
    out->len += 4;
  }

  return 0;
}

/* returns
     -1 out of memory
      0 all of the input chars decoded ok
      1 some ok
      2 none ok
*/
int b64decode(void *__in, unsigned long int len, stralloc* out)
{
  unsigned long t, x, y;
  unsigned char c, tmpch;
  int g = 3;
  int seenvalid = 0;
  int seeninvalid = 0;
  unsigned char *in = __in;

  if (!stralloc_copys(out, "")) return -1;
  if (len == 0) {
    return 0;
  }

  for (x = y = t = 0; x < len; x++) {
    c = chmap[in[x]];
    if (c == 255) { seeninvalid = 1; continue; }
    seenvalid = 1;

    /* the final = symbols are read and used to trim the remaining bytes */
    if (c == 254) {
      c = 0; 
      /* prevent g < 0 which would potentially allow an overflow later */
      if (--g < 0) {
        return 1;
      }
    } else if (g != 3) {
      /* we only allow = to be at the end */
      return 1;
    }

    t = (t<<6U)|c;

    if (++y == 4) {
      tmpch = (unsigned char)((t>>16U)&255);
      if (!stralloc_append(out, &tmpch)) return -1;
      if (g > 1) {
        tmpch = (unsigned char)((t>>8)&255);
        if (!stralloc_append(out, &tmpch)) return -1;
      }
      if (g > 2) {
        tmpch = (unsigned char)(t&255);
        if (!stralloc_append(out, &tmpch)) return -1;
      }
      y = t = 0;
    }
  }

  if ((y != 0) || !seenvalid) {
    return 2;
  }

  return seeninvalid;
}

